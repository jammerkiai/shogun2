<?php
/*
extended template class
*/

class AMOTemplate {
    var $html;
    var $literal=array();
    var $block=array();
    var $blockitem=array();
    
    function AMOTemplate($file="", $default=0) {
        if($default) {
            $this->html=$file;
            $this->getelements("blockitem");
            $this->getblocks("repeater");
        }else{
            $this->html=(file_exists($file)) ? file_get_contents($file) : "";
            $this->getliterals();
            $this->getblocks();
			$this->getstaticelements();
        }
    }
    
    function show() {
        echo $this->html;
    }
    
    function gethtml() {
        return $this->html;
    }
 
    function getliterals() {
        $this->getelements("literal");
    }
       
    function getelements($element="literal") {
		preg_match_all("|<amo:$element(.*)/>|U", $this->html, $this->$element);
    }
    
	function getstaticelements() {
		preg_match_all('|{(.*)}|U', $this->html, $this->staticelements);
	}
	
    function getblocks($element="block") {
       preg_match_all("|<amo:$element (.*)>(.*)</amo:$element>|U",preg_replace("/\n/","",$this->html), $this->$element);
    }
    
    function getblockitems($element="blockitem") {
        for($i=0;$i<count($this->$element);$i++) {
            $elementid = $this->getid($this->{$element}[1][$i]);
        }
    }
    
    function setliterals($c,$element="literal") {
        //change content
        $x = count($this->{$element}[0]);
        for($i=0;$i < $x;$i++) {
            $elementid = $this->getid($this->{$element}[1][$i]);
            $this->html = str_replace($this->{$element}[0][$i], $c[$elementid], $this->html);
        }
    }
    
    function setelements($c,$element="literal") {
        //change content
        $x = count($this->{$element}[0]);
        for($i=0;$i < $x;$i++) {
            $elementid = $this->getid($this->{$element}[1][$i]);
            $this->html = str_replace($this->{$element}[0][$i], $c[$elementid], $this->html);
        }
    }
	
	function setstaticelements($c,$element="staticelements") {
        $x = count($this->staticelements[0]);
        for($i=0;$i < $x;$i++) {
            $elementid =$this->staticelements[1][$i];
            $this->html = str_replace($this->staticelements[0][$i], $c[$elementid], $this->html);
        }
    }
    
    function setblocks($c,$element="block") {
        //change content
        $x = count($this->{$element}[1]);
        for($i=0;$i < $x;$i++) {
            $elementid = $this->getid($this->{$element}[1][$i]);
            $this->html = str_replace($this->{$element}[0][$i], $c[$elementid], preg_replace("/\n/","",$this->html));
        }
    }
    
    function setspecificliteral($handle,$content) {
        $this->html = str_replace($handle,$content,$this->html);
    }
    
    function setspecificblock($handle,$content) {
        $this->html = str_replace($handle,$content,preg_replace("/\n/","",$this->html));
    }
    
   function getid($str) {
        $attribs = explode("=", $str);
        return trim(str_replace("\"","",$attribs[1]));
    }
    
    function listblocks($element="block") {
        $retarr = array();
        $x = count($this->{$element}[1]);
        for($i=0;$i < $x;$i++) {
            $elementid = $this->getid($this->{$element}[1][$i]);
            $retarr[$i] = $elementid;
        }
        return $retarr;
    }
    
	function setallelements($c) {
		$this->setliterals($c);
		reset($c);
		$this->setblocks($c);
		reset($c);
		$this->setstaticelements($c);
	}
    
}
?>