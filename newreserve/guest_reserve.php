<?php
//guest_reserve.php
if (isset($reservation->guest_id)) {
	$sql = "select guest_id, firstname, lastname from guests where guest_id='{$reservation->guest_id}'";
	$res = mysql_query($sql);
	$guest = mysql_fetch_object($res);
}
?>
<script>
	$(function() {
		$('#verify').bind('click', function(e) {
			e.preventDefault();
			var fn = $('#firstname').val(),
				ln = $('#lastname').val();
			
			if (fn.trim() === '' || ln.trim() === '' ) {
				alert('First and Last names must not be blank.');
			} else {
				$.post(
					'guestverify.php',
					{fn: fn.trim(), ln: ln.trim()},
					function(resp){
						$('#guest_message').html(resp.msg);
						$('#new_guestid').val(resp.gid);
					},
					'json'
				);
			}
		});
	});
</script>
<div class="easyui-panel" id="guest_panel" title="Guest Name" style="width:220px;padding:10px;height:204px">
		<div style="display:inline-block">
			<label>First Name</label>
			<input type="text" name="firstname" id="firstname" 
				value="<?php echo $guest->firstname ? $guest->firstname : '' ?>" 
			 	required="true" class="easyui-validatebox" />
		</div>
		<div style="display:inline-block">
			<label>Last Name</label>
			<input type="text" name="lastname" id="lastname" 
				value="<?php echo $guest->lastname ? $guest->lastname : '' ?>" 
			    required="true" class="easyui-validatebox" />
			<input type="hidden" name="new_guestid" id="new_guestid" 
				value="<?php echo $guest->guest_id ? $guest->guest_id : '' ?>" />
		</div>
		<div style="display:inline-block;color:#f60;margin-top:4px;" id="guest_message">
		<?php if (!isset($guest)) { ?>
		<a href="#" id="verify" class="easyui-linkbutton" plain="true" iconCls="icon-help">Verify</a>
		<?php } ?>
		</div>
</div>