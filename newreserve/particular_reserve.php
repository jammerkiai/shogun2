<?php
//particular_reserve.php

?>
<div id="particulars" class="easyui-panel" title="Particulars" style="width:220px;padding:10px;">
<div>
	<label for="new_pax">No. of Pax</label>
	<input type="text" name="new_pax" id="new_pax" 
		value="<?php echo $reservation->pax?$reservation->pax:1 ?>" 
		class="easyui-numberspinner" min="1" max="100" />
</div>
<div>
	<label for="new_reserve_date">Pickup Time</label>
	<input type="text" class="easyui-timespinner" name="new_pickup_time" id="new_pickup_time" 
		value="<?php echo $reservation->pickup_time ? $reservation->pickup_time : ''?>" />
</div>
<div>
	<label for="new_reserve_date">Pickup Location</label>
	<input type="text"  name="new_pickup_location" id="new_pickup_location" 
		value="<?php echo $reservation->pickup_location ? $reservation->pickup_location : ''?>" class="keypadfield" />
</div>
<div>
	<label for="new_notes">Notes</label>
	<textarea cols='18' rows='4' name='new_notes' id='new_notes' class="keypadfield">
		<?php echo $reservation->notes ? $reservation->notes : ''?></textarea>
</div>
</div>