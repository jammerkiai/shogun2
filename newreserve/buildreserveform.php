<?php
//build reserve room form
//buildreserveform.php
include_once('../fds/ajax/config/config.inc.php');
include_once('../fds/ajax/reserve.functions.php');

function getBookingRoomTypes($selected = '') {
	$sql = "select room_type_id, room_type_name 
			from booking_roomtypes
			where active = 1";
	$res = mysql_query($sql) or die(mysql_error());
	$retval = "<select name='' class='roomtypeDD'>";
	$retval.= "<option value=''></option>";
	while (list($id, $name)=mysql_fetch_row($res)) {
		$retval.= "<option value='$id' ";
		$retval.= ($selected === $name) ? ' selected ' : '';
		$retval.= ">$name</option>";
	}
	$retval.= "</select>";
	return $retval;
}



if (isset($_GET['rtid'])) {
	$rtid = $_GET['rtid'];
	echo getRoomOptions($rtid);
	exit;
}


if (isset($_GET['count'])) {
$count = $_GET['count'];
$rtTpl = getBookingRoomTypes();
$now = date('Y-m-d');
$form = "<table id='roomlisttable'>";
$form.= "<thead><tr><th>Room Type</th><th>Room</th><th>CheckIn</th><th>CheckOut</th></tr></thead>";
for ($i=1; $i <= $count; $i++) {
	$ddName = "roomType_$i";
	$rtDD = str_replace("<select name=''", "<select name='$ddName' id='$ddName'", $rtTpl);
	$form.="<tr>";
	$form.="<td>$rtDD</td>";
	$form.="<td><select name='room_$i' id='room_$i'><option value='0'>TBD</option></select></td>";
	$form.="<td><input type='text' name='checkin_$i' id='checkin_$i' value='$now'
		class='easyui-datebox easyui-validatebox checkin' required=true style='width:100px'></td>";
	$form.="<td><input type='text' name='checkout_$i' id='checkout_$i' value='$now'
		class='easyui-datebox easyui-validatebox' required=true style='width:100px'></td>";
	$form.="</tr>";
}
$form.= "</table>";
echo $form;
}


?>
