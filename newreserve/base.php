<?php
//base.php
include_once('../fds/ajax/config/config.inc.php');
if (isset($_GET)) {
	$act = $_GET['act'];
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Reservations</title>
	<style type="text/css">
		label{
			width:120px;
			display:block;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="themes/icon.css">
	<link rel="stylesheet" type="text/css" href="demo.css">
	<script type="text/javascript" src="jquery-1.6.min.js"></script>
	<script type="text/javascript" src="jquery.easyui.min.js"></script>
	</head>
<body>
	<?php 
		if ($act == 'new') {
			include_once('newreserve.php');
		} else {
			include_once('search.php');
		}
	?>
</body>
</html>