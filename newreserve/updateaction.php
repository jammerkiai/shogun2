<?php
//newreserveaction.php
session_start();
include_once('../fds/ajax/config/config.inc.php');
include_once('../fds/ajax/reserve.functions.php');
include_once("../fds/ajax/currentcash.function.php");
include_once("../fds/ajax/date.function.php");

if (isset($_POST)) {
	foreach ($_POST as $key => $val) {
		${$key} = $val; 
	}
	
	list($code, $oldin, $oldout, $oldroom) = getOneRow('select reserve_code, checkin, checkout, room_id from reserve_rooms where rr_id='.$rid);
	
	//get doorname
	$door = '';
	
	if (isset($room) && ($room != $oldroom)) {
		$door = getOneValue('select door_name from rooms where room_id='.$room);
		$remarks = "NEW ROOM: $door, ";
	}
	
	if ($oldin != $in) {
		$remarks .= "IN: $in,";
	}
	
	if ($oldout != $out) {
		$remarks .= "OUT: $out";
	}
	
	$sql = "update reserve_rooms set		
		checkin='$in',
		checkout= '$out'";
	$sql.= isset($room) ? ", room_id='$room' " : '';
	$sql.= " where rr_id='$rid'";
	mysql_query($sql) or die('Unable to update. Please try again.' . $sql);
	logReserveTransaction(array(
		'remarks' => $remarks,
		'code' => $code
	));
	echo 'Update successful.';
}
