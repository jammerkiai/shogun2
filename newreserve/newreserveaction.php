<?php
//newreserveaction.php
include_once('../fds/ajax/config/config.inc.php');
include_once('../fds/ajax/reserve.functions.php');
include_once("../fds/ajax/currentcash.function.php");
include_once("../fds/ajax/date.functions.php");

if (isset($_POST)) {
	//simplify variable referencing;
	foreach ($_POST as $key => $val) {
		${$key} = $val;
	}

	$now = date('Y-m-d H:i:s');
	
	$newcode = (isset($new_reserve_code)&&trim($new_reserve_code)!="") ? $new_reserve_code : getnextreservecode();
	if(trim($new_reserve_code)!="") //update
	{
				//get old reserve data
		$sql = "select * from reservations where reserve_code='$newcode'";
		$res = mysql_query($sql);
		$oldRes = mysql_fetch_object($res);
		//update reservation data
		$new_notes = trim($new_notes);
		$sql = "update reservations set 
			reserve_fee='$new_reserve_fee',
			pax='$new_pax',
			notes='$new_notes',
			date_updated='$now',
			pickup_time='$new_pickup_time',
			pickup_location='$new_pickup_location',
			updated_by='$user'
			where reserve_code='$newcode'";
		mysql_query($sql) or die(mysql_error());
	
		//update partner info
		$sql = "
				update partner_transactions set
				booking_number='$bookingnumber',
				payable = '$commission'
				where reserve_code='$newcode'
			";
		mysql_query($sql) or die(mysql_error());
		
		//check if reserve_fee was modified; adjust currentcash as necessary
		$diff = $new_reserve_fee - $oldRes->reserve_fee;
		if ($diff > 0) {
			//insert into reservation_transactions
			$sql = "insert into reservation_transactions (transaction_date,reservation_code,amount_deposit, update_by)
				values('$now','$newcode','$diff', '$user')";
			mysql_query($sql) or die(mysql_error() . $sql);
			
			//update currentcash
			setCurrentCash( $diff ,'in', $user);
		}
	}
	else //insert
	{
		$new_reserve_date = $datebooked;
		$partner = $Partner;
		if($new_paymenttype=='') $new_paymenttype='Cash';	
		//reserve_rooms
		$values="";
		for ($x = 1; $x <= $new_roomcount; $x++ ) {
			$checkin = ${"checkin_$x"};
			$checkout = ${"checkout_$x"};
			$roomtype = ${"roomType_$x"};
			$roomid = ${"room_$x"};
			$deposit = 0;
			$values .= ($values=="") ? "" : ",";
			$values .= "('$newcode','$roomtype', '$roomid','$checkin','$checkout', '$deposit')";
			//$totaldeposit += $deposit;
		}
		$sql = " insert into reserve_rooms (reserve_code, room_type_id, room_id, checkin, checkout, deposit ) values $values ";
		mysql_query($sql) or die(mysql_error());
		
		
		$sql  =" insert into reservations (reserve_code, 
		guest_id, reserve_date, pax, reserve_fee, payment_type,pickup_time,pickup_location,
		notes,date_created, created_by,date_updated,updated_by, partner, card_type, approval_code, 
		batch_number, is_debit, card_suffix, original_amount_paid) 
		values ('$newcode', '$new_guestid', '$new_reserve_date','$new_pax',
		'$new_reserve_fee','$new_paymenttype','$new_pickup_time',
		'$new_pickup_location','$new_notes','$now','$user','$now','$user','$partner',
		'$new_card_type', '$new_approvalcode', '$new_batchnumber', '$new_is_debit', 
		'$new_card_suffix', '$new_reserve_fee'
		)
		" ;
		
		mysql_query($sql) or die(mysql_error());

		$sql  ="insert into reservation_transactions(transaction_date,reservation_code, occupancy_id, amount_deposit,update_by )
			values('$now', '$newcode','0','$new_reserve_fee','$user');
		" ;
		mysql_query($sql);
		
		//insert into partner_transactions
		if ($partner != '') {
			$sql = "insert into partner_transactions(transaction_date, reserve_code, booking_number, partner_name, payable, remarks, result_status) 
				values ('$now', '$newcode', '$bookingnumber', '$partner', '$commission', '', 'Confirmed')";
			mysql_query($sql);
		}
		//reservation_payment_details
		if($new_paymenttype=='Cash') {
			setCurrentCash($new_reserve_fee,'in',$user);
		}else{
		
		}
	}
	header('location: base.php?act=new&code=' . $newcode);
}



?>
