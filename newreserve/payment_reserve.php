<?php
//payment_reserve.php
?>
<script>
	$(function(){
		$('#cd').panel('collapse');
		$('input[name=new_paymenttype]').bind('click', function(){
			if ($(this).val() == 'Card') {
				$('#cd').panel('expand');
			} else {
				$('#cd').panel('collapse');
			}
		});
	});
</script>
<div id="payment_box"  class="easyui-panel" title="Deposit/Fee" style="width:220px;padding:10px;">

<div style="display:inline-block">
	<label>Payment Type</label>
	<input type="radio" name="new_paymenttype" value="Cash" id='new_paymenttype_cash' 
		<?php if($reservation->payment_type=='Cash') echo 'checked' ?>> Cash
	
	<input type="radio" name="new_paymenttype" value="Card" id='new_paymenttype_card' 
		<?php if($reservation->payment_type=='Card') echo 'checked' ?>> Card
</div>

<div style="margin-top:4px;">
	<label>Amount</label>
	<input type="text" id="new_reserve_fee" name="new_reserve_fee" class="easyui-numberbox" style="text-align:right;" 
		value="<?php echo isset($reservation->reserve_fee) ? $reservation->reserve_fee : 0 ?>" >
</div>

<div id="cd" class="easyui-panel" title="Card Details" style="padding:10px;" >
<div style="display:inline-block">
<label for='new_card_suffix'>Card Suffix</label>
<input type="text" name="new_card_suffix" id="new_card_suffix" value="<?=$reservation->card_suffix ? $reservation->card_suffix : 0?>" class="numkeypadfield" />
</div>
<div style="display:inline-block">
<label for='new_approvalcode'>Approval Code</label>
<input type="text" name="new_approvalcode" id="new_approvalcode" value="<?=$reservation->approval_code ? $reservation->approval_code : 0?>" class="numkeypadfield" />
</div>
<div style="display:inline-block">
<label for='new_batchnumber'>Batch Number</label>
<input type="text" name="new_batchnumber" id="new_batchnumber" value="<?=$reservation->batch_number ? $reservation->batch_number : 0?>" class="numkeypadfield" />
</div>
<div style="display:inline-block">
<input type="checkbox" name="new_is_debit" id="new_is_debit" value='1' "<?=$reservation->is_debit==1 ? " checked " : '' ?>" />
<label for='new_batchnumber' style='display:inline;'>Debit/ATM</label>
</div>
<br><br>
<div id="ct" class="easyui-panel" style="padding:10px;block;">
<div>
<input type="radio" name="new_card_type" <?php if($reservation->card_type=='AMEX') echo 'checked' ?> value="AMEX"  id="ct_4" /> AMEX
</div>
<div>
<input type="radio" name="new_card_type" <?php if($reservation->card_type=='JCB') echo 'checked' ?>  value="JCB"  id="ct_3" /> JCB
</div>
<div>
<input type="radio" name="new_card_type" <?php if($reservation->card_type=='Mastercard') echo 'checked' ?>  value="Mastercard"  id="ct_2" /> Mastercard
</div>
<div>
<input type="radio" name="new_card_type" <?php if($reservation->card_type=='Visa') echo 'checked' ?>  value="Visa"  id="ct_1" /> Visa
</div>
<div>
<input type="radio" name="new_card_type" <?php if($reservation->card_type=='BDO Card') echo 'checked' ?>  
	value="BDO Card"  id="ct_4" /> BDO Card
</div>
<div>
<input type="radio" name="new_card_type" <?php if($reservation->card_type=='ExpressNet') echo 'checked' ?>  value="ExpressNet"  id="ct_5" /> ExpressNet
</div>
<div>
<input type="radio" name="new_card_type" <?php if($reservation->card_type=='Megalink') echo 'checked' ?>  value="Megalink"  id="ct_6" /> Megalink
</div>
<div>
<input type="radio" name="new_card_type" <?php if($reservation->card_type=='BancNet') echo 'checked' ?>  value="BancNet"  id="ct_7" /> BancNet
</div>
</div>
