<?php
//rooms_reserve.php
include_once('../fds/ajax/reserve.functions.php');
if (isset($reservation)) {
	$sql = "select guest_id, firstname, lastname from guests where guest_id='{$reservation->guest_id}'";
	$res = mysql_query($sql);
	$guest = mysql_fetch_object($res);
    $sql = "SELECT a.rr_id, a.room_type_id, b.room_type_name, a.room_id, a.checkin, a.checkout, a.status, c.door_name
            FROM reserve_rooms a
            INNER JOIN room_types b ON a.room_type_id = b.room_type_id
            LEFT OUTER JOIN rooms c ON c.room_id = a.room_id
            WHERE a.reserve_code = '$code'
            ORDER BY a.status DESC";
	$res = mysql_query($sql);
	$num_rooms = mysql_num_rows($res);
	$table = "";
	$modify = isset($_GET['modify']) ? $_GET['modify'] : null;
	$pendingcount = 0;
	$displaycount = 0;
	while (list($rid, $rtid, $rtype, $room, $in, $out, $status, $name) = mysql_fetch_row($res)) {
		$door = getRoomOptionsForCheckin($rtid, $room);
		$sql2 = "select date(expected_checkout) 
		        from occupancy
		        where room_id=$room
		        and actual_checkout='0000-00-00 00:00:00'
		        order by occupancy_id desc
		        limit 0, 1";
		$res2 = mysql_query($sql2);
		$expected_checkout = '';
		if (mysql_num_rows($res) > 0) {
		    $row2 = mysql_fetch_row($res2);
    		$expected_checkout = $row2[0];
		}
		$table.= "<tr>";
		if (!is_null($modify)) {
			$table.= "<td>$rtype<input type='hidden' value='$rtid' id='roomType_$rid'></td>";
			$table.= "<td><input type='text' value='$in' id='checkin_$rid' name='checkin_$rid' 
				class='easyui-datebox easyui-validatebox checkin datebox-f combo-f'></td>";
			$table.= "<td><input type='text' value='$out' id='checkout_$rid' name='checkout_$rid'
				class='easyui-datebox easyui-validatebox checkout datebox-f combo-f'></td>";
		} else {
			$table.= "<td>$rtype <br>#$name<br>$expected_checkout</td>";
			$table.= "<td>$in</td>";
			$table.= "<td>$out</td>";
		}
		
		$table.= "<td>";
		$displaycount++;
		if ($status == 'Pending' ) {
			$pendingcount++;
			$table .= "<select name='rm_$rid' id='rm_$rid'>$door</select></td><td>";
			if ($modify) {
				$table.= "
					<a href='base.php?act=new&code=$code' 
					class='easyui-linkbutton updatelink' id='link_$rid'>Update</a>
					";
				$table.= "
					<a href='base.php?act=new&code=$code' 
					class='easyui-linkbutton'>Cancel</a>
				";	
			} else {
				if ($num_rooms > 1 && $pendingcount > 1 || $displaycount <= $num_rooms) {
					$table.= "
						<a href='/fds/ajax/checkinform.php?rr=$rid&res=$code&fn={$guest->firstname}&ln={$guest->lastname}&dep={$reservation->reserve_fee}&room=' 
						class='easyui-linkbutton checkinlink' id='link_$rid'>Check In</a><br>
					";
				}
				$table.= "
					<a href='/fds/ajax/checkinform.php?close=1&rr=$rid&res=$code&fn={$guest->firstname}&ln={$guest->lastname}&dep={$reservation->reserve_fee}&room=' 
					class='easyui-linkbutton checkinlink' id='link_$rid'>Check In & Close</a>
				";
				$table.= "<br>
					<a href='base.php?act=new&code=$code&modify=$rid' 
					class='easyui-linkbutton modifylink'>Modify</a>
				";
			}
		} else {
			$table.= "$room</td><td>";
			$table.= "<span style='font-weight:bold;color:#F00;'>$status</span>";
		}
		$table.= "</td></tr>";
	} 
}

?>
<script>
	function getRoomlist(rows) {
		$.get(
			'buildreserveform.php', 
			{count: rows}, 
			function(resp) { 
				$('#room_list').html(resp);
				/*initialize dateboxes*/
				$('.easyui-datebox').datebox({
				    onSelect: function() {
				        if ($(this).hasClass('checkin')) {
				            var thisid = $(this).attr('id'),
				                index = thisid.split('_')[1],
				                rtid = $('#roomType_' + index).val(),
				                rid = $('#room_' + index).val(),
				                rid2 = $('#rm_' + index).val();
				            $.get(
				                'checkdate.php',
				                {
				                    rtid : rtid,
				                    rid  : rid ? rid : rid2,
				                    dt   : $('input.combo-value[name=' + thisid +']').val()
				                },
				                function(resp) {
				                    if (resp.success) {
				                    } else {
				                        $.messager.alert('Overbooking Warning', resp.message, 'warning');
				                        $('input.combo-value[name=' + thisid +']').val( $('#' + thisid).val() );
				                    }
				                    
				                },
				                'json'
				            );
				        }
				    },
					formatter: function(date) {
						var y = date.getFullYear();
    					var m = date.getMonth()+1;
    					var d = date.getDate();
    					return y+'-'+(m<10?'0'+m:m)+'-'+(d<10?'0'+d:d);
					}
				});
			}
		);
	}
	
	
	$(function(){
		getRoomlist($('#new_roomcount').val());
		$('#new_roomcount').bind('change', function(){
			getRoomlist($(this).val());
		});

		$('.roomtypeDD').live('change', function(){
			var val = $(this).val(),
				thisid = $(this).attr('id'),
				index = thisid.split('_')[1],
				roomid = '#room_' + index;
			$.get('buildreserveform.php',
				{rtid: val},
				function(resp){
					$(roomid).empty().html(resp);
				}
			);
			
		});
		$('.checkinlink').live('click', function(e){
			e.preventDefault();
			var thisid = $(this).attr('id'),
				index = thisid.split('_')[1],
				selectId = '#rm_' + index,
				roomId = $(selectId).val(),
				href = $(this).attr('href');
			if (roomId == 0) {
				alert('Please select a room first.');
				exit;	
			}
			parent.document.location.href= href + roomId;
		});
		
		$('.updatelink').live('click', function(e){
			e.preventDefault();
			var thisid = $(this).attr('id'),
				index = thisid.split('_')[1],
				chkin = 'checkin_' + index,
				chkout = 'checkout_' + index,
				room = '#room_' + index,
				updateroom = '#rm_' + index,
				href = $(this).attr('href'),
				roomid = '';
			if ($(room).val() == undefined) {
				roomid = $(updateroom).val();
			} else {
				roomid = $(room).val();
			}
			$.post(
				'updateaction.php',
				{
					in: $('input.combo-value[name=' + chkin + ']').val(),
					out: $('input.combo-value[name=' + chkout + ']').val(),
					rid: index,
					room: roomid
				},
				function(resp) {
					$.messager.alert('Update Booking Status', resp, 'info');
					document.location.href= href;
				}
			);
			
		});
	});
</script>
<?php if (!isset($reservation)) : ?>
<div id="room_panel" class="easyui-panel" title="Rooms to Reserve" style="width:540px;padding:10px;">
	<div>
		<label for="new_roomcount" style="display:inline">No. of Rooms</label>
		<input type="text" name="new_roomcount" id="new_roomcount" value="1"
			class="easyui-numberbox" style="width:40px;text-align:right;" min="1" max="100" />
	</div>
	<div id="room_list"></div>
</div>
<?php else: ?>
<div id="reserved_room_panel" class="easyui-panel" title="Reserved Rooms" style="width:580px;padding:10px;">
	<div>
		<table id="reserved_room_list" class="easyui-datagrid" style="border:1px inset #ccc;">
		<thead>
			<tr>
				<th field="rtype" width="100px">Room Type</th>
				<th field="in" width="80px">Check In</th>
				<th field="out" width="80px">Check Out</th>
				<th field="room" width="120px">Select Room</th>
				<th field="actions" width="170px">Action</th>
			</tr>
		</thead>
		<tbody>
		<?php echo $table ?>
		</tbody>
		</table>
	</div>
</div>
<?php endif; ?>
