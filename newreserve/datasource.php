<?php
//datasource.php
include_once('../fds/ajax/config/config.inc.php');
include_once('../fds/ajax/reserve.functions.php');

$retval = array();
if (isset($_GET)) {
	$act = $_GET['act'];
	$opt = $_GET['opt'];
	$val = $_GET['val'];
	
	$userFn = $act . $opt;
	$retval = call_user_func($userFn, $val);
}

echo $retval;

/** function definitions **/
function searchguest($val) {
	$now = date('Y-m-d');
	$sql = " select distinct a.reserve_code, a.reserve_date, 
			concat_ws(' ', b.firstname, b.lastname) as guestname,
			a.reserve_fee, a.pax
			from reservations a, guests b, reserve_rooms c
			where a.status='Active'  
			and a.guest_id=b.guest_id
			and a.reserve_code=c.reserve_code 
			and c.checkin >= '$now' 
			";
	$sql.=" and (b.firstname like '%$val%' ";
	$sql.=" or b.lastname  like '%$val%') ";

	$res = mysql_query($sql) or die(mysql_error());
	$numrows = mysql_num_rows($res);
	$data = array();
	while ($row = mysql_fetch_object($res)) {
		$data[] = $row;
	}
	$retval = array(
		'sql'   => $sql,
		'total' => $numrows,
		'rows'  => $data
	);
	return json_encode($retval);
}


?>