<?php
//checkdate.php
include_once('../fds/ajax/config/config.inc.php');
include_once('checkdatefunctions.php');

if (isset($_GET)) {
    foreach ($_GET as $k => $v) {
        ${$k} = $v;
    }
    $retval = array();    
    if ($rtid == '') {
        $retval = array('message' => 'Please select a room type first.');
        echo json_encode($retval);
        exit;
    }
    

    if ($rid !== 0) {
        $code = isRoomAvailableForDate($rid, $dt);
        if ($code === 3) {
            $retval = array(
                'success' => true,
                'code' => 3,
                'message' => "Selected room is available for $dt."
            );            
        } elseif ($code === 2) {
             $retval = array(
                'success' => false,
                'code' => 2,
                'message' => "Selected room is already occupied for $dt."
            );
        } elseif ($code === 4) {
             $retval = array(
                'success' => false,
                'code' => 4,
                'message' => "Guest in selected room will check out on $dt."
            );
        } else {
        	$retval = array(
                'success' => false,
                'code' => 1,
                'message' => "Selected room is already booked for $dt."
            );
        }
        echo json_encode($retval);
        exit;
    }
}
?>
