<?php
//partner.php
include_once('../fds/ajax/reserve.functions.php');
$now = date('Y-m-d');
if (isset($reservation)) {
	$sql = "select booking_number, payable, result_status from partner_transactions 
			where reserve_code='$code'";
	$res = mysql_query($sql);
	$partner = mysql_fetch_object($res);
}
?>
<div id="partners" class="easyui-panel" title="Partner Information" 
	style="width:220px;padding:10px;" collapsible="true"  >
	<div>
		<label for='datebooked'>Date Booked</label>
		<input type="text" id="datebooked" name="datebooked" class="easyui-datebox datebox-f combo-f" 
		value="<?php echo isset($reservation->reserve_date) ? $reservation->reserve_date : $now ?>">
	</div>
	<div>
		<label>Partner</label> 
			<?php
			echo getPartners(isset( $reservation->Partner ) ? $reservation->Partner : '');
			?>
	</div>
	<div>
		<label for='bookingnumber'>Booking Number</label>
		<input type="text" id="bookingnumber" name="bookingnumber"
			value="<?php echo $partner->booking_number?$partner->booking_number:''?>">
	</div>
	<div>
		<label for='commission'>Commission</label>
		<input type="text" id="commission" name="commission"
			value="<?php echo $partner->payable?$partner->payable:''?>">
	</div>
	<?php //echo $partner->result_status? '<em>Booking Status: ' . $partner->result_status . '</em>':''?>
</div>
