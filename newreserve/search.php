<?php
//search.php
?>
	<script>
		function searchHandler(value,name){
			if (name === 'code') {
				document.location.href='base.php?act=new&code=' + value; 
				exit;
			}
			var url = 'datasource.php?act=search&opt=' + name + '&val=' + value;
			$('#test').datagrid({
				url: url,
				title: 'Guests with Reservations',
				width: 700,
				height: 400,
				fitColumns: true,
				striped: true,
				collapsible:true,
				pagination:true,
				rownumbers:true,
				columns:[[
					{field:'reserve_code',title:'Code',width:90,
						formatter:function(value,rec){
							var link = 'base.php?act=new&code=' + value;
							return '<a href="' + link + '" style="color:blue;text-decoration:none;font-weight:bold;">' + value + '</a>';
						}
					},
					{field:'reserve_date',title:'Booking Date',width:90},
					{field:'guestname',title:'Guest Name',width:180},
					{field:'reserve_fee',title:'Fee/Deposit',width:70,align:'right'},
					{field:'pax',title:'Pax',width:70,align:'right'}
				]],
				onHeaderContextMenu: function(e, field){
					e.preventDefault();
					if (!$('#tmenu').length){
						createColumnMenu();
					}
					$('#tmenu').menu('show', {
						left:e.pageX,
						top:e.pageY
					});
				}
			});
		}
	</script>
	<div style="background:#EEEEFF;padding:5px;width:auto;">
		<a href="base.php?act=new" id="mb1" class="easyui-linkbutton" iconCls="icon-add">New</a>
		&nbsp;
		Search By: 
		<input id="searchBox" class="easyui-searchbox"
			searcher="searchHandler"
			prompt="Please Input Value" menu="#searchBoxOptions" style="width:300px"></input>
			<div id="searchBoxOptions" style="width:120px">
				<div name="guest">Guest Name</div>
				<div name="code">Reserve Code</div>
			</div>
		
	</div>
	
	<table id="test"></table>
			
	
