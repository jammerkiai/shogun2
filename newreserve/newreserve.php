<?php
//newreserve.php
include_once('../fds/ajax/config/config.inc.php');
$code = '';
if (isset($_GET['code']) && $_GET['code'] != '') {
	$code = $_GET['code'];
	//get reservation
	$sql = "select * from reservations where reserve_code='$code'";
	$res = mysql_query($sql);
	$reservation = mysql_fetch_object($res);
	if ( isset($_GET['act2']) && $_GET['act2'] == 'print') {
		$sql1 = "select a.door_name,b.checkin from 
			rooms a, reserve_rooms b 
			where
			a.room_id=b.room_id 
			and b.reserve_code='$code'";
		$res1 = mysql_query($sql1);
		if(mysql_num_rows($res1)) {
			list($door, $checkin)=mysql_fetch_row($res1);
		}else{
			$door = "No room selected";
		}
	
		$sql = " select a.reserve_date, a.pax, a.reserve_fee, a.notes, a.pickup_time, a.pickup_location, a.date_created,
			concat_ws(' ', b.firstname, b.lastname) as 'guestname' 
			from reservations a, guests b
			where a.guest_id=b.guest_id
			and a.reserve_code='$code'
			";
		$res = mysql_query($sql);
		
		$row = mysql_fetch_array($res);
		$body="Shogun 2: Reservation Slip\n\n\n";
		$body.="\nReservation Code: ". $code;
		$body.="\nDate: ". $row['date_created'];
		$body.="\nExpected Checkin: ". $checkin; //$row['reserve_date'];
		$body.="\nGuest Name: ". $row['guestname'];
		$body.="\nExpected No. of Guests: ". $row['pax'];
		$body.="\nReservation Fee: ". $row['reserve_fee'];
		$body.="\nRoom No: $door";
		$body.="\nDetails: " .$row['notes'];
		if($row['pickup_time']!='00:00:00') {
			$body.="\nPickup Time: " .$row['pickup_time'];
		}
		if($row['pickup_location'] != '') {
			$body.="\nPickup Location: " .$row['pickup_location'];
		}
		$retval=chr(hexdec('1B')).chr(hexdec('4D')).chr(1);	
		$retval.= $body."\n\n\n\n\n";	
		$retval.=chr(hexdec('1D')).chr(hexdec('56')).chr(49);
		$file = "reserve$new_reserve_code.txt";
		$fp = fopen("../fds/ajax/reports/" .$file, "w");
		fwrite( $fp,$retval);
		fclose($fp);
		shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
		shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
		echo "<script>document.location.href='base.php?act=new&code=$code'</script>";
		exit;
	}	
}
?>
<script>
	$(function(){
		$('#mb1').bind('click', function(e){
			e.preventDefault();
			if ( $('#newReserveForm').form('validate') ) {
				$('#newReserveForm').submit();
			}
		});
		
		$('#mb10').bind('click', function(e){
			e.preventDefault();
			$.messager.confirm('Confirm', 'Continue updating this reservation?',
				function(r) {
					if (r) {
						if ( $('#newReserveForm').form('validate') ) {
							$('#newReserveForm').submit();
						}
					}
				}
			)
			
		});
		
		$('#cancel').bind('click', function(e) {
		    e.preventDefault();
		    $('#noshowDialog')
		        .show()
		        .dialog({
		            title: 'Booking Cancellation',
		            resizable: true,
				    buttons:[{
					    text:'Save',
					    iconCls:'icon-save',
					    handler:function(){
					    	/*
						    $.post(
						    	'./cancelaction.php',
						    	{
						    		code: $('#new_reserve_code').val(),
						    		penalty: $('#cancelPenalty').val(),
						    		reason: $('input[name=cancelReason]').val(),
						    		remark: $('#cancelRemarks').val(),
						    		bookingnum: $('#bookingnumber').val(),
						    		partner: $('#Partner').val()
						    	},
						    	function(resp) {
						    		$.messager.alert('Alert', resp);
						    	}
						    );
						    $('#noshowDialog').dialog('close');
						    document.location.href = 'base.php?act=new&code=' + $('#new_reserve_code').val() + parms;
						    */
						    var code = $('#new_reserve_code').val(),
						    		penalty = $('#cancelPenalty').val(),
						    		reason = $('input[name=cancelReason]').val(),
						    		remark = $('#cancelRemarks').val(),
						    		bookingnum = $('#bookingnumber').val(),
						    		approval = $('#approvalNumber').val(),
						    		batchnum = $('#batchNumber').val(),
						    		cardtype = $('input[name=cardType]').val(),
						    		cardselect = $('#cardselect').prop('checked') ? 1 : 0,
						    		partner = $('#Partner').val();
						    var parms = '&penalty=' + penalty;
						    parms += '&reason=' + reason;
						    parms += '&remark=' + remark;
						    parms += '&bookingnum=' + bookingnum;
						    parms += '&cardtype=' + cardtype;
						    parms += '&batchnum=' + batchnum;
						    parms += '&approval=' + approval;
						    parms += '&partner=' + partner;
						    parms += '&cardselect=' + cardselect;
						    
						    document.location.href = 'cancel.php?code=' + $('#new_reserve_code').val() + parms;
					    }
				    },{
					    text:'Disregard',
					    iconCls: 'icon-undo',
					    handler:function(){
						    $('#noshowDialog').dialog('close');
					    }
				    }]
			    });
		});
	});
</script>
<form method="post" action="newreserveaction.php" id="newReserveForm">
	<div style="background:#EEEEFF;padding:5px;width:auto;">
		<?php if ($code === '') { ?>
			<a href="base.php" id="mb2" class="easyui-linkbutton" iconCls="icon-search">Back to Search</a>
			<a href="#" id="mb1" class="easyui-linkbutton" iconCls="icon-save">Save new Reservation</a>
			<a href="base.php?act=new" id="mb4" class="easyui-linkbutton" iconCls="icon-reload">Refresh</a>
		<?php } else { ?>
			<input type="hidden" name="new_reserve_code" id="new_reserve_code" value="<?php echo $code?>" />
			<span style="font-weight:bold;font-size:14px;color:#f60;">Reservation #<?php echo $code ?>
			: <span style='font-style:italic;color:#33F;font-size:14px;'><?php echo $reservation->status ?></span>
			</span>
			&nbsp;
			<?php if ($reservation->status == 'Active') : ?>
			<a href="#" id="mb10" class="easyui-linkbutton" iconCls="icon-edit">Update</a>
			<a href="base.php" id="cancel" class="easyui-linkbutton" iconCls="icon-cancel">Cancel</a>
			<a href="base.php?act=new&act2=print&code=<?php echo $reservation->reserve_code ?>" id="print" class="easyui-linkbutton" iconCls="icon-print">Print</a>
			<?php endif; ?>
			<a href="base.php" id="mb2" class="easyui-linkbutton" iconCls="icon-search">Back to Search</a>
			<a href="base.php?act=new" id="mb4" class="easyui-linkbutton" iconCls="icon-add">New</a>
		<?php }  ?>
	</div>
	<table>
	<tr>
		<td valign="top">
			<?php include_once('rooms_reserve.php'); ?>
			<?php include_once('room_transactions.php'); ?>
		</td>
		<td valign="top"><?php include_once('partner.php'); ?><?php include_once('particular_reserve.php'); ?></td>
		<td valign="top"><?php include_once('guest_reserve.php'); ?><?php include_once('payment_reserve.php'); ?></td>
	</tr>
	</table>

       <div id="noshowDialog" icon="icon-cancel" style="padding:5px;width:540px;height:280px;display:none;">
        <div style="padding:10px;">
            <div style="width: 50%; float: left;">
                <div>
                    <label for="cancelReason">Reason</label>
                    <select id="cancelReason" class="easyui-combobox" 
                        name="cancelReason"
                        multiple="false" 
            			panelHeight="auto">
                        <option value="No Show">No Show</option>
                        <option value="Cancelled">Cancelled</option>
                    </select>
                </div>
                <div>
                    <label for="cancelPenalty">Penalty</label>
                    <input type="text" id="cancelPenalty" name="cancelPenalty"
                        style="display:inline-block;" class="easyui-numberbox" style="text-align:right;">
                    
                </div>
                <div>
                    <label for="cancelRemarks">Remarks</label>
                    <textarea name="cancelRemarks" id="cancelRemarks" style="width:200px;height:40px;"></textarea>
                </div>
            </div>
            <div style="width: 50%; float: left;">
                <div>
                    <fieldset style="border: 1px solid #ccf;">
                        <legend>
                        <input type="checkbox" name="cardselect" id="cardselect" value="1" />
                        <label for='cardselect' style='display:inline-block;width:100%;'>Card Details</label>
                        </legend>
                        <div id="cardPanel">
                            <div>
                                <label for="cardType">Card Type</label>
                                <select id="cardType" class="easyui-combobox" 
                                    name="cardType"
                                    multiple="false" 
                        			panelHeight="auto">
                                    <option value="AMEX">AMEX</option>
                                    <option value="JBC">JBC</option>
                                    <option value="Visa">Visa</option>
                                    <option value="Mastercard">MasterCard</option>
                                    <option value="BDO Card">BDO Card</option>
                                    <option value="Express Net">ExpressNet</option>
                                    <option value="Megalink">Megalink</option>
                                    <option value="BancNet">BancNet</option>
                                </select>
                            </div>
                            <div>
                                <label for="approvalNumber">Approval Number</label>
                                <input type="text" id="approvalNumber" name="approvalNumber"
                                style="display:inline-block;" class="easyui-numberbox" style="text-align:right;">
                            </div> 
                                                    <div>
                                <label for="batchNumber">Batch Number</label>
                                <input type="text" id="batchNumber" name="batchNumber"
                                style="display:inline-block;" class="easyui-numberbox" style="text-align:right;">
                            </div>  
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    </form>

