<?php
include('classes/rb.php');

//alter table occupancy add discount_rate decimal(12,2) not null default 0;

date_default_timezone_set("Asia/Manila");
ini_set('error_reporting', 1);

$dbname = "shogunfds2";
$host = "192.168.1.162";
$user = "root";
$pass = "shogunWarrior55";


R::setup( "mysql:host=$host;dbname=$dbname",
    $user, $pass );

//append new field to occupancy table

$sql = "select occupancy_id from occupancy where actual_checkout='0000-00-00 00:00:00' and year(actual_checkin) = 2016";

$occ = R::getCol($sql);

foreach($occ as $oid) {
    $rate = getRoomRate($oid);
    $disc = getDiscount($oid);

    if ($disc) {
        $drate = number_format(-100 * $disc/$rate, 2);
        resetOccupancyDiscount($oid, $drate);
    }
}

echo "Done";

/****************************** support functions ********************************/

function resetOccupancyDiscount($occupancy_id, $discount) {
    $sql = "update occupancy set discount_rate=?
            where occupancy_id=?";
    R::exec($sql, [$discount, $occupancy_id]);
}

function getRoomSaleItem($item_id, $occupancy_id) {
    $sql = "select unit_cost
            from room_sales
            where category_id=3
            and item_id=? and occupancy_id=?
            order by roomsales_id
            limit 0, 1";
    $rs = R::getCol($sql, [$item_id, $occupancy_id])[0];

    return $rs;
}

function getDiscount($occupancy_id) {
    return getRoomSaleItem(17, $occupancy_id);
}

function getRoomRate($occupancy_id) {
    return getRoomSaleItem(15, $occupancy_id);
}

