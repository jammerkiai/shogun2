<?php
/**
* this will evaluate the group level of the current logged user
* prevents access if group_id > 4
*/

session_start();
if ($_SESSION["hotel"]["groupid"] > 4 ) {
// if TO or Cashier, prevent access
	echo "<h1>You do not have permission to view this page.</h1>";
} 
// allow access, redirect to the indicated page
else {
	header("location: " . $_GET['to']);
}


?>