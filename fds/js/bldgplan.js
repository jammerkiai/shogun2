Ext.ns('Application');
Application.BuildingPlan = Ext.extend(Ext.TabPanel, {
	deferredRender:true,
	tabPosition:'top',
	defaults:{iconCls:'x-icon-building'},
    initComponent:function() {
		Ext.apply(this, Ext.apply(this.initialConfig));
		Ext.apply(this, {
			store : new Ext.data.JsonStore({
				autoDestroy:true,
				url: "./ajax/json.php",
				batch:true,
				method: "POST", 
				baseParams: {
					act:'building', owner: this.id
				}
			})
		});
        Application.BuildingPlan.superclass.initComponent.apply(this, arguments);
    } 

    ,onRender:function(ct,position) {
		Application.BuildingPlan.superclass.onRender.apply(this, arguments);
		this.store.load();
		this.store.on({
			'load' : {
					fn: function(r) {
						r.each(
							 function(r){
								Ext.getCmp(this.store.baseParams.owner).add({title: r.data.floor_label, xtype: 'floorplan', floorid: r.data.floor_id});
								Ext.getCmp(this.store.baseParams.owner).doLayout();
							}
						);
					}
					,scope:this
				}
		});
		this.setActiveTab(0);
	}
});
 
Ext.reg('buildingplan', Application.BuildingPlan);