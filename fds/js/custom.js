/*
function for changing main 
iframe source
*/
function changeView(url){
	/*$('mnIframe').src=url;*/
	location.href=url;
}
/*
code for appending custom scrollbar to iframe
*/
var csbObj,csbDragX,csbMseX,csbDragY,csbMseY,csbTO,csbSpd,csbD;

function csbCustomXScroll(){
 csbarg=csbCustomXScroll.arguments;
 for (csb0=0;csb0<csbarg.length;csb0+=2){
  csbdobj=document.getElementById(csbarg[csb0]);
  if (csbdobj.tagName=='IFRAME'){ csbiobj=window.frames[csbarg[csb0]].document.getElementsByTagName('DIV')[0]; }
  else { csbiobj=csbdobj.getElementsByTagName('DIV')[0];  }
  csbpobj=document.getElementById(csbarg[csb0+1]);
  csbsobj=csbpobj.getElementsByTagName('DIV')[0];
  csblobj=csbpobj.getElementsByTagName('DIV')[1];
  csbrobj=csbpobj.getElementsByTagName('DIV')[2];
  csbsobj.os=0;
  if (csblobj&&csbrobj){
   csbsobj.os=csblobj.offsetWidth;
   csblobj.obj=csbsobj;
   csblobj.onmousedown=function(){ csbSpd=1; csbLeftRight(this.obj,0); }
   csblobj.onmouseup=function(){ clearTimeout(csbTO); }
   csblobj.style.left='0px';
   csblobj.style.zIndex=1;
   csbrobj.obj=csbsobj;
   csbrobj.onmousedown=function(){ csbSpd=1; csbLeftRight(this.obj,1); }
   csbrobj.onmouseup=function(){ clearTimeout(csbTO); }
   csbrobj.style.left=(csbpobj.offsetWidth-csbrobj.offsetWidth)+'px';
   csbrobj.style.zIndex=1;
  }
  csbsobj.style.left=(csbsobj.os)+'px';
  csbsobj.onmousedown=function(event){ csbXMseDown(event,this);}
  csbsobj.inw=csbiobj.offsetWidth-csbdobj.offsetWidth+csbsobj.offsetWidth*2;
  csbsobj.pw=csbsobj.parentNode.offsetWidth-csbsobj.offsetWidth;
  csbsobj.obj=csbiobj;
  csbsobj.objos=csbiobj.offsetLeft;
 }
}

function csbCustomYScroll(){
 csbarg=csbCustomYScroll.arguments;
 for (csb0=0;csb0<csbarg.length;csb0+=2){
  csbdobj=document.getElementById(csbarg[csb0]);
  if (csbdobj.tagName=='IFRAME'){ csbiobj=window.frames[csbarg[csb0]].document.getElementsByTagName('DIV')[0]; }
  else { csbiobj=csbdobj.getElementsByTagName('DIV')[0];  }
  csbpobj=document.getElementById(csbarg[csb0+1]);
  csbsobj=csbpobj.getElementsByTagName('DIV')[0];
  csbtobj=csbpobj.getElementsByTagName('DIV')[1];
  csbbobj=csbpobj.getElementsByTagName('DIV')[2];
  csbsobj.os=0;
  if (csbtobj&&csbtobj){
   csbsobj.os=csbtobj.offsetHeight;
   csbtobj.obj=csbsobj;
   csbtobj.onmousedown=function(){ csbSpd=1; csbUpDown(this.obj,0); }
   csbtobj.onmouseup=function(){ clearTimeout(csbTO); }
   csbtobj.style.top='0px';
   csbtobj.style.zIndex=1;
   csbbobj.obj=csbsobj;
   csbbobj.onmousedown=function(){ csbSpd=1; csbUpDown(this.obj,1); }
   csbbobj.onmouseup=function(){ clearTimeout(csbTO); }
   csbbobj.style.top=(csbpobj.offsetHeight-csbbobj.offsetHeight)+'px';
  }
  csbsobj.style.top=(csbsobj.os)+'px';
  csbsobj.onmousedown=function(event){ csbYMseDown(event,this);}
  csbsobj.inh=csbiobj.offsetHeight-csbdobj.offsetHeight;
  csbsobj.ph=csbpobj.offsetHeight-csbsobj.offsetHeight-csbsobj.os*0;
  csbsobj.obj=csbiobj;
  csbsobj.objos=csbiobj.offsetTop;
 }
}

function csbLeftRight(csbobj,csbd){
 csbObj=csbobj; csbD=csbd;
 if (csbD){ csbObj.style.left=(csbObj.offsetLeft-csbSpd)+'px'; }
 else { csbObj.style.left=(csbObj.offsetLeft+csbSpd)+'px'; }
 csbPosInnerX();
 csbSpd=csbSpd*1.1;
 if (csbSpd>95){ csbSpd=95; }
 if (csbObj.offsetLeft>0+csbObj.os&&csbObj.offsetLeft<csbObj.pw-csbObj.os){
  csbTO=setTimeout('csbLeftRight(csbObj,csbD)',100-csbSpd);
 }
 else {
  if (csbObj.offsetLeft<csbObj.os){ csbObj.style.left=csbObj.os+'px'; }
  if (csbObj.offsetLeft>csbObj.pw-csbObj.os){ csbObj.style.left=(csbObj.pw-csbObj.os)+'px'; }
  csbPosInnerX();
 }
}

function csbUpDown(csbobj,csbd){
 csbObj=csbobj; csbD=csbd;
 if (csbD){ csbObj.style.top=(csbObj.offsetTop-csbSpd)+'px'; }
 else { csbObj.style.top=(csbObj.offsetTop+csbSpd)+'px'; }
 csbPosInnerY();
 csbSpd=csbSpd*1.1;
 if (csbSpd>95){ csbSpd=95; }
 if (csbObj.offsetTop>0+csbObj.os&&csbObj.offsetTop<csbObj.ph-csbObj.os){
  csbTO=setTimeout('csbUpDown(csbObj,csbD)',100-csbSpd);
 }
 else {
  if (csbObj.offsetTop<csbObj.os){ csbObj.style.top=csbObj.os+'px'; }
  if (csbObj.offsetTop>csbObj.ph-csbObj.os){ csbObj.style.top=(csbObj.ph-csbObj.os)+'px'; }
  csbPosInnerY();
 }
}

function csbXMseDown(event,csbobj) {
 document.onmousemove=function(event){csbXDrag(event);}
 document.onmouseup=function(event){csbMseUp(event);}
 csbObj=csbobj;
 csbMse(event);
 csbDragX=csbMseX-csbObj.offsetLeft;
}

function csbXDrag(event) {
 csbMse(event);
 if ((csbMseX-csbDragX)>0+csbObj.os&&(csbMseX-csbDragX)<csbObj.pw-csbObj.os){
  csbObj.style.left=(csbMseX-csbDragX)+'px';
 }
 csbPosInnerX();
}

function csbYMseDown(event,csbobj) {
 document.onmousemove=function(event){csbYDrag(event);}
 document.onmouseup=function(event){csbMseUp(event);}
 csbObj=csbobj;
 csbMse(event);
 csbDragY=csbMseY-csbObj.offsetTop;
}

function csbYDrag(event) {
 csbMse(event);
 if ((csbMseY-csbDragY)>=csbObj.os&&(csbMseY-csbDragY)<csbObj.ph-csbObj.os){
  csbObj.style.top=(csbMseY-csbDragY)+'px';
 }
 csbPosInnerY();
}

function csbMse(event){
 if(!event) var event=window.event;
 if (document.all){ csbMseX=event.clientX; csbMseY=event.clientY; }
 else {csbMseX=event.pageX; csbMseY=event.pageY; }
}

function csbMseUp(event) {
 document.onmousemove=null; csbDragX=-1; csbDragY=-1;
}

function csbPosInnerX() {
 csbObj.obj.style.left=(-((csbObj.offsetLeft-csbObj.os)*csbObj.inw/(csbObj.pw-csbObj.os*2))+csbObj.objos)+'px'
}

function csbPosInnerY(){
 csbObj.obj.style.top=(-((csbObj.offsetTop-csbObj.os)*csbObj.inh/(csbObj.ph-csbObj.os*2))+csbObj.objos)+'px'
}
/*
end of code for appending custom scrollbar
*/


/*
this block of code is used for checkbox toggle
all or single selection
*/
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
for (i = 0; i < field.length; i++) {
field[i].checked = true;}
checkflag = "true";
return; }
else {
for (i = 0; i < field.length; i++) {
field[i].checked = false; }
checkflag = "false";
return; }
}
function MM_openBrWindow(theURL,winName,features) { //v2.0"
	 window.open(theURL,winName,features);
}
function toggleIndeterminate(oElement) 
{ 
  oForm = oElement.form; 
  oElement = oForm.elements[oElement.name]; 
  if(oElement.length) 
  { 
    bIndeterminate = false; 
    bChecked = true; 
    nChecked = 0; 
    for(i = 1; i < oElement.length; i++) 
      if(oElement[i].checked) 
        nChecked++; 
    if(nChecked < oElement.length - 1) 
    { 
      if(nChecked) 
        bIndeterminate = true; 
      else 
      { 
        bIndeterminate = false; 
        bChecked = false; 
      } 
    } 
    else 
    { 
      bIndeterminate = false; 
    } 
    oElement[0].indeterminate = bIndeterminate; 
    oElement[0].checked = bChecked; 
  } 
}
//javascript for checkbox select/unselect All
function toggleController(oElement)
{
	oForm=oElement.form;oElement=oForm.elements[oElement.name];
	if(oElement.length) {
		bChecked=true;nChecked=0;for(i=1;i<oElement.length;i++)
		if(oElement[i].checked)
			nChecked++;if(nChecked<oElement.length-1)
			bChecked=false;oElement[0].checked=bChecked;
	}
}

function toggleChecked(oElement) 
{ 
  oForm = oElement.form; 
  oElement = oForm.elements[oElement.name]; 
  if(oElement.length) 
  { 
    bChecked = oElement[0].checked; 
    for(i = 1; i < oElement.length; i++) 
      oElement[i].checked = bChecked; 
  } 
}

function toggleIndeterminate(oElement) 
{ 
  oForm = oElement.form; 
  oElement = oForm.elements[oElement.name]; 
  if(oElement.length) 
  { 
    bIndeterminate = false; 
    bChecked = true; 
    nChecked = 0; 
    for(i = 1; i < oElement.length; i++) 
      if(oElement[i].checked) 
        nChecked++; 
    if(nChecked < oElement.length - 1) 
    { 
      if(nChecked) 
        bIndeterminate = true; 
      else 
      { 
        bIndeterminate = false; 
        bChecked = false; 
      } 
    } 
    else 
    { 
      bIndeterminate = false; 
    } 
    oElement[0].indeterminate = bIndeterminate; 
    oElement[0].checked = bChecked; 
  } 
}

/*
end checkbox toggle
*/

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function GP_popupConfirmMsg(msg) { //v1.0
  document.MM_returnValue = confirm(msg);
}

function submitbutton(pressbutton) {
	submitform(pressbutton);
}
function submitform(pressbutton){
	document.adminForm.task.value=pressbutton;
	try {
		document.adminForm.onsubmit();
		}
	catch(e){}
	document.adminForm.submit();
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function YY_checkform() { //v4.71
//copyright (c)1998,2002 Yaromat.com
  var a=YY_checkform.arguments,oo=true,v='',s='',err=false,r,o,at,o1,t,i,j,ma,rx,cd,cm,cy,dte,at;
  for (i=1; i<a.length;i=i+4){
    if (a[i+1].charAt(0)=='#'){r=true; a[i+1]=a[i+1].substring(1);}else{r=false}
    o=MM_findObj(a[i].replace(/\[\d+\]/ig,""));
    o1=MM_findObj(a[i+1].replace(/\[\d+\]/ig,""));
    v=o.value;t=a[i+2];
    if (o.type=='text'||o.type=='password'||o.type=='hidden'){
      if (r&&v.length==0){err=true}
      if (v.length>0)
      if (t==1){ //fromto
        ma=a[i+1].split('_');if(isNaN(v)||v<ma[0]/1||v > ma[1]/1){err=true}
      } else if (t==2){
        rx=new RegExp("^[\\w\.=-]+@[\\w\\.-]+\\.[a-zA-Z]{2,4}$");if(!rx.test(v))err=true;
      } else if (t==3){ // date
        ma=a[i+1].split("#");at=v.match(ma[0]);
        if(at){
          cd=(at[ma[1]])?at[ma[1]]:1;cm=at[ma[2]]-1;cy=at[ma[3]];
          dte=new Date(cy,cm,cd);
          if(dte.getFullYear()!=cy||dte.getDate()!=cd||dte.getMonth()!=cm){err=true};
        }else{err=true}
      } else if (t==4){ // time
        ma=a[i+1].split("#");at=v.match(ma[0]);if(!at){err=true}
      } else if (t==5){ // check this 2
            if(o1.length)o1=o1[a[i+1].replace(/(.*\[)|(\].*)/ig,"")];
            if(!o1.checked){err=true}
      } else if (t==6){ // the same
            if(v!=MM_findObj(a[i+1]).value){err=true}
      }
    } else
    if (!o.type&&o.length>0&&o[0].type=='radio'){
          at = a[i].match(/(.*)\[(\d+)\].*/i);
          o2=(o.length>1)?o[at[2]]:o;
      if (t==1&&o2&&o2.checked&&o1&&o1.value.length/1==0){err=true}
      if (t==2){
        oo=false;
        for(j=0;j<o.length;j++){oo=oo||o[j].checked}
        if(!oo){s+='* '+a[i+3]+'\n'}
      }
    } else if (o.type=='checkbox'){
      if((t==1&&o.checked==false)||(t==2&&o.checked&&o1&&o1.value.length/1==0)){err=true}
    } else if (o.type=='select-one'||o.type=='select-multiple'){
      if(t==1&&o.selectedIndex/1==0){err=true}
    }else if (o.type=='textarea'){
      if(v.length<a[i+1]){err=true}
    }
    if (err){s+='* '+a[i+3]+'\n'; err=false}
  }
  if (s!=''){alert('The required information is incomplete or contains errors:\t\t\t\t\t\n\n'+s)}
  document.MM_returnValue = (s=='');
}

function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode
if (charCode > 31 && (charCode < 48 || charCode > 57))
return false;
return true;
}
