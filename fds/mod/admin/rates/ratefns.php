<?php
/*
functions for room-type-rates admin
*/

function getRecord($id){
	if(is_array($id) ) {
		$id = $id[0];
	} 
	$sql = "select * from rates where rate_id='$id'";
	return R::getRow($sql);
}

function formEntry($value_arr = '') {
	$form_array = array('rate_id', 'rate_name', 'hour_start', 'hour_end', 'duration');
	$form = '';
	foreach($form_array as $field) {
		$fieldName = ucwords(str_replace('_', ' ', $field));
		$type = ($field == 'rate_id') ? 'hidden' : 'text';
		$form .= "<div class='element $type'>";
		$form .= "<label for='$field'>$fieldName</label>";
		$postValue = $value_arr[$field];
		$form .= "<input type='$type' name='new_$field' value='$postValue' />";
		if ($type == 'hidden') $form .= $postValue; 
		$form .= "</div>";
	}
	return $form;
}

function rateTable() {
	$sql = "select * from rates ";
	$res = R::getAll($sql);
	$retval = '<table>';
	$retval .= '<tr>';
	$retval .= "<th><input type='checkbox' id='cbmaster' /></th>";
	$retval .= "<th>Rate Name</th>";
	$retval .= "<th>Hour Start</th>";
	$retval .= "<th>Hour End</th>";
	$retval .= "<th>Duration</th>";
	$retval .= '</tr>';
	foreach ($res as $r) {
		$retval .= '<tr>';
		$retval .= "<td><input type='checkbox' name='rateid[]' value='{$r['rate_id']}' class='cbItem' /></td>";
		$retval .= "<td>{$r['rate_name']}</td>";
		$retval .= "<td>{$r['hour_start']}</td>";
		$retval .= "<td>{$r['hour_end']}</td>";
		$retval .= "<td>{$r['duration']}</td>";
		$retval .= '</tr>';
	}	
	$retval.= '</table>';
	return $retval;
}

function buildButtons($arr) {
	$buttons = '<div class="menubar">';
	foreach($arr as $button) {
		$buttons.= "<input type='submit' name='cmd' value='$button' id='$button' />";
	}
	$buttons.='</div>';
	return $buttons;
}

/*
$filters = assoc array of field=>value pairs to filter room_type_rate data
-> rtr_id, room_type_id, rate_id
*/
function rtrTable($filters = '') {
	$filter = '';
	$sql = "select room_type_rates.*, room_types.room_type_name, rates.rate_name
		from room_type_rates, room_types, rates 
		where rates.rate_id=room_type_rates.rate_id
		 and room_types.room_type_id=room_type_rates.room_type_id ";
	if (is_array($filters) ) {
		foreach($filters as $field=>$value) {
			$filter .= " and room_type_rates.$field = '$value' ";	
		}
	}
	$sql .= $filter;
	$res = R::getAll($sql);
	$retval = '<table width="500px">';
	$retval.='<tr>';
	$retval.='<th><input type="checkbox" id="cbmaster" /></th>';
	$retval.='<th>Room Type</th>';
	$retval.='<th>Rate Name</th>';
	$retval.='<th>Basic Rate</th>';
	$retval.='<th>Overtime Rate</th>';
	$retval.='<th>Publish Rate</th>';
	$retval.='<th>Active</th>';
	$retval.='</tr>';
	foreach($res as $row) {
		$retval.='<tr>';
		$retval.='<td><input type="checkbox" value="' . $row['rtr_id'] . '" name="rtr_id[]" class="cbitem" /></td>';
		$link = "<a href='roomrates.php?id={$row['rate_id']}&rtid={$row['room_type_id']}'>" . $row['room_type_name'] . "</a>";
		$retval.='<td>' . $link . '</td>';
		$retval.='<td>' . $row['rate_name'] . '</td>';
		$retval.='<td>' . $row['amount'] . '</td>';
		$retval.='<td>' . $row['ot_amount'] . '</td>';
		$retval.='<td>' . $row['publishrate'] . '</td>';
		$retval.='<td>' . $row['active'] . '</td>';
		$retval.='</tr>';
	}
	$retval .= '</table>';
	return $retval;
}

function getDD($params) {
	$name = $params['name'];
	$selected = $params['selected'];
	$sql = $params['sql'];
	$res = R::getAll($sql);

	$retval = "<select name='$name' id='$name'>";
	$retval.= "<option></option>";
	foreach($res as $row) {
		$retval.="<option value='" .$row[0]. "'";
		if ($selected == $row[0]) $retval.= " selected ";
		$retval.="/>" . $row[1] . "</option>";
	}
	$retval.= "</select>";
	return $retval;
}

function ddRoomtypes($selected = '') {
	$params['name'] = 'new_room_type_id';
	$params['sql'] = 'select room_type_id  as "0", room_type_name as "1" from room_types order by room_type_name';
	$params['selected'] = $selected;
	return getDD($params);
}

function ddRates($selected = '') {
	$params['name'] = 'new_rate_id';
	$params['sql'] = 'select rate_id as "0", rate_name as "1" from rates ';//order by rate_name';
	$params['selected'] = $selected;
	return getDD($params);
}

function ddDiscounts($selected = '') {
	$params['name'] = 'new_discount_id';
	$params['sql'] = 'select discount_id as "0", discount_label  as "1" from discounts ';//order by rate_name';
	$params['selected'] = $selected;
	return getDD($params);
}

function frmRoomTypeRates($value_arr = '') {
	$roomTypeId = (isset($value_arr['room_type_id'])) ? $value_arr['room_type_id'] : $value_arr['new_room_type_id'];
	$rateId = ($value_arr['new_rate_id']) ? $value_arr['new_rate_id'] : $value_arr['rate_id'];

	$form_array = array( 'amount', 'ot_amount', 'active', 'publishrate');
	$form = '';
	if(isset($value_arr['rtr_id'])) {
		$form .= '<input type="hidden" name="new_rtr_id" value="' . $value_arr['rtr_id'] . '" />';
	}
	
	$form .= "<div class='element'>";
	$form .= "<label for='new_room_type_id'>Room Type</label>";
	$form .= ddRoomTypes($roomTypeId);
	$form .= "</div>";
	$form .= "<div class='element'>";
	$form .= "<label for='new_rate_id'>Rate Name</label>";
	$form .= ddRates($rateId);
	$form .= "</div>";
	foreach($form_array as $field) {
		$fieldName = ucwords(str_replace('_', ' ', $field));
		$type = ($field == 'rate_id') ? 'hidden' : 'text';
		$form .= "<div class='element $type'>";
		$form .= "<label for='$field'>$fieldName</label>";
		$postValue = $value_arr[$field];
		$form .= "<input type='$type' name='new_$field' value='$postValue' />";
		if ($type == 'hidden') $form .= $postValue; 
		$form .= "</div>";
	}
	return $form;
}

function getRtrRecord($id){
	if(is_array($id) ) {
		$id = $id[0];
	} 
	$sql = "select * from room_type_rates where rtr_id='$id'";
	return R::getCol($sql);
}

function getRooms($rtid) {
	$sql = "select room_type_name from room_types where room_type_id='$rtid'";
	$typename = R::getCell($sql);
	$sql = "select door_name from rooms where room_type_id='$rtid' order by door_name";
	$res = R::getAll($sql);
	$retval = "<h3>$typename Rooms:</h3>";
	foreach ($res as $r) {
		$door = $r['door_name'];
		$retval.= "$door, ";
	}
	return $retval;
}

function listRoomTypeDiscounts($rtid) {
	$sql = "select b.rate_name, c.discount_label, a.rate_id, a.discount_id
		from room_type_discounts a, rates b, discounts c
		where a.rate_id=b.rate_id 
		and a.discount_id=c.discount_id
		and a.room_type_id=$rtid";
	$res = R::getAll($sql);
	$retval = '<table width="500px">';
	$retval.='<tr>';
	$retval.='<th><input type="checkbox" id="cbmaster" /></th>';
	$retval.='<th>Rate Name</th>';
	$retval.='<th>Discount</th>';
	$retval.='</tr>';

	foreach($res as $r) {
		$rate = $r['rate_name'];
		$disc = $r['discount_label'];
		$rid = $r['$rate_id'];
		$did = $r['discount_id'];
		$retval.="<tr>";
		$newid  = $rid . '_' . $did;
		$retval.="<td><input type='checkbox' name='rate_disc' class='cbitem' value='$newid'></td>";
		$retval.="<td>$rate</td>";
		$retval.="<td>$disc</td>";
		$retval.="</tr>";
	}
	$retval.="</table>";
	return $retval;
}

function listRooms($rtid) {
	$sql = "select  floor_id, floor_label
		from floors order by floor_label ";
	$res = R::getAll($sql);
	$retval = '<table width="640px" class="rooms">';
	$retval.='<tr>';
	$retval.='<th>Floor</th>';
	$retval.='<th>Rooms</th>';
	$retval.='</tr>';
	$fid = '';
	foreach ($res as $r) {
		$fid = $r['floor_id'];
		$fname = $r['floor_label'];
		$retval.="<tr>";
		$retval.="<td>$fname</td>";
		$retval.="<td>";
		$retval.= listRoomsByFloor($fid, $rtid);
		$retval.="</td>";
		$retval.="</tr>";
	}
	$retval.="</table>";
	return $retval;
}

function listRoomsByFloor($floor, $rtype) {
	$sql = "select room_id, door_name, room_type_id
		from rooms where floor_id=$floor ";
	$res = R::getAll($sql);
	$retval.='<div class="roomList">';
	foreach ($res as $r) {
		$rid = $r['room_id'];
		$door = $r['door_name'];
		$rtid = $r['room_type_id'];
		$retval.="<span>";
		$checked =  ($rtid == $rtype) ? 'checked' : '';
		$retval.="<input type='checkbox' id='room_$rid' name='roomid[]' class='cbitem_$floor' value='$rid' $checked>";
		$retval.="&nbsp;<label for='room_$rid' class='$checked'>$door</label>";
		$retval.="</span> ";
	}
	$retval.="</div>";
	return $retval;
} 

?>
