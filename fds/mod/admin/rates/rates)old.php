<?php
require "../../../ajax/config/config.inc.php";

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] != '' ) {
	header('Location: rates_add.php') ;
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: rates_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (rate_name LIKE '".addslashes($_POST['search'])."%' OR rate_name LIKE '%".addslashes($_POST['search'])."%')";
}

$maxRows_rsRates = 10;
$pageNum_rsRates = 0;
if (isset($_GET['pageNum_rsRates'])) {
  $pageNum_rsRates = $_GET['pageNum_rsRates'];
}
$startRow_rsRates = $pageNum_rsRates * $maxRows_rsRates;
$param_rsRates = " WHERE 1=1 ".$param_search;

//$query_rsRates = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsRates, $sortDate);
$query_rsRates = sprintf("select * from rates_dow %s ", $param_rsRates);
$query_limit_rsRates = sprintf("%s LIMIT %d, %d", $query_rsRates, $startRow_rsRates, $maxRows_rsRates);
$rsRates = mysql_query($query_limit_rsRates) or die(mysql_error());
$row_rsRates = mysql_fetch_assoc($rsRates);

if (isset($_GET['totalRows_rsRates'])) {
  $totalRows_rsRates = $_GET['totalRows_rsRates'];
} else {
  $all_rsRates = mysql_query($query_rsRates);
  $totalRows_rsRates = mysql_num_rows($all_rsRates);
}
$totalPages_rsRates = ceil($totalRows_rsRates/$maxRows_rsRates)-1;

$queryString_rsRates = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsRates") == false && 
        stristr($param, "totalRows_rsRates") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsRates = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsRates = sprintf("&totalRows_rsRates=%d%s", $totalRows_rsRates, $queryString_rsRates);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsRates + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsRates + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

?>
<html>
<head>
<title>Rates</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>
<form name="form1" method="post" action="">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">RATE</font></p></div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Rate Name:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" id="cmdAdd" value="Add New" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="13%"><strong style="color:#678197;">Rate Name</strong></td>
				  <td align="center" width="10%"><strong style="color:#678197;">Hour Start</strong></td>
                  <td align="center" valign="middle" width="9%"><strong style="color:#678197;">Hour End</strong></td>
				  <td align="center" valign="middle" width="25%"><strong style="color:#678197;">Days of Week</strong></td>
				  <td align="center" valign="middle" width="9%"><strong style="color:#678197;">Duration</strong></td>
				  <td align="center" valign="middle" width="10%"><strong style="color:#678197;">Display</strong></td>
				  <td align="center" valign="middle" width="9%"><strong style="color:#678197;">Action</strong></td>
                </tr></thead>
                <?php if ($totalRows_rsRates > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php do { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsRates['rate_id']; ?>"></td>
                  <td align="left"><?php echo $row_rsRates['rate_name']; ?></td>
				  <td align="center"><?php echo $row_rsRates['hour_start'];?></td>
                  <td align="center"><?php echo $row_rsRates['hour_end'];?></td>
				  <td align="center" ><?php echo $row_rsRates['dow'];?></td>
				  <td align="center"><?php echo $row_rsRates['duration'];?></td>
				  <td align="center"><?php echo $row_rsRates['display'];?></td>
				  <td align="center"><p><img src="../../../img/application_edit.png" align="bottom">&nbsp;<a href="rates_edit.php?id=<?php echo $row_rsRates['rate_id']; ?>">Edit</a></p></td>
                </tr>
                <?php } while ($row_rsRates = mysql_fetch_assoc($rsRates)); ?>
                <?php } // Show if recordset not empty ?>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsRates > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsRates > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsRates=%d%s", $currentPage, 0, $queryString_rsRates); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsRates=%d%s", $currentPage, max(0, $pageNum_rsRates - 1), $queryString_rsRates); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsRates) {
    printf('<a href="'."%s?pageNum_rsRates=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsRates.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsRates < $totalPages_rsRates) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsRates=%d%s", $currentPage, min($totalPages_rsRates, $pageNum_rsRates + 1), $queryString_rsRates); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsRates=%d%s", $currentPage, $totalPages_rsRates, $queryString_rsRates); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsRates == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>