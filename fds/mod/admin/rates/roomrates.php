<?php
require "../../../ajax/config/config.inc.php";
require_once('ratefns.php');

if(!isset($_GET['id'])) {
	$mesg = 'Please select a rate.<br>';
	$mesg .= '<a href="rates.php">Back to Rates</a>';
}

$rateid = $_GET['id'];
$rate_data = getRecord($rateid);

$cmd = $_GET['cmd'];

if ($cmd == 'rates') {
	header('location: rates.php');
} elseif ($cmd == 'room types') {
	header('location: ../mastertable/room_type.php');
} elseif ($cmd == 'activate') {
	if( is_array($_GET['rtr_id'])) {
		$rtrid = implode(',',$_GET['rtr_id']); 
	} else {
		$rtrid = $_GET['rtr_id'];
	}
	$sql = "update room_type_rates set active=1 where rtr_id in ($rtrid)";
	R::exec($sql);
	header('location: roomrates.php?id=' . $_GET['new_rate_id']);
} elseif ($cmd == 'deactivate') {
	if( is_array($_GET['rtr_id'])) {
		$rtrid = implode(',',$_GET['rtr_id']); 
	} else {
		$rtrid = $_GET['rtr_id'];
	}
	$sql = "update room_type_rates set active=0 where rtr_id in ($rtrid)";
	R::exec($sql);
	header('location: roomrates.php?id=' . $_GET['new_rate_id']);
} elseif ($cmd =='update') {
	$fields = '';
	foreach($_GET as $name => $value) {
		if( strpos($name, 'new_') !== false ) {
			$fieldName = str_replace('new_', '', $name);
			$fields .= ($fields=='') ? '' : ',';
			$fields .= " $fieldName='$value' ";
		}
	}
	$rtr_id= $_GET['new_rtr_id'];
	$sql = "update room_type_rates set $fields where rtr_id='$rtr_id'";
	R::exec($sql);
	header('location: roomrates.php?id=' . $_GET['new_rate_id']);
} elseif ($cmd =='save') {
	$fields = '';
	foreach($_GET as $name => $value) {
		if( strpos($name, 'new_') !== false && $name != 'new_rtr_id') {
			$fieldName = str_replace('new_', '', $name);
			$fields .= ($fields=='') ? '' : ',';
			$fields .= $fieldName;
			$values .= ($values == '') ? '' : ',';
			$values .= "'$value'";
		}
	}
	$sql = "insert into room_type_rates ($fields) values ($values ) ";
	R::exec($sql);
	header('location: roomrates.php?id=' . $_GET['new_rate_id']);
}elseif ($cmd =='delete') {
	if (isset($_GET['rtr_id'])) {
		if( is_array($_GET['rtr_id'])) {
			$rateid = implode(',',$_GET['rtr_id']); 
		} else {
			$rateid = $_GET['rtr_id'];
		}
		$sql = "delete from room_type_rates where rtr_id in ($rateid)";
		if (!R::exec($sql)) {
			$mesg = "Error encountered: ";
			$mesg.= "<br><a href='roomrates.php?id=".$_GET['new_rate_id']."'>Back</a>";
			die($mesg);
		}
		header('location: roomrates.php?id=' . $_GET['new_rate_id']);
	} else {
		$mesg = 'Please select at least one item.';
		$mesg.= "<br><a href='roomrates.php?id=".$_GET['new_rate_id']."'>Back</a>";
		die($mesg);
	}
}



?>
<script src="jquery.js"></script>
<link  type="text/css" rel="stylesheet" href="style.css" />
<form method="get" action="roomrates.php">
<?php 
if ($cmd == 'new') {
	echo buildButtons(array('save', 'cancel', 'rates', 'room types'));
	echo "<h2>New Rate per Room Type</h2>";
	echo frmRoomTypeRates($_GET);
} elseif ($cmd == 'edit') {
	echo buildButtons(array('update', 'cancel'));
	echo "<h2>Edit Rate per Room Type</h2>";
	if( is_array($_GET['rtr_id'])) {
		$rtrid = implode(',',$_GET['rtr_id']); 
	} else {
		$rtrid = $_GET['rtr_id'][0];
	}
	echo frmRoomTypeRates(getRtrRecord($rtrid));
} else {
	echo buildButtons(array('new', 'edit', 'delete', 'activate', 'deactivate', 'rates'));
	echo "<h2>Rates per Room Type</h2>";
	echo "Selected Rate: ". ddRates($rateid);
	echo rtrTable(array('rate_id'=> $rateid) );
	if ($_GET['rtid']) {
		echo getRooms($_GET['rtid']);
	}
}
?>
</form>
<!--//
javascript section
 //-->
<script>
$(document).ready(function(){
	$('#cbmaster').click(function(){
		$('.cbitem').attr('checked', $(this).attr('checked'));
	});
	$('#new_rate_id').change(function(){
		document.location.href = 'roomrates.php?id=' + $(this).val();
	});
	$('#delete').click(function(){
		return confirm('Delete selected item(s)?');
	});
});
</script>