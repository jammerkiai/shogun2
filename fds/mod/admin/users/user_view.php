<?php
require "../../../ajax/config/config.inc.php";

if (isset($_GET['id']) && $_GET['id'] != "") {
	$userId = trim($_GET['id']);
	$row_rsUser = R::getRow("SELECT * FROM users WHERE user_id = ".$userId);
	
	$rsGroup = R::getAll("SELECT * FROM groups ");
}
else {
	header("Location:users.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>View User</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
	<tr>
		<td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top"><td colspan="2" align="center" class="bgHeader"><strong>VIEW USER</strong></td></tr>      
                <tr align="left" valign="top">
                  <td align="left" width="15%">User Name  </td>
                  <td><?=$row_rsUser['username']?></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Fullname</td>
                  <td><?=$row_rsUser['fullname']?>
				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Group</td>
                  <td>
				  		<?php 
							foreach ($rsGroup as $row_rsGroup) {
								if ($row_rsGroup['group_id'] == $row_rsUser['group_id']) echo $row_rsGroup['group_name'];
						}?>
				  </td>
                </tr>
 
                <tr align="left" valign="top">
                  <td>&nbsp;<input type="hidden" name="user_id" value="<?=trim($_GET['id'])?>"/></td>
                  <td align="right"><input name="edit" type="button" id="Edit" value="Edit" class="buttons" onClick="MM_goToURL('parent','../users/user_edit.php?id=<?=trim($_GET['id'])?>');return document.MM_returnValue" />
                  &nbsp;<input name="Back" type="submit" id="Back" onClick="MM_goToURL('parent','../users/users.php');return document.MM_returnValue" value="Back" class="buttons" />				                </td>
                </tr>
              </table>
		</td>
	</tr>
</table>
</body>
</html>
