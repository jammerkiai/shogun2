<?php
require "../../../ajax/config/config.inc.php";

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] == 'Add New' ) {
	header('Location: user_add.php') ;
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: user_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (username LIKE '".addslashes($_POST['search'])."%' OR username LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsUsers = 10;
$pageNum_rsUsers = 0;
if (isset($_GET['pageNum_rsUsers'])) {
  $pageNum_rsUsers = $_GET['pageNum_rsUsers'];
}
$startRow_rsUsers = $pageNum_rsUsers * $maxRows_rsUsers;
$param_rsUsers = " WHERE 1=1 ".$param_search;

//$query_rsUsers = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsUsers, $sortDate);
$query_rsUsers = sprintf("select * from users %s ", $param_rsUsers);
$query_limit_rsUsers = sprintf("%s LIMIT %d, %d", $query_rsUsers, $startRow_rsUsers, $maxRows_rsUsers);
$rsUsers = R::getAll($query_limit_rsUsers);

if (isset($_GET['totalRows_rsUsers'])) {
  $totalRows_rsUsers = $_GET['totalRows_rsUsers'];
} else {
  $all_rsUsers = R::getAll($query_rsUsers);
  $totalRows_rsUsers = count($all_rsUsers);
}
$totalPages_rsUsers = ceil($totalRows_rsUsers/$maxRows_rsUsers)-1;

$queryString_rsUsers = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsUsers") == false && 
        stristr($param, "totalRows_rsUsers") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsUsers = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsUsers = sprintf("&totalRows_rsUsers=%d%s", $totalRows_rsUsers, $queryString_rsUsers);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsUsers + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsUsers + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);


if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] == 'Add New' ) {
	header('Location: user_add.php') ;
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: user_delete.php?id='.$ids) ;
	}
}

$group = array();
$rsGroup = R::getAll("SELECT * FROM groups");
foreach ($rsGroup as $row_rsGroup) {
	$group[$row_rsGroup['group_id']] = $row_rsGroup['group_name'];
}

?>
<html>
<head>
<title>Users</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>
<form name="form1" method="post" action="">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if ( isset($_REQUEST['strMsg']) && ($_REQUEST['strMsg'] != '')) { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">USERS</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Username:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" id="cmdAdd" value="Add New" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="20%"><strong style="color:#678197;">Username</strong></td>
				  <td align="center" width="35%"><strong style="color:#678197;">Fullname</strong></td>
                  <td align="center" valign="middle" width="20%"><strong style="color:#678197;">Group</strong></td>
				  <td align="center" valign="middle" width="20%"><strong style="color:#678197;">Action</strong></td>
                </tr></thead>
                <?php if ($totalRows_rsUsers > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php foreach ($rsUsers as $row_rsUsers) { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsUsers['user_id']; ?>"></td>
                  <td align="left"><a href="../users/user_edit.php?id=<?php echo $row_rsUsers['user_id']; ?>"><?php echo $row_rsUsers['username']; ?></a></td>
				  <td align="center"><?php echo $row_rsUsers['fullname']; ?></td>
                  <td align="center" valign="top">
				  	<?php
						foreach($group as $keyGroup  => $valueGroup) {
							if ($row_rsUsers['group_id'] == $keyGroup) echo $valueGroup;
						}
					?>
				  </td>
				  <td align="center"><p><img src="../../../img/application_edit.png" align="bottom">&nbsp;<a href="../users/user_edit.php?id=<?php echo $row_rsUsers['user_id']; ?>">Edit</a></p></td>
                </tr>
                <?php } ; ?>
                <?php } // Show if recordset not empty ?>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsUsers > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsUsers > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsUsers=%d%s", $currentPage, 0, $queryString_rsUsers); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsUsers=%d%s", $currentPage, max(0, $pageNum_rsUsers - 1), $queryString_rsUsers); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsUsers) {
    printf('<a href="'."%s?pageNum_rsUsers=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsUsers.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsUsers < $totalPages_rsUsers) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsUsers=%d%s", $currentPage, min($totalPages_rsUsers, $pageNum_rsUsers + 1), $queryString_rsUsers); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsUsers=%d%s", $currentPage, $totalPages_rsUsers, $queryString_rsUsers); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsUsers == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>