<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($_GET['id']) && $_GET['id'] != "") {
	$FnbId = trim($_GET['id']);
	$row_rsFnb = R::getRow("SELECT * FROM fnb WHERE fnb_id = ".$FnbId);
	
	$rsFoodCat = R::getAll("SELECT * FROM food_categories ORDER BY food_category_name");
	$rownum_rsFoodCat = count($rsFoodCat);
}
else {
	header("Location:fnb.php");
}

if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {
	$qry = "SELECT fnb_id FROM fnb WHERE (fnb_name = '".trim($_POST['fnb_name'])."' OR fnb_code = '".trim($_POST['fnb_code'])."') AND (fnb_name != '".trim($_POST['old_fnb_name'])."' AND fnb_code != '".trim($_POST['old_fnb_code'])."')";
	//echo $qry;die();
	$rsFnbExist = R::getAll($qry);
	$rownum_rsFnbExist = count($rsFnbExist);
	if ($rownum_rsFnbExist == 0) {
 		$updateSQL = sprintf("UPDATE fnb SET fnb_name=%s, fnb_code=%s, 
      food_category_id=%s, fnb_price=%s, active=%s
      WHERE fnb_id=%s",
                       GetSQLValueString($_POST['fnb_name'], "text"),
					   GetSQLValueString($_POST['fnb_code'], "text"),
					   GetSQLValueString($_POST['food_category'], "int"),
					   GetSQLValueString($_POST['price'], "double"),
             GetSQLValueString($_POST['active'], "int"),
					   GetSQLValueString($_POST['fnb_id'], "int"));

  $Result1 = R::exec($updateSQL);

  $insertGoTo = "fnb.php?strMsg=Record has been successfully updated.";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
 }
 else echo "<script>alert('* FNB name/code already exists.')</script>";
} 
elseif ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:fnb.php");
}# end update validate

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit FNB</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="post" name="fnb_edit" >
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
	<tr>
		<td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top"><td colspan="2" align="center" class="bgHeader"><strong>EDIT FNB</strong></td></tr>
				
				<tr align="left" valign="top">
                  <td align="left" width="15%">FNB Name</td>
                  <td><input name="fnb_name" type="text" id="fnb_name" class="textbox-style" value="<?=$row_rsFnb['fnb_name']?>" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">FNB Code</td>
                  <td><input name="fnb_code" type="text" id="fnb_code" class="textbox-style" maxlength="10" onBlur="this.value=this.value.toUpperCase()" value="<?=$row_rsFnb['fnb_code']?>" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Food Category</td>
                  <td><select name="food_category" id="food_category" style="width:200px;" class="textbox-style">
				  		<?php 
							if ($rownum_rsFoodCat > 0) {
							foreach($rsFoodCat as $row_rsFoodCat) {
								if ($row_rsFoodCat['food_category_id'] == $row_rsFnb['food_category_id']) $selected_food_category = "selected";
								else $selected_food_category = "";
						?>
				  		<option value="<?=$row_rsFoodCat['food_category_id']?>" <?=$selected_food_category?> ><?=$row_rsFoodCat['food_category_name']?></option>
						<?php } }?>
						</select>				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Price</td>
                  <td><input name="price" type="text" class="textbox-style" id="price" style="width:300px;" value="<?=$row_rsFnb['fnb_price']?>" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Active</td>
                  <td><input name="active" type="checkbox" class="textbox-style" id="active" value="1" <?php if ($row_rsFnb['active'] == 1) echo 'checked' ?> /></td>
                </tr>
                <tr align="left" valign="top">
                  <td>&nbsp;<input type="hidden" name="fnb_id" value="<?=trim($_GET['id'])?>"/><input type="hidden" name="old_fnb_name" value="<?=trim($row_rsFnb['fnb_name'])?>"/><input type="hidden" name="old_fnb_code" value="<?=trim($row_rsFnb['fnb_code'])?>"/></td>
                  <td align="right"><input name="save" type="submit" class="buttons" id="Save" onClick="YY_checkform('fnb_edit','fnb_name','#q','0','Field FNB name is required.','fnb_code','#q','0','Field FNB Code is required.');return document.MM_returnValue" value="Save" />&nbsp;<input name="back" type="submit" id="back" value="Back" class="buttons" />				                </td>
                </tr>
          </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
