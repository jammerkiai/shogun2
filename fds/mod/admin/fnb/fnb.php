<?php
require "../../../ajax/config/config.inc.php";

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] == 'Add New' ) {
	header('Location: fnb_add.php') ;
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: fnb_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (fnb_name LIKE '".addslashes($_POST['search'])."%' OR fnb_name LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsFnb = 10;
$pageNum_rsFnb = 0;
if (isset($_GET['pageNum_rsFnb'])) {
  $pageNum_rsFnb = $_GET['pageNum_rsFnb'];
}
$startRow_rsFnb = $pageNum_rsFnb * $maxRows_rsFnb;
$param_rsFnb = " WHERE 1=1 ".$param_search;

//$query_rsFnb = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsFnb, $sortDate);
$query_rsFnb = sprintf("select * from fnb %s ", $param_rsFnb);
$query_limit_rsFnb = sprintf("%s LIMIT %d, %d", $query_rsFnb, $startRow_rsFnb, $maxRows_rsFnb);
$rsFnb = R::getAll($query_limit_rsFnb);

if (isset($_GET['totalRows_rsFnb'])) {
  $totalRows_rsFnb = $_GET['totalRows_rsFnb'];
} else {
  $all_rsFnb = R::getAll($query_rsFnb);
  $totalRows_rsFnb = count($all_rsFnb);
}
$totalPages_rsFnb = ceil($totalRows_rsFnb/$maxRows_rsFnb)-1;

$queryString_rsFnb = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsFnb") == false && 
        stristr($param, "totalRows_rsFnb") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsFnb = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsFnb = sprintf("&totalRows_rsFnb=%d%s", $totalRows_rsFnb, $queryString_rsFnb);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsFnb + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsFnb + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);


$foodCat = array();
$rsFoodCat = R::getAll("SELECT * FROM food_categories ORDER BY food_category_name");
$rownum_rsFoodCat = count($rsFoodCat);
if ($rownum_rsFoodCat >0) {
	foreach($rsFoodCat as $row_rsFoodCat) {
		$foodCat[$row_rsFoodCat['food_category_id']] = $row_rsFoodCat['food_category_name'];
	}
}
?>
<html>
<head>
<title>FNB</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>
<form name="form1" method="post" action="">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">FNB</font></p>
              </div><div style="float:left; width:70%;">
                <p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">FNB Name:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" id="cmdAdd" value="Add New" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
				  <td align="left" width="35%"><strong style="color:#678197;">FNB Name</strong></td>
                  <td align="center" valign="middle" width="13%"><strong style="color:#678197;">FNB Code</strong></td>
				  <td align="center" valign="middle" width="25%"><strong style="color:#678197;">Category</strong></td>
				  <td align="center" valign="middle" width="12%"><strong style="color:#678197;">Price</strong></td>
          <td align="center" valign="middle" width="12%"><strong style="color:#678197;">Active</strong></td>
				  <td align="center" valign="middle" width="20%"><strong style="color:#678197;">Action</strong></td>
                </tr></thead>
                <?php if ($totalRows_rsFnb > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php foreach ($rsFnb as $row_rsFnb) { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsFnb['fnb_id']; ?>"></td>
				  <td align="left"><?php echo $row_rsFnb['fnb_name'];?></td>
                  <td align="center" valign="top"><?php echo $row_rsFnb['fnb_code'];?>
				  </td>
				  <td align="center" valign="top"><?php
				  	foreach($foodCat as $key => $value) {
						if ($key == $row_rsFnb['food_category_id']) echo $value;
					}
				  ?></td>
				  <td align="center"><?=$row_rsFnb['fnb_price']?></td>
          <td align="center"><?=$row_rsFnb['active']?></td>
				  <td align="center"><p><img src="../../../img/application_edit.png" align="bottom">&nbsp;<a href="../fnb/fnb_edit.php?id=<?php echo $row_rsFnb['fnb_id']; ?>">Edit</a></p></td>
                </tr>
                <?php } ; ?>
                <?php } // Show if recordset not empty ?>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsFnb > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsFnb > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsFnb=%d%s", $currentPage, 0, $queryString_rsFnb); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsFnb=%d%s", $currentPage, max(0, $pageNum_rsFnb - 1), $queryString_rsFnb); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsFnb) {
    printf('<a href="'."%s?pageNum_rsFnb=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsFnb.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsFnb < $totalPages_rsFnb) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsFnb=%d%s", $currentPage, min($totalPages_rsFnb, $pageNum_rsFnb + 1), $queryString_rsFnb); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsFnb=%d%s", $currentPage, $totalPages_rsFnb, $queryString_rsFnb); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsFnb == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>