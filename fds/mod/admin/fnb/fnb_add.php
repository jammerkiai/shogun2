<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$error = "";
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {
	$rsFnbExist = R::getAll("SELECT fnb_id FROM fnb WHERE fnb_name = '".trim($_POST['fnb_name'])."' OR fnb_code = '".trim($_POST['fnb_code']) . "'");
  
	$rownum_rsFnbExist = count($rsFnbExist);
	if ($rownum_rsFnbExist == 0) {
	  $active = ($_POST['active']) ? $_POST['active'] : 0;
	  $insertSQL = sprintf("INSERT INTO fnb (fnb_name, fnb_code, food_category_id, fnb_price, active) VALUES (%s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['fnb_name'], "text"),
					   GetSQLValueString($_POST['fnb_code'], "text"),
					   GetSQLValueString($_POST['food_category'], "int"),
					   GetSQLValueString($_POST['price'], "double"),
             GetSQLValueString($active, "int"));
 	 $Result1 = R::exec($insertSQL);
 	 $insertGoTo = "fnb_add.php?strMsg=Record has been successfully added.";
	  if (isset($_SERVER['QUERY_STRING'])) {
 	   $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
 	   $insertGoTo .= $_SERVER['QUERY_STRING'];
 	 }
 	 header(sprintf("Location: %s", $insertGoTo));
  }
  else echo "<script>alert('* FNB name/code entry already Exists.')</script>";
}
elseif ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:fnb.php");
} # end insert validate

$rsFoodCat = R::getAll("SELECT * FROM food_categories ORDER BY food_category_name");
$rownum_rsFoodCat = count($rsFoodCat);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Add FNB</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="post" name="fnb_add" id="fnb_add" >
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
    <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
    <tr>
    	<td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
    </tr>
    <?php } ?>
	<tr>
	  <td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top"><td colspan="2" align="center" class="bgHeader"><strong>ADD FNB</strong></td></tr>      
                <tr align="left" valign="top">
                  <td align="left" width="15%">FNB Name</td>
                  <td><input name="fnb_name" type="text" id="fnb_name" class="textbox-style" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">FNB Code</td>
                  <td><input name="fnb_code" type="text" id="fnb_code" class="textbox-style" maxlength="10" onBlur="this.value=this.value.toUpperCase()" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Food Category</td>
                  <td><select name="food_category" id="food_category" style="width:200px;" class="textbox-style">
				  		<option value="">[Choose]</option>
				  		<?php 
							if ($rownum_rsFoodCat > 0) {
							foreach($rsFoodCat as $row_rsFoodCat) {
						?>
				  		<option value="<?=$row_rsFoodCat['food_category_id']?>"><?=$row_rsFoodCat['food_category_name']?></option>
						<?php } }?>
						</select>				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Price</td>
                  <td><input name="price" type="text" class="textbox-style" id="price" style="width:300px;" /></td>
                </tr>
        <tr align="left" valign="top">
                  <td align="left" width="15%">Active</td>
                  <td><input name="active" type="checkbox" class="textbox-style" id="active" value="1" checked/></td>
                </tr>
                <tr align="left" valign="top">
                  <td>&nbsp;</td>
                  <td align="right"><input name="save" type="submit" class="buttons" id="Save" onclick="YY_checkform('fnb_add','fnb_name','#q','0','Field FNB name is required.','fnb_code','#q','0','Field FNB Code is required.','price','#0_99999','1','Field Price is required and must be a number.','food_category','#q','1','Field Food Category is required.');return document.MM_returnValue" value="Save" />&nbsp;<input name="back" type="submit" id="Back" value="Back" class="buttons" />				                </td>
                </tr>
          </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
