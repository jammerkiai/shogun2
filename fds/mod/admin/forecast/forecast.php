<?php
/**
* forecast.php
*/
include_once("../../../ajax/config/config.inc.php");

$month= isset($_GET['m']) ? $_GET['m'] : date('m');
$year= isset($_GET['y']) ? $_GET['y'] : date('Y');

$arrOptions = array('month'=>$month,'year'=>$year);
$fore = new forecast($arrOptions);

if($_POST["submit"]=='save') {
	$fore->save();
}elseif($_POST["submit"]=='update'){
	$fore->update();
}

$fore->view();


class forecast
{
	public function __construct($options) 
	{
		foreach($options as $key=>$value) {
			$this->{$key}=$value;
		}
		if(!isset($this->month)) $this->month=date('m');
		if(!isset($this->year)) $this->year=date('Y');
		$this->sql = "select forecast_id, forecast_date, forecast_value 
			from forecast 
			where month(forecast_date)='$this->month' 
			and year(forecast_date)='$this->year'
			";
			
		$this->res=R::getAll($this->sql);
	}
	
	public function view()
	{
		$newdate = date('m/d/Y');
		$ret  = '<form method="post" id="fcform" name="fcform">';
		$ret.=  '<h1>Forecast for: ' . $this->monthdrop($this->month) 
			. ' ' . $this->yeardrop($this->year) .'</h1>';
		$ret .= '<div class="workpanel">';
		$ret .= '<label>Enter new values:</label>
				<input type="text" name="newdate" id="newdate" value="'.$newdate.'" />
				<input type="text" name="newvalue" id="newvalue"  />
				<input type="submit" name="submit" value="save"  />
				<input type="submit" name="submit" value="update"  />
				';
		$ret .= '<hr />';
		$ret .= '<table>';
		$ret .='<th></th><th>Date</th><th>Forecast</th>';
		foreach($this->res as $r) {
			$id = $r['forecast_id'];
			$date = $r['forecast_date'];
			$value = $r['forecast_value'];
			$outDate = date('l, d M Y', strtotime($date));
			$ret.='<tr>';
			$ret.="<td><input type='checkbox' name='cbox[]' value='$id' id='cb_$id' /></td>";
			$ret.="<td>$outDate</td>";
			$ret.="<td><input type='text' 
				name='forecast_$id' 
				id='forecast_$id' 
				value='$value' class='valinput' /></td>";
			$ret.='</tr>';
		}
		$this->getnextdate($date);
		$ret .= '</table></div></form>';
		echo $ret;
	}
	
	public function getnextdate($date) 
	{
		$this->nextdate = date('Y-m-d', strtotime("$date +1 day"));
	}
	
	public function save()
	{
		$date = date('Y-m-d', strtotime($_POST['newdate']));
		$value = $_POST['newvalue'];
		$sql = "insert into forecast (forecast_date,forecast_value)
			value('$date','$value');
		";
		$res = R::exec($sql);
		$month = date('m',strtotime($date));
		$year = date('Y',strtotime($date));
		header("location:forecast.php?m=$month&y=$year");
	}
	
	public function update()
	{
		//print_r($_POST);
		
		if(is_array($_POST['cbox'])) {
			foreach($_POST['cbox'] as $id) {
				$newvalue = $_POST['forecast_' . $id];
				$sql = " update forecast set forecast_value='$newvalue' 
						where forecast_id='$id'
					";
				R::exec($sql) or die($sql);
			}
		}
		header('location:forecast.php');
		header('location:forecast.php?m=' . $_POST['selectedmonth'] . '&y=' .$_POST['selectedyear'] );
	}
	
	public function yeardrop($sel=0)
	{
		$startyear = 2010;
		if($sel==0) $sel=date('Y');
		$ret = '<select name="selectedyear" id="selectedyear" class="dateselect">';
		for($x=0; $x < 5; $x++) {
			$yr = $startyear + $x;
			$ret .= "<option value='$yr' ";
			if($sel==$yr) $ret.=' selected ';
			$ret .= ">$yr</option>";
		}
		$ret .= '</select>';
		return $ret;
	}
	
	public function monthdrop($sel=0)
	{
		if($sel==0) $sel=date('m');
		$arrMonth = array(
				'01'=>'January',
				'02'=>'Febuary',
				'03'=>'March',
				'04'=>'April',
				'05'=>'May',
				'06'=>'June',
				'07'=>'July',
				'08'=>'August',
				'09'=>'September',
				'10'=>'October',
				'11'=>'November',
				'12'=>'December',
			);
		$ret = '<select name="selectedmonth" id="selectedmonth" class="dateselect">';
		foreach($arrMonth as $index => $name) {
			$ret .= "<option value='$index' ";
			if($sel==$index) $ret.=' selected ';
			$ret .= ">$name</option>";
		}
		$ret .= '</select>';
		return $ret;
	}
}
?>
<link type="text/css" href="css/cupertino/jquery-ui-1.8.custom.css" rel="stylesheet" />	
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.custom.min.js"></script>

<script language='javascript'>
$(document).ready(function(){
	$('#newdate').datepicker();
	$('.dateselect').change(function(){
		document.location.href='forecast.php?m=' + $('#selectedmonth').val() + '&y=' + $('#selectedyear').val();
	});
	$('.valinput').change(function(){
		$(this).addClass('changed');
		name = $(this).attr('id');
		indx = name.split('_');
		$('#cb_' + indx[1]).attr('checked','checked');
	});
});
</script>
<style>
body {
	margin:0px;
	marginwidht:0px;
	marginheight:0px;
}
h1 {
	font-size:14px;
	border:1px solid #dddddd;
	background-color:#ccccff;
	padding:4px;
}
.valinput {
	border:0px;
	background-color:#eeeeee;
	text-align:right;
}
.workpanel {
	padding:10px;
}

table {
	font-family:arial;
	font-size:12px;
}

td {
	border-bottom:1px dotted #cccccc;
}

.changed {
	border:1px dotted #ff0000;
}
</style>