<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] != '' ) {
	$rsSasCatExist = R::getAll("SELECT * FROM sas_category WHERE sas_cat_name = '".addslashes(trim($_POST['sas_category']))."'");
	$rownum_rsSasCatExists = count($rsSasCatExist);
	if ($rownum_rsSasCatExists == 0) {
		$insertSQL = sprintf("INSERT INTO sas_category (sas_cat_name) VALUES (%s)",GetSQLValueString($_POST['sas_category'], "text"));
		$Result1 = R::exec($insertSQL);

	} else {
		echo "<script>alert('Sas Category already Exists.')</script>";
	}
}
if ( isset ( $_POST['cmdUpdate']) && $_POST['cmdUpdate'] != '' ) {
	$sasCat_array = array();
	$sasCat_array = explode("|",$_POST['sas_cat_array']);
	foreach($sasCat_array as $key => $value) {
		if (isset($_POST['sas_cat_'.$value]) && $_POST['sas_cat_'.$value] != "") { 
			$updateSQL = sprintf("UPDATE sas_category SET sas_cat_name=%s WHERE sas_cat_id=%s",
				GetSQLValueString($_POST['sas_cat_'.$value], "text"), GetSQLValueString($value, "int"));
  			$Result2 = R::exec($updateSQL);
		}
	}
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: sas_category_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (sas_cat_name LIKE '".addslashes($_POST['search'])."%' OR sas_cat_name LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsSasCat = 10;
$pageNum_rsSasCat = 0;
if (isset($_GET['pageNum_rsSasCat'])) {
  $pageNum_rsSasCat = $_GET['pageNum_rsSasCat'];
}
$startRow_rsSasCat = $pageNum_rsSasCat * $maxRows_rsSasCat;
$param_rsSasCat = " WHERE 1=1 ".$param_search;

//$query_rsSasCat = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsSasCat, $sortDate);
$query_rsSasCat = sprintf("select * from sas_category %s ", $param_rsSasCat);
$query_limit_rsSasCat = sprintf("%s LIMIT %d, %d", $query_rsSasCat, $startRow_rsSasCat, $maxRows_rsSasCat);
$rsSasCat = R::getAll($query_limit_rsSasCat) ;

if (isset($_GET['totalRows_rsSasCat'])) {
  $totalRows_rsSasCat = $_GET['totalRows_rsSasCat'];
} else {
  $all_rsSasCat = R::getAll($query_rsSasCat);
  $totalRows_rsSasCat = count($all_rsSasCat);
}
$totalPages_rsSasCat = ceil($totalRows_rsSasCat/$maxRows_rsSasCat)-1;

$queryString_rsSasCat = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsSasCat") == false && 
        stristr($param, "totalRows_rsSasCat") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsSasCat = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsSasCat = sprintf("&totalRows_rsSasCat=%d%s", $totalRows_rsSasCat, $queryString_rsSasCat);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsSasCat + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsSasCat + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

?>
<html>
<head>
<title>SAS Category</title>
<script type="text/javascript" src="../../../js/custom.js"></script>

<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>

<form name="form1" method="post" action="sas_category.php">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">SAS Category</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">SAS Category:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" class="buttons" id="cmdAdd" style="width:70px;" onClick="YY_checkform('form1','sas_category','#q','0','Field SAS Category is required.');return document.MM_returnValue" value="Add New" />&nbsp;<input name="cmdUpdate" type="submit" id="cmdUpdate" value="Update" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="3%"><strong style="color:#678197;">ID</strong></td>
				  <td align="left" width="90%"><strong style="color:#678197;">SAS Category</strong></td>
                </tr></thead>
                <?php
					$sasCatArray = ""; 
					if ($totalRows_rsSasCat > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php foreach ($rsSasCat as $row_rsSasCat) { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
					$sasCatArray .= $row_rsSasCat['sas_cat_id']."|";
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsSasCat['sas_cat_id']; ?>"></td>
                  <td align="left"><?php echo $row_rsSasCat['sas_cat_id']; ?></td>
				  <td align="left"><input type="text" name="sas_cat_<?=$row_rsSasCat['sas_cat_id']?>" value="<?php echo $row_rsSasCat['sas_cat_name']; ?>" class="textbox-style"></td>
                </tr>
                <?php } ; ?>
                <?php } // Show if recordset not empty ?>
				<tr><td>&nbsp;</td><td><input type="hidden" name="sas_cat_array" value="<?=substr($sasCatArray,0,-1)?>">&nbsp;</td><td><input name="sas_category" id="sas_category" type="text" class="textbox-style" ><script>document.getElementById('sas_category').focus()</script></td></tr>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsSasCat > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsSasCat > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsSasCat=%d%s", $currentPage, 0, $queryString_rsSasCat); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsSasCat=%d%s", $currentPage, max(0, $pageNum_rsSasCat - 1), $queryString_rsSasCat); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsSasCat) {
    printf('<a href="'."%s?pageNum_rsSasCat=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsSasCat.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsSasCat < $totalPages_rsSasCat) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsSasCat=%d%s", $currentPage, min($totalPages_rsSasCat, $pageNum_rsSasCat + 1), $queryString_rsSasCat); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsSasCat=%d%s", $currentPage, $totalPages_rsSasCat, $queryString_rsSasCat); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsSasCat == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>