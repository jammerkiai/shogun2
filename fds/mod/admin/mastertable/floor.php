<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] != '' ) {
	$rsFloorExist = R::getAll("SELECT * FROM floors WHERE floor_label = '".addslashes(trim($_POST['floor_label']))."'");
	$rownum_rsFloorExists = count($rsFloorExist);
	if ($rownum_rsFloorExists == 0) {
		$insertSQL = sprintf("INSERT INTO floors (floor_label, site_id) VALUES (%s, %s)",GetSQLValueString($_POST['floor_label'], "text"),GetSQLValueString($_POST['site_id'], "int"));
		$Result1 = R::exec($insertSQL);
	} 
	else {
		echo "<script>alert('Floor number already Exists.')</script>";
	}
}
if ( isset ( $_POST['cmdUpdate']) && $_POST['cmdUpdate'] != '' ) {
	$floor_array = array();
	$floor_array = explode("|",$_POST['floor_array']);
	foreach($floor_array as $key => $value) {
		if (isset($_POST['floor_label_'.$value]) && $_POST['floor_label_'.$value] != "") { 
			$updateSQL = sprintf("UPDATE floors SET floor_label=%s, site_id=%s WHERE floor_id=%s",
				GetSQLValueString($_POST['floor_label_'.$value], "text"), GetSQLValueString($_POST['site_id_'.$value], "text"), GetSQLValueString($value, "int"));
  			$Result2 = R::exec($updateSQL);
		}
	}
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: floor_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (floor_label LIKE '".addslashes($_POST['search'])."%' OR floor_label LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsFloor = 10;
$pageNum_rsFloor = 0;
if (isset($_GET['pageNum_rsFloor'])) {
  $pageNum_rsFloor = $_GET['pageNum_rsFloor'];
}
$startRow_rsFloor = $pageNum_rsFloor * $maxRows_rsFloor;
$param_rsFloor = " WHERE 1=1 ".$param_search;

//$query_rsFloor = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsFloor, $sortDate);
$query_rsFloor = sprintf("select * from floors %s ", $param_rsFloor);
$query_limit_rsFloor = sprintf("%s LIMIT %d, %d", $query_rsFloor, $startRow_rsFloor, $maxRows_rsFloor);
$rsFloor = R::getAll($query_limit_rsFloor);

if (isset($_GET['totalRows_rsFloor'])) {
  $totalRows_rsFloor = $_GET['totalRows_rsFloor'];
} else {
  $all_rsFloor = R::getAll($query_rsFloor);
  $totalRows_rsFloor = count($all_rsFloor);
}
$totalPages_rsFloor = ceil($totalRows_rsFloor/$maxRows_rsFloor)-1;

$queryString_rsFloor = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsFloor") == false && 
        stristr($param, "totalRows_rsFloor") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsFloor = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsFloor = sprintf("&totalRows_rsFloor=%d%s", $totalRows_rsFloor, $queryString_rsFloor);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsFloor + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsFloor + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

$rsSite = R::getAll("SELECT site_id, site_name FROM sites ORDER BY site_name");
$rownum_rsSite = count($rsSite);
if ($rownum_rsSite > 0) { 
	$site = array();
	foreach($rsSite as $row_rsSite) {
		$site[][$row_rsSite['site_id']] = $row_rsSite['site_name'];
	}
}

?>
<html>
<head>
<title>Floor</title>
<script type="text/javascript" src="../../../js/custom.js"></script>

<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>

<form name="form1" method="post" action="floor.php">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">FLOOR</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Floor:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" class="buttons" id="cmdAdd" style="width:70px;" onClick="YY_checkform('form1','floor_label','#q','0','Field Floor name/label is required.');return document.MM_returnValue" value="Add New" />&nbsp;<input name="cmdUpdate" type="submit" id="cmdUpdate" value="Update" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="3%"><strong style="color:#678197;">ID</strong></td>
				  <td align="left" width="50%"><strong style="color:#678197;">Floor #</strong></td>
				  <td align="left" width="42%"><strong style="color:#678197;">Site Name</strong></td>
                </tr></thead>
                <?php
					$floorArray = ""; 
					if ($totalRows_rsFloor > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php foreach ($rsFloor as $row_rsFloor) { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
					$floorArray .= $row_rsFloor['floor_id']."|";
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsFloor['floor_id']; ?>"></td>
                  <td align="left"><?php echo $row_rsFloor['floor_id']; ?></td>
				  <td align="left"><input type="text" name="floor_label_<?=$row_rsFloor['floor_id']?>" value="<?php echo $row_rsFloor['floor_label']; ?>" class="textbox-style"></td>
				  <td align="left"><select name="site_id_<?=$row_rsFloor['floor_id']?>" id="site_id_<?=$row_rsFloor['floor_id']?>" class="textbox-style">
				  	<?php
						if ($rownum_rsSite > 0) {
							 foreach($site as $keySite => $valueSite) {
							 	foreach($valueSite as $key => $value) {
									if ($row_rsFloor['site_id']==$key) echo '<option value="'.$key.'" selected>'.$value.'</option>';
									else echo '<option value="'.$key.'">'.$value.'</option>';
								}
							}
						}
					?>
				  </select></td>
                </tr>
                <?php } ; ?>
                <?php } // Show if recordset not empty ?>
				<tr><td>&nbsp;</td><td><input type="hidden" name="floor_array" value="<?=substr($floorArray,0,-1)?>">&nbsp;</td><td><input name="floor_label" id="floor_label" type="text" class="textbox-style" ><script>document.getElementById('floor_label').focus()</script></td><td><select name="site_id" id="site_id" class="textbox-style">
					<?php
						foreach($site as $keySite => $valueSite) {
						 	foreach($valueSite as $key => $value) {
								echo '<option value="'.$key.'">'.$value.'</option>';
							}
						}
					?>
				</select></td></tr>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsFloor > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsFloor > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsFloor=%d%s", $currentPage, 0, $queryString_rsFloor); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsFloor=%d%s", $currentPage, max(0, $pageNum_rsFloor - 1), $queryString_rsFloor); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsFloor) {
    printf('<a href="'."%s?pageNum_rsFloor=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsFloor.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsFloor < $totalPages_rsFloor) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsFloor=%d%s", $currentPage, min($totalPages_rsFloor, $pageNum_rsFloor + 1), $queryString_rsFloor); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsFloor=%d%s", $currentPage, $totalPages_rsFloor, $queryString_rsFloor); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsFloor == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>