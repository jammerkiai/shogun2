<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] != '' ) {
	$rsGroupExist = R::getAll("SELECT * FROM groups WHERE group_name = '".addslashes(trim($_POST['group']))."'");
	$rownum_rsGroupExists = coun($rsGroupExist);
	if ($rownum_rsGroupExists == 0) {
		$insertSQL = sprintf("INSERT INTO groups (group_name,group_desc) VALUES (%s,%s)",GetSQLValueString($_POST['group'], "text"),GetSQLValueString($_POST['group_desc'], "text"));
		$Result1 = R::exec($insertSQL);
	} 
	else {
		echo "<script>alert('Group Name already Exists.')</script>";
	}
}
if ( isset ( $_POST['cmdUpdate']) && $_POST['cmdUpdate'] != '' ) {
	$group_array = array();
	$group_array = explode("|",$_POST['group_array']);
	foreach($group_array as $key => $value) {
		if (isset($_POST['group_'.$value]) && $_POST['group_'.$value] != "" && isset($_POST['group_desc_'.$value]) && $_POST['group_desc_'.$value] != "") { 
			$updateSQL = sprintf("UPDATE groups SET group_name=%s, group_desc=%s WHERE group_id=%s",
				GetSQLValueString($_POST['group_'.$value], "text"), GetSQLValueString($_POST['group_desc_'.$value], "text"), GetSQLValueString($value, "int"));
  			$Result2 = R::exec($updateSQL);
		}
	}
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: group_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (group_name LIKE '".addslashes($_POST['search'])."%' OR group_name LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsGroup = 10;
$pageNum_rsGroup = 0;
if (isset($_GET['pageNum_rsGroup'])) {
  $pageNum_rsGroup = $_GET['pageNum_rsGroup'];
}
$startRow_rsGroup = $pageNum_rsGroup * $maxRows_rsGroup;
$param_rsGroup = " WHERE 1=1 ".$param_search;

//$query_rsGroup = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsGroup, $sortDate);
$query_rsGroup = sprintf("select * from groups %s ", $param_rsGroup);
$query_limit_rsGroup = sprintf("%s LIMIT %d, %d", $query_rsGroup, $startRow_rsGroup, $maxRows_rsGroup);
$rsGroup = R::getAll($query_limit_rsGroup) ;

if (isset($_GET['totalRows_rsGroup'])) {
  $totalRows_rsGroup = $_GET['totalRows_rsGroup'];
} else {
  $all_rsGroup = R::getAll($query_rsGroup);
  $totalRows_rsGroup = count($all_rsGroup);
}
$totalPages_rsGroup = ceil($totalRows_rsGroup/$maxRows_rsGroup)-1;

$queryString_rsGroup = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsGroup") == false && 
        stristr($param, "totalRows_rsGroup") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsGroup = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsGroup = sprintf("&totalRows_rsGroup=%d%s", $totalRows_rsGroup, $queryString_rsGroup);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsGroup + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsGroup + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

?>
<html>
<head>
<title>Group</title>
<script type="text/javascript" src="../../../js/custom.js"></script>

<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>

<form name="form1" method="post" action="group.php">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">GROUP</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Group:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" class="buttons" id="cmdAdd" style="width:70px;" onClick="YY_checkform('form1','group','#q','0','Field Group is required.','group_desc','#q','0','Field Group Desc is required.');return document.MM_returnValue" value="Add New" />&nbsp;<input name="cmdUpdate" type="submit" id="cmdUpdate" value="Update" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="3%"><strong style="color:#678197;">ID</strong></td>
				  <td align="left" width="40%"><strong style="color:#678197;">Group Name</strong></td>
				  <td align="left" width="57%"><strong style="color:#678197;">Group Desc</strong></td>
                </tr></thead>
                <?php
					$groupArray = ""; 
					if ($totalRows_rsGroup > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php  foreach ($rsGroup as $row_rsGroup) { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
					$groupArray .= $row_rsGroup['group_id']."|";
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsGroup['group_id']; ?>"></td>
                  <td align="left"><?php echo $row_rsGroup['group_id']; ?></td>
				  <td align="left"><input type="text" name="group_<?=$row_rsGroup['group_id']?>" value="<?php echo $row_rsGroup['group_name']; ?>" class="textbox-style"></td>
				  <td align="left"><input type="text" name="group_desc_<?=$row_rsGroup['group_id']?>" value="<?php echo ($row_rsGroup['group_desc'] ? $row_rsGroup['group_desc'] : "none"); ?>" class="textbox-style"></td>
                </tr>
                <?php } ?>
                <?php } // Show if recordset not empty ?>
				<tr><td>&nbsp;</td><td><input type="hidden" name="group_array" value="<?=substr($groupArray,0,-1)?>">&nbsp;</td><td><input name="group" id="group" type="text" class="textbox-style" ><script>document.getElementById('group').focus()</script></td><td><input name="group_desc" id="group_desc" type="text" class="textbox-style" value="none" ></td></tr>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsGroup > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsGroup > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsGroup=%d%s", $currentPage, 0, $queryString_rsGroup); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsGroup=%d%s", $currentPage, max(0, $pageNum_rsGroup - 1), $queryString_rsGroup); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsGroup) {
    printf('<a href="'."%s?pageNum_rsGroup=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsGroup.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsGroup < $totalPages_rsGroup) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsGroup=%d%s", $currentPage, min($totalPages_rsGroup, $pageNum_rsGroup + 1), $queryString_rsGroup); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsGroup=%d%s", $currentPage, $totalPages_rsGroup, $queryString_rsGroup); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsGroup == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>