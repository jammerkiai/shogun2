<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] != '' ) {
	$rsRoomTypeExist = R::getALL("SELECT * FROM room_types WHERE room_type_name = '".addslashes(trim($_POST['room_type_name']))."'");
	$rownum_rsRoomTypeExists = count($rsRoomTypeExist);
	if ($rownum_rsRoomTypeExists == 0) {
		$insertSQL = sprintf("INSERT INTO room_types (room_type_name) VALUES (%s)",GetSQLValueString($_POST['room_type_name'], "text"));
		$Result1 = R::exec($insertSQL) ;
	} 
	else {
		echo "<script>alert('Room Type Name already Exists.')</script>";
	}
}
if ( isset ( $_POST['cmdUpdate']) && $_POST['cmdUpdate'] != '' ) {
	$room_type_array = array();
	$room_type_array = explode("|",$_POST['room_type_array']);
	foreach($room_type_array as $key => $value) {
		if (isset($_POST['room_type_name_'.$value]) && $_POST['room_type_name_'.$value] != "") { 
			$updateSQL = sprintf("UPDATE room_types SET room_type_name=%s WHERE room_type_id=%s",
				GetSQLValueString($_POST['room_type_name_'.$value], "text"), GetSQLValueString($value, "int"));
  			$Result2 = R::exec($updateSQL) ;
		}
	}
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: room_type_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (room_type_name LIKE '".addslashes($_POST['search'])."%' OR room_type_name LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsRoomType = 10;
$pageNum_rsRoomType = 0;
if (isset($_GET['pageNum_rsRoomType'])) {
  $pageNum_rsRoomType = $_GET['pageNum_rsRoomType'];
}
$startRow_rsRoomType = $pageNum_rsRoomType * $maxRows_rsRoomType;
$param_rsRoomType = " WHERE 1=1 ".$param_search;

//$query_rsRoomType = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsRoomType, $sortDate);
$query_rsRoomType = sprintf("select * from room_types %s ", $param_rsRoomType);
$query_limit_rsRoomType = sprintf("%s LIMIT %d, %d", $query_rsRoomType, $startRow_rsRoomType, $maxRows_rsRoomType);
$rsRoomType = R::getAll($query_limit_rsRoomType) ;

if (isset($_GET['totalRows_rsRoomType'])) {
  $totalRows_rsRoomType = $_GET['totalRows_rsRoomType'];
} else {
  $all_rsRoomType = R::getAll($query_rsRoomType);
  $totalRows_rsRoomType = count($all_rsRoomType);
}
$totalPages_rsRoomType = ceil($totalRows_rsRoomType/$maxRows_rsRoomType)-1;

$queryString_rsRoomType = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsRoomType") == false && 
        stristr($param, "totalRows_rsRoomType") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsRoomType = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsRoomType = sprintf("&totalRows_rsRoomType=%d%s", $totalRows_rsRoomType, $queryString_rsRoomType);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsRoomType + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsRoomType + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

?>
<html>
<head>
<title>Room Type</title>
<script type="text/javascript" src="../../../js/custom.js"></script>

<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>

<form name="form1" method="post" action="room_type.php">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">ROOM TYPE</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Room Type:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" class="buttons" id="cmdAdd" style="width:70px;" onClick="YY_checkform('form1','room_type_name','#q','0','Field Room Type Name is required.');return document.MM_returnValue" value="Add New" />&nbsp;<input name="cmdUpdate" type="submit" id="cmdUpdate" value="Update" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="3%"><strong style="color:#678197;">ID</strong></td>
				  <td align="left" width="90%"><strong style="color:#678197;">Room Type Name</strong></td>
                </tr></thead>
                <?php
					$roomTypeArray = ""; 
					if ($totalRows_rsRoomType > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php foreach ($rsRoomType as $row_rsRoomType ) { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
					$roomTypeArray .= $row_rsRoomType['room_type_id']."|";
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsRoomType['room_type_id']; ?>"></td>
                  <td align="left"><?php echo $row_rsRoomType['room_type_id']; ?></td>
				  <td align="left"><input type="text" name="room_type_name_<?=$row_rsRoomType['room_type_id']?>" value="<?php echo $row_rsRoomType['room_type_name']; ?>" class="textbox-style"></td>
                </tr>
                <?php } ; ?>
                <?php } // Show if recordset not empty ?>
				<tr><td>&nbsp;</td><td><input type="hidden" name="room_type_array" value="<?=substr($roomTypeArray,0,-1)?>">&nbsp;</td><td><input name="room_type_name" id="room_type_name" type="text" class="textbox-style" ><script>document.getElementById('room_type_name').focus()</script></td></tr>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsRoomType > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsRoomType > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsRoomType=%d%s", $currentPage, 0, $queryString_rsRoomType); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsRoomType=%d%s", $currentPage, max(0, $pageNum_rsRoomType - 1), $queryString_rsRoomType); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsRoomType) {
    printf('<a href="'."%s?pageNum_rsRoomType=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsRoomType.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsRoomType < $totalPages_rsRoomType) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsRoomType=%d%s", $currentPage, min($totalPages_rsRoomType, $pageNum_rsRoomType + 1), $queryString_rsRoomType); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsRoomType=%d%s", $currentPage, $totalPages_rsRoomType, $queryString_rsRoomType); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsRoomType == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>