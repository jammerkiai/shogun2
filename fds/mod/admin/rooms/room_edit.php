<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($_GET['id']) && $_GET['id'] != "") {
	$roomId = trim($_GET['id']);
	$row_rsRoom = R::getRow("SELECT * FROM rooms WHERE room_id = ".$roomId);
	$rsSite = R::getAll("SELECT site_id, site_name FROM sites ORDER BY site_name");
	$rsFloor = R::getAll("SELECT floor_id, floor_label FROM floors ORDER BY floor_label");
	$rsRoomType = R::getAll("SELECT * FROM room_types ORDER BY room_type_name");
	$rsTheme = R::getAll("SELECT * FROM themes ORDER BY theme_name");
	$rsStatus = R::getAll("SELECT * FROM statuses ORDER BY status_name");
}
else {
	header("Location:rooms.php");
}


if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {
	$rsRoomExist = R::getAll("SELECT door_name FROM rooms WHERE door_name = '".trim($_POST['door_name'])."' AND room_id != ".trim($_POST['room_id']));
	$rownum_rsRoomExists = count($rsRoomExist);
	if ($rownum_rsRoomExists == 0) {
  $updateSQL = sprintf("UPDATE rooms SET door_name=%s, site_id=%s, floor_id=%s, room_type_id=%s, theme_id=%s, ui_top=%s, ui_left=%s, ui_width=%s, ui_height=%s, status=%s, last_update=NOW(), update_by=%s WHERE room_id=%s",
                       GetSQLValueString($_POST['door_name'], "text"),
					   GetSQLValueString($_POST['sites'], "int"),
					   GetSQLValueString($_POST['floor'], "int"),
					   GetSQLValueString($_POST['room_type'], "int"),
					   GetSQLValueString($_POST['theme'], "int"),
					   GetSQLValueString($_POST['ui_top'], "text"),
					   GetSQLValueString($_POST['ui_left'], "text"),
					   GetSQLValueString($_POST['ui_width'], "text"),
					   GetSQLValueString($_POST['ui_height'], "text"),
                       GetSQLValueString($_POST['status'], "int"),
					   GetSQLValueString("1", "int"),
					   GetSQLValueString($_POST['room_id'], "int"));

  $Result1 = R::exec($updateSQL);

  $insertGoTo = "rooms.php?strMsg=Record has been successfully updated.";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
 }
 else echo "<script>alert('* Door Name already Exists.')</script>";
} 
elseif ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:rooms.php");
}# end update validate

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit Room</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="post" name="room_edit" >
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
	<tr>
		<td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top"><td colspan="2" align="center" class="bgHeader"><strong>EDIT ROOM</strong></td></tr>      
                <tr align="left" valign="top">
                  <td align="left" width="15%">Door Name  </td>
                  <td><input name="door_name" type="text" id="door_name" style="width:300px;" class="textbox-style" value="<?php echo $row_rsRoom['door_name']?>" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Sites</td>
                  <td>
				  		<?php 
							foreach ($rsSite as $row_rsSite) {
								if ($row_rsRoom['site_id'] == $row_rsSite['site_id']) $checked_site = "checked";
								else $checked_site = "";
						?>
				  		<input type="radio" name="sites" value="<?php echo $row_rsSite['site_id']?>" class="radio-style" <?php echo $checked_site?> ><?php echo $row_rsSite['site_name']?>&nbsp;
						<?php }?>				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Floor</td>
                  <td><select name="floor" id="floor" style="width:200px;" class="textbox-style">
				  		<?php 
							foreach ($rsFloor as $row_rsFloor) {
								if ($row_rsRoom['floor_id'] == $row_rsFloor['floor_id']) $selected_floor = "selected";
								else $selected_floor = "";
						?>
				  		<option value="<?php echo $row_rsFloor['floor_id']?>" <?php echo $selected_floor?>><?php echo $row_rsFloor['floor_label']?></option>
						<?php }?>
						</select>				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Room Type</td>
                  <td><select name="room_type" id="room_type" style="width:200px;" class="textbox-style">
				  		<?php 
							foreach ($rsRoomType as $row_rsRoomType) {
								if ($row_rsRoom['room_type_id'] == $row_rsRoomType['room_type_id']) $selected_room_type = "selected";
								else $selected_room_type = "";
						?>
				  		<option value="<?php echo $row_rsRoomType['room_type_id']?>" <?php echo $selected_room_type?>><?php echo $row_rsRoomType['room_type_name']?></option>
						<?php }?>
						</select>				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Theme</td>
                  <td><select name="theme" id="theme" style="width:200px;" class="textbox-style">
				  		<?php 
							foreach ($rsTheme as $row_rsTheme) {
								if ($row_rsRoom['theme_id'] == $row_rsTheme['theme_id']) $selected_theme = "selected";
								else $selected_theme = "";
						?>
				  		<option value="<?php echo $row_rsTheme['theme_id']?>" <?php echo $selected_theme?>><?php echo $row_rsTheme['theme_name']?></option>
						<?php }?>
						</select>				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Top</td>
                  <td><input name="ui_top" type="text" id="ui_top" style="width:300px;" class="textbox-style" value="<?php echo $row_rsRoom['ui_top']?>" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Left</td>
                  <td><input name="ui_left" type="text" id="ui_left" style="width:300px;" class="textbox-style" value="<?php echo $row_rsRoom['ui_left']?>" onkeypress="return isNumberKey(event)" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Width</td>
                  <td><input name="ui_width" type="text" id="ui_width" style="width:300px;" class="textbox-style" value="<?php echo $row_rsRoom['ui_width']?>" onkeypress="return isNumberKey(event)" /></td>
				</tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Height</td>
                  <td><input name="ui_height" type="text" id="ui_height" style="width:300px;" class="textbox-style" value="<?php echo $row_rsRoom['ui_height']?>" onkeypress="return isNumberKey(event)" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Status</td>
                  <td>
				  		<?php 
							foreach ($rsStatus as $row_rsStatus) {
								if ($row_rsRoom['status'] == $row_rsStatus['status_id']) $checked_status = "checked";
								else $checked_status = "";
						?>
				  		<input type="radio" name="status" value="<?php echo $row_rsStatus['status_id']?>" class="radio-style" <?php echo $checked_status?>><?php echo $row_rsStatus['status_name']?>&nbsp;
						<?php }?>				  </td>
                </tr>
 
                <tr align="left" valign="top">
                  <td>&nbsp;<input type="hidden" name="room_id" value="<?php echo trim($_GET['id'])?>"/></td>
                  <td align="right"><input name="save" type="submit" id="Save" value="Save" class="buttons" onClick="YY_checkform('room_edit','door_name','#q','0','Field Door Name is required.','ui_top','#1_999','1','Field Top is required and must be a number.','ui_left','#1_999','1','Field Left is required and must be a number.','ui_width','#1_999','1','Field Width is required and must be a number.','ui_height','#1_999','1','Field Height is required and must be a number.');return document.MM_returnValue" />
                  &nbsp;<input name="back" type="submit" id="Back" value="Back" class="buttons" />				                </td>
                </tr>
          </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
