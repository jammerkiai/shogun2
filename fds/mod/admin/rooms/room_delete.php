<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ((isset($_REQUEST['id'])) && ($_REQUEST['id'] != "")) {
	$deleteSQL = sprintf("DELETE FROM rooms WHERE room_id IN (%s)",GetSQLValueString($_REQUEST['id'], "text2"));
	$Result1 = R::exec($deleteSQL);
	$deleteGoTo = "rooms.php?strMsg=Record has been successfully deleted.";
	
	if (isset($_SERVER['QUERY_STRING'])) {
    	$deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
	    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  	}
 	header(sprintf("Location: %s", $deleteGoTo));
}
?>
