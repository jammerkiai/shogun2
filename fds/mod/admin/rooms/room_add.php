<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$error = "";
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {
	$rsRoomExist = R::getAll("SELECT door_name FROM rooms WHERE door_name = '".trim($_POST['door_name'])."'");
	$rownum_rsRoomExists = count($rsRoomExist);
	if ($rownum_rsRoomExists == 0) {
	  $insertSQL = sprintf("INSERT INTO rooms (door_name, site_id, floor_id, room_type_id, theme_id, ui_top, ui_left, ui_width, ui_height, status, last_update, update_by) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, NOW(), %s)",
                       GetSQLValueString($_POST['door_name'], "text"),
					   GetSQLValueString($_POST['sites'], "int"),
					   GetSQLValueString($_POST['floor'], "int"),
					   GetSQLValueString($_POST['room_type'], "int"),
					   GetSQLValueString($_POST['theme'], "int"),
					   GetSQLValueString($_POST['ui_top'], "text"),
					   GetSQLValueString($_POST['ui_left'], "text"),
					   GetSQLValueString($_POST['ui_width'], "text"),
					   GetSQLValueString($_POST['ui_height'], "text"),
                       GetSQLValueString($_POST['status'], "int"),
					   GetSQLValueString("1", "int"));

 	 $Result1 = R::exec($insertSQL);

 	 $insertGoTo = "room_add.php?strMsg=Record has been successfully added.";
	  if (isset($_SERVER['QUERY_STRING'])) {
 	   $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
 	   $insertGoTo .= $_SERVER['QUERY_STRING'];
 	 }
 	 header(sprintf("Location: %s", $insertGoTo));
  }
  else echo "<script>alert('* Door Name already Exists.')</script>";
} 
else if ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:rooms.php");
} # end insert validate

$rsSite = R::getAll("SELECT site_id, site_name FROM sites ORDER BY site_name");
$rsFloor = R::getAll("SELECT floor_id, floor_label FROM floors ORDER BY floor_label");
$rsRoomType = R::getAll("SELECT * FROM room_types ORDER BY room_type_name");
$rsTheme = R::getAll("SELECT * FROM themes ORDER BY theme_name");
$rsStatus = R::getAll("SELECT * FROM statuses ORDER BY status_name");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Add Room</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="post" name="room_add" id="room_add" >
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
    <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
    <tr>
    	<td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
    </tr>
    <?php } ?>
	<tr>
		<td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top"><td colspan="2" align="center" class="bgHeader"><strong>ADD ROOM</strong></td></tr>      
                <tr align="left" valign="top">
                  <td align="left" width="15%">Door Name  </td>
                  <td><input name="door_name" type="text" class="textbox-style" id="door_name" style="width:300px;" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Sites</td>
                  <td>
				  		<?php 
							$checked=0;
							foreach($rsSite as $row_rsSite) {
						?>
				  		<input name="sites" type="radio" class="radio-style" value="<?=$row_rsSite['site_id']?>" <?php if ($checked==0) echo "checked"; ?>>
				  		<?=$row_rsSite['site_name']?>&nbsp;
						<?php $checked++; }?></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Floor</td>
                  <td><select name="floor" id="floor" style="width:200px;" class="textbox-style">
				  		<option value="">[Choose]</option>
				  		<?php 
							foreach($rsFloor as $row_rsFloor) {
						?>
				  		<option value="<?=$row_rsFloor['floor_id']?>"><?=$row_rsFloor['floor_label']?></option>
						<?php }?>
						</select>				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Room Type</td>
                  <td><select name="room_type" id="room_type" style="width:200px;" class="textbox-style">
				  <option value="">[Choose]</option>
				  		<?php 
							foreach ($rsRoomType as $row_rsRoomType) {
						?>
				  		<option value="<?=$row_rsRoomType['room_type_id']?>"><?=$row_rsRoomType['room_type_name']?></option>
						<?php }?>
						</select>				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Theme</td>
                  <td><select name="theme" id="theme" style="width:200px;" class="textbox-style">
					  <option value="">[Choose]</option>
				  		<?php 
							foreach($rsTheme as $row_rsTheme) {
						?>
				  		<option value="<?=$row_rsTheme['theme_id']?>"><?=$row_rsTheme['theme_name']?></option>
						<?php }?>
						</select>				  </td>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Top</td>
                  <td><input name="ui_top" type="text" id="ui_top" style="width:300px;" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Left</td>
                  <td><input name="ui_left" type="text" id="ui_left" style="width:300px;" /></td>
                </tr>
				<tr>
					<td align="left" width="15%">Width</td>
                 	<td><input name="ui_width" type="text" id="ui_width" style="width:300px;" /></td>
				</tr>
				<tr>
					<td align="left" width="15%">Height</td>
                 	<td><input name="ui_height" type="text" id="ui_height" style="width:300px;" /></td>
				</tr>
				<tr align="left" valign="top">
                  <td align="left">Status</td>
                  <td>
				  		<?php
							$checked=0; 
							foreach($rsStatus as $row_rsStatus) {
						?>
				  		<input type="radio" name="status" value="<?=$row_rsStatus['status_id']?>" class="radio-style" <?php if ($checked==0) echo "checked"; ?> ><?=$row_rsStatus['status_name']?>&nbsp;
						<?php $checked++; }?>				  </td>
                </tr>
 
                <tr align="left" valign="top">
                  <td>&nbsp;</td>
                  <td align="right"><input name="save" type="submit" id="Save" value="Save" class="buttons" onClick="YY_checkform('room_add','door_name','#q','0','Field Door Name  is required.','ui_top','#1_999','1','Field Top is required and must be a number.','ui_left','#1_999','1','Field Left is required and must be a number.','ui_width','#1_999','1','Field Width is required and must be a number.','ui_height','#1_999','1','Field Height is required and must be a number.','floor','#q','1','Field Floor is required.','room_type','#q','1','Field Room Type is required.','theme','#q','1','Field Theme is required.');return document.MM_returnValue" />
                  &nbsp;<input name="back" type="submit" id="Back" value="Back" class="buttons" />				                </td>
                </tr>
          </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
