<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["edit"])) && ($_POST["edit"] == "Edit")) {
	header("Location:room_edit.php?id=".trim($_POST['room_id']));
} 
elseif ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:rooms.php");
}# end update validate

if (isset($_GET['id']) && $_GET['id'] != "") {
	$roomId = trim($_GET['id']);
	$row_rsRoom = R::getRow("SELECT * FROM rooms WHERE room_id = ".$roomId);
	$rsSite =  R::getAll("SELECT site_id, site_name FROM sites ORDER BY site_name");
	$rsFloor =  R::getAll("SELECT floor_id, floor_label FROM floors ORDER BY floor_label");
	$rsRoomType =  R::getAll("SELECT * FROM room_types ORDER BY room_type_name");
	$rsTheme =  R::getAll("SELECT * FROM themes ORDER BY theme_name");
	$rsStatus =  R::getAll("SELECT * FROM statuses ORDER BY status_name");
}
else {
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>View Room</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form name="room_add" method="post" onSubmit="return validate_room_form(this);">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
	<tr>
		<td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top"><td colspan="2" align="center" class="bgHeader"><strong>VIEW ROOM</strong></td></tr>      
                <tr align="left" valign="top">
                  <td align="left" width="15%">Door Name  </td>
                  <td><?=$row_rsRoom['door_name']?></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Sites</td>
                  <td>
				  		<?php 
							foreach($rsSite as $row_rsSite) {
								if ($row_rsRoom['site_id'] == $row_rsSite['site_id']) echo $row_rsSite['site_name'];
						}?>
						
				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Floor</td>
                  <td>
				  		<?php 
							foreach($rsFloor as $row_rsFloor) {
								if ($row_rsRoom['floor_id'] == $row_rsFloor['floor_id']) echo $row_rsFloor['floor_label'];
						}?>
				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Room Type</td>
                  <td>
				  		<?php 
							foreach ($rsRoomType as $row_rsRoomType) {
								if ($row_rsRoom['room_type_id'] == $row_rsRoomType['room_type_id']) echo $row_rsRoomType['room_type_name'];
						}?>
				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Theme</td>
                  <td>
				  		<?php 
							foreach ($rsTheme as $row_rsTheme) {
								if ($row_rsRoom['theme_id'] == $row_rsTheme['theme_id']) echo $row_rsTheme['theme_name'];
							}
						?>
				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Top</td>
                  <td><?php echo $row_rsRoom['ui_top']?></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Left</td>
                  <td><?php echo $row_rsRoom['ui_left']?></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Width</td>
                  <td><?php echo $row_rsRoom['ui_width']?></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Height</td>
                  <td><?php echo $row_rsRoom['ui_height']?></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Status</td>
                  <td>
				  		<?php 
							foreach ($rsStatus as $row_rsStatus) {
								if ($row_rsRoom['status'] == $row_rsStatus['status_id']) echo $row_rsStatus['status_name'];
						 }?>
				  </td>
                </tr>
 
                <tr align="left" valign="top">
                  <td>&nbsp;<input type="hidden" name="room_id" value="<?=trim($_GET['id'])?>"/></td>
                  <td align="right"><input name="edit" type="submit" id="Edit" value="Edit" class="buttons"  />
                  &nbsp;<input name="back" type="submit" id="Back" value="Back" class="buttons" />				                </td>
                </tr>
              </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
