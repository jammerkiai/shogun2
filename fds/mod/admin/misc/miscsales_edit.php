<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($_GET['id']) && $_GET['id'] != "") {
	$SasId = trim($_GET['id']);
	$row_rsSas = R::getRow("SELECT * FROM sales_and_services  WHERE sas_id = ".$SasId);
	
	$rsSasCat = R::getAll("SELECT * FROM sas_category ORDER BY sas_cat_name");
	$rownum_rsSasCat = count($rsSasCat);

  $rsGLcode = R::getAll("SELECT code, name FROM chart_of_accounts");
}
else {
	header("Location:miscsales.php");
}

if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {
	$qry = "SELECT Sas_id FROM  sales_and_services WHERE (sas_description = '".trim($_POST['sas_description'])."' OR sas_cat_name = '".trim($_POST['sas_cat_name'])."') AND (sas_description != '".trim($_POST['old_sas_description'])."' AND sas_cat_name != '".trim($_POST['old_sas_cat_name'])."')";
	//echo $qry;die();
	$rsSasExist = R::getAll($qry);
	$rownum_rsSasExist = count($rsSasExist);
	if ($rownum_rsSasExist == 0) {
 		$updateSQL = sprintf("UPDATE  sales_and_services SET sas_description=%s,  
            sas_cat_id=%s, sas_amount=%s , code=%s
            WHERE sas_id=%s",
                       GetSQLValueString($_POST['sas_description'], "text"),
					   GetSQLValueString($_POST['sas_cat_id'], "int"),
					   GetSQLValueString($_POST['price'], "double"),
             GetSQLValueString($_POST['code'], "text"),
					   GetSQLValueString($_POST['sas_id'], "int"));

  $Result1 = R::exec($updateSQL);

  $insertGoTo = "miscsales.php?strMsg=Record has been successfully updated.";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
 }
 else echo "<script>alert('* SAS Description/Category already exists.')</script>";
} 
elseif ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:miscsales.php");
}# end update validate

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit SAS</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="post" name="sas_edit" >
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
	<tr>
		<td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top"><td colspan="2" align="center" class="bgHeader"><strong>EDIT SAS</strong></td></tr>
				
				<tr align="left" valign="top">
                  <td align="left" width="15%">SAS Description</td>
                  <td><input name="sas_description" type="text" id="sas_description" class="textbox-style" value="<?php echo $row_rsSas['sas_description']?>" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">SAS Category</td>
                  <td><select name="sas_cat_id" id="sas_cat_id" style="width:200px;" class="textbox-style">
				  		<?php 
							if ($rownum_rsSasCat > 0) {
							foreach($rsSasCat as $row_rsSasCat) {
								if ($row_rsSasCat['sas_cat_id'] == $row_rsSas['sas_cat_id']) $selected_sas_cat_id = "selected";
								else $selected_sas_cat_id = "";
						?>
				  		<option value="<?php echo $row_rsSasCat['sas_cat_id']?>" <?php echo $selected_sas_cat_id?> ><?php echo $row_rsSasCat['sas_cat_name']?></option>
						<?php } }?>
						</select>				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Price</td>
                  <td><input name="price" type="text" class="textbox-style" id="price" style="width:300px;" value="<?php echo $row_rsSas['sas_amount']?>" /></td>
                </tr>
				<tr align="left" valign="top">
              <td align="left">GL Code</td>
              <td><select name="code" id="code" style="width:200px;" class="textbox-style">
              <option value="">[Choose]</option>
              <?php 
              if ($rownum_rsSasCat > 0) {
              foreach($rsGLcode as $row_rsGLcode) {
            ?>
              <option value="<?=$row_rsGLcode['code']?>" 
              <?php if ($row_rsSas['code'] == $row_rsGLcode['code']) echo " selected " ?>
              ><?=$row_rsGLcode['code'] . ' : ' . $row_rsGLcode['name']?></option>
            <?php } }?>
            </select>         
            </td>
        </tr>
                <tr align="left" valign="top">
                  <td>&nbsp;<input type="hidden" name="sas_id" value="<?php echo trim($_GET['id'])?>"/><input type="hidden" name="old_sas_description" value="<?php echo trim($row_rsSas['sas_description'])?>"/><input type="hidden" name="old_sas_cat_id" value="<?php echo trim($row_rsSas['sas_cat_id'])?>"/></td>
                  <td align="right"><input name="save" type="submit" class="buttons" id="Save" onClick="YY_checkform('Sas_edit','sas_description','#q','0','Field SAS name is required.','sas_cat_id','#q','0','Field SAS Code is required.','price','#0_99999','1','Field Price is required.');return document.MM_returnValue" value="Save" />&nbsp;<input name="back" type="submit" id="back" value="Back" class="buttons" />				                </td>
                </tr>
          </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>















