<?php

class CreditLine
{
    public function __construct()
    {
        //
        $this->grouping = time();
    }
    
    public function save($codepair, $details)
    {
        $this->_insert($codepair[0], 'dr', $details);
        $this->_insert($codepair[1], 'cr', $details);
    }
    
    protected function _insert($code, $type, $details)
    {
        $sql = " insert into creditline_transactions 
            (occupancy_id, transaction_date, post_date, post_shift, glcode, $type, reftable, refnum, remarks, grouping)
            values (
                '{$details['occupancy']}', 
                '{$details['tdate']}', 
                '{$details['pdate']}',
                '{$details['shift']}',
                '$code',  
                '{$details['amount']}', 
                '{$details['reftable']}', 
                '{$details['refnum']}', 
                '{$details['remarks']}',
                '{$this->grouping}'
            ) ";
            
//        mysql_query($sql) or die($sql. mysql_error());
    }
    
    public function postByRef($reftable, $refnum, $indexField)
    {
        if (is_array($refnum)) {
            $refnum = implode(',', $refnum);
        }
        
        //transfer all records with reftable and refnum to posted table       
        $sql = "";
        
    }
}
