<?php
session_start();
include_once("./config/config.inc.php");
include_once("./common.inc.php");
include_once("./class.room.php");


$floor = getSpecialFloorId();

$sql  ="select group_concat(a.occupancy_id) from occupancy a, rooms b where a.room_id=b.room_id and b.floor_id = $floor ";
$strOcc = R::getCol($sql)[0];


$sql = "select b.rate_name, count(a.occupancy_id) rcnt from occupancy a, rates b
where a.rate_id=b.rate_id and a.occupancy_id not in ($strOcc) and a.actual_checkout='00-00-00 00:00:00' group by a.rate_id ";

$rates = R::getAll($sql);

$summ="";
$arr=array('','3Hr','12Hr','24Hr');
$cl_arr=array('','#660066','#ff00ff','#660000');
$summ="<table  class='frontstats'  width='100'>";
$summ .= "<tr><td class='hdr' colspan=2>By Rate</td></tr>";
foreach($rates as $rc){
	$rid = $rc['rate_name'];
	$rcnt = $rc['rcnt'];
	$summ .= "<tr><td>$rid</td><td align='right'>$rcnt</td></tr>";
	$total+=$rcnt;
}
$summ.="<tr><td class='total'>Total</td><td class='total num'>$total</td></tr>";
$summ.="</table>";


$sql = "select b.status_name, count(a.room_id) as scnt from 
rooms a, statuses b where a.status=b.status_id and a.floor_id <> $floor group by a.status ";
$stat = R::getAll($sql);

$summ2="<table class='frontstats' width='100'>";
$summ2 .= "<tr><td class='hdr' colspan=2>By Status</td></tr>";
$arr = array(
	"Available"=>"img/flag_blue.png",
	"Occupied" =>"img/flag_green.png",
	"Cleaning" =>"img/flag_yellow.png",
	"Maintenance" =>"img/flag_red.png",
	"Make Up" =>"img/flag_pink.png",
	);
foreach($stat as $sc){
	$rid= $sc['status_name'];
	$rcnt = $sc['scnt'];
	$summ2 .= "<tr><td><img src='".$arr[$rid]."' border='0' />$rid</td><td class='num'>$rcnt</td></tr>";
	$total1+=$rcnt;
}
$summ2.="<tr><td class='total'>Total</td><td class='total num'>$total1</td></tr>";
$summ2.="</table>";


?>
<style>
table.frontstats{font-family:lucida,arial,helvetica;font-size:12px;font-weight:normal;}
table.frontstats td {border-bottom:1px dotted #eeeeee}
table.frontstats td.total {color:green;font-weight:bold;}
table.frontstats td.num {text-align:right;}
table.frontstats td.hdr {color:#0000ff;font-weight:bold;background-color:#FFCC33;padding-left:4px;}
</style>
<table width='100%'>
<tr><td valign=top><?=$summ?></td><td valign=top><?=$summ2?></td></tr>
</table>

