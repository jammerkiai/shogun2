<?php
//transfer.php
session_start();
include_once("config/config.inc.php");
include_once("common.inc.php");
include_once("config/lobby.inc.php");
include_once("class.room.php");

$p = $_POST;

$croomid = $_GET["roomid"];

$occupancy = R::getAssocRow("select * from occupancy where actual_checkout='0000-00-00 00:00:00' and room_id=?", [$croomid])[0];


if($p["cmd"]=='viewdifference'){
	
	echo getFormTransferDifference($p["croomid"],$p["nroomid"]);
	exit;
}elseif($p["cmd"]=='proceed'){	
		setTransferRoom($p["croomid"],$p["nroomid"],$p["remark"],$p["new_amount"], $p['recomputeDiscount'], $p['keepCurrentRate'], $p['newDiscountRate']);
		$rm = new room($p["nroomid"]);
		$f=$rm->floor_id - 1;
		echo json_encode(array('success' => true, 'floor' => $f, 'room' => $p["nroomid"]));
		//echo "{ message : \"<script>alert('successful transfer');parent.document.location.href='../index.php?f=$f&room=".$p["nroomid"]."'</script>\", error : '' }";	
		exit;

}

function checkIfUsernameIsOIC($username,$password){
	$sql = "select user_id from users where username = '$username'
		and userpass = '$password'";

	$cnt = R::getCol($sql)[0];

	return (!empty($cnt));
}

function setTransferRoom($current_room_id,$new_room_id,$newreason,$newamount,$recomputeDiscount,$keepCurrentRate, $discount_rate){

	$user = $_SESSION["hotel"]["userid"];

	//retrieve occupancy id
	$sql  = " select occupancy_id, discount_rate from occupancy where room_id='$current_room_id' and actual_checkout='0000-00-00 00:00:00' limit 1";
	$occupancy = R::getAssocRow($sql)[0];
	$occupancy_id = $occupancy['occupancy_id'];

	//update occupancy
	$now = date("Y-m-d H:i:s");
	$sql = " update occupancy set room_id='$new_room_id', discount_rate='$discount_rate', update_by='$user' where occupancy_id='$occupancy_id'";
	R::exec($sql)  or die($sql);

	//new occupancy_log record
	$sql  = " insert into occupancy_log (transaction_date, occupancy_id, update_by, remarks, transaction_type) 
		values ('$now', '$occupancy_id', '$user', '$newreason', 'Transfer')";
	R::exec($sql) or die($sql);

	//update rooms --> set  as make up
	$sql = " update rooms set status=5, last_update='$now', update_by='$user' where room_id='$current_room_id' ";
	R::exec($sql) or die($sql);

	//update new room - set status as occupied
	$sql = " update rooms set status=2, last_update='$now', update_by='$user' where room_id='$new_room_id' ";
	R::exec($sql) or die($sql);

	$start = new DateTime('now');

	if ($keepCurrentRate == 'true') {
		$start->add(new DateInterval('P1D'));
	}

	$next = $start->format('Y-m-d');

	//update room_sales to set the new room for the remainder of stay
	$sql = " select roomsales_id, unit_cost from room_sales 
			where date_format(sales_date, '%Y-%m-%d') = '$next'
			and occupancy_id=$occupancy_id
			and item_id=15
			order by roomsales_id desc
			limit 0,1";


	$roomsToEdit = R::getAssocRow($sql)[0];

	if (!empty($roomsToEdit)) {

		$rid = $roomsToEdit['roomsales_id'];
		$ucost = $roomsToEdit['unit_cost'];

		$sql = " update room_sales set room_id='$new_room_id'
		where occupancy_id='$occupancy_id' and category_id=3 and roomsales_id >= $rid ";

		R::exec($sql) or die($sql);

		$sql = " update room_sales
		set unit_cost='$newamount'
		where occupancy_id='$occupancy_id'
		and category_id='3' and item_id='15' and roomsales_id >= '$rid' ";
		R::exec($sql) or die($sql);

		if ($recomputeDiscount == 'true') {
			$newCost = -1 * $newamount * ($discount_rate / 100);
			$sql3 = "update room_sales set unit_cost='$newCost'
			where occupancy_id='$occupancy_id' 
			and category_id=3 and item_id=17 
			and roomsales_id >= '$rid' ";
			R::exec($sql3) or die($sql3);

		}
	}

}

function getFormDDLRemarks(){
	
	$ret = "<select name='new_remark' id='new_remark' width='100%'>";
	$ret .= "<option value=''></option>";
	$sql = "select remark_id,remark_text from remarks where remark_classification = '2'";

	$remarks = R::getAll($sql);
	foreach($remarks as $remark){
		$remark_id = $remark['remark_id'];
		$remark_text= $remark['remark_text'];
		$ret .= "<option value='$remark_id'>$remark_text</option>";
	}
	$ret .= "</select>";
	return $ret;
}


function getFormOIC()
{
	//$ret = "Supervisor/OIC:<br>";
	$ret .= "Username:";
	$sql = "select username, fullname from users where group_id=4";
	$oics = R::getAll($sql);
	$ret .= "<select name='new_user' id='new_user'>";
	$ret .= "<option ></option>";
	foreach($oics as $oic)
	{
		$ret .= "<option value='{$oic['username']}'>{$oic['fullname']}</option>";
	}
	$ret .= "</select>";
	
	$ret .= "<br>Password:";
	$ret .= '<input type="password" name="new_pass" id="new_pass"  />';
	return $ret;
}

function getAllAvailableRooms($croomid) {
	$sql = "select a.room_id, a.door_name, b.amount as amount, c.rate_name, c.rate_id
			from rooms a, room_type_rates b, rates c
			where a.room_id not in ($croomid)
			and a.status=1
			and a.room_type_id=b.room_type_id
			and b.rate_id=c.rate_id
			and b.active=1
			and c.duration=24"
			;

	$rooms = R::getAll($sql);

	$select = "<select name='new_room' id='new_room' placeholder='Select new room' class=''><option value=''></option>";
	
	foreach($rooms as $room) {
		$amount = number_format($room['amount'],2);
		$select .= "<option value='{$room['room_id']}-{$room['rate_id']}-{$room['amount']}' 
						alt='{$room['amount']}'>
						{$room['door_name']} Amount: $amount {$room['rate_name']}
					</option>";

	}
	$select.="</select>";
	return $select;

}

?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery.approver.css" />
<link rel="stylesheet" type="text/css" href="../css/selectize.css" />

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.approver.js"></script>
<script type="text/javascript" src="../js/select2/js/selectize.min.js"></script>
<style>
	#newDiscountRate,
	.roomselect {
		border: 1px solid #ccc;
		font-size: 14px;
	}

	.formtable {
		width: 90%;
	}

</style>
</head>
<body>
<form>
<fieldset id="changestatus">
<legend>Room Transfer</legend>
<table class="formtable">
<tr>
<td style="width: 20%">Select New Room: </td>
<td>
	<?php echo getAllAvailableRooms($croomid) ?>
</td>
</tr>

<tr>
<td >Reasons:</td>
<td>
<?php echo getFormDDLRemarks() ?>
</td>
</tr>
	<tr>
		<td></td>
		<td>
			<input type="checkbox" name="recomputeDiscount" id="recomputeDiscount" />
			<label for="recomputeDiscount">Recompute Discount</label>
			<span id="discountContainer" style="display:none">
				<br><input type="number" id="newDiscountRate" name="newDiscountRate" value="<?php echo $occupancy['discount_rate']?>" />%
			</span>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="checkbox" name="keepCurrentRate" id="keepCurrentRate" />
			<label for="keepCurrentRate">Keep rate on current day</label>

		</td>
	</tr>
<tr>
<td>&nbsp;</td>
<td><div id="divmessage"></div></td>
</tr>
</tr>
<tr>
<td colspan="2" align="right"><input type="submit" name="act" id="act" value="Proceed with transfer" style="font-size:15px">
</td>
</tr>
</table>

</form>

<script lang="javascript">
$(document).ready(function(){	

	$('#new_room').selectize({
    	sortField: 'text',
    	maxItems: 1
	});

	$('#new_remark').selectize({
    	sortField: 'text',
    	maxItems: 1
	});

	$('#recomputeDiscount').on('click', function(){
		$('#discountContainer').toggle();
	});

	$("#act").on('click',
	    function(e){
		    e.preventDefault();
			console.log('posting');
			var newroom = ($('#new_room').val()).split('-')[0];
			var amount = ($('#new_room').val()).split('-')[2];
		    $.post("transfer.php",{
					cmd:'proceed',
					croomid: <?=$croomid?>,
					nroomid: newroom,
				    new_amount: amount,
				    new_user:$("#new_user").val(),
					new_pass:$("#new_pass").val(),
					remark:$("#new_remark").val(),
				    newDiscountRate:$('#newDiscountRate').val(),
					recomputeDiscount: $('#recomputeDiscount').is(":checked"),
					keepCurrentRate: $('#keepCurrentRate').is(":checked") },
					function(resp){
						console.log(resp);
						if (resp.success == true) {
							alert('successful transfer');
							parent.document.location.href='../index.php?f=' + resp.floor + '&room=' + resp.room;
						}
						$("#diverror").html(resp.error);			
						$("#divmessage").html(resp.message);
						return false;		
					}, "json");
	    })
	    .approver({
			url: 'oiclist_json.php',
			approvaltype : 'Transfer',
			approvalinfo : $("#new_remark").val()
		});


});
</script>
</body>
</html>
