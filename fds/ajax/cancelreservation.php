<?php
//cancelreservation
header("access-control-allow-origin: *");
session_start();

include_once("config/config.inc.php");
include_once("currentcash.function.php");
include_once("printerfns.php");

$user = $_SESSION["hotel"]["userid"];

if (isset($_POST['code'])) {
	//simplify variable referencing;
	foreach ($_POST as $key => $val) {
		${$key} = $val;
	}

	$now = date('Y-m-d H:i:s');
	//update reservations 
	$sql = "update reservations set status='$reason', 
			date_updated = '$now'
			where reserve_code='$code'";
	$res = R::exec($sql);
			
	//update partner_transactions
	$sql = "insert into partner_transactions (transaction_date, 
		reserve_code, booking_number, partner_name, recievable, remarks, result_status) values (
		'$now', '$code', '$bookingnum', '$partner', '$penalty', '$remark', '$reason') ";
	$res = R::exec($sql);
	
	//update reserve_rooms
	$sql = "update reserve_rooms set status='Cancelled'
			where reserve_code='$code'";
	$res = R::exec($sql);
	
	//money matters here
	$sql = "select reserve_fee from reservations where reserve_code='$code'";
	$fee = R::getCell($sql);
	$claim = $fee - $penalty;
	
	//insert the penalty paid by guest
	$sql = "insert into reservation_transactions (transaction_date,reservation_code,amount_deposit, update_by, remarks, updated_at, created_at)
		values('$now','$code','$penalty', '$user', 'Penalty: $reason $remarks', '$now', '$now')";
	R::exec($sql);
	
	//insert the amount claimed by guest from deposit
	$sql = "insert into reservation_transactions (transaction_date,reservation_code,amount_claimed, update_by, remarks, updated_at, created_at)
		values('$now','$code','$claim', '$user', 'To be returned to guest', '$now', '$now')";
	R::exec($sql);
	
	
	$tenderType =  ($cardselect == 1) ? 'Card' : 'Cash';
	$thisitem = ($reason == 'No Show') ? 47 : 48;
	if (!$occupancy) {
		$occupancy = 37346;
	}
	if ($penalty > 0) {
	    $sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount,reference_id,update_by) 
			values ('$now', '$occupancy', '$tenderType', '$penalty','$code','$user')";
	    R::exec($sql);
	}
		
	if ($tenderType == 'Card') {   
		//insert card details
		$newsalesid = mysql_insert_id();
		$sql = " insert into card_payment_details (salesreceipt_id, card_type, approval_code, batch_number) 
					values ('$newsalesid','$cardtype','$approval','$batchnum')";
		R::exec($sql);
	} else {
	    if ($penalty > 0) {
	        setCurrentCash( $penalty, 'in', $user, $now);
	    }
	    if ($claim > 0) {
	        setCurrentCash( $claim, 'out', $user, $now);
	    }
	}

	//update roomsales 
	$sql = "insert into room_sales (sales_date, order_code, occupancy_id, category_id, item_id,  unit_cost, qty, update_by ,remarks, status, update_date )
				values ('$now', '$code', '$occupancy', '5' ,'$thisitem', '$penalty' ,'1' ,'$user','$remark', 'Paid', '$now');
		";
	R::exec($sql);
	
	$body = <<< REPORT
CANCEL RESERVATION

DATE: $now
RESERVE CODE: $code

REASON: $reason

PENALTY: $penalty
CLAIM: $claim
TENDERTYPE: $tendertype

BY: $user


REPORT;
	

	printreport($body);

	echo json_encode(['success' => true]);
	//echo "Cancellation completed.";
	// header('location: base.php?act=new&code=' . $code);
}


function printreport($body) {
	$retval=chr(hexdec('1B')).chr(hexdec('4D')).chr(1);
	$retval.= $body ."\n\n\n\n\n";
	$retval.=chr(hexdec('1D')).chr(hexdec('56')).chr(49);	
	$file = "cancel_{$id}.txt";
	$fp = fopen("reports/" .$file, "w");
	fwrite( $fp,$retval);
	fclose($fp);
	$prnt = new printerfns();
	$prnt->sendToPrinter('front', "reports/" . $file);
}

?>
