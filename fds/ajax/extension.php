<?php
/**
* extension.php
*/
session_start();
include_once("./config/config.inc.php");
include_once("common.inc.php");
include_once("./config/lobby.inc.php");

include_once("date.functions.php");
include_once("class.room.php");
include_once("reserve.functions.php");


if($_GET["act"]=="del") {
	$rid = $_GET["rid"];
	$occ = $_GET["occ"];
	$updateDate = $_GET["update"];


	$sql = "delete from room_sales where occupancy_id='$occ' and update_date='$updateDate' ";
	R::exec($sql);

	$sql = "update occupancy 
			set expected_checkout=(select max(rechit_date) from room_sales where occupancy_id=$occ)
			where occupancy_id=$occ";
	R::exec($sql);
}

function getCostByItemId($itemid) {
	$sql ="select sas_amount from sales_and_services where sas_id='$itemid'";
	return R::getCol($sql)[0];
}

// function doExtras($startdate,$now,$ordercode) {
// 	$occupancy = $_POST["hidden_occupancy"];
// 	$user = $_SESSION["hotel"]["userid"];
// 	$roomid=$_POST["room_id"];
// 	//discount
// 	if($_POST['discount']==17) {
		
// 		$cat = 3;
// 		$item = 17;
// 		$discamount = -1 * $_POST['newdiscount'];
// 		addRoomSalesItem($cat, $item, $qty, $amt, $now, $occupancy, $user, $roomid, $startdate, $ordercode);
// 	}

// 	//xbed
// 	if($_POST['xbed']==11) {
// 		//echo "xbed";
// 		$cat = 1;
// 		$item = 11;
// 		$qty = $_POST['newxbed'];
// 		$amt = $_POST['hidden_bed_amount'];
// 		addRoomSalesItem($cat, $item, $qty, $amt, $now, $occupancy, $user, $roomid, $startdate, $ordercode);
// 	}

// 	//xpax
// 	if($_POST['xpax']==23) {
// 		//echo "xpax";
// 		$cat = 1;
// 		$item = 23;
// 		$qty = $_POST['newxpax'];
// 		$amt = $_POST['hidden_pax_amount'];
// 		addRoomSalesItem($cat, $item, $qty, $amt, $now, $occupancy, $user, $roomid, $startdate, $ordercode);
// 	}


// }

// function doOriginals($startdate,$now,$ordercode) {
//     $occupancy = $_POST["hidden_occupancy"];
//     $user = $_SESSION["hotel"]["userid"];
//     $roomid=$_POST["room_id"];

//     //check for discount
//     $discount = R::getCell("select unit_cost from room_sales where occupancy_id=$occupancy and item_id=17");
//     if ($discount) {
//         $cat = 3;
//         $item = 17;
//         $qty = 1;
//         $amt = $discount;
//         addRoomSalesItem($cat, $item, $qty, $amt, $now, $occupancy, $user, $roomid, $startdate, $ordercode);
//     }


//     //check for commission
//     $commission = R::getCell("select unit_cost from room_sales where occupancy_id=$occupancy and item_id=120");
//     if ($commission) {
//         $cat = 3;
//         $item = 120;
//         $qty = 1;
//         $amt = $commission;
//         addRoomSalesItem($cat, $item, $qty, $amt, $now, $occupancy, $user, $roomid, $startdate, $ordercode);
//     }

//     //check for vat
//     $vat = R::getCell("select unit_cost from room_sales where occupancy_id=$occupancy and item_id=121");
//     if ($vat) {

//         $cat = 3;
//         $item = 121;
//         $qty = 1;
//         $amt = $vat;
//         addRoomSalesItem($cat, $item, $qty, $amt, $now, $occupancy, $user, $roomid, $startdate, $ordercode);
//     }

//     //check for $bpg
//     $bpg = R::getCell("select unit_cost from room_sales where occupancy_id=$occupancy and item_id=122");
//     if ($bpg) {

//         $cat = 3;
//         $item = 122;
//         $qty = 1;
//         $amt = $bpg;
//         addRoomSalesItem($cat, $item, $qty, $amt, $now, $occupancy, $user, $roomid, $startdate, $ordercode);
//     }

//     //check for wtax
//     $wtax = R::getCell("select unit_cost from room_sales where occupancy_id=$occupancy and item_id=123");
//     if ($wtax) {
//         $cat = 3;
//         $item = 123;
//         $qty = 1;
//         $amt = $wtax;
//         addRoomSalesItem($cat, $item, $qty, $amt, $now, $occupancy, $user, $roomid, $startdate, $ordercode);
//     }
// }

function addRoomSalesItem($cat, $item, $qty, $amt, $now, $occupancy, $user, $roomid, $startdate, $ordercode, $remarks='') {
	$sql  = " insert into room_sales( occupancy_id, charge_to, sales_date, category_id, item_id ,  unit_cost, qty,update_date,order_code,status,update_by,room_id, regflag, remarks, rechit_date) 
				values ('$occupancy', '$occupancy', '$now', $cat,$item,'$amt','$qty','$startdate','$ordercode','Draft','$user','$roomid', 0, 'Extension $remarks', date_add('$now', interval 1 day)) ";
        R::exec($sql);
}

if($_POST["cmdbtn"]=="Submit") {

	$ordercode = mktime();
	$duration_days=$_POST["new_day_extension"];
	$roomid=$_POST["room_id"];
	$occupancy = $_POST["hidden_occupancy"];
	$hidden_ot_amount = $_POST["hidden_ot_amount"];
	$hidden_ot_12_amount = $_POST["hidden_ot_12_amount"];
	$hidden_ot_day_amount = $_POST["hidden_ot_day_amount"];
	$remarks = $_POST['remarks'];
	
	$rm = new room($roomid);
	$rm->getoccupancy();
	$indate = new DateTime($rm->expected_checkout);
	$startdate = $indate->format("Y-m-d H:i:s");
	
	if(!$duration_days)$duration_days=0;
	$newcheckout = date_add($indate, new DateInterval("P".$duration_days."D"));
	$newcheckout = $newcheckout->format("Y-m-d H:i:s");

	$now = date("Y-m-d H:i:s");
	$user = $_SESSION["hotel"]["userid"];
	//step 1. update occupancy_id expected checkout
	$sql = "update occupancy set expected_checkout='$newcheckout',wakeup='0',isalerted='No' where occupancy_id='$occupancy'";
	R::exec($sql);

	//step 2. insert into logs 
	$sql = " insert into occupancy_log(transaction_date, occupancy_id, update_by, transaction_type ) value ('$startdate', '$occupancy', '$user', 'Extension' ) ";
	R::exec($sql);
	

	if($duration_days)
	{
        for($x=0; $x < $duration_days; $x++) {
            $newdate = date('Y-m-d H:i:s', strtotime("$startdate + $x days"));
            //add extension, ordercode should be zero
            addRoomSalesItem(3, 15, 1, $_POST['computedRate'], $newdate, $occupancy, $user, $roomid, $now, 0, $remarks);

            if ($_POST['computedDiscount'] != 0) {
            	$computedDiscount = abs($_POST['computedDiscount']) * -1;
            	addRoomSalesItem(3, 17, 1, $computedDiscount, $newdate, $occupancy, $user, $roomid, $now, 0, $remarks);
            }

            if ($_POST['computedCommission'] != 0) {
            	$computedCommission = abs($_POST['computedCommission']) * -1;
            	addRoomSalesItem(3, 121, 1, $computedCommission, $newdate, $occupancy, $user, $roomid, $now, 0, $remarks);
            }

            if ($_POST['computedBpg'] != 0) {
            	$computedBpg = abs($_POST['computedBpg'])  * -1;
            	addRoomSalesItem(3, 123, 1, $computedBpg, $newdate, $occupancy, $user, $roomid, $now, 0, $remarks);
            }

            if ($_POST['computedWtax'] != 0) {
            	$computedWtax = abs($_POST['computedWtax'])  * -1;
            	addRoomSalesItem(3, 124, 1, $computedWtax, $newdate, $occupancy, $user, $roomid, $now, 0, $remarks);
            }

            // if ($_POST['apply_original'] == 1) {
            //     doOriginals( $now, $newdate, $ordercode);
            // } else {
            //     doExtras($newdate, $now, $ordercode);
            // }
        }

	}

	header("location: extension.php?roomid=$roomid&msg=1");
}



$roomid=$_GET["roomid"];
$rm = new room($roomid);
$rm->getoccupancy();
$rm->getroomrates();
$rm->getroomdiscounts();

$ot_amount = $rm->getotamount();
$ot_amount12 = $rm->getExt12Amount();
$ot_amountday = $rm->getExtDayAmount();

$x_bed = getCostbyItemId(11);
$x_pax = getCostbyItemId(23);

$occupancy = $rm->occupancy_id;
if($_GET["msg"]==1) {
	$mesg = "<div style='color:#ff0000;'>Extension has been saved.</div>";
}

?>
<style>
body {font-size:.7em;}
legend.part{
	font-size:1.2em;
	font-weight:bold;
	border:1px solid #cccccc;
	background-color:#efefef;
	padding:2px;color:#9BD1E6;
}
.staytable {
	font-size:1em;
	border:1px solid #ffcccc;
	padding:4px;
}
div {
	margin:10px;
	border:1px solid #ffeeff;
}

div.form-group {
	position: relative;
	height: 20px;
	border: 0px;
	width: 240px;
}

div.form-group input,
div.form-group select {
	position: absolute;
	right:0;
	text-align: right;
	width: 150px;
}

div.choicediv {
	padding: 4px;
}



#availability { width: 300px;}
	.availability {border: 1px solid #ccc; padding:4px; font-size: 11px; margin: 1px; float:left;}
	.reserved {background-color: #f33; color: #fff;cursor:pointer; float:left;}
	.occupied {background-color: green; color: #fff; float:left;}
</style>
<form method="post" action="extension.php" name="myform">
<fieldset id="availability">
<legend>Availability</legend>
<?php
	echo getRoomAvailabilityForeCast($roomid, 27);
?>
</fieldset>
<fieldset>
<legend class="part">Set Extension </legend>
<div style="float:left">
<?php echo $mesg?>
<?php
	$sql = "select * from rooms where site_id = '2' and room_id = '$roomid'";
	$row = R::getRow($sql);
	if (!empty($row)) {
?>
    <div class="choicediv">
        <h3>Extend by: </h3>
        <span id='new_day_extension_span' class=''>
            <input type="text" name="new_day_extension" id="new_day_extension" value="0" size="4" class="computable" />
            Days
        </span>
    </div>

    <div class="choicediv">
        <h3>Apply original values?</h3>
        <input type="checkbox" value="1" name="apply_original" id="apply_original" /> Yes
    </div>

    <div class='choicediv'>
        <h3>Apply new values</h3>
        <div class="form-group">
	        <label>Cost per Day </label>
	        <select id="cost_per_day" name="cost_per_day" class="computable" >
	            <option value="0">Select rate</option>
	            <?php foreach($rm->rates as $rate): ?>
	                <option value="<?php echo $rate['amount'] ?>">
	                    <?php echo $rate['rate_name'] ?>   [ &#8369;<?php echo $rate['amount'] ?>  ]
	                </option>
	            <?php endforeach; ?>
	        </select>
        </div>
        <div class="form-group">
        	<label >Discount</label> 
        	<input type="text" name="newdiscount" id="newdiscount" value="0" size="4" class="computable"  />
        </div>
        <hr style="border:1px solid #eee;" />
        <div class="form-group">
	        <label for='rdb2'>Partner</label>

	        <?php $partners = R::getAll('select * from partners'); ?>

	        <select id="partner" name="partner" class="computable" >
	        	<option value=''>Select partner</option>
	        	<?php foreach ($partners as $p): ?>
	        		<option value='<?php echo $p['partner_id'] ?>' 
	        			data-commission='<?php echo $p['commission'] ?>'
	        			data-bpg='<?php echo $p['bpg'] ?>'>
	        			<?php echo $p['partner_name'] ?>
	        		</option>
	        	<?php endforeach; ?>
	        </select>
	        
        </div>
        

        <div class="form-group">
        	<label>Commission</label>
        	 <input type="text" name="newcommission" id="newcommission" value="0" size="4" class="computable"  />
       	</div>

       	<div class="form-group">
        	<label>BPG</label>
        	 <input type="text" name="newbpg" id="newbpg" value="0" size="4" class="computable" />
       	</div class="form-group">

       	<div class="form-group">
        	<label>Promo Discount</label>
        	 <input type="text" name="newpromo" id="newpromo" value="0" size="4" class="computable" />
       	</div>

       	<div class="form-group">
        	<label>VAT</label>
        	<input type="checkbox" name="vatex" id="vatex" value="1"  class="computable"  />
       	</div>

       	<div class="form-group">
        	<label>WTax</label>
        	 <input type="checkbox" name="wtax" id="wtax" value="1"  class="computable"  />
       	</div>
    </div>

<?php } ?>
</div>


<br />
<input type="submit" name="cmdbtn" value="Submit" style="width:60px;height:50px" 
onclick="return confirm('Submit Extension?  '+ myform.new_day_extension.value +' Days');"/>

<input type="hidden" name="room_id" value="<?=$roomid?>" />
<input type="hidden" name="hidden_ot_day_amount" id="hidden_ot_day_amount" value="<?=$ot_amountday?>" />
<input type="hidden" name="hidden_occupancy" value="<?=$occupancy?>" />
<input type="hidden" name="hidden_bed_amount" value="<?=$x_bed?>" />
<input type="hidden" name="hidden_pax_amount" value="<?=$x_pax?>" /></div>
<div style="float:left">
<?php echo $rm->getStaySummary(); ?>

<input type="hidden" id="origRate" value="<?php echo $rm->lastRoomRate ?>" />
<input type="hidden" id="origDiscount" value="<?php echo $rm->lastDiscount ?>" />
<input type="hidden" id="origCommission" value="<?php echo $rm->lastCommission ?>" />
<input type="hidden" id="origBPG" value="<?php echo $rm->lastBPG ?>" />
<input type="hidden" id="origWtax" value="<?php echo $rm->lastWtax ?>" />

	<div>
		<h3>Computed Rates</h3>
		<div class="form-group">
			<label>Rate</label>
			<input type="text" name="computedRate" id="computedRate" value="" />
		</div> 
		<div class="form-group">
			<label>Discount</label>
			<input type="text" name="computedDiscount" id="computedDiscount" value="" />
		</div> 
		<div class="form-group">
			<label>Commission</label>
			<input type="text" name="computedCommission" id="computedCommission" value="" />
		</div> 
		<div class="form-group">
			<label>BPG</label>
			<input type="text" name="computedBpg" id="computedBpg" value="" />
		</div> 
		<div class="form-group">
			<label>Wtax</label>
			<input type="text" name="computedWtax" id="computedWtax" value="" />
		</div> 

		<div class="form-group">
			<label>Amount Due</label>
			<input type="text" name="computedAmount" id="computedAmount" value="" />
		</div>
	</div>
	<h3>Remarks</h3>
	<textarea name="remarks" id="remarks" style="width:100%"></textarea>
</div>


<div style="clear:both"></div>
</fieldset>
</form>

<script type='text/javascript' src='../js/jquery.js'></script>
<script type='text/javascript' src='../js/jquery.plugin.min.js'></script>
<script type='text/javascript' src='../js/jquery.keypad.min.js'></script>
<link rel="stylesheet" type="text/css" href="../js/jquery.keypad.css" />
 <link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
 <script lang="javascript">

 	function recompute() {
 		var days = $('#new_day_extension').val();
		var partnerCommission = $('#newcommission').val();
		var promo_disc_rate = $('#newpromo').val() - 0;
		var partnerId = $('#partner').val();
		var bpg = $('#newbpg').val() - 0;

		var rate = $("#cost_per_day").val()  - 0;
		var disc = $("#newdiscount").val()  - 0;
		var disc_rate = $('#newdiscount').val() - 0;

		var commission = 0;
		var commissionableAmount = 0;
		var wtax = 0;
		var selected_rate = rate;

	
		if ($('#vatex').is(':checked')){
			rate = rate/1.12;
			xdisc = rate*disc_rate/100;
			wtax = (rate - xdisc) * 0.02;
		} else {
			wtax = 0;

			if ($('#wtax').is(':checked')) {	
				xdisc = selected_rate*disc_rate/100;
				baserate = selected_rate - xdisc;
				wtax = (baserate/1.12) * 0.02;
			} 
		}

		$('#computedRate').val(rate.toFixed(2));
		$('#computedWtax').val(wtax.toFixed(2));

		commissionableAmount =  rate - bpg;
		disc = commissionableAmount * disc_rate /  100;
		promo_disc = 0;

		if (partnerCommission > 0) {
			if (partnerId == 2) {
				
				disc = commissionableAmount * disc_rate /  100;
				commission = (commissionableAmount - disc) / 1.12 * (partnerCommission/100);
			} else if (partnerId == 3) {
				commissionableAmount =  rate;
				disc = commissionableAmount * disc_rate /  100;
				commission = 0; //commission for booking.com taken at end of month
			} else {
				
				disc = commissionableAmount * disc_rate /  100;
				commission = (rate - disc) * partnerCommission/100;
			}
		}

		if (promo_disc_rate > 0) {
			promo_disc = ((commissionableAmount - disc) * promo_disc_rate) / 100;

			commission = (commissionableAmount - disc - promo_disc) * partnerCommission/100;
		}

		disc = disc + promo_disc;

		var amountdue = rate - disc - commission - bpg - wtax;
		amountdue=amountdue * (days);

		$('#computedAmount').val(amountdue.toFixed(2));
		$('#computedDiscount').val(disc.toFixed(2));
		$('#computedCommission').val(commission.toFixed(2));
		$('#computedBpg').val(bpg.toFixed(2));
		$('#computedWtax').val(wtax.toFixed(2));

	}

	$(document).ready(function(){

		$("#cost_per_day").on('change', function(e){
			e.preventDefault();
			$('#hidden_ot_day_amount').val($(this).val());
		});

		$('.availability.reserved').on('click', function(){
			var mesg = 'Reserve Code: ' + $(this).attr('data-code');
			mesg += '\nGuest: ' + $(this).attr('data-guest');
			mesg += '\nPartner: ' + $(this).attr('data-partner');
			mesg += '\nReserve Fee: ' + $(this).attr('data-fee');
			alert(mesg);
		});

		$('#partner').on('change', function(e){
			e.preventDefault();
			$('#newcommission').val( $(this).find('option:selected').data('commission') );
			$('#newbpg').val( $(this).find('option:selected').data('bpg') );
		});

		var keypadOptions = {
			separator: '|', 
			prompt: '', 
    		layout: [
    			'7|8|9|' + $.keypad.CLOSE, 
        		'4|5|6|' + $.keypad.CLEAR, 
        		'1|2|3|' + $.keypad.BACK, 
        		'.|0|00'
        	]
        };

		$("#new_extension").keypad(keypadOptions);
		$("#new_12_extension").keypad(keypadOptions);
		$("#new_day_extension").keypad(keypadOptions);
		$("#newdiscount").keypad(keypadOptions);
		$("#newbpg").keypad(keypadOptions);
		$("#newcommission").keypad(keypadOptions);
		$("#newpromo").keypad(keypadOptions);

		$('#apply_original').on('click', function(e){

			if ($(this).is(':checked')) {
				$('#computedRate').val( $('#origRate').val() );
				$('#computedDiscount').val( $('#origDiscount').val() );
				$('#computedCommission').val( $('#origCommission').val() );
				$('#computedBpg').val( $('#origBPG').val() );
				$('#computedWtax').val( $('#origWtax').val() );
				$('#computedAmount').val( '');
			} else {
				$('#computedRate').val( 0 );
				$('#computedDiscount').val( 0 );
				$('#computedCommission').val( 0 );
				$('#computedBpg').val( 0 );
				$('#computedWtax').val( 0 );
				$('#computedAmount').val( 0 );
			}

		});

		$('.computable').on('change', function(e){
			recompute();
		});
	});
 </script>
