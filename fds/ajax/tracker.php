<?php

require_once('config/config.inc.php');

/*--- get start date ---*/
$sql = "select `datetime` from `shift-transactions` where `shift`='start' order by `datetime` desc limit 0, 1";
$start = R::getCell($sql);

/*--- get current cash ---*/
$sql = "select cc_id as id, cc_date as date, transaction_amount, current_amount as running_total, transaction_type
		 from current_cash where cc_date >= :st order by cc_id desc";
$currentCash = R::getAll($sql, [':st' => $start]);

/*--- get safekeep---*/
$sql = "select safekeep_id, safekeep_date, amount, current_amount 
		from safekeep where safekeep_date >= :st order by safekeep_id desc";
$safekeep = R::getAll($sql, [':st' => $start]);

/*--- get salesreceipts ---*/
$sql = "select salesreceipt_id, receipt_date, tendertype, amount 
		from salesreceipts where receipt_date >= :st 
		order by salesreceipt_id desc";
$salesreceipts = R::getAll($sql, [':st' => $start]);


/*--- get reservations ---*/
$sql = "select transaction_id as id, transaction_date as date, reservation_code, amount_deposit, amount_claimed 
		from reservation_transactions where transaction_date >= :st 
		order by transaction_id desc";
$reservations = R::getAll($sql, [':st' => $start]);


/*--- get reservations ---*/
$sql = "select sr_id as id, date_endorsed, date_remitted, amount, remarks 
		from security_receivables where date_endorsed >= :st or date_remitted >= :st2
		order by sr_id desc";
$receivables = R::getAll($sql, [':st' => $start, ':st2' => $start]);

function getRunningCash() {
	$sql = "select current_amount from current_cash order by cc_id desc limit 0, 1";
	$currentCash = R::getCell($sql);
	$sql = "select current_amount from safekeep order by safekeep_id desc limit 0, 1";
	$safekeep = R::getCell($sql);
	return number_format($safekeep + $currentCash, 2) ;
}


?>
<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="refresh" content="10">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Cash Transactions Tracker</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<style>
			body {
				font-size: 11px;
			}
		</style>
	</head>
	<body>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Shift Started: <?php echo $start ?></a>

				</div>	
				<button class="navbar-brand btn btn-default pull-right"> 
				Running Total: 
				<span class="badge">
				<?php echo getRunningCash() ?>
				</span>	
				</button>
			</div>
		</nav>
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Current Cash</h3>
						</div>
						<div class="panel-body ">
							<table class="table table-hover">
								<thead>
									<tr>
										<?php foreach ($currentCash[0] as $field => $value): ?>
										<th><?php echo str_replace('_', ' ', $field) ?></th>
										<?php endforeach ?>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($currentCash as $key => $line): ?>
										<tr>
										<?php foreach ($line as $field => $value): ?>
										<td><?php echo $value ?></td>
										<?php endforeach ?>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>	
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Safekeep</h3>
						</div>
						<div class="panel-body ">
							<table class="table table-hover">
								<thead>
									<tr>
										<?php foreach ($safekeep[0] as $field => $value): ?>
										<th><?php echo $field ?></th>
										<?php endforeach ?>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($safekeep as $key => $line): ?>
										<tr>
										<?php foreach ($line as $field => $value): ?>
										<td><?php echo $value ?></td>
										<?php endforeach ?>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>	

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Sales Receipts</h3>
						</div>
						<div class="panel-body ">
							<table class="table table-hover">
								<thead>
									<tr>
										<?php foreach ($salesreceipts[0] as $field => $value): ?>
										<th><?php echo $field ?></th>
										<?php endforeach ?>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($salesreceipts as $key => $line): ?>
										<tr>
										<?php foreach ($line as $field => $value): ?>
										<td><?php echo $value ?></td>
										<?php endforeach ?>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>	

					
				</div>

				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Reservations</h3>
						</div>
						<div class="panel-body ">
							<table class="table table-hover">
								<thead>
									<tr>
										<?php foreach ($reservations[0] as $field => $value): ?>
										<th><?php echo str_replace('_', ' ', $field) ?></th>
										<?php endforeach ?>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($reservations as $key => $line): ?>
										<tr>
										<?php foreach ($line as $field => $value): ?>
										<td><?php echo $value ?></td>
										<?php endforeach ?>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>	


					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Security Receivables</h3>
						</div>
						<div class="panel-body ">
							<table class="table table-hover">
								<thead>
									<tr>
										<?php foreach ($receivables[0] as $field => $value): ?>
										<th><?php echo str_replace('_', ' ', $field) ?></th>
										<?php endforeach ?>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($receivables as $key => $line): ?>
										<tr>
										<?php foreach ($line as $field => $value): ?>
										<td><?php echo $value ?></td>
										<?php endforeach ?>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>	
				</div>
			</div>
		</div>
		

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script src="Hello World"></script>
	</body>
</html>