<?php
include_once('config/config.inc.php');
require_once('class.forecast.php');
session_start();

/**
get all sales for occupancy items in forecast_list where checkout tag is true
and group them by the receipt flag
*/

$today = isset($_POST['fcdate']) ? $_POST['fcdate'] : ( isset($_GET['d']) ? $_GET['d'] : date('Y-m-d') );
$shift = isset($_POST['fcshift']) ? $_POST['fcshift'] : ( isset($_GET['s']) ? $_GET['s'] : forecast::getshift() ) ;
$rcptstart = isset($_POST['rcptstart']) ? $_POST['rcptstart'] : forecast::getlastreceiptnumber();

$fc = new forecast($today, $shift);
$printflag = 0;

if (isset($_GET['m'])) {
    $result = $_GET['m'];
}

if (isset($_POST['act'])) {
    if ($_POST['act'] === 'register') {
        $fc->registeritems($_POST);
    } elseif ($_POST['act'] === 'post') {
        $fc->post($_POST);    
        $messagebag='Posting successful';
    } elseif ($_POST['act'] == 'prepare for printing') {
        $fc->printregistered($_POST);
        $printflag = 1;
    }
    if ($printflag != 1) {
        header('location: fc_registry.php?d='. $today .'&s=' . $shift .'&t=' . time() .'&m=' . $messagebag);
    }
}

?>
<html>
<head>
<title>Forecast</title>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<style>
body {font-family:helvetica;margin:0px;}
table {font-size:12px;font-family:helvetica;}
th {width:auto; border-bottom:1px solid #cccccc;}
.totals {color:#00F;font-weight:bold;}
td {border-bottom:1px solid #cccccc; text-align:center }
.amt {text-align:right}
h3 {font-size:12px;}
.menubar {background-color:#eeeeee;font-size:11px;padding:4px;border-bottom:1px solid #cccccc;}
.content {padding:10px; font-size:11px}
.message {display:inline; color:#ff0000;}
.targets {color: orange;}
.actuals {color: green;}
</style>
</head>
<body>

<form method="post">
<div class="menubar">
<div style="display:inline">
Today: <input type="text" value="<?php echo $today ?>" name="fcdate" id="fcdate" />
</div>
<div style="display:inline">
Shift: <input type="text" value="<?php echo $shift ?>" name="fcshift" id="fcshift" size="4" maxlength="1"/>
</div>

<input type="submit" name="submit" value="GO" />
<div class="message"><?php echo $result ?></div>
</div>


<div class="content">
<h3>Registered</h3>

<div>
<input type="submit" value="<?php echo ($printflag==1) ? 'print' : 'prepare for printing'?>" name="act" id="print" />
<input type="hidden" value="<?php echo $printflag ?>" name="printflag" id="printflag" />
</div>
<table>
<thead>
<?php
$headers = array('ID', 'Room No.', 'Checkout', 'Room Sales', 'Food Sales' , 
                'Misc Sales', 'SubTotal', 'Tax', 'Total Sales', 'Receipt');
foreach ($headers as $label) {
    echo "<th>$label</th>";
}
?>
</thead>
<?php 
echo $fc->getregistered();
?>
</table>
<h2>
Target: <span  class="targets"><?php echo $fc->adjusted ?></span> <==> 
Amount to Post: <span  class="actuals"><?php echo $fc->postamount ?> </span>
<input type="submit" value="post" name="act" id="postaction" />
</h2>
<input type="hidden" name="targetamount" value="<?php echo $fc->adjusted ?>">
<input type="hidden" name="postamount" value="<?php echo $fc->postamount ?>">
<h3>Unregistered</h3>
<div>
Begin Series No. <input type="text" name="receipt_start" size="8" id="receipt_start" value="<?php echo $rcptstart ?>" />
<input type="submit" value="register" name="act" />
</div>
<table>
<thead>
<?php
reset($headers);
array_pop($headers);
foreach ($headers as $label) {
    echo "<th>$label</th>";
}
?>
</thead>
<?php 
echo $fc->getcollectiondata(0);
?>
</table>
</div>
</form>
<script type="text/javascript">

$(document).ready(function(){
    $('#fcdate').datepicker({dateFormat:'yy-mm-dd'});
    $('#postaction').on('click', function(e) {
        return confirm('Are you sure you want to post this total?');
    });
    
    $('#print').on('click', function(e){
    	if ($('#printflag').val() == 1) {
	  e.preventDefault();
        $('#mf').attr('src', 'reports/fcsummary.html');
	  window.open('reports/fcsummary.html', 'fcreport', 'toolbar=yes,menubar=yes,width=200px,height=100px');
    	}
    });
    
});
</script>

<iframe src="" id="mf" title="forecastsummary" style="border: 0; width: 0; height: 0;">
</iframe>
</body>
</html>

