<?php
/**
* config
*/



if (file_exists('../classes/rb.php')) {
    require('../classes/rb.php');
}elseif(file_exists('./classes/rb.php')) {
    require('./classes/rb.php');
}elseif(file_exists('../../../classes/rb.php')) {
    require('../../../classes/rb.php');
}elseif(file_exists('../../classes/rb.php')) {
    require('../../classes/rb.php');
}elseif(file_exists('../fds/classes/rb.php')) {
    require('../fds/classes/rb.php');
}



date_default_timezone_set("Asia/Manila");
ini_set('error_reporting', 1);

$dbname = "shogunfds2";
$host = "192.168.1.162";
$user = "root";
$pass = "";


$rb = R::setup( "mysql:host=$host;dbname=$dbname",
   $user, $pass );


function dd($obj) {
	die('<pre>'. print_r($obj, 1));
}   

function occdd($var, $occ, $mesg) {
    if ($var == $occ) {
        dd(__METHOD__.' | ' . __LINE__ . ' MSG: ' . print_r($mesg, 1));
    }
}


function postGL($pair, $total=0, $ref=0, $src = '', $remarks = '', $type = 'n', $now = null) {
    if (is_null($now)) {
        $now = date('Y-m-d');
    }
    $dr = $pair['dr'];
    $sql = "insert into gl_trans (transdate, ref_id, src, code, debit, credit, amount, remarks, type)
            values ('$now', '$ref', '$src', '$dr', 0, $total, $total, '$remarks', '$type')
          ";

    R::exec($sql);

    $cr = $pair['cr'];
    $sql = "insert into gl_trans (transdate, ref_id, src, code, debit, credit, amount, remarks, type)
            values ('$now', '$ref', '$src', '$cr', $total, 0, $total, '$remarks', '$type')
          ";
    R::exec($sql);
}


/** depreatec by php7
$dbid=mysql_connect($host,$user,$pass) or die(mysql_error());
mysql_select_db($dbname);
*/

/*************** log functions - begin ************************************************/


function accessLogs($user, $action, $desc) {

	$sql = "insert into access_logs (user_id, action_desc, action_result) 

			values ('$user', '$action', '$desc');

	";
	R::exec($sql);

}

function logInfo($file, $line, $comment='') {
    $date = date('Y-m-d H:i:s');
    $sql = "insert into info_log values (null, '$date', '$file', '$line', '$comment');";
    R::exec($sql);

}

/*************** log functions - end ************************************************/

/*************** fingerprint scanner default parameters - begin ***************/

//for windows:
$FPT_TEMPLATE_PATH  = 'C:\\\\dpfps\\\\fpt\\\\';
$FPT_FILE_VALIDATE  = 'C:/dpfps/Verification/pool/validate.txt';
$FPT_FILE_RESULT    = 'C:/dpfps/Verification/pool/result.txt';
$FPT_COMMAND        = "C:\PsTools\psexec ../fscan.bat";

//for linux
//$FPT_TEMPLATE_PATH  = '/home/shogunerp/Sites/S2fds/ajax/reports';
//$FPT_FILE_VALIDATE  = '/home/shogunerp/Sites/S2fds/ajax/reports/validate.txt';
//$FPT_FILE_RESULT    = '/home/shogunerp/Sites/S2fds/ajax/reports/result.txt';
//$FPT_COMMAND        = 'nautilus --browser';


/*************** fingerprint scanner default parameters - end ***************/

//echo $_SERVER["REQUEST_URI"];

//check if not yet check in
/*
if($_SERVER["REQUEST_URI"] != "/fds/ajax/shiftstart.php" && ($_SESSION["hotel"]["groupid"] == "5" || $_SESSION["hotel"]["groupid"] == "1"))
{
	$sql = "select shift from `shift-transactions`  order by datetime desc limit 0,1";
	$res = mysql_query($sql);

	
	list($usershift)=mysql_fetch_row($res);
	$num = mysql_num_rows($res);

	if(($usershift == 'end' || $usershift == '') || $num == 0)
	{
		header("location: notauthorize.php");
	}
}*/
	
