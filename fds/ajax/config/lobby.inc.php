<?php
/**
* lobby config
*/

if(ifLobby())
{
	
	if($_SERVER['PHP_SELF'] != "/fds/ajax/order.php ")
	{
		die("You cannot acces this page.");
	}
}

function ifLobby()
{
	$val = getSpecialFloorId();

	$sql = "select room_id from rooms where floor_id=$val";
	$arrRooms = R::getCol($sql);

	$room =($_GET["roomid"]) ? $_GET["roomid"] : $_POST["roomid"];

	if(in_array($room,$arrRooms))
	{
		return true;
	}

	return false;
}
