<?php 
/* payment_fieldset.php */
?>
<fieldset><legend>Payment</legend>
<table class="formtable" border=0 width="290">
<tr>
<td>Cash</td>
<td>
<input type="text"  class="money tender" id="new_cash_tendered" name="new_cash_tendered" value="0" />
</td>
<td>
<input type="text"  class="money tender" id="new_cash_tendered_alt" name="new_cash_tendered_alt" value="0" />
</td>
</tr>
<tr>
<td  width="80">Card</td>
<td>
<input type="text"  class="money tender" id="new_card_tendered" name="new_card_tendered" value="0" />
</td>
<td>
<input type="hidden"  class="money tender" id="new_card_tendered_alt" name="new_card_tendered_alt" value="0" />
</td>
</tr>

<tr>
<td colspan=3>
<?php include_once('card_input.php') ?>
</td>
</tr>

<tr>
<td colspan=3 style="background-color:#EFE0D1" align=middle>
<input type="button" class="denomination" value="1" />
<input type="button" class="denomination" value="5" />
<input type="button" class="denomination" value="10" />
<input type="button" class="denomination" value="20" />
<input type="button" class="denomination" value="50" />
<input type="button" class="denomination" value="100" />
<input type="button" class="denomination" value="500" />
<input type="button" class="denomination" value="1000" />
<input type="button" class="denomination half" value="Clear" />
<input type="button" class="denomination" value="0.1" />
<input type="button" class="denomination" value="0.01" />
</td>
</tr>
</table>
<input type="button" class="denomination full" value="Check In" id="checkinsubmit"/>
</fieldset>