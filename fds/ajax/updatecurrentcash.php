<?php
header("access-control-allow-origin: *");
include_once("config/config.inc.php");
include_once("currentcash.function.php");
session_start();

if (isset($_POST['by']) && isset($_POST['amt']) && isset($_POST['type']) && $_POST['type'] != '') {
	setCurrentCash($_POST['amt'], $_POST['type'], $_POST['by']);
}