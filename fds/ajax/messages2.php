<?php
//transfer.php
session_start();
include_once("config/config.inc.php");
include_once("common.inc.php");
include_once("class.room.php");
$room = $_GET["roomid"];
$cmd = $_POST["cmd"];

$occupancy_id = getOccupancyIdByRoomID($room);



if($cmd == "add"){
	parse_str($_POST["data"]);		
	setGuestRequest("",$_POST["occupancy"],$daterequired." ".$hourcombo.":".$mincombo.":00",$txtmessage,$requestType);	
	$grid = getGridMessage($_POST["occupancy"]);
	echo $grid;
	exit;
}elseif($cmd == "specific") {
	$gr_id =  $_POST["grid"];
	
	$sql = " select * from guest_requests where gr_id='$gr_id' ";
	$res = R::getAll($sql);
	$numrows = count($res);
	$retval="";
	$row = $res[0];	
	
	$date = substr($row["date_required"], 0, 10);  
	$hour = substr($row["date_required"], 11, 2);
	$min = substr($row["date_required"], 14, 2);;
	echo '{ data :  '.json_encode($row).', date : "'.$date.'",hour : "'.$hour.'", min :"'.$min.'" }';
	exit;
}elseif($cmd == "edit") {
	parse_str($_POST["data"]);
	$grid = $_POST["gr_id"];

	setGuestRequest($grid,$_POST["occupancy"],$daterequired." ".$hourcombo.":".$mincombo.":00",$txtmessage,$requestType);
	
	$grid = getGridMessage($_POST["occupancy"]);
	echo $grid;
	exit;
}elseif($cmd == "delete"){
	$grid = $_POST["grid"];
	$sql = "delete from guest_requests where gr_id = '$grid'";	
	R::exec($sql);
	$grid = getGridMessage($_POST["occupancy"]);
	echo $grid;
	exit;

}

function getOccupancyIdByRoomID($roomid){
	$sql = "select occupancy_id from occupancy where room_id = '$roomid' and actual_checkout = '0000-00-00 00:00:00'";
	return R::getCell($sql);	
}

function getFormRequestType($gr_id)
{
	$rtype = "";
	if($gr_id != "")
	{
		$sql = "select request_type from guest_requests where gr_id = '$gr_id'";
		$rtype = R::getCell($sql);
	}
	
	$sql = "show columns from guest_requests like 'request_type'";
	$row = R::getRow($sql);

	$ret = "<select name='requestType' id='requestType'>";
	$ret .= "<option value=''></option>";
	$regex = "/'(.*?)'/"; 
	preg_match_all( $regex , $row[1], $enum_array ); 
	$enum_fields = $enum_array[1]; 
	
	foreach($enum_fields as $value){ 
		if($rtype == $value){
			$selected = "selected";
		}else{
			$selected = " ";
		}
		$ret .= "<option value='$value' $selected>$value</option>";		
	}	
	$ret .= "</select>";

	return $ret;
}

function getFormDateRequired($date_required)
{
	$ret = '';
	return $ret;
}

function getFormTextareaMessage($gr_id)
{
	$ret = "<textarea cols='18' rows='4' name='txtmessage' id='txtmessage'></textarea>";
	
	return $ret;
}

function getGridMessage($occupancy_id)
{
	$ret = '<table id="messagelist">';

	
	$sql = "select gr_id,date_required,request_type from guest_requests where occupancy_id = '$occupancy_id'";
	$res = R::getAll($sql);
	
	$num = count($res);
	if($num > 0){
		$ret .= "<tr><th>Datetime</th><th>Type</th><th></th></tr>";
	}

	foreach ($res as $r)
	{
		$gr_id = $r['gr_id'];
		$date_required = $r['date_required'];
		$request_type = $r['request_type'];

		$ret .= "<tr><td><a href='#$gr_id' alt='$gr_id' class='agr_id'>$date_required</a></td><td>$request_type</td>
		<td><a href='#$gr_id' alt='$gr_id' class='gr_del' >cancel</a></td></tr>";		
	}	
	$ret .= "</table>";	
	return $ret;
}

function setGuestRequest($gr_id,$occupancy_id,$date_required,$message,$request_type)
{
	$date_requested = date("Y-m-d H:i:s");
	if($gr_id == ""){
		$sql = "insert into guest_requests(occupancy_id,date_requested,date_required,message,request_type) 
			values('$occupancy_id','$date_requested','$date_required','$message','$request_type')";
	}else{
		$sql = "update guest_requests set date_required='$date_required',message='$message',request_type='$request_type'
			where gr_id = '$gr_id'";
	}
	
	R::exec($sql);	
}

?>

<form name="myform" id="myform" action="post">
<fieldset id="changestatus">
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td valign="top">Date</td>
<td><input type="text" name="daterequired" id="daterequired"  class="datefield" /></td>
</tr>
<tr>
<td valign="top">Time</td>
<td width="500"><div id="timerequired" name="timerequired"></div></td>
</tr>
<tr>
<td>Type</td>
<td><?php echo getFormRequestType("") ?></td>
</tr>
<tr>
<td valign="top">Message</td>
<td><?php echo getFormTextareaMessage("") ?></td>
</tr>
</table>
<input type="button" name="cmd" id="add" value="Add new" />
<input type="reset" name="cmd" id="reset" value="Clear" />
<input type="hidden" name="hiddenval" id="hiddenval" value="" />
<br>
<br>
<div id="tblgrid">
<?php echo getGridMessage($occupancy_id) ?>
</div>
</form>

<!-- <form name="listform" id="listform" action="post">
<input type="submit" name="cmd" id="cmd" value="add new" />
<div id="tblgrid">
<?php echo getGridMessage($occupancy_id) ?>
</div>
</form> -->


</fieldset>

<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery.timepicker.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.jjtimepicker.js"></script>
<style>
#messagelist {empty-cells:show; border-collapse:false;border-spacing:0px;}
</style>

<script lang="javascript">
$(document).ready(function(){
	$("#daterequired").datepicker({dateFormat:'yy-mm-dd'});
	$("#timerequired").jtimepicker({});

	$("#add").click(function(){
		if($("#add").val() == "Add new"){
			$.post("messages.php",{ cmd:'add', occupancy:<?=$occupancy_id?>, data: $("#myform").serialize()}, function(resp){
				$("#tblgrid").hide().html(resp).fadeIn('fast');	
				
				$("a.agr_id").click(
					function(){
					var gr_id  = $(this).attr("alt"); 
					$.post("messages.php",{ cmd:'specific', grid: gr_id}, function(resp){		
						$("#hiddenval").val(gr_id)
						$("#txtmessage").val(resp.data.message);
						$("#requestType").val(resp.data.request_type);
						$("#daterequired").val(resp.date);			
						$("#hourcombo").val(resp.hour);
						$("#mincombo").val(resp.min);
						$("#add").val("Edit");			
					}, "json");
				});
			});
		}else{
			$.post("messages.php",{ cmd:'edit',  occupancy:<?=$occupancy_id?>,gr_id: $("#hiddenval").val(), data: $("#myform").serialize()}, 
			function(resp){
				$("#tblgrid").hide().html(resp).fadeIn('fast');	
				
				$("a.agr_id").click(
					function(){
					var gr_id  = $(this).attr("alt"); 
					$.post("messages.php",{ cmd:'specific', grid: gr_id}, function(resp){		
						$("#hiddenval").val(gr_id)
						$("#txtmessage").val(resp.data.message);
						$("#requestType").val(resp.data.request_type);
						$("#daterequired").val(resp.date);			
						$("#hourcombo").val(resp.hour);
						$("#mincombo").val(resp.min);
						$("#add").val("Edit");			
					}, "json");
				});				
			});
		}
	});	

	$("a.agr_id").click(
		function(){
		var gr_id  = $(this).attr("alt"); 
		$.post("messages.php",{ cmd:'specific', grid: gr_id}, function(resp){		
			$("#hiddenval").val(gr_id)
			$("#txtmessage").val(resp.data.message);
			$("#requestType").val(resp.data.request_type);
			$("#daterequired").val(resp.date);			
			$("#hourcombo").val(resp.hour);
			$("#mincombo").val(resp.min);
			$("#add").val("Edit");			
		}, "json");
	});

	$("a.gr_del").click(
		function(){
		if(confirm('Delete record?')){
			var gr_id  = $(this).attr("alt"); 	
			$.post("messages.php",{ cmd:'delete', grid: gr_id, occupancy:<?=$occupancy_id?>}, function(resp){
				$("#tblgrid").hide().html(resp).fadeIn('fast');	
				$("a.agr_id").click(
					function(){
					var gr_id  = $(this).attr("alt"); 
					$.post("messages.php",{ cmd:'specific', grid: gr_id}, function(resp){		
						$("#hiddenval").val(gr_id)
						$("#txtmessage").val(resp.data.message);
						$("#requestType").val(resp.data.request_type);
						$("#daterequired").val(resp.date);			
						$("#hourcombo").val(resp.hour);
						$("#mincombo").val(resp.min);
						$("#add").val("Edit");			
					}, "json");
				});			
			});
		}		
	});

	$("#reset").click(function(){
		$("#add").val("Add new");
	});
	return false;	
});
</script>