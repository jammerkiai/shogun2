<?php
session_start();
include_once("config/config.inc.php");
require_once('acctg/class.baseobject.php');
require_once('acctg/class.shift.php');
require_once('acctg/class.report.php');
require_once('acctg/reportfns.php');


$now = date("Y-m-d H:i:s");
$start = date('Y-m-d H:i:s',strtotime("$now - 30 days"));
$end = date('Y-m-d H:i:s',strtotime("$now + 1 day"));


$sql  ="select a.roomsales_id, a.sales_date, a.order_code, 
		c.sas_cat_name, b.sas_description, a.unit_cost,
		a.qty, (a.unit_cost* a.qty) as total, a.status, a.remarks, a.update_date, d.fullname ,a.room_id,a.tendertype, a.rechit_date, a.paid_date

		from room_sales a, sales_and_services b, sas_category c ,users d
		where 
		a.item_id=b.sas_id
		and a.category_id=c.sas_cat_id
		and a.category_id=3
		and a.update_by=d.user_id		
		and a.status='Draft'
		and year(a.sales_date)=2016
		and month(a.sales_date)>=6";

$arrReport = array(
		'title'    =>  "Room Charges",
		'aggregates'=> array('total'),
		'sql'	   => $sql	
		);


$report = new report($arrReport);
$report->buildReport();
$report->show();


$totals['room']=$report->aggregate['total'];

$sql  ="select a.roomsales_id, a.sales_date, a.order_code, 
		c.sas_cat_name, b.sas_description, a.unit_cost,
		a.qty, (a.unit_cost* a.qty) as total, a.status, a.remarks, 
		a.update_date, d.fullname,a.tendertype
		from room_sales a, sales_and_services b, sas_category c ,users d
		where 
		a.item_id=b.sas_id
		and a.category_id=c.sas_cat_id
		and a.category_id not in (2,3)
		and a.update_by=d.user_id
		and a.status='Draft'
		and year(a.sales_date)=2016
		and month(a.sales_date)>=6
		order by a.occupancy_id";
$arrReport = array(
		'title'    =>  "Misc Sales",
		'aggregates'=> array('total'),
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
$report->show();
$totals['misc']=$report->aggregate['total'];



$sql  ="select a.fnbsales_id, a.sales_date, a.order_code, 
		c.food_category_name, b.fnb_name, a.unit_cost,
		a.qty, (a.unit_cost* a.qty) as total, a.status, 
		a.remarks, a.update_date, d.fullname,a.tendertype
		from fnb_sales a, fnb b, food_categories c, users d  
		where 
		a.category_id=c.food_category_id
		and a.item_id=b.fnb_id
		and a.category_id not in (17,21)
		and a.update_by=d.user_id
		and a.status='Draft'
		and year(a.sales_date)=2016
		and month(a.sales_date)>=6
		order by a.occupancy_id";
$arrReport = array(
		'title'    =>  "Food Sales Details: $occ",
		'aggregates'=> array('total'),
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
$report->show();
$totals['food']=$report->aggregate['total'];

$sql  ="select a.fnbsales_id, a.sales_date, a.order_code, 
		c.food_category_name, b.fnb_name, a.unit_cost,
		a.qty, (a.unit_cost* a.qty) as total, a.status, 
		a.remarks, a.update_date, d.fullname,a.tendertype
		from fnb_sales a, fnb b, food_categories c, users d  
		where 
		a.category_id=c.food_category_id
		and a.item_id=b.fnb_id
		and a.category_id in (17,21)
		and a.update_by=d.user_id
		and a.status='Draft'
		and year(a.sales_date)=2016
		and month(a.sales_date)>=6
		order by a.occupancy_id";
$arrReport = array(
		'title'    =>  "Beer Sales Details: $occ",
		'aggregates'=> array('total'),
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
$report->show();


?>
<style>
body,h1,h2,h3,h4,h5 {
	font-family: arial, helvetica, sans-serif;
	margin:0;
	margin-width:0;
	margin-height:0;
	font-size:14px;
}

.toolbar {
			background-color:#cccccc;
			padding:4px;
		}

div {
	font-size:13px;
}

h1 {
	font-size:14px;
	padding-top:8px;
	border-top:1px solid #111199;
	
}

table {
	font-size:12px;
	padding:2px;
	border:1px solid #dddddd;
}

th, td{
	background-color:#eeeeee;
	padding:2px;
}
.summary th, td {
	text-align:right;
	border-bottom:1px dotted #cccccc;
}
td {
	background-color:#fcfcfc;
}

.aggregates th{
	border-top:1px solid #000000;
}
</style>

<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery-ui.js" type="text/javascript"></script>