<?php

class shiftendsales 
{
    /** tax computation constants **/
    const VAT_TAX = 0.12;
    const VAT_DIV = 1.12;
    
    /** special occupancy_id **/
    const LOBBY2 = 500;
    const BANQUET = 1428;
    const INHOUSE = 1427;
    const GARDEN = 24479;
    const RESTAURANT = 24481;
    const NOSHOWCANCEL = 37346;
        
    /** variable declarations **/
    var $html;
    var $start;
    var $end;
    var $data;
    var $totals;
    var $revenuesrc = array(
    	self::LOBBY2 => 'Lobby 2',
    	self::BANQUET => 'Banquet',
    	self::INHOUSE => 'In House',
    	self::GARDEN => 'Garden',
    	self::RESTAURANT => 'Restaurant',
    	self::NOSHOWCANCEL => 'No Show/Cancel'
	);
    
    public function __construct($start, $end) {
        $this->start = $start;
        $this->end = $end;
        $this->data = array();
        $this->totals = array();
    }

    public function getshift($date) {
		if(!$date)$date=date("Y-m-d H:i:s");
		list($d, $t) = explode(" ", $date);
		list($h, $m, $s) = explode(":", $t);

		if($h==16||$h==15)
		{
			$this->shiftnum=3;
			return $shift = "3rd";
		}
		elseif($h==8||$h==7)
		{
			$this->shiftnum=2;
			return $shift = "2nd";
		}
		$this->shiftnum=1;
	    return  "1st";
	}
	
	public function getLatestShifts($shiftid)
	{
		$sql = "SELECT `shift-transaction_id`,datetime,user_id  FROM `shift-transactions` where shift = 'start' order by datetime desc ";
		$res = R::getAll($sql);
		$ret = "<select name='rblshifts' id='rblshifts' onchange='myform.submit();'>";
		$select = ($shiftid) ? '' : ' selected ';
		$ret .= "<option value='' $select>&nbsp;</option>";
		foreach ($res as $r)
		{

            $shift_transaction_id = $r['shift-transaction_id'];
            $datetime = $r['datetime'];
            $userid = $r['user_id'];

		    $select = '';
			if($shiftid == $shift_transaction_id)
			{
				$select = "selected";
			}			
            $__sql = "SELECT user_id  FROM `shift-transactions` where shift = 'end' 
			and `datetime` > '$datetime'
			order by datetime asc 
			limit 0,1";
            $userid = R::getCell($__sql);
			$_sql = "select fullname from users where user_id = '$userid'";
            $username = R::getCell($_sql);
			$ret .= "<option value='$shift_transaction_id' $select>$datetime - $username - ".$this->getshift($datetime)."</option>";
		}
		$ret .= "</select>";

		return $ret;
	}
	
	public function getOccupancies() {
        R::exec('truncate table temp_occupancy');
        $special = self::LOBBY2 . ',' . self::BANQUET . ',' . self::INHOUSE .','. self::GARDEN .',' .
	                    self::RESTAURANT .','. self::NOSHOWCANCEL;
	    $sql = "insert into temp_occupancy 
	                select * from occupancy
	                    where 
	                    (actual_checkout = '0000-00-00 00:00:00' 
	                    or (actual_checkout >= '{$this->start}' and actual_checkout <= '{$this->end}'))
	                    and occupancy_id not in ($special);
	        ";
	        
	        
	    R::exec($sql);
	    return $this;
	}
	
    public function prepareData($start, $end) {
        R::exec('truncate table temp_occupancy_sales');
        $sql = "insert into temp_occupancy_sales 
                    select * 
                    from temp_occupancy 
                    where occupancy_id in (
                        select occupancy_id from room_sales 
                        where (sales_date >= '{$start}' and sales_date <= '{$end}')
                        or (update_date >= '{$start}' and update_date <= '{$end}')
                        union
                        select occupancy_id from fnb_sales 
                        where update_date >= '{$start}'
                        and update_date <= '{$end}'
                        );
                "; 
        R::exec($sql) or die($sql);
        return $this;
    }

    
    public function getInTransitRoomSales() {
        $salesstart = date('Y-m-d H:i:s', strtotime($this->start . '-1 day'));
        $salesend = date('Y-m-d H:i:s', strtotime($this->end . '-1 day'));
        
        $sql = "select 'Room Sales', 'Cash', 'Room Sales', a.occupancy_id, a.sales_date, a.update_date,  
                sum(a.qty * a.unit_cost) as total
                from room_sales a
                where a.occupancy_id in (
                	select occupancy_id from occupancy 
						where actual_checkout = '0000-00-00 00:00:00'
						or (actual_checkout > '{$this->end}')
                )
                and a.sales_date >= '$salesstart' and a.sales_date <= '$salesend'
                and a.category_id=3
                group by a.occupancy_id, a.sales_date
            ";
        
        $this->appendData($sql);
        //echo $sql . '<hr>';
        return $this;
    }
    
    public function getCheckoutRoomSales() {
        $salesstart = date('Y-m-d H:i:s', strtotime($this->start . '-1 day'));
        $salesend = date('Y-m-d H:i:s', strtotime($this->end . '-1 day'));
        $sql = "select 'Room Sales', 'Cash', 'Room Sales', a.occupancy_id, a.sales_date, a.update_date, 
                sum(a.qty * a.unit_cost) as total
                from room_sales a
                where a.occupancy_id in 
                    (select occupancy_id from temp_occupancy 
                        where (actual_checkout >= '$this->start' 
                        and actual_checkout <= '$this->end'))
                and a.sales_date >= '$salesstart'
                and a.category_id=3
                group by a.occupancy_id, a.sales_date
            ";
        $this->appendData($sql);
        //echo $sql . '<hr>';
        return $this;
    }
    
    public function getInTransitMiscSales() {
        $salesstart = date('Y-m-d H:i:s', strtotime($this->start . '-1 day'));
        $salesend = date('Y-m-d H:i:s', strtotime($this->end . '-1 day'));
           
        $sql = "select 'Room Sales', 'Cash', 'Misc Sales' as SalesType, a.occupancy_id, a.sales_date, a.update_date,  
                sum(a.qty * a.unit_cost) as total
                from room_sales a
                where (         
                	a.occupancy_id in (
						select distinct b.occupancy_id from occupancy b, room_sales c
							where b.occupancy_id = c.occupancy_id
							and (b.actual_checkout = '0000-00-00 00:00:00'
							or b.actual_checkout > '{$this->end}')
							and c.sales_date >= '$salesstart' and c.sales_date <= '$salesend'
							and c.category_id = 3
						)
                )
                and a.update_date >= (
                	select if (
                		(select actual_checkout 
                		from temp_occupancy where occupancy_id=a.occupancy_id) <> '0000-00-00 00:00:00', 
						(select min(`datetime`)
							from `shift-transactions` st
							where st.shift = 'start' 
							and `datetime` <= (
								select actual_checkout from temp_occupancy 
								where occupancy_id=a.occupancy_id)
							and `datetime` >= (
								select max(sales_date) from room_sales 
								where occupancy_id=a.occupancy_id 
								and category_id=3)),
						    (select if (		
						        (select datediff(date('{$this->start}'), actual_checkin) from occupancy
						            where occupancy_id=a.occupancy_id) > 1, 
						        '$salesend',
						        '$salesstart'
						    ))
						)
					)
  			    and a.update_date <= '$this->end'
                and a.category_id <> 3
                and a.status='Paid'
                group by SalesType, a.occupancy_id, a.sales_date
            ";
        //echo __METHOD__ . ' Intransit ===>' .  $sql . '<br>';
        $this->appendData($sql);
        return $this;
    }
    
    public function getCheckoutMiscSales() {
        $salesstart = date('Y-m-d H:i:s', strtotime($this->start . '-1 day'));
        $salesend = date('Y-m-d H:i:s', strtotime($this->end . '-1 day'));
        $sql = "select 'Room Sales', 'Cash', 'Misc Sales', a.occupancy_id, a.sales_date, a.update_date, 
                sum(a.qty * a.unit_cost) as total
                from room_sales a
                where a.occupancy_id in 
                    (select occupancy_id from temp_occupancy 
                        where (actual_checkout >= '$this->start' 
                        and actual_checkout <= '$this->end'))
                 and a.update_date >= (
                	select if (
                		(select datediff(actual_checkout, actual_checkin) 
                		from temp_occupancy where occupancy_id=a.occupancy_id) > 1, 
                		
						(select min(`datetime`)
							from `shift-transactions` st
							where st.shift = 'start' 
							and `datetime` <= (
								select actual_checkout from temp_occupancy 
								where occupancy_id=a.occupancy_id)
							and `datetime` >= (
								select max(sales_date) from room_sales 
								where occupancy_id=a.occupancy_id 
								and status = 'Paid'
								and category_id=3)),
								
						'$salesstart'
						)
					)
				and a.update_date <= '$this->end'
                and a.category_id <> 3
                and a.status = 'Paid'
                group by a.occupancy_id, a.sales_date
            ";
        //echo __METHOD__ . ' Checkout ===>' .  $sql . '<br>';
        $this->appendData($sql);
        return $this;
    }
    
    /**
    * int $categ:
    *   1 - food
    *   2 - beer
    *   3 - non-beer beverage
    */
    public function getCheckoutFnbSales() {
        $salesstart = date('Y-m-d H:i:s', strtotime($this->start . '-1 day'));
        $salesend = date('Y-m-d H:i:s', strtotime($this->end . '-1 day'));
    	$sql = "select 'Room Sales', 'Cash', 
				case 
					when a.category_id in (17, 21) then 'Beer Sales'
					when a.category_id in (5, 6, 16, 24, 27) then 'Beverage Sales'
					when a.category_id in (22) then 'Discount'
					else 'Food Sales'
				end as SalesType, 
				a.occupancy_id, a.sales_date, a.update_date, 
                sum(a.qty * a.unit_cost) as total
                from fnb_sales a
                where a.occupancy_id in 
                    (
                    select occupancy_id from temp_occupancy 
                        where (actual_checkout >= '$this->start' 
                        and actual_checkout <= '$this->end')
                    )
                and a.update_date >= (
                	select if (
                		(select datediff(actual_checkout, actual_checkin) 
                		from temp_occupancy where occupancy_id=a.occupancy_id) > 1, 
                		
						(select min(`datetime`)
							from `shift-transactions` st
							where st.shift = 'start' 
							and `datetime` <= (
								select actual_checkout from temp_occupancy 
								where occupancy_id=a.occupancy_id)
							and `datetime` >= (
								select max(sales_date) from room_sales 
								where occupancy_id=a.occupancy_id 
								and category_id=3)),
								
						'$salesstart'
						)
					)
                and a.status = 'Paid'
                group by SalesType, a.occupancy_id
                having sum(a.qty * a.unit_cost) > 0
            ";

        $this->appendData($sql);
        return $this;
    }
    
    /*******************************
    * using new function
    */
    public function getInTransitFoodForRoomSales() {
    	$salesstart = date('Y-m-d H:i:s', strtotime($this->start . '-1 day'));
        $salesend = date('Y-m-d H:i:s', strtotime($this->end . '-1 day'));
    	$sql = "select distinct b.occupancy_id from occupancy b, room_sales c
				where b.occupancy_id = c.occupancy_id
				and (b.actual_checkout = '0000-00-00 00:00:00'
				or b.actual_checkout > '{$this->end}')
				and c.sales_date >= '$salesstart' and c.sales_date <= '$salesend'
				and c.category_id = 3
			";
		//echo "$sql<hr>";
    	$res = R::getAll($sql);
    	
    	$occlist = array();
    	foreach ($res as $r) {
            $occ = $r['occupancy_id'];
    		array_push($occlist, $occ);
    	}
    	
    	if (count($occlist)) {
    		$this->getInTransitFoodByOccupancy($occlist);
    	}
    }
    
    public function getInTransitFoodByOccupancy($occ) {
    	$salesstart = date('Y-m-d H:i:s', strtotime($this->start . '-1 day'));
        $salesend = date('Y-m-d H:i:s', strtotime($this->end . '-1 day'));
    	if (is_array($occ)) {
    		$occlist = implode(',', $occ);
    		$src = 'Room Sales';
    		$updaterange = "and a.update_date >= '$salesend' and a.update_date <= '$this->end'";
    	} else {
    		$occlist = $occ;
    		$src = $this->revenuesrc[$occ];
    		$updaterange = "and a.update_date >= '$this->start' and a.update_date <= '$this->end'";
    	}

    	$sql = "
    		select '$src', 'Cash',
    			case 
    				when a.category_id in (17, 21) then 'Beer Sales'
    				when a.category_id in (5, 6, 16, 24, 27) then 'Beverage Sales'
    				when a.category_id in (22) then 'Discount'
    				else 'Food Sales'
    			end as SalesType, 
    			a.occupancy_id, a.sales_date, a.update_date, 
                sum(a.qty * a.unit_cost) as total
    		from fnb_sales a
    		where a.occupancy_id in ($occlist)
    		$updaterange
			and a.status = 'Paid'
			group by SalesType, a.occupancy_id, a.update_date
			having sum(a.qty * a.unit_cost) <> 0    		
    	";
    	//echo __METHOD__ . "<br>$src ===>" . $sql;
    	//die();
		$this->appendData($sql);
		
		return $this;
    }
    
    /**
    * get misc sales from all revenue sources 
    */
    public function getInTransitMiscByOccupancy($occ) {
    	if (is_array($occ)) {
    		$occlist = implode(',', $occ);
    		$src = 'Room Sales';
    	} else {
    		$occlist = $occ;
    		$src = $this->revenuesrc[$occ];
    		$updaterange = "and a.update_date >= '$this->start' and a.update_date <= '$this->end'";
    	}
    	$salesstart = date('Y-m-d H:i:s', strtotime($this->start . '-1 day'));
        $salesend = date('Y-m-d H:i:s', strtotime($this->end . '-1 day'));
    	$sql = "
    		select '$src', 'Cash',
    			'Misc Sales' as SalesType, 
    			a.occupancy_id, a.sales_date, a.update_date, 
                sum(a.qty * a.unit_cost) as total
    		from room_sales a
    		where a.occupancy_id in ($occlist)
    		$updaterange
			and a.status = 'Paid'
			and a.category_id <> 3
			group by SalesType, a.occupancy_id
			having sum(a.qty * a.unit_cost) <> 0    		
    	";
		$this->appendData($sql);
		//echo "<br>$src ===>" . $sql;
		return $this;
    }
    
    public function getSalesBySpecialOccupancy() {
    	$this->getInTransitFoodByOccupancy(self::LOBBY2);
		$this->getInTransitFoodByOccupancy(self::BANQUET);
		$this->getInTransitFoodByOccupancy(self::INHOUSE);
		$this->getInTransitFoodByOccupancy(self::GARDEN);
		$this->getInTransitFoodByOccupancy(self::RESTAURANT);
		$this->getInTransitFoodByOccupancy(self::NOSHOWCANCEL);
		$this->getInTransitMiscByOccupancy(self::LOBBY2);
		$this->getInTransitMiscByOccupancy(self::BANQUET);
		$this->getInTransitMiscByOccupancy(self::INHOUSE);
		$this->getInTransitMiscByOccupancy(self::GARDEN);
		$this->getInTransitMiscByOccupancy(self::RESTAURANT);
		$this->getInTransitMiscByOccupancy(self::NOSHOWCANCEL);
    }
    
    public function getInTransitFnbSales($categ = 1) {
        if ($categ === 1) { //non-beverage 
    		$fnbcatlist = " not in (5, 6, 16, 24, 27, 17, 21) ";
    		$var = 'Food Sales';	
    	} elseif ($categ === 2) { //beer
    		$fnbcatlist = " in (17, 21) ";
    		$var = 'Beer Sales';
    	} else { //non-beer beverage
    		$fnbcatlist = " in (5, 6, 16, 24, 27) ";
    		$var = 'Beverage Sales';
    	}
        $salesstart = date('Y-m-d H:i:s', strtotime($this->start . '-1 day'));
        $salesend = date('Y-m-d H:i:s', strtotime($this->end . '-1 day'));
        
        $sql = "select 'Cash', '$var', a.occupancy_id, a.sales_date, a.update_date, 
                sum(a.qty * a.unit_cost) as total
                from fnb_sales a
                where (         
                	a.occupancy_id in (
						select distinct b.occupancy_id from temp_occupancy b, room_sales c
							where b.occupancy_id = c.occupancy_id
							and (b.actual_checkout = '0000-00-00 00:00:00'
							or b.actual_checkout > '{$this->end}')
							and c.sales_date >= '$salesstart' and c.sales_date <= '$salesend'
							and c.category_id = 3
						)
                )
                and a.update_date >= '$salesend' and a.update_date <= '$this->end'
                and a.category_id $fnbcatlist
                and a.status = 'Paid'
                group by a.occupancy_id
                having sum(a.qty * a.unit_cost) > 0
            "; 
            
        $this->appendData($sql);
        //echo $sql . '<hr>';
        return $this;
    }
    
    public function getSpecialSales($categ = 1) {
        if ($categ === 1) { //non-beverage 
    		$fnbcatlist = " not in (5, 6, 16, 24, 27, 17, 21) ";
    		$var = 'Food Sales';	
    	} elseif ($categ === 2) { //beer
    		$fnbcatlist = " in (17, 21) ";
    		$var = 'Beer Sales';
    	} else { //non-beer beverage
    		$fnbcatlist = " in (5, 6, 16, 24, 27) ";
    		$var = 'Beverage Sales';
    	}
        $salesstart = date('Y-m-d H:i:s', strtotime($this->start . '-1 day'));
        $salesend = date('Y-m-d H:i:s', strtotime($this->end . '-1 day'));
        
        $sql = "select 'Cash', '$var', a.occupancy_id, a.sales_date, a.update_date, 
                sum(a.qty * a.unit_cost) as total
                from fnb_sales a
                where a.occupancy_id in (
					500, 1428, 1427, 24479, 24481, 37346	
                )
                and a.update_date >= '$this->start' and a.update_date <= '$this->end'
                and a.category_id $fnbcatlist
                and a.status = 'Paid'
                group by a.occupancy_id
                having sum(a.qty * a.unit_cost) > 0
            "; 
            
        $this->appendData($sql);
        //echo $sql . '<hr>';
        return $this;
    }
    
    /**
    * clears temporary variable
    */
    public function resetData() {
    	unset($this->data);
		$this->data = array();
		return $this;
    }
    
    /**
    * add data to a temp variable for processing all info together
    */
    public function appendData($sql) {
    	$res = R::getAll($sql);
    	
    	foreach ($res as $row) {
    		array_push($this->data, $row);
    	}
    	
    	return $this;
    }
    
    /**
    * cycles through all data and applies checktender function to each row
    */
    public function processTenderType() {
    	array_walk($this->data, array($this, 'checkTender'));
    	rsort($this->data);
    }
    
    /**
    * function used for array update
    * checks tendertype for the transaction and automatically updates the row
    */
    public function checkTender(&$data, $key) {
    	$occ = $data[3];
    	$date = $data[4];
    	$sql = "select if (is_debit=0, 'Credit', 'Debit') as type
    		from CardDetailsSummary 
    		where occupancy_id=$occ
    		and receipt_date='$date'";

    	$data[1] = R::getCell($sql);
    	
    	$this->totals[$data[0]][$data[1]][$data[2]] += $data[6];
    	$this->totals[$data[0]][$data[1]]['Total'] += $data[6];
    }
    
    public function displayTotals($type, $tender) {
    	$room = number_format($this->totals[$type][$tender]['Room Sales'] / self::VAT_DIV, 2);
    	$misc = number_format($this->totals[$type][$tender]['Misc Sales'] / self::VAT_DIV, 2);
    	$food = number_format($this->totals[$type][$tender]['Food Sales'] / self::VAT_DIV, 2);
    	$beer = number_format($this->totals[$type][$tender]['Beer Sales'] / self::VAT_DIV, 2);
    	$bev = number_format($this->totals[$type][$tender]['Beverage Sales'] / self::VAT_DIV, 2);
    	$typetotal = $this->totals[$type][$tender]['Total'];
    	$vat = number_format($typetotal / self::VAT_DIV * self::VAT_TAX, 2);
    	$typetotal = number_format($typetotal, 2);
    	$room = $this->totals[$type][$tender]['Room Sales'] ;
    	$misc = $this->totals[$type][$tender]['Misc Sales'];
    	$food = $this->totals[$type][$tender]['Food Sales'];
    	$beer = $this->totals[$type][$tender]['Beer Sales'];
    	$bev = $this->totals[$type][$tender]['Beverage Sales'];
    	$typetotal = $room + $misc + $food + $beer + $bev;
    	$vat = 0;//number_format($typetotal / self::VAT_DIV * self::VAT_TAX, 2);
    	$typetotal = $typetotal;
    	
$output = <<< OUT
<table>
<tr><td>$tender</td><td>$typetotal</td><td colspan=2></td></tr>
<tr><td colspan=2></td><td>Room Sales</td><td class='amount'>$room</td></tr>
<tr><td colspan=2></td><td>Misc Sales</td><td class='amount'>$misc</td></tr>
<tr><td colspan=2></td><td>Food Sales</td><td class='amount'>$food</td></tr>
<tr><td colspan=2></td><td>Beer Sales</td><td class='amount'>$beer</td></tr>
<tr><td colspan=2></td><td>Beverage Sales</td><td class='amount'>$bev</td></tr>
<tr><td colspan=2></td><td>VAT</td><td class='amount'>$vat</td></tr>
</table>
OUT;
		return $output;
    }
    
    public function displaySpecial() {
    	$retval = '';
    	foreach ($this->totals as $source => $transactions) {
    		$label = "<h3>$source</h3>";
    		//$retval.= '<pre>' . print_r($transactions, true) . '</pre>' ;
    		$srchtml = '';
    		foreach ($transactions as $tender => $types) {
    			if ($types['Total'] != 0) {
    				//start with the total
    				$typetotal = $types['Total'];
    				$srchtml.= "<tr><td>$tender</td><td>$typetotal</td><td colspan=2></td></tr>";
    				foreach ($types as $key => $value) {
    					if (!in_array($key, array('Total', 'Discount'))) {
    						$srchtml.="<tr><td colspan=2></td><td>$key</td><td class='amount'>$value</td></tr>";	
    					}
    				}  				
    			} 
    		}
    		if ($srchtml !== '') {
    			$retval .= "$label <table> $srchtml </table>";
    		}
    	}
    	
    	return $retval;
    }
    
    
}
?>
