<?php
/*
* add columns:
* - checkin , checkout
  - management type
  - 
*/
require "./config/config.inc.php";
session_start();
if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (guest_fname LIKE '".addslashes($_POST['search'])."%' 
					OR guest_lname LIKE '%".addslashes($_POST['search'])."%' 
					OR remarks LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsUsers = 10;
$pageNum_rsUsers = 0;
if (isset($_GET['pageNum_rsUsers'])) {
  $pageNum_rsUsers = $_GET['pageNum_rsUsers'];
}
$startRow_rsUsers = $pageNum_rsUsers * $maxRows_rsUsers;

if($_GET['filterall']==1) {
	$param_rsUsers = " WHERE 1 $param_search";
} elseif ($_GET['filterall']==2) {
	$param_rsUsers = " WHERE date_remitted<>'0000-00-00 00:00:00' and year(date_endorsed)>2012 ".$param_search;
} else {
	$param_rsUsers = " WHERE date_remitted='0000-00-00 00:00:00' and year(date_endorsed)>2012 ".$param_search;
}
//$query_rsUsers = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsUsers, $sortDate);
$query_rsUsers = sprintf("select * from security_receivables %s order by date_endorsed desc", $param_rsUsers);

$query_limit_rsUsers = sprintf("%s LIMIT %d, %d", $query_rsUsers, $startRow_rsUsers, $maxRows_rsUsers);

$row_rsUsers = R::getAll($query_limit_rsUsers);

if (isset($_GET['totalRows_rsUsers'])) {
  $totalRows_rsUsers = $_GET['totalRows_rsUsers'];
} else {
  $all_rsUsers = R::getAll($query_rsUsers);
  $totalRows_rsUsers = count($all_rsUsers);
}
$totalPages_rsUsers = ceil($totalRows_rsUsers/$maxRows_rsUsers)-1;

$queryString_rsUsers = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsUsers") == false && 
        stristr($param, "totalRows_rsUsers") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsUsers = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsUsers = sprintf("&totalRows_rsUsers=%d%s", $totalRows_rsUsers, $queryString_rsUsers);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsUsers + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsUsers + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

$now = date("Y-m-d H:i:s");

if($_POST["cmdRemit"]=="Remit security receivable")
{
	if (!isset($_POST['rec_id'])) {
		echo "<h3>Please select a valid receivable transaction.</h3>
		Click <a href='receivables.php'>here</a> to go back to main page.";
		exit;
	} else {
		$recid = is_array($_POST['rec_id']) ? $_POST['rec_id'][0] : $_POST['rec_id']; 
	}

	$oic = getOicList();
	$oiclist = "<select name='oicname'>";
	foreach ($oic as $o) {
		$oiclist .= "<option value='" . $o['username'] . "'>";
		$oiclist .= $o['fullname'] . "</option>";
	}
	$oiclist.="</select>";

	$retval = "<table><tr><td colspan=2>
		<fieldset id='carddetails'>
		<legend style='font-size:14px'><input type='checkbox' name='tendertype' value='Card'>Card Payment Details</legend>
		<table>
		<tr><td>
		Card Type
		</td><td nowrap=true>
		<input type='radio' name='ctype' value='AMEX' id='ctype1' />
		<label class='cardtype' for='ctype1'>AMEX</label>
		<input type='radio' name='ctype' value='JBE' id='ctype2' />
		<label class='cardtype' for='ctype2'>JBE</label>		
		<input type='radio' name='ctype' value='Visa' id='ctype3' />
		<label  class='cardtype' for='ctype3'>Visa</label>
		
		<input type='radio' name='ctype' value='Mastercard' id='ctype4' />
		<label  class='cardtype' for='ctype4'>Mastercard</label>
		<br />
		<input type='radio' name='ctype' value='BDO Card' id='ctype5' />
		<label  class='cardtype' for='ctype5'>BDO Card</label>
		
		<input type='radio' name='ctype' value='ExpressNet' id='ctype7' />
		<label  class='cardtype' for='ctype7'>ExpressNet</label>
		
		<input type='radio' name='ctype' value='Megalink' id='ctype6' />
		<label  class='cardtype' for='ctype6'>Megalink</label>
		
		</td></tr>
		<tr>
		<td><label for='newapproval'>Card Suffix</label></td>
		<td><input type='text' id='newsuffix' name='newsuffix' class='numfield'></td>
		</tr>

		<tr>
		<td><label for='newapproval'>Approval #</label></td>
		<td><input type='text' id='newapproval' name='newapproval' class='numfield'></td>
		</tr>

		<tr><td><label for='newbatch'>Batch #</label></td>
		<td><input type='text' id='newbatch' name='newbatch' class='numfield' ></td>
		</tr>

		<tr>
			<td>
			Terminal</td>
			<td>
			<select id='terminal' name='terminal'>
				<option value='BDO'>BDO</option>
				<option value='BPI'>BPI</option>
			</select>
			</td>
		</tr>
		<tr>
			<td align=right><input type='checkbox' name='newisdebit' value='1'  id='newisdebit' class='' /></td>
			<td><label for='newisdebit'>Debit / ATM</label></td>
		</tr>
		</table>
		</fieldset>
	</td></tr></table>";
	$form = "<h4>Confirm Transaction</h4>";
	$form.="<form method='post'>";
	$form.=getDetails($recid);
	$form.=$retval;
	$form.="<fieldset><legend>OIC Authorization</legend><div style='width: 240px;'><label style='float:left'>OIC:</label>$oiclist<div style='clear:both'></div></div>";
	$form.="<div style='width: 240px;'><label style='float:left'>Password:</label><input style='float:right' type='password' name='oicpass' ><div style='clear:both'></div></div></fieldset>";
	$form.="<input type='submit' name='cmdSubmit' value='Submit'>";
	$form.="<input type='submit' name='cmdSubmit' value='Cancel'>";
	$form.="<input type='hidden' name='rec_id' value='$recid'>";
	$form.="</form>";
	echo $form;
	exit;
} elseif ($_POST['cmdSubmit'] == 'Submit') {
	
	//validate oic credentials
	if (isAuthorized($_POST['oicname'], $_POST['oicpass'])) {
		echo "<h3>Authorization Accepted.</h3>";
		$now = date('Y-m-d H:i:s');
		$_sr_id = $_POST["rec_id"];
		recieve_security($_sr_id, $now);
		$sql = " update security_receivables set date_remitted = '$now' where sr_id='$_sr_id'";
		R::exec($sql);
		echo "<h4>Saving transaction.</h4>Click <a href='receivables.php'>here</a> to go back to main page.";
		exit;
	} else {
		echo "<h3>Authorization Failed.</h3>
		Click <a href='receivables.php'>here</a> to go back to main page.";
		exit;
	}
}

function getDetails($recid) {
	$sql = "select date_endorsed, amount, guest_fname, guest_lname, remarks
		from security_receivables where sr_id='$recid'";
	
	$row = R::getRow($sql);
	
	$retval = "<h4>Details: </h4><ul><li>Date Endorsed: ". $row['date_endorsed'] ."</li>";
	$retval.= "<li>Guest: ". $row['guest_fname'] . ' ' . $row['guest_lname'] ."</li>";
	$retval.= "<li>Amount: ". $row['amount'] ."</li>";
	$retval.= "<li>Remarks: ". $row['remarks'] ."</li></ul>";
	return $retval;
}

function isAuthorized($user, $pass) {
	$passed = false;
	$sql = "select count(*) from users 
		where username='$user' and userpass='$pass' and group_id < 5";
	
	$row = R::getCell($sql);
	if ($row) {
		$passed = true;
	}
	return $passed;
}

function completeUpdate() {
	$now = date('Y-m-d H:i:s');
	foreach($_POST["rec_id"] as $sr_id)
	{
		$_sr_id .= $sr_id . ",";
		recieve_security($sr_id, $now);
	}
	
	$sql = " update security_receivables set date_remitted = '$now' where sr_id in (".substr_replace($_sr_id ,"",-1).") ";
	R::exec($sql);
	header("location:receivables.php");
}

function recieve_security($sr, $date) {
	$user = $_SESSION["hotel"]["userid"];
	//get amount
	$sql = "select amount, remarks from security_receivables where sr_id='$sr'";
	$res = R::getRow($sql);
	$amount = $res['amount'];
	$remarks = $res['remarks'];
	//get occupancy, type, refid
	$tmp = explode('[', $remarks);
	$type = 'Security Reversal';
	if ( trim($tmp[0]) == 'Management Endorsement') {
		$type = 'ME Reversal';
	}
	list($refid) = explode(']:', $tmp[1]);
	$sql2 = "select occupancy_id from salesreceipts where salesreceipt_id='$refid'";
	$occ = R::getCell($sql2);
	
	//check tendertype
	$type = 'Cash';
	if (isset($_POST['tendertype'])) {
		//set as card transaction type
		$type=$_POST['tendertype'];
	}
	//insert into salesreciepts (reference_id=$sr, tendertype=SR Reversal/ME Reversal, occupancy_id)
	$sql3 = "insert into salesreceipts( receipt_date, reference_id, occupancy_id, tendertype, amount, update_by) values('$date', '$sr', '$occ', '$type', '$amount', '$user')";
	$res3 = R::exec($sql3);
	$newsalesid = R::getInsertId();
	//currentcash
	if ($type ==  'Cash') {
		include_once('currentcash.function.php');
		setcurrentcash($amount,'in',$user,$date);
	} elseif ($type=='Card') {
		//save card payment details
		$sql = " insert into card_payment_details (
				salesreceipt_id, card_type, approval_code, batch_number,
				card_suffix, terminal, is_debit
				)	values ('$newsalesid','{$_POST["ctype"]}',
					'{$_POST["newapproval"]}',
					'{$_POST["newbatch"]}',
					'{$_POST["newsuffix"]}',
					'{$_POST["terminal"]}',
					'{$_POST["newisdebit"]}'
					)";
		R::exec($sql);
	}
}

function getSalesReceiptId($str) {
	$tmp1 = explode('[', $str);
	$tmp2 = explode(']', $tmp1[1]);
	return trim($tmp2[0]);
}

function getDoorName($salesId) {
	$sql = "select c.door_name 
			from salesreceipts a, occupancy b, rooms c
			where a.occupancy_id=b.occupancy_id
			and b.room_id=c.room_id
			and a.salesreceipt_id=$salesId
		";
	return R::getCell($sql);
}

function getOicList() {
	$sql = "select username, fullname from users where group_id=4";
	return R::getAll($sql);
}

?>
<html>
<head>
<title>Security Receivables</title>
<script type="text/javascript" src="../js/custom.js"></script>
<link href="../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>
<form name="form1" method="post" action="" id='thisform'>
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">RECEIVABLES</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Receivables:</font>&nbsp;
			  <input type"text" name="search" id="search" class="textbox-search-style">&nbsp;
			  <input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;
			  <input name="cmdRemit" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to remit a security receivable(s). Are you sure you want to continue?');return document.MM_returnValue" value="Remit security receivable" class="buttons" />
			  <br />
			  <?php if($_GET['filterall'] == 1) { ?>
				<a href="?filterall=0">View Unpaid</a> |
				<a href="?filterall=2">View Remitted</a>
			  <?php } elseif($_GET['filterall'] == 2) {?>
			    <a href="?filterall=1">View All</a> |
				<a href="?filterall=0">View Unpaid</a>
				<?php } else {?>
				<a href="?filterall=1">View All</a> |
				<a href="?filterall=2">View Remitted</a>
				<?php }?>
				</p>
			  </div></td>
			  
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" value="0" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="20%" nowrap><strong style="color:#678197;">Guest Name</strong></td>
				  <td align="left" valign="middle" width="20%" nowrap><strong style="color:#678197;">Date Endorsed</strong></td>
				  <td align="left" valign="middle" width="20%" nowrap><strong style="color:#678197;">Date Remitted</strong></td>
				  <td align="left" valign="middle" width="20%"><strong style="color:#678197;">Check IN</strong></td>
				  <td align="left" valign="middle" width="20%"><strong style="color:#678197;">Check OUT</strong></td>
				  <td align="left" valign="middle" width="20%"><strong style="color:#678197;">Amount</strong></td>
				  <td align="left" valign="middle" width="20%"><strong style="color:#678197;">Type</strong></td>
				  <td align="center" valign="middle" width="20%"><strong style="color:#678197;">Room</strong></td>
				  <td align="center" valign="middle" width="20%"><strong style="color:#678197;">Remarks</strong></td>
                </tr></thead>
                <?php if ($totalRows_rsUsers > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php foreach ($row_rsUsers as $row_rsUser) { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
					$tmp = explode('[', $row_rsUser['remarks']);
					list($referno) = explode(']:', $tmp[1]);
					$sql = "select a.actual_checkin, a.actual_checkout from occupancy a, salesreceipts b where a.occupancy_id=b.occupancy_id
					    and b.salesreceipt_id='$referno'";
					$res = R::getRow($sql);
					$checkin = $res['actual_checkin'];
					$checkout = $res['actual_checkout'];
					
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider">
              		<input name="rec_id[]" type="checkbox" 
              				class="options" 
              				id="rec_id<?php echo $bgNumber?>" 
              				onClick="toggleController(this)" 
              				value="<?php echo $row_rsUser['sr_id']; ?>">
              		</td>
				  <td align="left"><?php echo $row_rsUser['guest_fname']." ".$row_rsUser['guest_lname']; ?></td>
                  <td align="left"><?php echo date("m/d/y - g:i A", strtotime($row_rsUser['date_endorsed'])); ?></td>
				  <td align="left"><?php if($row_rsUser['date_remitted']=='0000-00-00 00:00:00') echo "Not remitted yet"; else echo date("m/d/y - g:i A", strtotime($row_rsUser['date_remitted'])); ?></td>				 
				  
				  <td align="left"><?php echo $checkin ?></td>
				  <td align="left"><?php echo $checkout ?></td>
				  <td align="left"><?php echo number_format($row_rsUser['amount'],2) ?></td>
				  <td align="left">
							<?php 
							if(stripos($row_rsUser['remarks'],'Management Endorsement')===false) { 
								echo 'Security Deposit'; 
							}else{
								echo 'Management Endorsement' ;
							}?>
				  </td>
				  <td>
				  <?php
					$salesId = getSalesReceiptId($row_rsUser['remarks']);
					$doorname = getDoorName($salesId);
					echo $doorname;
				  ?>
				  </td>
				  <td align="center">
				  <?php
					$data = explode(']:', $row_rsUser['remarks']);
					echo $data[1];
				  ?>
				  </td>
                </tr>
                <?php } ?>
                <?php } // Show if recordset not empty ?>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsUsers > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsUsers > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsUsers=%d%s", $currentPage, 0, $queryString_rsUsers); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsUsers=%d%s", $currentPage, max(0, $pageNum_rsUsers - 1), $queryString_rsUsers); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsUsers) {
    printf('<a href="'."%s?pageNum_rsUsers=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsUsers.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsUsers < $totalPages_rsUsers) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsUsers=%d%s", $currentPage, min($totalPages_rsUsers, $pageNum_rsUsers + 1), $queryString_rsUsers); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsUsers=%d%s", $currentPage, $totalPages_rsUsers, $queryString_rsUsers); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsUsers == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>
