<?php
/**
* newdailyreport.function.php
*/

function getReportVer2($cashdeclaration,$l)
{
	$startshift = getStartTimeFromDB();
	$endShift = getEndTimeFromDB();
	$shiftno = getCurrentShiftNum($startshift);


	$shift= new shift(array('date'=>date('Y-m-d',strtotime($startshift)),'shiftno'=>$shiftno));
	$shift->getShiftDuration();

	if ($l) {
		$end = $endShift;
	} else {
		$end = $shift->shiftEnd;
	}

	echo "<hr>$startshift ::: $end ::: $endShift ::: $shiftno<hr>";

	//$startshift = $shift->shiftStart;
	$report = new salesreceiptsreport( array('start'=>$startshift, 'end'=>$end) );

	if($l) {
		$ret = "SHOGUN 2: SHIFT END REPORT";
	}else{
		$ret = "SHOGUN 2: MID SHIFT REPORT";
	}
	$ret .= "\n";
	$ret .= date("l F d, Y g:i:s A");
	$ret .= "\n";
	$ret .= "\n";
	/*
	- Sales Summary
	  - Shift Sales
	  - Intransit Sales
	  - Intransit Receivable

	- Checkout Sales Breakdown
	  - Food
	  - Beverages
	  - Beer
	  - Misc

	# checkouts
	# Food transactions / room
	*/

	$shiftSales = getShiftSales($startshift, $shiftno, '1020');
	$intransitSales = getIntransitSales($startshift, $shiftno, '1035');
	$intransitRecievables = getIntransitReceivables($startshift, $end);

    $roomSales = getCheckoutRoomDeposits($startshift,$end) + getCheckoutExtensions($startshift,$end);
	$foodSales = getFoodCheckoutSalesByCategory($startshift, $end, '21, 17, 16', $inclusion = ' not ');
	$beer = getFoodCheckoutSalesByCategory($startshift, $end, '21, 17', $inclusion = '');
	$beverage = getFoodCheckoutSalesByCategory($startshift, $end, '16', $inclusion = '');
	$misc = getCheckoutMiscTotalAmount($startshift,$end,$lobbyid);
	$adjustments = getCheckoutRoomAdjustmentTotal($startshift,$end);
	$grosssales = $rooms+$foods+$miscs+$beers+$adjustments;


	$drooms = getCheckoutDiscountRoomTotalAmount($startshift,$end);
	$dfoods = 0;
	$dmiscs = 0;

	$deposit = getTotalDeposit($startshift,$end,$lobbyid);
	$refund = getTotalRefund($startshift,$end,$lobbyid);

	$creditCard = getCheckoutPaidByCard($startshift,$end);
	$safeKeep = getSafekeepAmountByTimeframe($startshift,$end);

	$currentcash = getCurrentCash($l);
	$totalcashdeclared = $creditCard + $safeKeep + $cashdeclaration;
	$overshortages =  $cashdeclaration-$currentcash;

	$securityRedemption = getSecurityRedemption($startshift, $end);
	$roomTotal = getPrintableAmountRoomTotal($startshift, $end);
	$salesBreakdown = "\n\n".$report->getPrintableSalesreceipts();
	list($reservationDeposit, $reservationClaim) = getReservationDeposits($startshift, $end);

	$ret .= <<<REPORT

=== Sales Summary ===

Shift Sales:           $shiftSales
Intransit Sales:       $intransitSales
Intransit Recievables: $intransitRecievables


=== Checkout Sales Breakdown ===

Room Sales:            $roomSales
Food Sales:            $foodSales
Beer:                  $beer
Beverage:              $beverage
Misc:                  $misc

=== Cashier Summary ===

Cash Declaration:      $cashdeclaration
Cash on Hand:          $currentcash
Safekeep:              $safeKeep
Credit Card:           $creditCard
Security Redemption:   $securityRedemption

Reservation Deposits:  $reservationDeposit
Reservation Claims:    $reservationClaim

Total Cash Declared:   $totalcashdeclared
Over/Shortage:         $overshortages


$salesBreakdown

$roomTotal

REPORT;

	return $ret;
}

function getTotalFromCLPosting($date, $shift, $glcode) {
    $sql = "select sum(dr)
            from creditline_transactions
            where post_date = date('$date')
            and post_shift = $shift
            and glcode in ($glcode)
            ";
    return R::getCell($sql);
}

function getShiftSales($date, $shift, $glcode) {
    $sql = "select sum(dr)
            from creditline_transactions a, occupancy b
            where post_date = date('$date')
            and post_shift = $shift
            and glcode in ($glcode)
            and a.occupancy_id = b.occupancy_id
            and date(b.actual_checkout) = date('$date')
            ";
	return R::getCell($sql);
}

function getIntransitSales($date, $shift, $glcode) {
    $sql = "select sum(dr)
            from creditline_transactions a, occupancy b
            where post_date = date('$date')
            and post_shift = $shift
            and glcode in ($glcode)
            and a.occupancy_id = b.occupancy_id
            and b.actual_checkout = '0000-00-00 00:00:00'
            ";
	return R::getCell($sql);
}

function getIntransitReceivables($startshift, $end) {
    $sql = "select sum(amount) from security_receivables
			where date_endorsed >= '$startshift' and date_endorsed <='$end'
		";
	return R::getCell($sql);
}

function getSecurityRedemption($startshift, $end) {
    $sql = "select sum(amount)from security_receivables
			where date_remitted >= '$startshift' and date_remitted <='$end'
		";
	return R::getCell($sql);
}

function getFoodCheckoutSalesByCategory($startdt, $end, $categlist, $inclusion = '') {
    $_sql = "select sum(unit_cost*qty) as cost from fnb_sales a, occupancy b
			where a.status in ('Paid')
			and a.category_id $inclusion in ($categlist)
			and a.update_date >= '$startdt'
			and a.update_date <= '$end'
			and a.occupancy_id = b.occupancy_id
			and b.actual_checkout >= '$startdt'
			and b.actual_checkout <= '$end'
			";
	$res = R::getAll($_sql);
	$gtotal = 0;
	foreach ($res as $r) {
		$total = $r['cost'];
	    $gtotal += $total;
	}
	return $gtotal;
}

function getFoodIntransitSalesByCategory($startdt, $end, $categlist, $inclusion = '') {
    $_sql = "select sum(unit_cost*qty) from fnb_sales a, occupancy b
			where a.status in ('Printed')
			and a.category_id $inclusion in ($categlist)
			and a.update_date >= '$startdt'
			and a.update_date <= '$end'
			and a.occupancy_id = b.occupancy_id
			and b.actual_checkout = '0000-00-00 00:00:00'
			";
	return R::getCell($_sql);
}

function getReservationDeposits($startshift, $end)
{
	$sql = "select amount_deposit, amount_claimed from reservation_transactions
			where transaction_date >= '$startshift' and transaction_date <='$end'
		";
	$res = R::getAll($sql);

	$deposits = $claims = 0;

	foreach ($res as $r) {
		$dep = $r['amount_deposit'];
		$claim = $r['amount_claimed'];
		$deposits += $dep;
		$claims += $claim;
	}

	return array($deposits, $claims);
}



?>
