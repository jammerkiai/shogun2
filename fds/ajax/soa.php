<?php
/**
* soa.php
*/

session_start();
include_once("config/config.inc.php");
include_once("reporting.php");
include_once("printerfns.php");

$room =($_GET["roomid"]) ? $_GET["roomid"] : $_POST["roomid"];
$obj = new Reporting($room);

if($obj->occupied > 0) {
	if($_GET["print"]==1) {
		$retval=chr(hexdec('1B')).chr(hexdec('4D')).chr(1);
		$retval.=$obj->printablesoa()."\n\n\n\n\n";
		$retval.=chr(hexdec('1D')).chr(hexdec('56')).chr(49);	
		$file = "soa{$room}.txt";
		$fp = fopen("reports/" .$file, "w");
		fwrite( $fp,$retval);
		fclose($fp);
		//shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
		$prnt = new printerfns();
		$prnt->sendToPrinter('front', "reports/" . $file);
		
	}elseif($_GET["print"]==2){
		$retval=chr(hexdec('1B')).chr(hexdec('4D')).chr(1);
		$retval.=$obj->printablefinalsoa()."\n\n\n\n\n";
		$retval.=chr(hexdec('1D')).chr(hexdec('56')).chr(49);	
		$file = "soa{$room}.txt";
		$fp = fopen("reports/" .$file, "w");
		fwrite( $fp,$retval);
		fclose($fp);
		//shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
		$prnt = new printerfns();
		$prnt->sendToPrinter('front', "reports/" . $file);
	}

	$retval= "
		<h1>Statement of Account</h1>
		<br />
		<table width='300'>
		<tr>
		<td>Room</td><td>$obj->doorname </td>
		<td>Check-in</td><td>".date("Y-m-j h:i A",strtotime($obj->checkindate))."</td>
		</tr>
		<tr>
		<td>Rate</td><td>$obj->ratename </td>
		<td>Exp Checkout</td><td>".date("Y-m-j h:i A",strtotime($obj->etd))."</td>
		</tr>
		<tr>
		<td>Shift</td><td>$obj->shift </td>
		<td>&nbsp;</td><td>&nbsp;</td>
		</tr>
		</table>";
	
	$retval.= $obj->getRoomCharges();
	$retval.= $obj->getCheckoutFnbSales();
	$retval.= $obj->getCheckoutMosSales();
	$retval.= $obj->getPayments();
	$retval.= $obj->displayTotal();
	$retval.= $obj->displayTotalBalance();
	
	echo $retval;
}else{
	echo "Not checked in.";
	exit;
}


?>
