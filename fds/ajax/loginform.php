<?php

require_once("config/config.inc.php");
require_once("common.inc.php");



/**
* login authentication
*/



if( !empty($_POST) ) {

	$user = $_POST["uname"];
	$pass = $_POST["pword"];
	
	$sql = "select a.user_id, a.group_id, b.group_name, a.fullname 
		from users a, groups b
		where a.group_id=b.group_id
		and a.username=binary('$user') 
		and a.userpass=binary('$pass')
	";
	
	$user = R::getRow($sql);

	if($user) {
		session_start();
		$_SESSION["hotel"]["logged"]=true;
		$_SESSION["hotel"]["userid"]=$user['user_id'];
		$_SESSION["hotel"]["groupid"]=$user['group_id'];
		$_SESSION["hotel"]["groupname"]=$user['group_name'];
		$_SESSION["hotel"]["fullname"]=$user['fullname'];
		$shiftstatus = hasShiftStart();
		$msg[0]= 'Identity confirmed. Please perform shift start activities first.';
		$msg[1] = 'Identity confirmed. Welcome ' . $user['fullname'];
		
		$resp = array('success'=>true, 'msg'=> $msg[$shiftstatus], 'shiftstatus'=>$shiftstatus);
		$json =  JSON_encode($resp);
		accessLogs($user['user_id'],"Login",$json);
		die($json);
	}
}

$resp = array('success'=>false,'msg'=>"Authentication $user failed. Please contact your administrator."  );
$json =  JSON_encode($resp);
accessLogs('0',"Login",$json);
die($json);
