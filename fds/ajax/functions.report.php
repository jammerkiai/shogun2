<?php
/**
* checkout functions
*/

function getTotalCostByItemID() {

}

function getLobbyFoodSalesByItemNotIn($lobbyid, $start, $end, $arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$_sql = "select sum(unit_cost*qty) from fnb_sales 
			where status in ('Paid')
			and category_id not in ($strItems)
			and occupancy_id = '$lobbyid'
			and update_date >= '$start' 
			and update_date <= '$end' ";
	return R::getCell($_sql);
}

function getLobbyFoodSalesByItemIn($lobbyid, $start, $end, $arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$_sql = "select sum(unit_cost*qty) from fnb_sales 
			where status in ('Paid')
			and category_id in ($strItems)
			and occupancy_id = '$lobbyid'
			and update_date >= '$start' 
			and update_date <= '$end' ";
	return R::getCell($_sql);
}

function getLobbyMiscSalesByItemNotIn($lobbyid, $start, $end, $arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$_sql = "select unit_cost*qty from room_sales 
		where status in ('Paid')
		and category_id not in ($strItems)
		and occupancy_id = '$lobbyid'
		and update_date >= '$start' 
		and update_date <= '$end' ";
	return R::getCell($_sql);
}

function getLobbyMiscSalesByItemIn($lobbyid, $start, $end, $arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$_sql = "select unit_cost*qty from room_sales 
		where status in ('Paid')
		and category_id in ($strItems)
		and occupancy_id = '$lobbyid'
		and update_date >= '$start' 
		and update_date <= '$end' ";
	return R::getCell($_sql);
}

function getTotalRoomSalesByItemOccupancy($itemid,$occupancyid) {
	$sql = " select sum(unit_cost*qty) from room_sales 
			where status in ('Paid')
			and item_id = '$itemid'
			and occupancy_id = '$occupancyid' ";
	return R::getCell($_sql);
}

function getTotalFoodSalesByItemOccupancyNotIn($occupancy, $arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$_sql = "select sum(unit_cost*qty) from fnb_sales 
			where status in ('Paid')
			and category_id not in ($strItems)
			and occupancy_id = '$occupancy'";
	return R::getCell($_sql);
}

function getTotalFoodSalesByItemOccupancyIn($occupancy, $arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$_sql = "select sum(unit_cost*qty) from fnb_sales 
			where status in ('Paid')
			and category_id in ($strItems)
			and occupancy_id = '$occupancy'";
	return R::getCell($_sql);
}

function getTotalMiscSalesByItemOccupancyIn($occupancy,$arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$sql = " select sum(unit_cost*qty) from room_sales 
			where status in ('Paid')
			and item_id in ($strItems)
			and occupancy_id = '$occupancy' ";
	return R::getCell($_sql);
}

function getTotalMiscSalesByCategoryOccupancyNotIn($occupancy,$arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$sql = " select sum(unit_cost*qty) from room_sales 
			where status in ('Paid')
			and category_id not in ($strItems)
			and occupancy_id = '$occupancy' ";
	return R::getCell($_sql);
}


?>