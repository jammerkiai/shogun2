<?php
/** yearlysalessummary **/

session_start();
include_once("config/config.inc.php");
require_once('acctg/class.baseobject.php');
require_once('acctg/class.shift.php');
require_once('acctg/class.report.php');
require_once('acctg/reportfns.php');

$startmonth = ($_POST['startmonth']) ? $_POST['startmonth'] : 1;
$startyear = ($_POST['startyear']) ? $_POST['startyear'] : date('Y');
$endmonth = ($_POST['endmonth']) ? $_POST['endmonth'] : date('m');
$endyear = ($_POST['endyear']) ? $_POST['endyear'] : date('Y');

function getMonth($name, $selected) {
	$ret = "<select name='$name' id='$name'>";
	for($x=1; $x <=12; $x++) {
		$month = date('F', strtotime('2010-' . $x . '-01'));
		$ret.="<option value='$x' ";
		if($selected==$x) $ret.=" selected ";
		$ret.=">$month</option>";
	}
	$ret.="</select>";
	return $ret;
}

function getYear($name, $selected) {
	$ret = "<select name='$name' id='$name'>";
	for($x=2010; $x <=2019; $x++) {
		$ret.="<option value='$x' ";
		if($selected==$x) $ret.=" selected ";
		$ret.=">$x</option>";
	}
	$ret.="</select>";
	return $ret;
}

?>
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery-ui.js" type="text/javascript"></script>
<body>
<div>
<form method="post">
FROM: <?php echo getmonth('startmonth', $startmonth) . getyear('startyear', $startyear) ?>
TO: <?php echo getmonth('endmonth', $endmonth) . getyear('endyear', $endyear) ?>

<input type="submit" name="submit" value="go" />
<input type="submit" name="submit" value="export to excel" />
</form>
</div>
<?php
if(strlen($startmonth) === 1) $startmonth = '0' . $startmonth;
if(strlen($endmonth) === 1) $endmonth = '0' . $endmonth;

$start = $startyear . $startmonth;
$end = $endyear . $endmonth;


$sql  ="select * 
		from yearlysalessummary 
		where postdate between '$start' and '$end'
		order by postdate
		";

$arrReport = array(
		'title'    =>  "Yearly Sales Summary",
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
$report->show();


?>
</body>
<style>
body,h1,h2,h3,h4,h5 {
	font-family: arial, helvetica, sans-serif;
	margin:0;
	margin-width:0;
	margin-height:0;
	font-size:14px;
}

.toolbar {
			background-color:#cccccc;
			padding:4px;
		}

div {
	font-size:13px;
}

h1 {
	font-size:14px;
	padding-top:8px;
	border-top:1px solid #111199;
	
}

table {
	font-size:12px;
	padding:2px;
	border:1px solid #dddddd;
}

th, td{
	background-color:#eeeeee;
	padding:2px;
}
.summary th, td {
	text-align:right;
	border-bottom:1px dotted #cccccc;
}
td {
	background-color:#fcfcfc;
}

.aggregates th{
	border-top:1px solid #000000;
}
</style>

