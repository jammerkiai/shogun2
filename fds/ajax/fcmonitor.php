<?php
/*
fcmonitor.php
*/
include_once('forecast.function.php');

$shift = new shift(array());
$thisshift = $shift->shiftno;
$now = date('Y-m-d');

runfc($now, $thisshift);

?>
<style>
body {font-family:helvetica;margin:0px;}
table {font-size:12px;font-family:helvetica;}
th {width:auto; border-bottom:1px solid #cccccc;}
td {border-bottom:1px solid #cccccc; width:auto; text-align:center }
.amt {text-align:right}
h3 {font-size:12px;}
.menubar {background-color:#eeeeee;font-size:11px;padding:4px;border-bottom:1px solid #cccccc;}
.content {padding:10px; font-size:11px}
.message {display:inline; color:#ff0000;}
</style>
<div class="menubar">
<div style="display:inline">
Today: <?php echo $now ?>
</div>&nbsp;&nbsp;&nbsp;&nbsp;
<div style="display:inline">
Shift: <?php echo $thisshift ?>
</div>
&nbsp;&nbsp;&nbsp;&nbsp;
<a href="?print=1" onclick="return confirm('Continue Printing?')"> Print </a>
</div>
<div>
<?php echo getForecastList($now, $thisshift, $_GET['print']); ?>
</div>