<?php
/**
    specialreceipt.php
*/
include_once('config/config.inc.php');
require_once('class.forecast.php');
session_start();

$today = isset($_POST['fcdate']) ? $_POST['fcdate'] : ( isset($_GET['d']) ? $_GET['d'] : date('Y-m-d') );
$shift = isset($_POST['fcshift']) ? $_POST['fcshift'] : ( isset($_GET['s']) ? $_GET['s'] : forecast::getshift() ) ;
$occ = $_GET['occ'];

//get occupancy details
$sql = "select a.*, b.door_name 
        from occupancy a, rooms b 
        where a.room_id = b.room_id
        and a.occupancy_id='$occ'";
$occdetail = R::getRow($sql);
$occdetail = json_decode(json_encode($occdetail));
//get guest details
$sql = "select a.* 
        from guests a, guest_history b
        where a.guest_id = b.guest_id
        and b.occupancy_id='$occ'";
$guestdetail = R::getRow($sql);     
$guestdetail = json_decode(json_encode($guestdetail));      

if (isset($_POST) && $_POST['submit'] === 'save') {
    $rcpt = $_POST['rcptno'];
    $regdate = $_POST['fcdate'];
    $regshift = $_POST['fcshift'];
    $occ = $_POST['occ'];
    $user = $_SESSION['hotel']['userid'];
    
    $sql = "insert into occupancy_receipts 
            (occupancy_id, receipt_date, receipt_shift, receipt_number, issued_by) 
            values
            ('$occ', '$regdate', '$regshift', '$rcpt', '$user')";
    R::exec($sql);
    
    if (isset($_POST['rcbsel'])) {
        $selected = implode(',', $_POST['rcbsel']);
        $sql = "update room_sales 
                set regshift = '$regshift', 
                    regdate = '$regdate',
                    regflag = '$rcpt'
                where charge_to = '$occ'
                and roomsales_id in ($selected); 
                ";
        R::exec($sql);
    }           
    
    if (isset($_POST['fcbsel'])) {
        $selected = implode(',', $_POST['fcbsel']);
        $sql = "update fnb_sales 
                set regshift = '$regshift', 
                    regdate = '$regdate',
                    regflag = '$rcpt'
                where charge_to = '$occ'
                and fnbsales_id in ($selected); 
                ";
        R::exec($sql);
    }
    header("location: specialreceipt.php?occ=$occ");
}


//room sales
$sql = "select a.*, b.sas_description
        from room_sales a, sales_and_services b
        where a.item_id = b.sas_id
        and a.occupancy_id = '$occ'
        and a.status='Paid'
        ";

$res = R::getAll($sql);
$data = '';
foreach ($res as $row) {
    $row = json_decode(json_encode($row));
    $total = $row->unit_cost * $row->qty;
    $data .= '<tr>';
    $sid = $row->roomsales_id;
    if ($row->regflag === '0') {
        $data .= '<td><input type="checkbox" value="' . $sid 
                    . '" name="rcbsel[]" class="cbitem rs" data-total="' . $total . '"></td>';
    } else {
        $data .= '<td>&nbsp;</td>';
    }
    $data .= '<td>'. $row->sales_date .'</td>';
    $data .= '<td>'. $row->sas_description .'</td>';
    $data .= '<td>'. $row->unit_cost .'</td>';
    $data .= '<td>'. $row->qty .'</td>';
    $data .= '<td>'. $total .'</td>';
    $data .= '<td>'. $row->status .'</td>';
    $data .= '<td>'. $row->regflag .'</td>';
    $data .= '<td>'. $row->regdate .'</td>';
    $data .= '<td>'. $row->regshift .'</td>';
    $data .= '</tr>';
}

//food sales
$sql = "select a.*, b.fnb_name
        from fnb_sales a, fnb b
        where a.item_id = b.fnb_id
        and a.status='Paid'
        and a.charge_to = '$occ'
        ";

$res = R::getAll($sql);
$data2 = '';
foreach ($res as $row) {
    $row = json_decode(json_encode($row));
    $total = $row->unit_cost * $row->qty;
    $data2 .= '<tr>';
    
    $sid = $row->fnbsales_id;
    if ($row->regflag === '0') {
        $data2 .= '<td><input type="checkbox" value="' . $sid . '" 
                        name="fcbsel[]" class="cbitem fs" data-total="' . $total . '"></td>';
    } else {
        $data2 .= '<td>&nbsp;</td>';
    }
    
    $data2 .= '<td>'. $row->sales_date .'</td>';
    $data2 .= '<td>'. $row->fnb_name .'</td>';
    $data2 .= '<td>'. $row->unit_cost .'</td>';
    $data2 .= '<td>'. $row->qty .'</td>';
    $data2 .= '<td>'. $total .'</td>';
    $data2 .= '<td>'. $row->status .'</td>';
    $data2 .= '<td>'. $row->regflag .'</td>';
    $data2 .= '<td>'. $row->regdate .'</td>';
    $data2 .= '<td>'. $row->regshift .'</td>';
    $data2 .= '</tr>';
}



?>
<html>
<head>
<title>Special Receipt Issuance</title>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<style>
body {font-family:helvetica;margin:0px;}
table {font-size:12px;font-family:helvetica;}
th {width:auto; border:1px solid #cccccc; padding: 2px 6px;}
.totals {color:#00F;font-weight:bold;}
td {border-bottom:1px solid #cccccc; text-align:center }
.amt {text-align:right}
h3 {font-size:12px;}
.menubar {background-color:#eeeeee;font-size:11px;padding:4px;border-bottom:1px solid #cccccc;}
.content {padding:10px; font-size:11px}
.message {display:inline; color:#ff0000;}
.targets {color: orange;}
.actuals {color: green;}
#summary {margin: 10px;width: auto;border: 1px solid #ddd;display: inline-block;}
#summary > div > span {display: inline-block; width: 200px;padding: 2px 5px;}
.sumlabel {background-color: #eee}
#rsales, #fsales, #salestax, #totalsales {text-align:right; border-bottom: 1px dotted #eee;background-color: white}
</style>
</head>
<body>
<form method="post">
<div class="menubar">
<strong>Issue Special Receipt ---|  </strong>
<div style="display:inline">
Date Today: <input type="text" value="<?php echo $today ?>" name="fcdate" id="fcdate" />
</div>
<div style="display:inline">
Shift: <input type="text" value="<?php echo $shift ?>" name="fcshift" id="fcshift" size="4" maxlength="1"/>
</div>
<div style="display:inline">
Receipt No.: <input type="text" value="<?php echo forecast::getlastreceiptnumber() ?>" name="rcptno" id="rcptno" size="4" />
</div>

<input type="submit" name="submit" value="refresh" />
<input type="submit" name="submit" value="save" />
<input type="hidden" name="occ" value="<?php echo $occ ?>" />
<div class="message"><?php echo $result ?></div>
</div>
<table>
<thead>
<?php
$headers = array('<input type="checkbox" value="1" id="cbmaster" class="cbmaster" />', 
            'Sales Date', 'Description', 'Unit Cost', 'Qty' , 
                'Total', 'Status', 'Receipt No.', 'Receipt Date', 'Shift');
foreach ($headers as $label) {
    echo "<th>$label</th>";
}
?>
</thead>
<tbody>
<?php
echo $data;
echo $data2;
?>
</tbody>

</table>
<h3>Receipt Details Summary</h3>
<div id="summary">
    <div><span class="sumlabel">Guest Name</span>
    <span><?php if (isset($guestdetail)) echo $guestdetail->firstname . ' ' . $guestdetail->lastname ?></span></div>
    <div><span class="sumlabel">Checkin Date</span><span><?php echo $occdetail->actual_checkin ?></span></div>
    <div><span class="sumlabel">Checkout Date</span><span><?php echo $occdetail->actual_checkout ?></span></div>
    <div><span class="sumlabel">Room Number</span><span><?php echo $occdetail->door_name ?></span></div>
    <div><span class="sumlabel">Room Sales</span><span id="rsales">0</span></div>
    <div><span class="sumlabel">Food Sales</span><span id="fsales">0</span></div>
    <div><span class="sumlabel">Tax</span><span id="salestax">0</span></div>
    <div><span class="sumlabel">Total Sales</span><span id="totalsales">0</span></div>
</div>
</form>
<script>

$(document).ready(function(){
    $('#fcdate').datepicker({dateFormat:'yy-mm-dd'});
    
    $('#cbmaster').on('click', function(e){
        $('.cbitem').attr('checked', $(this).is(':checked'));
        updateDetailTotals(); 
    });
    
    $('.cbitem').on('change', function(){
       updateDetailTotals(); 
    });
});

function updateDetailTotals() {
    var total = 0,
        tax = 0,
        rs = 0,
        fs = 0;
    $('.cbitem').each(function(){
        if ($(this).is(':checked') ) {
            var itemtotal = $(this).data('total') * 1;
            total += itemtotal;
            if ($(this).hasClass('rs')) {
                rs += itemtotal;
            } else {
                fs += itemtotal;
            }
        }
    });
    
    $('#rsales').html((rs/1.12).toFixed(2));
    $('#fsales').html((fs/1.12).toFixed(2));
    $('#salestax').html((total/1.12 * 0.12).toFixed(2));
    $('#totalsales').html(total.toFixed(2));
    
}

</script>
</body>
</html>
