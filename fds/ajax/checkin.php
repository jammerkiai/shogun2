<?php
/**
* checkin.php
*/

session_start();
include_once("config/config.inc.php");
include_once("reporting.php");
include_once("currentcash.function.php");
include_once("SalesLedgerPoster.php");

$user = $_SESSION["hotel"]["userid"];

function getshift($date) {
	list($d, $t) = explode(" ", $date);
	list($h, $m, $s) = explode(":", $t);
	$sql = "select shift_id from shifts where $h between shift_start and shift_end";
	$shifts = R::getCol($sql)[0];
	$shift = $shifts['shift_id'];

	if($h < 6 or $h >= 22)
	{
		$shift = 1;
	}
	elseif( $h >= 14 ){
		$shift = 3;
	}
	elseif($h >= 6)
	{
		$shift = 2;
	}

	return  $shift;
}


function getunitprice($item) {
	$sql = "select sas_amount from sales_and_services where sas_id='$item'";
	$row = R::getCol($sql)[0];
	return $row['sas_amount'];
}

function getSafeCheckInDate() {

    $now = new DateTime();
    $time = $now->format('His');

    // 10pm transition
    if ($time > '220000' && $time < '221500') {
        $time = '221500';
    } else if ($time < '220000' && $time > '214500') {
        $time = '214500';

    // 6am transition
    } else if ($time > '060000' && $time < '061500') {
        $time = '061500';
    } else if ($time < '060000' && $time > '054500') {
        $time = '054500';

    // 2pm transition
    } else if ($time > '140000' && $time < '141500') {
        $time = '141500';
    } else if ($time < '140000' && $time > '134500') {
        $time = '134500';
    }

    $date = $now->format('Y-m-d');
    $safedate = new DateTime($date . ' ' . $time);

    return $safedate->format('Y-m-d H:i:s');

}

if($_POST["act"]=="newcheckin")
{
	/* initialize direct variable access for posted data */
	reset($_POST);
	foreach($_POST as $key => $value)
	{
		${$key}=$value;
	}

	/* cancel operation if room is not available */
	$roomIsOccupied = isRoomOccupied($newroomid);
	if ($roomIsOccupied) {
		die("This room is not available!");
	}

    /* prepare common data */
	$user = $_SESSION["hotel"]["userid"];
	$now = getSafeCheckInDate();

    $shift = getshift($now);
	$new_checkin=$now;


	/* compute duration (length of stay) */
	$duration = $new_duration * 24 + $hidden_duration;// + $new_halfduration *12 + $new_extension ;
	if($duration) {
		$indate = new DateTime($new_checkin);
		$newcheckout = date_add($indate, new DateInterval("PT".$duration."H") );
		$newcheckout = $indate->format("Y-m-d H:i:s");
	}


	/* proceed with transaction */
	if(!$roomIsOccupied)
	{

        $addtoforecast = ($register_flag) ? 1 : 0;

        $discount_rate = ($discount_rate) ? $discount_rate : '0.00';

	    /* save occupancy */
	    $sql = "insert into occupancy(room_id, actual_checkin, expected_checkout, rate_id, update_by, shift_checkin,
        regflag, discount_rate)
		    values ('$newroomid','$new_checkin','$newcheckout', '$newrateid','$user', '$shift','$addtoforecast', '$discount_rate' ) ";

		R::exec($sql);
	    $newoccupancy = R::getInsertID();

	    $details['occupancy'] = $newoccupancy;

	    $sql = " insert into occupancy_log(transaction_date, occupancy_id, update_by, remarks, transaction_type )
	    value ('$now', '$newoccupancy', '$user', '', 'CheckIn' ) ";
		R::exec($sql);


	    /* update room status */
	    $sql = " update rooms set status=2, last_update='$now',update_by='$user' where room_id='$newroomid' ";
		R::exec($sql);
	    $sql = " insert into room_log (room_id,	status_id, update_date, update_by, remarks) values
			    ('$newroomid', '2', '$now', '$user', 'Standard CheckIn, $new_duration days, $new_extension extension')
	    ";
		R::exec($sql);


	    /* process guest details */
	    if($new_guestid) {
		    $sql = " insert into guest_history (guest_id, occupancy_id) values ($new_guestid, $newoccupancy) ";
			R::exec($sql);
	    } else {
		    if ($new_firstname != '' && $new_lastname != '') {
			    $sql = "select guest_id from guests where firstname='$new_firstname' and lastname='$new_lastname' ";
			    $new_guestid = R::getCell($sql);
			    if (empty($new_guestid)) {
				    $sql = "insert into guests (firstname, lastname) values ('$new_firstname','$new_lastname')";
				    $res = R::exec($sql);
				    $new_guestid = R::getInsertID();
			    }
			    $sql = " insert into guest_history (guest_id, occupancy_id) values ($new_guestid, $newoccupancy) ";
			    R::exec($sql);
		    }
	    }

	    /*
	    *   insert  salestransaction details
	    *   room sales --> category 3
        *	standard room charge item_id=15
        */

	    $status = 'Draft';
	    $remarks = $cltype = (isset($_POST["management_endorsement"]) && $_POST["management_endorsement"]=='on') ? 'Management Endorsement' : 'Regular';


	    /* if there is a reservation claimed */
	    if($reservecode) {
		    /**
		    *  new_roomdeposit is actual claim, taken from reservedeposit
		    *  reservedeposit may include other deposits for other rooms
		    */
		    if (isset($new_roomdeposit) && $new_roomdeposit > 0) {
		        $cltype = 'Reservation';
		    } else {
		        $cltype = 'Online Partner Reservation';
		    }
		    $newreservefee = $reservedeposit - $new_roomdeposit;
		    $remarks.=" Reservation Code: $reservecode, Deposit Claimed: $new_roomdeposit";
		    $sql  ="insert into reservation_transactions(transaction_date,reservation_code, occupancy_id, amount_claimed,update_by )
			    values('$now', '$reservecode','$newoccupancy','$new_roomdeposit','$user');
		    " ;
			R::exec($sql);

		    /* save to salesreceipts as claimed deposit */
		    $sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount, update_by, sas_cat_id, reference_id)
				    values ('$now', '$newoccupancy', 'Reservation Fee', '$new_roomdeposit','$user', 3, 0)";
			R::exec($sql);
		    $salesreceipt_id = R::getInsertID();

		    $reservestatus = '';
		    if ($reserveclose) {
			    $reservestatus = " status='Claimed', ";
		    }

		    $sql= "update reservations set $reservestatus
			    reserve_fee = '$newreservefee',
			    date_updated='$now', updated_by='$user'
			    where reserve_code='$reservecode'";
			R::exec($sql);

		    if ($reservedeposit >= $new_rate) {
			    //
		    }else{
			    $status = 'Draft';
		    }

		    $sql = "update reserve_rooms set status='Claimed' where rr_id='$reserveroomid' and reserve_code='$reservecode' ";
			R::exec($sql);

		    if ($reserveclose == 1) {
			    $sql = "update reserve_rooms set status='Cancelled' where status='Pending' and reserve_code='$reservecode' ";
				R::exec($sql);
		    }
	    }

	    /* first set up the initial GL log for the checkin */
	    if (isset($new_alreadypayed) && $new_alreadypayed == 1) {
	        $postAmount = $new_amountdue;
	        $paiddate = $now;
	    } else {
	        $postAmount = $new_cash_tendered;
	        $paiddate = '0000-00-00 00:00:00';
	    }


	    /* insert first day room sale */
	    $sql  =" insert into room_sales( occupancy_id, charge_to, sales_date,
					category_id, item_id , unit_cost, qty, update_date, 
					remarks, update_by,room_id,status,regflag, rechit_date, paid_date)
			    values ( '$newoccupancy', '$newoccupancy', '$now', 3, 15, '$new_rate',
			    		1,'$now','$remarks','$user','$newroomid','$status', 0, date_add('$now', interval 1 day), '$paiddate') ";
		R::exec($sql);
	    $newrefnum = R::getInsertID();

	    /* creditline logging */
	    /* first posting of roomsale, should reflect postdate which is 1 duration unit */
	    $firstDuration = 24;
	    if ($new_duration > 0) {
	        /* +24 hours */
	        $firstDuration = 24;
	    } else {
	        $firstDuration = 12;
	    }

	    /* determine date based on duration unit */
	    $indate2 = new DateTime($now);
		date_add($indate2, new DateInterval("PT".$firstDuration."H") );
		$postDate = $indate2->format("Y-m-d H:i:s");
        $postShift = getShift($postDate);

	    $details['reftable'] = 'room_sales';
        $details['pdate'] = $postDate;
        $details['shift'] = $postShift;

	    $discount = 0;

	    /* discount */
	    if($new_discount!='0.00')
	    {
	        $discount = $new_discount;
		    $new_discount = -1 * $new_discount;
		    $sql  =" insert into room_sales( occupancy_id, charge_to, sales_date, category_id, item_id ,  unit_cost, qty,update_date,remarks,update_by,room_id, regflag, rechit_date, paid_date)
			    values ('$newoccupancy','$newoccupancy', '$now', 3,17,'$new_discount','1','$now','$remarks','$user','$newroomid', 0, date_add('$now', interval 1 day), '$paiddate') ";
			R::exec($sql);

		    $sql = "insert into discount_log (occupancy_id, discount_given, oic, reason, update_date, remarks,update_by) values
				    ('$newoccupancy', '$new_discount', '$oic', '$new_reason', '$now','$remarks','$user');
			    ";
			R::exec($sql);

	    }

		/* commission */
		if($new_commission != '0.00')
		{
			//Commission = 121
			if($new_commission > 0) {
				$new_commission = -1 * $new_commission;
			}
			
			
			$sql  =" insert into room_sales( occupancy_id, charge_to, sales_date, category_id, item_id ,  unit_cost, qty,update_date,remarks,update_by,room_id, regflag, rechit_date, paid_date)
			    values ('$newoccupancy', '$newoccupancy', '$now', 3, 121,'$new_commission','1','$now','$remarks','$user','$newroomid', 0, date_add('$now', interval 1 day), '$paiddate') ";
			R::exec($sql);
			
			

			

		}

		if((int) $partner_bpg != 0) {
			
			$partner_bpg = -1 * abs($partner_bpg);
			
			$sql  =" insert into room_sales( occupancy_id, charge_to, sales_date, category_id, item_id ,  unit_cost, qty,update_date,remarks,update_by,room_id, regflag, rechit_date, paid_date)
		    values ('$newoccupancy', '$newoccupancy','$now', 3, 123,'$partner_bpg','1','$now','$remarks','$user','$newroomid', 0, date_add('$now', interval 1 day), '$paiddate') ";
			R::exec($sql);
		}

		if($new_wtax != '0.00') {
			if($new_wtax > 0) {
				$new_wtax = -1 * $new_wtax;
			}
			$sql  =" insert into room_sales( occupancy_id, charge_to, sales_date, category_id, item_id ,  unit_cost, qty,update_date,remarks,update_by,room_id, regflag, rechit_date, paid_date)
			    values ('$newoccupancy', '$newoccupancy', '$now', 3, 124,'$new_wtax','1','$now','$remarks','$user','$newroomid', 0, date_add('$now', interval 1 day), '$paiddate') ";
			R::exec($sql);
		}

	    /* extra bed */
	    if(isset($_POST['xtra_11']) && $_POST['xtra_11'] > 0) {
		    $newunitprice=getunitprice(11);
		    $val= $_POST['xtra_11'];
		    $itemid=11;
		    $sql = "insert into room_sales (sales_date, order_code, occupancy_id, charge_to, category_id, item_id,  unit_cost, qty, update_date,update_by,room_id, regflag, rechit_date, paid_date)
		    values ('$now', '0', '$newoccupancy', '$newoccupancy', '1' ,'$itemid', '$newunitprice' ,'$val','$now','$user','$newroomid', 0 , date_add('$now', interval 1 day), '$paiddate')";
			R::exec($sql);
		    $print=true;
	    }

	    /* for length of stay greater than 1 unit */
	    if($new_duration > 0) {
		    for($x=1; $x <= $new_duration;$x++) {
		        $count = $x + 1;
			    $indate =  new DateTime($now);
			    $mynewcheckout = date_add($indate, new DateInterval("P".$x."D") );
			    $mynewcheckout = $indate->format("Y-m-d H:i:s");
			    $sql  =" insert into room_sales( occupancy_id, charge_to, sales_date, category_id, item_id , unit_cost, qty, update_date, remarks,update_by,room_id, regflag, rechit_date, paid_date)
			    values ( '$newoccupancy', '$newoccupancy','$mynewcheckout', 3, 15, '$new_rate',1,'$now','$remarks','$user','$newroomid', 0, 
			    date_add('$mynewcheckout', interval 1 day), '$paiddate') ";
				R::exec($sql);
			    $newrefnum = R::getInsertID();

                /* creditline logging */
		        date_add($indate2, new DateInterval("PT". 24 ."H") );
		        $postDate = $indate2->format("Y-m-d H:i:s");
                $postShift = getShift($postDate);

	            $details['reftable'] = 'room_sales';
                $details['pdate'] = $postDate;
                $details['shift'] = $shift;
	            $discount = 0;
			    // discount
			    if($new_discount !='0.00')
			    {
				    //$new_discount = -1 * $new_discount;
				    $discount = $new_discount;
				    $sql  =" insert into room_sales( occupancy_id, charge_to, sales_date, category_id, item_id ,  unit_cost, qty,update_date,remarks,update_by,room_id, regflag, rechit_date, paid_date)
					    values ('$newoccupancy', '$newoccupancy','$mynewcheckout', 3,17,'$new_discount','1','$now','$remarks','$user','$newroomid', 0,
					    date_add('$mynewcheckout', interval 1 day), '$paiddate') ";
					R::exec($sql);

				    $sql = "insert into discount_log (occupancy_id, discount_given, oic, reason, update_date, remarks) values
						    ('$newoccupancy', '$new_discount', '$oic', '$new_reason', '$now','$remarks');
					    ";
					R::exec($sql);
			    }

				/* commission */
				if($new_commission != '0.00')
				{
					//Commission = 121
					if($new_commission > 0) {
						$new_commission = -1 * $new_commission;
					}
					
					
					$sql  =" insert into room_sales( occupancy_id, charge_to, sales_date, category_id, item_id ,  unit_cost, qty,update_date,remarks,update_by,room_id, regflag, rechit_date, paid_date)
					    values ('$newoccupancy', '$newoccupancy', '$mynewcheckout', 3, 121,'$new_commission','1','$now','$remarks','$user','$newroomid', 0, date_add('$mynewcheckout', interval 1 day), '$paiddate') ";
					R::exec($sql);
					
					
					
				}

				if((int) $partner_bpg != 0) {
					$partner_bpg = -1 * abs($partner_bpg);
					
					$sql  =" insert into room_sales( occupancy_id, charge_to, sales_date, category_id, item_id ,  unit_cost, qty,update_date,remarks,update_by,room_id, regflag, rechit_date, paid_date)
				    values ('$newoccupancy', '$newoccupancy', '$mynewcheckout', 3, 123,'$partner_bpg','1','$now','$remarks','$user','$newroomid', 0,
				    	date_add('$mynewcheckout', interval 1 day), '$paiddate') ";
					R::exec($sql);
				}

				if($new_wtax != '0.00') {
					if($new_wtax > 0) {
						$new_wtax = -1 * $new_wtax;
					}
					$sql  =" insert into room_sales( occupancy_id, charge_to,  sales_date, category_id, item_id ,  unit_cost, qty,update_date,remarks,update_by,room_id, regflag, rechit_date, paid_date)
					    values ('$newoccupancy', '$newoccupancy', '$mynewcheckout', 3, 124,'$new_wtax','1','$now','$remarks','$user','$newroomid', 0,
					    	date_add('$mynewcheckout', interval 1 day), '$paiddate') ";
					R::exec($sql);
				}

			    if(isset($_POST['xtra_11']) && $_POST['xtra_11'] > 0) {
				    $val= $_POST['xtra_11'];
				    $itemid=11;
				    $sql = "insert into room_sales (sales_date, order_code, occupancy_id, charge_to, category_id, item_id,  unit_cost, qty, update_date,update_by,room_id, regflag, rechit_date, paid_date)
				    values ('$mynewcheckout', '0', '$newoccupancy', '$newoccupancy', '1' ,'$itemid', '$newunitprice' ,'$val','$now','$user','$newroomid', 0, date_add('$mynewcheckout', interval 1 day), '$paiddate' )";
					R::exec($sql);

				    $print=true;

			    }

		    }
	    }

	   
	    reset($_POST);
	    //$print=false;
	    foreach($_POST as $key=>$val)
	    {
		    if(substr($key,0,5)=="xtra_" && $val > 0)
		    {
			    list($tmp, $itemid)=explode("_", $key);
			    if($itemid!=11) {
				    $newunitprice=getunitprice($itemid);
				    $sql = "insert into room_sales (sales_date, order_code, occupancy_id, category_id, item_id,  unit_cost, qty, update_date, update_by,room_id, regflag, rechit_date, paid_date)
				    values ('$now', '0', '$newoccupancy', '1' ,'$itemid', '$newunitprice' ,'$val','$now' ,'$user','$newroomid', 0, date_add('$mynewcheckout', interval 1 day), '$paiddate' )";
					R::exec($sql);
				    $print=true;
			    }
		    }
	    }


	    //payments

	    if( ($new_cash_tendered > 0)  &&  ($new_card_tendered > 0) )
	    {
		    $amt_tendered = $new_cash_tendered + $new_card_tendered;

	    }
	    elseif( ($new_cash_tendered > 0) &&  ($new_card_tendered <= 0) )
	    {

		    if($new_cash_tendered == $new_amountdue)
		    {
			    $sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount, update_by)
				    values ('$now', '$newoccupancy', 'Cash', '$new_cash_tendered','$user')";
				R::exec($sql);

			    setCurrentCash($new_cash_tendered,'in',$user);
			    /*
				    set all roomsales items to paid
			    */
			    $sql ="update room_sales set status='Paid',update_date = '$now', paid_date='$now' where occupancy_id='$newoccupancy'";
				R::exec($sql);
		    }
		    elseif($new_cash_tendered > $new_amountdue)
		    {
			    /*
				    insert only enough $new_amountdue
			    */
			    $sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount, update_by)
				    values ('$now', '$newoccupancy', 'Cash', '$new_amountdue','$user')";
				R::exec($sql);

			    setCurrentCash($new_amountdue,'in',$user);
			    /*
				    set all roomsales items to paid
			    */
			    $sql =" update room_sales set status='Paid',update_by='$user', update_date = '$now', paid_date='$now' where occupancy_id='$newoccupancy'";
				R::exec($sql);
			    /*
			    insert as deposit the balance from the account
			    */
			    $deposit = $new_cash_tendered - $new_amountdue;
			    if($new_alreadypayed != '1')
			    {
				    if($deposit >= 0) {
				    $sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount,update_by)
					    values ('$now', '$newoccupancy', 'Deposit', '$deposit','$user')";
					R::exec($sql);
				    setCurrentCash($deposit,'in',$user);
				    }
			    }
		    }
	    }
	    elseif( ($new_cash_tendered <= 0) &&  ($new_card_tendered > 0) )
	    {
			$tendertype = ($newisdebit == 1) ? 'Debit' : 'Credit';
		    if( $new_card_tendered >= $new_amountdue ) {

		        /* insert receipt details */
			    $sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount,update_by)
				    values ('$now', '$newoccupancy', 'Card', '$new_card_tendered','$user')";
				R::exec($sql);
			    $newsalesid = R::getInsertID();
			    $sql ="update room_sales set status='Paid',update_by='$user',update_date = '$now', paid_date = '$now',
					tendertype='$tendertype' where occupancy_id='$newoccupancy'";
				R::exec($sql);


			    /* insert card details */
			$new_card_suffix = $_POST['new_card_suffix'];
			$terminal = $_POST['terminal'];
			    $sql = " insert into card_payment_details (salesreceipt_id, card_type, approval_code, batch_number, is_debit, card_suffix, terminal)
					    values ('$newsalesid','{$_POST["new_card_type"]}','$new_approval_code','$new_batch_number', '$newisdebit', '$new_card_suffix', '$terminal')";
			    $fp = fopen("reports/sql.log", "a");
			    fwrite( $fp,$sql);
			    fclose($fp);
				R::exec($sql);
		    }
	    }

        /* print checkin details */
	    if($print) {
		    $obj=new Reporting($newroomid);
		    $retval=$obj->printablemos();
		    $file = "mos{$room}.txt";
		    $fp = fopen("reports/" .$file, "w");
		    fwrite( $fp,$retval);
		    fclose($fp);
		    //shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
			$prnt->sendToPrinter('front',"reports/" . $file);
	    }
	}

	include_once("class.room.php");
	$rm = new room($newroomid);
	$f = $rm->floor_id-1;
	echo '<script lang="javascript">parent.document.location.href="../index.php?f='. $f .'&room='.$newroomid.'"</script>';
}

function isRoomOccupied($roomid)
{
	$sql = "select status from rooms where room_id = '$roomid'";
	$room = R::getCell($sql);

	if($room['status'] == '2')
	{
		return true;
	}
	return false;
}


?>
