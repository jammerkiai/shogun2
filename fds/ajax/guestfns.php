<?php
//guestfns.php

function getCurrentOccupancies() {
	$sql = "select occupancy_id from occupancy
		where actual_checkout='0000-00-00 00:00:00'";
	return R::getCell($sql);
}

function getCheckedInGuests($key = '') {
    $sql = "select a.occupancy_id, b.guest_id, a.room_id,
        a.actual_checkin, a.expected_checkout,
        concat(b.firstname,' ', b.lastname) as guestname, d.door_name
        from occupancy a, guests b, guest_history c, rooms d
        where a.occupancy_id=c.occupancy_id
        and b.guest_id=c.guest_id
        and a.room_id=d.room_id
        and a.actual_checkout='0000-00-00 00:00:00' 
        order by guestname";
    $res = R::getAll($sql);
    $numrows = count($res);
    $retval = "<div>$numrows record(s) found.</div>";
    $retval.= "<table class='guesttable'>";
    $retval.= "<tr>";
    $retval.= "<th>Guest Name</th>";
    $retval.= "<th>Room</th>";
    $retval.= "<th>Checkin</th>";        
    $retval.= "<th>Expected Checkout</th>";    
    $retval.= "<th>Actions</th>";    
    $retval.= "</tr>";
    foreach ($res as $r) {
        $occ = $r['occupancy_id'];
        $gid = $r['guest_id'];
        $rid = $r['room_id'];
        $in = $r['actual_checkin'];
        $out = $r['expected_checkout'];
        $name = $r['guestname'];
        $room = $r['door_name'];

        $retval.="<tr>";
        $retval.="<td>$name</td>";
        $retval.="<td>$room</td>";
        $retval.="<td>$in</td>";
        $retval.="<td>$out</td>";
        $retval.="<td><input type='submit' name='gh_$gid' value='Guest History'>";
        $retval.="<input type='submit' name='occ_$occ' value='Get Details'></td>";        
        $retval.="</tr>";
    }
    $retval.= "</table>";
    return $retval;
}

function getGuestsByKeywordSearch($key = '') {
    $sql = "select guest_id, concat(firstname,' ', lastname) as guestname
        from guests 
        where  firstname like '%$key%' or lastname like '%$key%' 
        order by guestname";

    $res = R::getAll($sql);
    $numrows = count($res);
    $retval = "<div>$numrows record(s) found.</div>";
    $retval.= "<table class='guesttable'>";
    $retval.= "<tr>";
    $retval.= "<th>Guest Name</th>";  
    $retval.= "<th>Actions</th>";    
    $retval.= "</tr>";
    foreach ($res as $r) {
        $gid = $r['guest_id'];
        $name = $r['guestname'];
        $name = ucwords(strtolower($name));
        $retval.="<tr>";
        $retval.="<td>$name</td>";
        $retval.="<td><input type='submit' name='gh_$gid' value='Guest History'>";
        $retval.=" <input type='submit' name='del_$gid' value='Delete' onclick='return confirm(\"Are you sure?\")'></td>";        
        $retval.="</tr>";
    }
    $retval.= "</table>";
    return $retval;
}

function getGuestHistory($gid = '') {
    $sql = "select a.occupancy_id, a.actual_checkin, a.actual_checkout, c.door_name
    		from occupancy a, guest_history b, rooms c
    		where a.room_id=c.room_id
    		and a.occupancy_id=b.occupancy_id
    		and b.guest_id='$gid'
    		order by a.occupancy_id desc";
    $res = R::getAll($sql);
    $numrows = count($res);
    $retval = "<div>$numrows record(s) found.</div>";
    $retval.= "<table class='guesttable'>";
    $retval.= "<tr>";
    $retval.= "<th>Room #</th>";  
    $retval.= "<th>Check In</th>";
    $retval.= "<th>Check Out</th>"; 
    $retval.= "<th>Action</th>";
    $retval.= "</tr>";
    foreach ($res as $r) {
        $occ = $r['occupancy_id'];
        $in = $r['actual_checkin'];
        $out = $r['actual_checkout'];
        $name = $r['door_name'];
        $name = ucwords(strtolower($name));
        $retval.="<tr>";
        $retval.="<td>$name</td>";
        $retval.="<td>$in</td>";
        $retval.="<td>$out</td>";        
        $retval.="<td><input type='submit' name='occ_$occ' value='Get Details'></td>";
        $retval.="</tr>";
    }
    $retval.= "</table>";
    return $retval;
}

function deleteGuest($gid='') {
	if ($gid !== '') {
		$sql = "delete from guests where guest_id='$gid'";
		R::exec($sql);
	}
}

function getSalesHistoryByOccupancy($occ) {
	ini_set('allow_url_fopen', 1);
	ini_set('allow_url_include', 1);
	$retval = include_once("occupancydetails.php?occ=$occ&viewonly=1");
	
	return $retval;
}

?>
