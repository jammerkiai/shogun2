<?php
/**
* checkinform.php
*/
session_start();
include_once("./config/config.inc.php");
include_once("./class.room.php");
include_once("./reserve.functions.php");

function getRoomMinibarQty($room,$mb)
{
	$sql = "select qty from room_minibar where room_id='$room' and minibar_id='$mb' ";
	return R::getCell($sql);
}

$room = new room( $_GET["room"] );
$reservecode = $_GET["code"] ? $_GET["code"] : 0;
$reservedeposit = $_GET["dep"] ? $_GET["dep"] : 0;
$reserveroomid = $_GET["rr"] ? $_GET["rr"] : 0;
$reserveclose = $_GET["close"] ? $_GET["close"] : 0;
$now = date("Y-m-d H:i:s");
$displaydate = date("l, d F Y");
$room->getroomrates();
$room->getroomdiscounts();

$_SESSION["lastroom"]=$room->room_id;
$_SESSION["lasttab"]=$room->floor_id - 1;


$partners = R::getAll('select * from partners');

?>
<style>
	.availability {border: 1px solid #ccc; padding:4px; font-size: 11px; margin: 1px; float:left;}
	.reserved {background-color: #faf2cc; color: #00f;cursor:pointer;float:left;}
	.owner {background-color: blue;}
</style>
<link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery.keypad.css" />
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery.approver.css" />
<form method="post" name="checkinform" id="checkinform" action="checkin.php">
<div  style="border-bottom:1px solid #cccccc;padding-bottom:2px;">
<input type="button"  value="Back to Main"  onclick="javascript:document.location.href='../index.php?f=<?php echo $room->floor_id -1 ?>'"/>
<input type="button"  value="Reload"  onclick="javascript:document.location.href=''<?=$_SELF?>"/>
<input type="button"  value="Change Status"  onclick="javascript:document.location.href='restatus.php?room=<?php echo $_GET["room"] ?>'"/>
<div style="float:right;padding:2px;background-color:green;color:#ff9933;font-size:20px;padding-left:4px;padding-right:4px;text-shadow:#111111 1px 1px 1px">
<img src="../img/house.png" />
<span style="color:#FF0000">Room <?=$room->door_name ?> </span>
<?=$displaydate?> <span id="clock"></span>
</div>
<div style="clear:both"></div>
</div>
<table width="970" border=0 height='690'>
<tr>
<td width="330px" valign='top'>
<fieldset id="availability">
<legend>Availability</legend>
<?php
	$guest = '';
	if (isset($_GET['fn']) && isset($_GET['ln'])) {
		$guest = $_GET['fn'] . ' ' . $_GET['ln'];
	}
	echo getRoomAvailabilityForeCast($room->roomid, 27, $guest);
?>
</fieldset>
<fieldset id="rates">
<legend>Rates</legend>
<?php
for ($x=0; $x < count($room->rates);$x++) {
	$amt = $room->rates[$x]['amount'];
	$label = $room->rates[$x]['rate_name'];
	$publishrate = $room->rates[$x]['publishrate'];
	$id = "rate_" .  $room->rates[$x]['rate_id'];
	echo "<input type='button' id='$id' alt='$amt' data-publish='$publishrate'  value='$label' class='ratebuttons' />";
	if ($room->rates[$x]['duration']==12) {
		echo "<input type='hidden' id='new_12HRS_$label' value='$amt' />";
	}
}
?>
</fieldset>

<fieldset id="discounts">
<legend>Discount</legend>
<?php
foreach( $room->discounts as $discount ) {
	$percent = $discount['discount_percent'];
	$rateclass = "rate_" . $discount["rate_id"];
	$name = $discount["discount_label"];
	echo "<input type='button' class='discountbuttons $rateclass'  value='$name' alt='$percent' />";
}
?>
<input type='button' id="otherdisc" class='otherdisc'  value='Other'  />

<fieldset id="adjform"><legend>Please specify reason and enter the new discount amount</legend>
<table class="formtable">

<tr>
<td>Reason</td>
<td><?php
$sql  ="select remark_text from remarks where remark_classification=1" ;
$res= R::getAll($sql);
$ret ="<span style='font-size:.7em;'></span><select name='discount_reason' id='discount_reason'>";
$ret .= "<option></option>";
foreach ($res as $r) {
	$text= $r['remark_text'];
	$ret .= "<option value='$text'>$text</option>";
}
$ret.="</select>";
echo $ret;
?></td>
</tr>
<!-- <tr>
<td>OIC Password</td>
<td><input type="password"  id="new_password" name="new_password"  class="textfield" /></td>
</tr> -->
<tr id="adjrow">
<td>Adjustment</td>
<td><input type="text"  id="new_adjustment" name="new_adjustment"  class="money"  /></td>
</tr>
<tr>
<td>Amount</td>
<td><input type="text" name="new_adjustment_amount" id="new_adjustment_amount" class="adjustelement money" /></td>
</tr>
<tr>
<td valign="top">Supervisor/OIC:</td>
<td>

<input type="button" value="Approve" name="cmdApprove" id="cmdApprove" />
</td>
</tr>
 
</tr>
</table>
</fieldset>
</fieldset>

<fieldset id="discounts">
<legend>Length of Stay (Additional days or hours)</legend>
<table class="formtable">
<!--tr>
<td>Checkin</td>
<td><input type="text"  id="new_checkin" name="new_checkin"  class="dateinput" value="<?=$now?>" /></td>
</tr-->
<!-- <tr>
<td>OT Hours</td>
<td><input type="text" value='0' id='new_extension' name="new_extension"  class="numeric"  />
<input type='button' value='-' class='incbutton' alt="#new_extension" />
<input type='button' value='+' class='incbutton' alt="#new_extension"  /></td>
</tr>
<tr><td colspan=2><hr /></td></tr>
<tr id="halfdaysrow">
<td>12-Hour</td>
<td><input type="text" value='0' id='new_halfduration' name="new_halfduration"  class="numeric"  />
<input type='button' value='-' class='incbutton' alt="#new_halfduration" />
<input type='button' value='+' class='incbutton' alt="#new_halfduration"  /></td>
</tr> -->

<tr id="daysrow">
<td>Days</td>
<td><input type="text" value='0' id='new_duration' name="new_duration"  class="numeric"  />
<input type='button' value='-' class='incbutton' alt="#new_duration" />
<input type='button' value='+' class='incbutton' alt="#new_duration"  /></td>
</tr>
</table>
</fieldset>
<fieldset id="extras">
<legend>Extras</legend>
<table class="formtable">
<?php
$sql =" select sas_id, sas_description,sas_amount from sales_and_services where sas_cat_id=1 ";
$res = R::getAll($sql);
foreach ($res as $r) {
	$fnbid = $r['sas_id'];
	$fnbname = $r['sas_description']; 
	$amount = $r['sas_amount'];

	$xtra = "xtra_$fnbid";
	echo "<tr>";
	echo "<td>$fnbname</td>";
	echo "<td><input type='text' name='$xtra' id='$xtra' alt='$amount' value='0' class='numeric extras '></td>";
	echo "<td nowrap=true>
	<input type='button' value='-' class='incbutton' alt='#$xtra' />
<input type='button' value='+' class='incbutton' alt='#$xtra'  />
	</td>";
	echo "</tr>";
}
?>
</table>
</fieldset>
</td>
<?php if ($room->site_id == 2) { ?>
<!--------------------------------------   middle  -------------------------------------->
<td width="330px" valign='top'>
<fieldset id="guestinfo">
<legend>Guest Information</legend>
<input type="hidden" name="new_guestid" id="new_guestid" value="<?php echo $_GET['gid'] ?>" />
<table class="formtable">
<tr>
<td  width="80">First Name</td>
<td><input type="text"  id="new_firstname" name="new_firstname" value="<?php echo $_GET['fn'] ?>"  class="textfield" /></td>
</tr>
<tr>
<td>Last Name</td>
<td><input type="text"  id="new_lastname" name="new_lastname" value="<?php echo $_GET['ln'] ?>"  class="textfield" /></td>
</tr>
<tr>
<td>Company Name</td>
<td><input type="text" value='' id='new_company' name="new_company" value="<?php echo $_GET['cm'] ?>"   class="textfield"  />
</td>
</tr>
<tr>
<td>Remarks</td>
<td><input type="text" value='' id='new_remarks' name="new_remarks"  class="textfield"  />
</td>
</tr>
</table>
</fieldset>

	<fieldset id="partnerinfo">
		<legend>Partner Information</legend>
		<input type="hidden" name="new_guestid" id="new_guestid" value="<?php echo $_GET['gid'] ?>" />
		<table class="formtable">
			<tr>
				<td width="80">Reserve Code</td>
				<td>
					<input type="text" value='<?php echo $reservecode ?>' id='partner_reserve_code' name="partner_reserve_code"  class="textfield" />
				</td>
			</tr>
			<tr>
				<td width="80">Partner Name</td>
				<td>
					<select name="partner_id" id="partner_id">
						<option value=""></option>
						<?php foreach($partners as $partner): ?>
							<option value="<?php echo $partner['partner_id']?>"
								data-commission="<?php echo $partner['commission'] ?>"
								data-bpg="<?php echo $partner['bpg'] ?>" >
								<?php echo $partner['partner_name']?>
							</option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Commission</td>
				<td><input type="text"  id="partner_commission" name="partner_commission" value="0"  class="textfield" /> %</td>
			</tr>

			<tr>
				<td>BPG</td>
				<td><input type="text" value='0' id='partner_bpg' name="partner_bpg"  class="textfield"  />
				</td>
			</tr>

			<tr>
				<td>Promo Discount</td>
				<td><input type="text" value='0' id='promo_discount' name="promo_discount"  class="textfield"  />
				</td>
			</tr>

		</table>
	</fieldset>

<fieldset id="minibar">
<legend>Minibar</legend>
<table  class="formtable">
<?php

$sql =" select a.minibar_id, a.fnb_id, a.amount,b.fnb_name from minibar a, fnb b where a.fnb_id=b.fnb_id ";
$res = R::getAll($sql);
foreach ($res as $r) {
	$mbid = $r['minibar_id']; 
	$fnbid = $r['fnb_id'];
	$amount = $r['amount'];
	$fnbname = $r['fnb_name'];
	$barname = "bar_$mbid";
	$qty = getRoomMinibarQty($_GET["room"], $mbid);
	
	echo "<tr>";
	echo "<td>$fnbname</td>";
	echo "<td><input type='text' name='$barname' id='$barname' alt='$amount' value='$qty' class='minibar numeric'></td>";
	echo "<td nowrap=true>
	<input type='button' value='-' class='incbutton' alt='#$barname' />
<input type='button' value='+' class='incbutton' alt='#$barname'  />
	</td>";
	echo "</tr>";
}
?>
</table>
</fieldset>
</td>
<!--------------------------------------   middle  -------------------------------------->
<?php } ?>
<td width="330px" valign='top'>
<fieldset id="summary">
<legend>Summary</legend>

<table class="formtable">
<tr>
<td>&nbsp;</td>
<td  style="background-color:#ffcc99;">Converter</td>
<td  style="background-color:#ffcc99;">
<select name="currency" id="currency" class="money"  style="text-align:left">
<option value="1">Peso</option>
<?php
$sql = " select currency_name, currency_value from currency order by currency_name ";
$res = R::getAll($sql);
foreach ($res as $r) {
	$cname = $r['currency_name'];
	$cvalue = $r['currency_value'];
	echo "<option value='$cvalue'>$cname [Php $cvalue]</option>";
}
?>
</select>
</td>
</tr>
<?php if($reservedeposit) : ?>
<tr>
<td  width="80">Reserve Fee</td>
<td style='text-align:right;'><input type="text" class="money" name="new_roomdeposit" id="new_roomdeposit"
value="<?php echo $reservedeposit ?>">
</td>
<td></td>
</tr>
<?php endif; ?>
<tr>
<td  width="80">Publish Rate</td>
<td><input type="text"   class="money"  id="new_publishrate" name="new_publishrate" value="0" /></td>
<td><input type="hidden"   class="money"  id="new_publishrate_alt" name="new_publishrate_alt" value="0" /></td>
</tr>
<tr>
<td  width="80">Standard Rate</td>
<td><input type="text"   class="money"  id="new_rate" name="new_rate" value="0" /></td>
<td><input type="hidden"   class="money"  id="new_rate_alt" name="new_rate_alt" value="0" /></td>
</tr>
<tr>
<td>Discount</td>
<td><input type="text"  class="money" id="new_discount" name="new_discount" value="0" /></td>
<td><input type="hidden"   class="money" id="new_discount_alt" name="new_discount_alt" value="0" /></td>
</tr>
	<tr>
		<td>Commission</td>
		<td><input type="text"  class="money" id="new_commission" name="new_commission" value="0" /></td>
		<td><input type="hidden"   class="money" id="new_commission_alt" name="new_commission_alt" value="0" /></td>
	</tr>
<tr>
<td>Extras</td>
<td><input type="text"  class="money" id="new_extracost" name="new_extracost" value="0" /></td>
<td><input type="hidden"   class="money" id="new_extracost_alt" name="new_extracost_alt" value="0" /></td>
</tr>
<tr>
<td>WTax</td>
<td><input type="text"  class="money" id="new_wtax" name="new_wtax" value="0" /></td>
<td><input type="hidden"   class="money" id="new_wtax_alt" name="new_wtax_alt" value="0" /></td>
</tr>
<!-- <tr>
<td>Overtime Cost</td>
<td><input type="text"  class="money" id="new_extensioncost" name="new_extensioncost" value="0"/></td>
<td><input type="hidden"  class="money" id="new_extensioncost_alt" name="new_extensioncost_alt" value="0"/></td>
</tr> -->
<tr>
<td>Amount Due</td>
<td><input type="text"  class="money" id="new_amountdue" name="new_amountdue" value="0" /></td>
<td><input type="text"  class="money" id="new_amountdue_alt" name="new_amountdue_alt" value="0" /></td>
</tr>
<tr style="background-color:#333333;color:#ffffff;">
<td>Change Due</td>
<td><input type="text"  class="money" id="new_changedue" name="new_changedue" value="0" /></td>
<td><input type="hidden"  class="money" id="new_changedue_alt" name="new_changedue_alt" value="0" />
<input type="checkbox" value="1" name="new_alreadypayed" id="new_alreadypayed" /><label for="new_alreadypayed"> Change Returned </label>
</td>
</tr>
<tr>
<td colspan=3>
<input type="checkbox" id='management_endorsement' name='management_endorsement' /> <label for='management_endorsement'>Management Endorsement</label>
</td>
</tr>
<tr>
<td colspan=3>
<input type="checkbox" id='wtax' name='wtax' /> <label for='wtax'>WTAX</label>
</td>
</tr>
<tr>
<td colspan=3>
<input type="checkbox" id='vatex' name='vatex' /> <label for='vatex'>VATex</label>
</td>
</tr>
</table>
</fieldset>

<?php include('payment_fieldset.php'); ?>

</td>
</tr>
</table>
<input type="hidden" id="hidden_ot_amount"  name="hidden_ot_amount"  value="0" />
<input type="hidden" id="hidden_duration"  name="hidden_duration"  value="0" />
<input type="hidden" id="newroomid"  name="newroomid"  value="<?=$_GET["room"]?>" />
<input type="hidden" id="newrateid"  name="newrateid"  value="" />
<input type="hidden" id="reservedeposit"  name="reservedeposit"  value="<?php echo $reservedeposit ?>" />
<input type="hidden" id="reservecode"  name="reservecode"  value="<?php echo $reservecode ?>" />
<input type="hidden" id="reserveroomid"  name="reserveroomid"  value="<?php echo $reserveroomid ?>" />
<input type="hidden" id="reserveclose"  name="reserveclose"  value="<?php echo $reserveclose ?>" />
<input type="hidden" id="act"  name="act"  />
<input type="hidden" id="discount_rate"  name="discount_rate"  />
<input type="hidden" id="new_12HRS"  name="new_12HRS" value="0" />
<div id="otstore"></div><div id="durationstore"></div>

</form>
<link rel="stylesheet" type="text/css" href="../js/jquery.keypad.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.plugin.min.js"></script>
<script type="text/javascript" src="../js/jquery.keypad.min.js"></script>
<script type="text/javascript" src="../js/jclock.js"></script>
<script type="text/javascript" src="../js/jquery.approver.js"></script>
<script lang="javascript">


function recompute(){
	//compute extras
	var extracost = 0;
	$.each( $(".extras"), function(o, v) {
		//alert(v.alt);
		extracost =extracost + (  v.value * v.alt);
	});


	var partnerCommission = $('#partner_commission').val();
	var promo_disc_rate = $('#promo_discount').val() - 0;
	var partnerId = $('#partner_id').val();
	var bpg = $('#partner_bpg').val() - 0;

	var currencyconvert = $("#currency").val();
	var rate = $("#new_rate").val()  - 0;
	var disc = $("#new_discount").val()  - 0;
	var disc_rate = $('#discount_rate').val() - 0;

	var commission = 0;
	var commissionableAmount = 0;
	var wtax = 0;
	var selected_rate = $('.ratebuttons.pressed').prop('alt') - 0;

	

	if ($('#vatex').is(':checked')){
		rate = rate/1.12;
		xdisc = rate*disc_rate/100;
		wtax = (rate - xdisc) * 0.02;
	} else {
		wtax = 0;

		if ($('#wtax').is(':checked')) {	
			xdisc = selected_rate*disc_rate/100;
			baserate = selected_rate - xdisc;
			wtax = (baserate/1.12) * 0.02;
		} 
	}

	$('#new_rate').val(rate.toFixed(2));
	$('#new_wtax').val(wtax.toFixed(2));
	$('#new_wtax_alt').val(wtax.toFixed(2));

	commissionableAmount =  rate - bpg;
	disc = commissionableAmount * disc_rate /  100;
	promo_disc = 0;

	if (partnerCommission > 0) {
		if (partnerId == 2) {
			
			disc = commissionableAmount * disc_rate /  100;
			commission = (commissionableAmount - disc) / 1.12 * (partnerCommission/100);
		} else if (partnerId == 3) {
			commissionableAmount =  rate;
			disc = commissionableAmount * disc_rate /  100;
			commission = 0; //commission for booking.com taken at end of month
		} else {
			
			disc = commissionableAmount * disc_rate /  100;
			commission = (rate - disc) * partnerCommission/100;
		}
	}

	if (promo_disc_rate > 0) {
		promo_disc = ((commissionableAmount - disc) * promo_disc_rate) / 100;

		commission = (commissionableAmount - disc - promo_disc) * partnerCommission/100;
	}

	disc = disc + promo_disc;

	$('#new_discount').val(disc.toFixed(2));
	

	$('#new_commission').val(commission.toFixed(2));

	// var ot_amt = $("#hidden_ot_amount").val() - 0;
	// var ext = $("#new_extension").val() - 0;
	//var ot = ext * ot_amt;
	var reserve = ($("#new_roomdeposit").val()!= undefined) ? $("#new_roomdeposit").val() - 0 : 0;
	// var cost12hrs = $("#new_12HRS").val() - 0;
	$("#new_extracost").val( extracost );
	// $("#new_extensioncost").val(ot);
	var days = 1 * $("#new_duration").val() + 1;
	// var halfdays_amt = $("#new_halfduration").val() * cost12hrs ;
	
	var amountdue = rate + extracost - disc - commission - bpg - wtax;
	amountdue=amountdue * (days);
	// amountdue = amountdue + halfdays_amt;
	
	//with reservedeposit
	amountdue=amountdue - reserve; //+ ot
	$("#new_amountdue").val(amountdue.toFixed(2));
	//tendered
	var tender = $("#new_cash_tendered").val()*1 + $("#new_card_tendered").val()*1;

	$("#new_changedue").val(  (tender - amountdue).toFixed(2) );

	//converters 
	$("#new_rate_alt").val( (rate/currencyconvert).toFixed(2));
	$("#new_discount_alt").val( (disc/currencyconvert).toFixed(2) );
	$("#new_commission_alt").val( (commission/currencyconvert).toFixed(2));
	$("#new_extracost_alt").val( (extracost  / currencyconvert).toFixed(2) );
	// $("#new_extensioncost_alt").val( ($("#new_extensioncost").val()  / currencyconvert).toFixed(2)  );
	$("#new_amountdue_alt").val( ($("#new_amountdue").val() / currencyconvert).toFixed(2));
	$("#new_changedue_alt").val( ($("#new_changedue").val() / currencyconvert).toFixed(2));
	$("#new_cash_tendered_alt").val( ($("#new_cash_tendered").val() / currencyconvert).toFixed(2) );
	$("#new_card_tendered_alt").val( ($("#new_card_tendered").val() / currencyconvert).toFixed(2));
}

var myinterval='';
$(document).ready(function(){

	$('#vatex').on('click', function(e){
		$('#wtax').prop('checked', $(this).is(':checked'));	
		recompute();
	});

	$('#wtax').on('click', function(e){
		if($('#vatex').is(':checked')) {
			e.preventDefault();
		} else {
			recompute();	
		}
	});

	$('#partner_id').on('change', function() {
		var partnerid = $(this).val();

		var commission = $(this).find(':selected').data('commission');
		var bpg = $(this).find(':selected').data('bpg');

		$('#partner_commission').val( commission );
		$('#partner_bpg').val( bpg );

		recompute();
	});

	$('.availability.reserved').on('click', function(){
			var mesg = 'Reserve Code: ' + $(this).attr('data-code');
			mesg += '\nGuest: ' + $(this).attr('data-guest');
			mesg += '\nPartner: ' + $(this).attr('data-partner');
			mesg += '\nReserve Fee: ' + $(this).attr('data-fee');
			alert(mesg);
		});

	$('#new_card_suffix').keypad({keypadOnly:false,layout:$.keypad.qwertyLayout});
	$('#new_approval_code').keypad({keypadOnly:false,layout:$.keypad.qwertyLayout});
	$('#new_batch_number').keypad({keypadOnly:false,layout:$.keypad.qwertyLayout}); 
	$("#daysrow").show();
	$("#new_amountdue_alt").hide();
	$("#new_cash_tendered_alt").hide();
	$("#carddetails").hide();
	$("#clock").jclock({foreground:'yellow',background:'green',fontSize:'20px',timeNotation:'24h'});
	$("#adjform").hide();
	$("#adjrow").hide();
	var focusme="new_cash_tendered";
	<?php
		foreach($room->rates as $rate) {
			$name="rate_" . $rate["rate_id"];
			$value = $rate["ot_amount"];
			$duration = $rate["duration"];
			echo '$("#otstore").data("' . $name .'", ' . $value .');' . "\n";
			echo '$("#durationstore").data("' . $name .'", ' . $duration .');' . "\n";
		}
	?>
	
	$("#currency").change(function(){
		if($(this).val()==1) {
			$("#new_amountdue_alt").hide();
			$("#new_cash_tendered_alt").hide();
		}else{
			$("#new_amountdue_alt").show();	
			$("#new_cash_tendered_alt").show();
		}
		recompute(); });
	$("#otherdisc").click(function() { 
		if( $(this).hasClass("pressed") ) {
			$(this).removeClass("pressed");
			$("#adjform").hide();
			$("#adjrow").hide();
			$("#new_adjustment").val(0);
		}else{
			$(this).addClass("pressed");
			$("#adjform").show();
		}		
	} );
	$("#new_adjustment").change(function(){
		$("#new_discount").val( $(this).val() );
		recompute();
	});
	$("#new_password").keypad();
	$("#new_adjustment").keypad();

	$("#new_password").on('change', function(){
		$.post("passcheck.php", {act:'verify' , pwd: $(this).val()}, function(resp) {
			if(resp.success==true) {
				$("#adjrow").show();
			}
		} ,  'json');
	});

	$(".discountbuttons").on('click', function() {
		var disc = $(this).attr("alt") - 0;
		// var bpg = $('#partner_bpg').val();
		// var rate = $("#new_rate").val() - bpg;
		// var partnerId = $('#partner_id').val();

		// var newdisc = rate * disc/100;
		// console.log(newdisc);
		$('#discount_rate').val(disc);
		// $("#new_discount").val( newdisc );
		// console.log($('#new_discount').val());
		if($("#otherdisc").hasClass("pressed")) $("#otherdisc").trigger("click");
		
		recompute();
	});
	
	$(".tender").click(function(){
		$(".tender").removeClass("hilite");
		focusme = $(this).attr("id");
		$(this).addClass("hilite");
	});
	
	$(".ratebuttons").on('click', function(){
		var myid = $(this).attr("id");
		var myval =  $(this).val();
		var new12 = $("#new_12HRS_" + myval.replace('24','12')).val();
		$("#new_12HRS").val(new12);
		$("#new_discount").val(0);
		$(".ratebuttons").removeClass("pressed");
		$(this).addClass("pressed");
		$("#new_rate").val($(this).attr("alt"));
		$("#new_publishrate").val($(this).attr("data-publish"));
		$("#hidden_ot_amount").val($( "#otstore").data( myid )  );
		$("#hidden_duration").val($( "#durationstore").data( myid )  );
		$(".discountbuttons").hide();
		$("." + myid).show();
		var tmp = myid.split("_");

		$("#newrateid").val(tmp[1]);
		if( $("#hidden_duration").val() < 24 ) {
			$("#daysrow").hide();
			$("#halfdaysrow").hide();
		}else{
			$("#daysrow").show();
			$("#halfdaysrow").show();
		}
		
		recompute();
	});
	
	$(".incbutton").click(function(){
		var target = $(this).attr("alt");
		var unitcost = $(target).attr("alt");

		if($(this).val()=="+") 
		{
			var newval = $(target).val()*1 + 1;
			$(target).val(  ( newval > 100 ) ? 100 : newval  );
		}
		else if($(this).val()=="-") 
		{
			var newval = $(target).val()*1 - 1;
			$(target).val(   (newval <= 0) ? 0 : newval );
		}
		recompute();
		
		$(target).trigger('change');
	});
	
	$(".dateinput").datepicker({dateFormat:'yy-mm-dd'});
	
	$(".denomination").click(function() {
		if($(this).val()=="Clear") {
			$("#" + focusme).val( 0);
			if(focusme=="new_cash_tendered_alt") {
				$("#new_cash_tendered").val(  $("#" + focusme).val()  * $("#currency").val()  );
			}	
		}else if($(this).val()=="Check In") {	
			
			if($("#newrateid").val()=="") {
				alert("Please select a valid rate.");
			}else{
				
				if($("#new_changedue").val()< 0)
				{
					if(confirm('Lack of Payment. Do you still want to continue?')){
					$("#act").val("newcheckin");
					$("#checkinform").submit();	
					}
				}else{
					$("#act").val("newcheckin");
					$("#checkinform").submit();	
				}
				
			}
			return false;
		}else{
			var curval = $("#" + focusme).val() * 1;
			var newval = $(this).val() * 1;
			var amountDue = curval + newval;
			$("#" + focusme).val( amountDue.toFixed(2) );
			if(focusme=="new_cash_tendered_alt") {
				$("#new_cash_tendered").val(  ($("#" + focusme).val()  * $("#currency").val()).toFixed(2)  );
			}			
		}

		var changeDue = $("#" + focusme).val();
		$("#new_changedue").val( changeDue );
		if($("#new_card_tendered").val() > 0) $("#carddetails").show();
		else $("#carddetails").hide();
		recompute();
	});
	$("#new_duration").click(function(){
		recompute();
	});

	$("#partner_commission, #partner_bpg, #new_roomdeposit, #promo_discount").on('change', function(e){
		recompute();
	});

	$(".minibar").change(function(){
		$.post('minibar.php',{mb: $(this).attr('id'), qty: $(this).val(), rm: $("#newroomid").val() });
	});

	$("#cmdFingerScan").click(function(){
			$.post("oicfscan.php",{act:'scan', user: $("#new_adj_oic").val()});
			myinterval = setInterval(checkFScan, 3000);
		});

	$('#cmdApprove').approver({
		url: 'oiclist_json.php',
		approvaltype : 'Checkin Discount',
        approvalinfo : '',
		hidetarget: true,
		success: function() {
			$("#new_discount").val( $("#new_adjustment_amount").val() );
			recompute();
		}	
	});
/*
	$("#cmdApprove").click(function(){
			if($('#new_oic_pwd').val()=='') {
				//
				alert('Invalid input.');
			}else{
				$.post('oicvalidator.php',{usr: $("#new_adj_oic").val(), pwd:$('#new_adj_pwd').val()},
					function(resp) {
						if(resp==$("#new_adj_oic").val()) {
							$("#new_discount").val( $("#new_adjustment_amount").val() );
							recompute();
						}else{
							alert('Invalid input.');
						}
					}
				);
			}
			return false;
		});
*/
});

function checkFScan(){
	$.post("oicfscan.php",{act:'monitor', user: $("#new_adj_oic").val()},
	function(resp){
		if(resp.success==true) {
			$("#new_adj_pwd").val(resp.pass);
			//$("#cmdApprove").attr("enabled",true);
			$("#cmdApprove").trigger("click");
			clearInterval(myinterval);
		}
	},'json');
}
</script>
