<?php
/**
* guest.php
*/
session_start();
include_once("config/config.inc.php");
include_once("reserve.functions.php");

if($_POST["act"]=="savenew") {
	parse_str($_POST["data"]);
	
	if($hiddenid == "" ) { // new record
		$sql = "insert into guests (firstname, middlename, lastname, address, phone, mobile, email, 
					company_name, company_position, company_address, company_email, company_phone, company_mobile,
					gender, birthday, nationality, remarks) 
		values ('$new_firstname', '$new_middlename', '$new_lastname', '$new_address', '$new_phone', '$new_mobile', '$new_email',
			'$new_company', '$new_position', '$new_companyaddress', '$new_companyemail', '$new_companyphone', '$new_companymobile',
					'$new_gender', '$new_birthday', '$new_nationality','$new_remarks'
			) ";
		R::exec($sql);
		$newid = R::getInsertId();
		$msg =  "$new_firstname $new_lastname saved to database.";
	}else{ //old record, update this
		$sql = "update  guests set firstname='$new_firstname', 
					middlename='$new_middlename', 
					lastname='$new_lastname', 
					address='$new_address', 
					phone='$new_phone', 
					mobile='$new_mobile', 
					email='$new_email' ,
					company_name='$new_company', 
					company_position='$new_position',
					company_address='$new_companyaddress', 
					company_phone='$new_companyphone', 
					company_mobile='$new_companymobile', 
					company_email='$new_companyemail' ,
					gender='$new_gender',
					birthday='$new_birthday',
					nationality='$new_nationality',
					remarks='$new_remarks'
					where guest_id='$hiddenid' ";
		R::exec($sql);
		$msg =  "Information for $new_firstname $new_lastname updated.";
	}
	echo '{id: '. $newid .' ,mesg:"' . $msg . '"}';
	exit;
}elseif($_POST["act"]=="search") {
	$key =  $_POST["keyword"];
	$gfrom = $_POST["gfrom"];
	
	
	if($gfrom == "" || $gfrom == "all" || $gfrom=='undefined'){	
	$sql = " select guest_id, firstname,  lastname, company_name, email, mobile from guests 
				where firstname like '%$key%' or lastname like '%$key%' or company_name like '%$key%' ";
				
	}else{
	$sql = " select a.guest_id, a.firstname,  a.lastname, a.company_name, a.email, a.mobile from guests a, occupancy_guests b 
				where a.firstname like '%$key%' or a.lastname like '%$key%' or a.company_name like '%$key%'  and  a.guest_id = b.guest_id";
	}
	
	$res = R::getAll($sql)
	$retval="";
	$head = "";
	if(!empty($res))  {
		foreach ($res as $r) {
			$i = $r['guest_id']; 
			$f = $r['firstname'];
			$l = $r['lastname'];
			$c = $r['company_name'];
			$e = $r['email'];
			$m = $r['mobile'];
			$retval.="<tr id='$i'><td><a href='#$i' alt='$i' class='launcher'>$f</a></td><td>$l</td><td>$c</td><td>$e</td><td>$m</td></tr>";
		}
		$head = "<tr><th>First Name</th>";
		$head .= "<th>Last Name</th>";
		$head .= "<th>Company Name</th>";
		$head .= "<th>Email</th>";
		$head .= "<th>Mobile No.</th></tr>";
	}
	
	echo '{ count : ' . $numrows . ' , head : "'. $head . '" , body : "' . $retval . '"  }';
	exit;
}elseif($_POST["act"]=="specific") {
	$gid =  $_POST["gid"];
	$sql = " select * from guests 
				where guest_id='$gid' ";
	$res = R::getAll($sql);
	$retval="";
	$head = "";
	echo '{ data :  '.json_encode($res).'  }';
	exit;
}elseif($_POST["act"]=="delete") {
	parse_str($_POST["data"]);
	$sql = "delete from guests where guest_id='$hiddenid'";
	R::exec($sql);
	exit;
}

?>
<form method="post" action="" name="guestform" id="guestform">
<div id="mesg"></div>
<table width="900">
<tr><td  valign='top'>
<fieldset>
<legend>Personal Information</legend>
<table id="guestinfo">
<tbody>
<tr>
<td><label for="new_firstname">First Name</label><br />
<input type="text"  name="new_firstname" id="new_firstname" class="keypadfield" /></td>
<td><label for="new_middlename">Middle Name</label><br />
<input type="text" name="new_middlename" id="new_middlename"  class="keypadfield" /></td>
<td><label for="new_lastname">Last Name</label><br />
<input type="text" name="new_lastname" id="new_lastname"  class="keypadfield" /></td>
</tr>
<tr>
<td colspan=3><label for="new_address">Complete Address</label><br />
<input type="text"  name="new_address" id="new_address" class="keypadfield" /></td>
</tr>
<tr>
<td><label for="new_email">Email</label><br />
<input type="text"  name="new_email" id="new_email" class="keypadfield" /></td>
<td><label for="new_phone">Phone Number</label><br />
<input type="text" name="new_phone" id="new_phone"  class="keypadfield" /></td>
<td><label for="new_mobile">Mobile Number</label><br />
<input type="text" name="new_mobile" id="new_mobile"  class="keypadfield" /></td>
</tr>
<tr>
<td><label for="new_gender">Gender</label><br />
<input type="radio"  name="new_gender" id="new_gender_m"  value='M' /><label for="new_gender_m">Male</label>
<input type="radio"  name="new_gender" id="new_gender_f" value='F' /><label for="new_gender_f">Female</label>
</td>
<td><label for="new_birthday">Birthday</label><br />
<input type="text" name="new_birthday" id="new_birthday"  class="datefield" /></td>
<td><label for="new_nationality">Nationality</label><br />
<input type="text" name="new_nationality" id="new_nationality"  class="keypadfield" /></td>
</tr>
<tr>
<td colspan=3><label for="new_remarks" >Remarks / Special Preferences</label><br />
<textarea id="new_remarks" name="new_remarks" class="keypadfield" /></textarea>
</tr>
</tbody>
</table>
</fieldset>
<fieldset>
<legend>Company Information</legend>
<table id="companyinfo">
<tbody>
<tr>
<td colspan=2><label for="new_company">Company Name</label><br />
<input type="text"  name="new_company" id="new_company" class="keypadfield" /></td>
<td><label for="new_position">Position</label><br />
<input type="text" name="new_position" id="new_position"  class="keypadfield" /></td>
</tr>
<tr>
<td colspan=3><label for="new_companyaddress">Company Address</label><br />
<input type="text"  name="new_companyaddress" id="new_companyaddress" class="keypadfield" /></td>
</tr>
<tr>
<td><label for="new_companyemail">Email</label><br />
<input type="text"  name="new_companyemail" id="new_companyemail" class="keypadfield" /></td>
<td><label for="new_companyphone">Phone Number</label><br />
<input type="text" name="new_companyphone" id="new_companyphone"  class="keypadfield" /></td>
<td><label for="new_companymobile">Mobile Number</label><br />
<input type="text" name="new_companymobile" id="new_companymobile"  class="keypadfield" /></td>
</tr>
</tbody>
</table>
</fieldset>

<fieldset>
<input type="button" class="cmdbtn" value="Save" id="cmdsave" />
<input type="reset" class="cmdbtn" value="Clear" id="cmdreset" onclick="guestform.hiddenid.value=''"/>
<div id="selectedGuestActions">
<hr />
<input type="button" class="cmdbtn" value="Delete" id="cmddelete" />
<input type="button" class="cmdbtn" value="Check In" id="cmdcheckin" />
<?php echo getAvailableRoomsDropdown() ?>
</div>
<input type="hidden" name="hiddenid" id="hiddenid" />
</fieldset>
</td><td valign='top' width="50%">
<!-- search -->
<fieldset><legend>Search</legend>
<div>
<input type="text" class="keypadfield" name="searchkeyword" id="searchkeyword"  style='width:80%'/>
<input type="button"  name="searchgo" id="searchgo" value="go" class="cmdbtn" style='width:40px;padding:1px;' /><br />
<input type="radio" id="allguests" name="gfrom" value="all"><label for="allguests">All Guests</label>
<input type="radio" id="ciguests" name="gfrom" value="ci"><label for="ciguests">Checked In Guests</label>
<div id="searchcount"></div>
<table id="searchtable" width="100%">
<thead></thead>
<tbody></tbody>
</table>
</div>
</fieldset>
</td></tr>
</table>
</form>
<style>
body{font-family:lucida,arial,helvetica;font-size:.7em}
fieldset {margin-top:10px;}
fieldset legend {border:1px solid #cccccc;background-color:#ececec;font-family:lucida,arial,helvetica;font-size:.7em}
#guestinfo td {padding:2px; border:1px solid #dddddd;}
#companyinfo td {padding:2px; border:1px solid #dddddd;}
#searchtable {empty-cells:show; border-collapse:false;border-spacing:0px;}
#searchtable td {padding:2px; border-bottom:1px dotted #dddddd;font-size:.7em}
#searchtable  th {padding:2px; border-bottom:1px solid #51B1D8;font-size:.8em}
#guestinfo  label {font-size:.7em}
#companyinfo  label {font-size:.7em}
#searchcount   {font-size:.7em; color:#ff6600;margin-top:16px;}
.keypadfield{ border:1px solid #51B1D8; background-color:#E0EBEF; width:100%}
.cmdbtn {
	background-color:#95D5EF;
	border:1px solid #E0EBEF;
	width:80px;
	padding:4px;
	cursor:pointer;
}
label {font-size:.7em;}
</style>
 <link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type='text/javascript' src='../js/jquery.plugin.min.js'></script>
<script type='text/javascript' src='../js/jquery.keypad.min.js'></script>
<link rel="stylesheet" type="text/css" href="../js/jquery.keypad.css" />
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script lang="javascript">
$(document).ready(function(){
	$('#selectedGuestActions').hide();
	$(".keypadfield").keypad({keypadOnly:false, layout: $.keypad.qwertyLayout});
	$("#cmdsave").click(function(){
		$.post("guest.php",{act:'savenew', data: $("#guestform").serialize()}, function(resp) {
			$("#hiddenid").val(resp.id);
			$("#mesg").html(resp.mesg);
		}, "json");
		return false;
	});
	$("#cmddelete").click(function(){
		$.post("guest.php",{act:'delete', data: $("#guestform").serialize()}, function(resp) {
			//
		}, "json");
		$('cmdreset').trigger('click');
		return false;
	});
	$("#cmdcheckin").click(function(){
		var room = $('#checkinRoom').val();
		var fname = $('#new_firstname').val();
		var lname = $('#new_lastname').val();
		var comp  = $('#new_companyname').val();
		var gid	   = $('#hiddenid').val();
		document.location.href='checkinform.php?room=' + room + '&fn=' + fname + '&ln=' + lname + '&cm=' + comp + '&gid=' + gid;
		return false;
	});

	$("#allguests").attr('checked','checked');
	$("#new_birthday").datepicker({dateFormat:'yy-mm-dd'});
	$("#searchgo").click(function(){
		$.post("guest.php", {act:'search', keyword: $("#searchkeyword").val(), gfrom: $("input[@name='gfrom']:checked").val() } , 
			function(resp) {
				$("#searchcount").html( resp.count + " guest(s) found. ");
				$("#searchtable thead").html(resp.head);
				$("#searchtable tbody").html(resp.body);
				$("a.launcher").click( 
					function() {
						var wcgst  = $(this).attr("alt"); 
						$.post('guest.php', {act:'specific', gid: wcgst}, function(resp) {
							/*
							$.each(resp.data, function(x,y){
								$(x).val(y);
								//document.getElementById(x).value=y;
							});
							*/
							$("#new_firstname").val(resp.data.firstname);
							$("#new_middlename").val(resp.data.middlename);
							$("#new_lastname").val(resp.data.lastname);
							$("#new_company").val(resp.data.company_name);
							$("#new_address").val(resp.data.address);
							$("#new_email").val(resp.data.email);
							$("#new_phone").val(resp.data.phone);
							$("#new_mobile").val(resp.data.mobile);
							$("#new_birthday").val(resp.data.birthday);
							$("#new_nationality").val(resp.data.nationality);
							$("#new_remarks").val(resp.data.remarks);
							$("#new_position").val(resp.data.company_position);
							$("#new_companyaddress").val(resp.data.company_address);
							$("#new_companyemail").val(resp.data.company_email);
							$("#new_companyphone").val(resp.data.company_phone);
							$("#new_companymobile").val(resp.data.company_mobile);							
							$("#hiddenid").val(resp.data.guest_id);	
							var _val =  resp.data.gender;																				
							var varname;
							if(_val == "M"){
								varname = "#new_gender_m";
							}else{
								varname = "#new_gender_f";
							}							
							$(varname).attr("checked","checked");			
							$('#selectedGuestActions').show();
							return false;
						}, "json"  );
					} 
				);
			},"json");
	});
	
});
	
</script>
