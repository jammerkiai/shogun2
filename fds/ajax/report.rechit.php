<?php
session_start();
include_once("config/config.inc.php");

$lsql = "select settings_value from settings where id = '3'";
$lobbyid = R::getCell($lsql);


function getStartTime()
{
	$sql = "select datetime from `shift-transactions` where shift = 'start' order by datetime desc";

	return R::getCell($sql);
}


function getCheckoutReport($start,$end,$suser_id,$euser_id,$lobbyid)
{
	$sql = "select settings_value from settings where id = '1'";
	$value = R::getCell($sql);

	$ret.="<div class='report'>";
	$ret.= "<b>".strtoupper($value)."</b><br>";
	$ret.= "<b>ROOM CHECKOUT REPORT</b><br>";

	//$ret.= "<b>PRINTED TIME: </b>".date("l m/d/Y g:i:s A")."<br>";
	$ret.= "<b>SHIFT: </b>".getshift($start). "&nbsp;&nbsp;&nbsp;&nbsp;".date("m/d/Y g:i:s A",strtotime($start));
	$ret.= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>CASHIER: </b>";
	if($suser_id == $euser_id)
	{
		$_sql = "select fullname from users where user_id = '$euser_id'";
		$cashier = R::getCell($_sql);
		$ret.=$cashier;
	}else
	{

		//$ret.=" - ";

		$_sql = "select fullname from users where user_id = '$euser_id'";
		$ecashier = R::getCell($_sql);
		$ret.=$ecashier;
	}
	$ret.= "<br>";
	$ret.="</div>";
	$ret.= "<br>";
	$ret.= "<br>";
	$sql = "select * from occupancy a, rooms b, room_types c
	where a.actual_checkin <= '$start'
	and a.actual_checkout >= '$end' )
	and a.room_id = b.room_id
	and c.room_type_id = b.room_type_id
	order by c.rank asc,
	a.rate_id desc,
	a.actual_checkout asc";


	$res = R::getAll($sql);

	$ret.= "<table class='report' cellpadding=\"5\"  >";
	$ret.= "<tr>";
	//$ret.= "<th>OCCUPANCY</th>";
	$ret.= "<th>RM_TYPE</th>";
	$ret.= "<th>RM_NO</th>";
	$ret.= "<th>HRS</th>";
	$ret.= "<th>RATE</th>";
	$ret.= "<th>OT</th>";
	$ret.= "<th>DISC</th>";
	$ret.= "<th>INTOT</th>";
	$ret.= "<th>CHECKIN </th>";
	$ret.= "<th>CHECKOUT </th>";
	$ret.= "<th>FOOD</th>";
	$ret.= "<th>BEER</th>";
	$ret.= "<th>MISC</th>";
	$ret.= "<th>ADJUST</th>";
	$ret.= "<th>DEDUCT</th>";
	$ret.= "<th>OT</th>";
	$ret.= "<th>TOTAL</th>";
	$ret.= "</tr>";
	/***********
		LOBBY
	************/
	$ret.= "<tr>";
	$ret.= "<td>Lobby</td>";
	$ret.= "<td>Lobby</td>";
	$ret.= "<td>&nbsp;</td>";
	$ret.= "<td>&nbsp;</td>";
	$ret.= "<td>&nbsp;</td>";
	$ret.= "<td>&nbsp;</td>";
	$ret.= "<td>&nbsp;</td>";
	$ret.= "<td>&nbsp;</td>";
	$ret.= "<td>&nbsp;</td>";

	//Lobby Food
	$_sql = "select sum(unit_cost*qty) from fnb_sales
			where status in ('Paid')
			and category_id <> '21'
			and category_id <> '17'
			and occupancy_id = '$lobbyid'
			and update_date >= '$start'
			and update_date <= '$end' ";
	$fnbtot = R::getCell($_sql);

	//Lobby Beer
	$_sql = "select unit_cost*qty as cost from fnb_sales
		where status in ('Paid')
		and (category_id = '21'
		or category_id = '17')
		and occupancy_id = '$lobbyid'
		and update_date >= '$start'
		and update_date <= '$end' ";
	$_res = R::getAll($_sql);
	$beertot = 0;
	foreach ($_res as $r)
	{
		$beerlobbyamount = $r['cost'];
		$beerlobbytot = $beerlobbytot + $beerlobbyamount;
	}

	//misc
	$_sql = "select sum(unit_cost*qty) from room_sales
		where status in ('Paid')
		and category_id <> '3'
		and occupancy_id = '$lobbyid'
		and update_date >= '$start'
		and update_date <= '$end' ";

	$misctot = R::getCell($_sql);

	if($fnblobbytot=='')$fnblobbytot='';
	else $fnblobbytot =number_format($fnblobbytot,2);
	if($beerlobbytot=='')$beerlobbytot='';
	else $beerlobbytot=number_format($beerlobbytot,2);
	if($misclobbytot=='')$misclobbytot='';
	else $misclobbytot=number_format($misclobbytot,2);
	if($lobbytotal=='0')$lobbytotal='0.00';
	else $lobbytotal=number_format($lobbytotal);


	$ret.= "<td>$fnblobbytot</td>";
	$ret.= "<td>$beerlobbytot</td>";
	$ret.= "<td>$misclobbytot</td>";
	$ret.= "<td>&nbsp;</td>";
	$ret.= "<td>&nbsp;</td>";
	$ret.= "<td>&nbsp;</td>";
	$ret.= "<td>$lobbytotal</td>";
	$ret.= "</tr>";

	foreach ($res as $row)
	{
		$_sql = "select a.room_type_id,a.door_name,b.room_type_name
				from rooms a, room_types b
				where a.room_type_id=b.room_type_id
				and a.room_id = '".$row["room_id"]."'";
		$_res = R::getRow($_sql);
		$id = $_res['room_type_id'];
		$door_name = $_res['door_name'];
		$room_type_name = $_res['room_type_name'];



		$ret .="<tr>";
		//$ret .="<td>".$row["occupancy_id"]."</td>";
		$ret .="<td>".$room_type_name."</td>";
		$ret .="<td>".$door_name."</td>";

		$_sql = "select rate_name from rates where rate_id = '".$row["rate_id"]."'";
		
		$rate_name = R::getCell($_sql);
		$rate_name = str_replace("HRS","",$rate_name);
		$ret .="<td>".$rate_name."</td>";

		$_sql = "select unit_cost*qty from room_sales where occupancy_id = '".$row["occupancy_id"]."'
				and status in ('Paid')
				and item_id = '15'
				order by roomsales_id desc
				limit 0,1";
		$cost = R::getCell($_sql);

		$ret .="<td>".number_format($cost)."</td>";

		$_sql = "select a.unit_cost*a.qty as total, a.roomsales_id
			FROM room_sales a, occupancy_log b
			WHERE a.occupancy_id = b.occupancy_id
			AND b.transaction_date = a.update_date
			AND a.item_id = '16'
			AND b.transaction_type in ('CheckIn')
			AND a.status in ('Paid')
			AND a.occupancy_id = '".$row["occupancy_id"]."'";
		$_res = R::getRow($_sql);
		$in_ot = $_res['total'];
		$inextid = $_res['roomsales_id'];

		if($in_ot=='')
		{
			$din_ot = '';
		}else{
			$din_ot=$in_ot;
		}
		$ret .="<td>".$din_ot."</td>";

		$_sql = "select unit_cost*qty from room_sales
			where status in ('Paid')
			and item_id = '17'
			and occupancy_id = '".$row["occupancy_id"]."'";
		$disc = R::getCell($_sql);
		if(!$disc) {
			$disc ='';
		}else{
			$disc=abs($disc);
		}
		$ret .="<td>".$disc."</td>";


		$in_total = ($cost+$in_ot)-abs($disc);

		$ret .="<td>".$in_total."</td>";



		$ret .="<td NOWRAP>".date("m/d/y - g:i A", strtotime($row["actual_checkin"]))."</td>";

		$ret .="<td NOWRAP>".date("m/d/y - g:i A", strtotime($row["actual_checkout"]))."</td>";


		$_sql = "select unit_cost*qty as total
			FROM room_sales
			WHERE item_id = '16'
			AND roomsales_id not in ('$inextid')
			AND occupancy_id = '".$row["occupancy_id"]."'";
		$_res = R::getAll($_sql);
		$out_ot1 = 0;
		foreach ($_res as $r)
		{
			$ecost = $r['total'];
			$out_ot1 += $ecost;
		}





		//food
		$_sql = "select sum(unit_cost*qty) from fnb_sales
			where status in ('Paid')
			and category_id <> '21'
			and category_id <> '17'
			and occupancy_id = '".$row["occupancy_id"]."'";
		
		$fnbtot =0;
		$fnbtot = R::getCell($_sql);

		//beer
		$_sql = "select sum(unit_cost*qty) from fnb_sales
			where status in ('Paid')
			and (category_id = '21'
			or category_id = '17')
			and sales_date between '$start' and '$end'
			and occupancy_id = '".$row["occupancy_id"]."'";
		$beertot = 0;
		$beertot = R::getCell($_sql);

		//misc
		$_sql = "select sum(unit_cost*qty) from room_sales
			where status in ('Paid')
			and category_id <> '3'
			and sales_date between '$start' and '$end'
			and occupancy_id = '".$row["occupancy_id"]."'";
		$misctot = R::getCell($_sql);

		//adjustment
		$_sql = "select sum(unit_cost*qty) as total from room_sales
			where status in ('Paid')
			and item_id = '18'
			and sales_date between '$start' and '$end'
			and occupancy_id = '".$row["occupancy_id"]."'";
		$adjtot = 0;
		$adjtot = R::getCell($_sql);

		//deduction
		$_sql = "select sum(unit_cost*qty) from room_sales
			where status in ('Paid')
			and item_id = '27'
			and sales_date between '$start' and '$end'
			and occupancy_id = '".$row["occupancy_id"]."'";
		$detot = 0;
		$detot = R::getCell($_sql);




		if($fnbtot=='0.00')$fnbtot ='';
		else $fnbtot =number_format($fnbtot,2);
		if($beertot=='0.00')$beertot ='';
		else $beertot =number_format($beertot,2);
		if($misctot=='0.00')$misctot ='';
		else $misctot =number_format($misctot,2);
		if($adjtot=='0.00')$adjtot ='';
		else $adjtot =number_format($adjtot,2);
		$ret .="<td>".$fnbtot."</td>";
		$ret .="<td>".$beertot."</td>";
		$ret .="<td>".$misctot."</td>";
		$ret .="<td>".$adjtot."</td>";
		$ret .="<td>(".number_format(abs($detot),2).")</td>";


		$ot_total = $in_ot+$out_ot1;
		if($ot_total=='0.00')$ot_total ='';

		$total = ($in_total + $out_ot1 + $fnbtot + $misctot + $beertot + $adjtot) - abs($detot);
		$ret .="<td>".$ot_total."</td>"; //OT TOTAL
		$ret .="<td>".number_format($total)."</td>";
		$ret .="</tr>";
		$ftotal += $total;

		}
		$ret .="</table>";
		//$ret .= getCheckoutSummary($start,$end,$suser_id,$euser_id,$lobbyid);
		//echo $ftotal;
		return $ret;

}



function getCheckoutSummary($start,$end,$suser_id,$euser_id,$lobbyid)
{
	$sql = "SELECT `room_type_id`, `room_type_name`, `site_id` FROM `room_types` order by rank asc";

	$ret .= "<div class='report'>";
	$ret .= "<b>Rechit SUMMARY (Inclusive of food, misc, beer and others)</b><br>";
	$ret.= "<b>SHIFT: </b>".getshift($start). "&nbsp;&nbsp;&nbsp;&nbsp;".date("m/d/Y - g:i:s A",strtotime($start));
	$ret.= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>CASHIER: </b>";
	if($suser_id == $euser_id)
	{
		$_sql = "select fullname from users where user_id = '$euser_id'";
		$cashier = R::getCell($_sql);

		$ret.=$cashier;
	}else
	{
		$_sql = "select fullname from users where user_id = '$suser_id'";
		$scashier = R::getCell($_sql);
		$ret.=$scashier;

		$ret.=" - ";

		$_sql = "select fullname from users where user_id = '$euser_id'";
		$ecashier = R::getCell($_sql);
		$ret.=$ecashier;
	}
	$ret.= "<br>";
	$ret.="</div>";
	$ret .= "<table cellpadding=5 cellspacing=5 class='summary'>";
	$ret .= "<tr>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td>#TOTAL</td>";
	$ret .= "<td>3 HRS</td>";
	$ret .= "<td>12 HRS</td>";
	$ret .= "<td>24 HRS</td>";
	$ret .= "<td>ROOM</td>";
	$ret .= "<td>OT</td>";
	$ret .= "<td>FOOD</td>";
	$ret .= "<td>BEER</td>";
	$ret .= "<td>MISC</td>";
	$ret .= "<td>ADJUST</td>";
	$ret .= "<td>DEDUC</td>";
	$ret .= "<td>TOTAL</td>";
	$ret .= "</tr>";

	$ret .= "<tr>";
	$ret .= "<td>Lobby</td>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td>&nbsp;</td>";


	$_sql = "select sum(unit_cost*qty) from fnb_sales
			where status in ('Paid')
			and category_id <> '21'
			and category_id <> '17'
			and occupancy_id = '$lobbyid'
			and update_date >= '$start'
			and update_date <= '$end' ";
	$fnbtot =0;
	$fnblobbytot = R::getCell($_sql);
	

	//Lobby Beer
	$_sql = "select sum(unit_cost*qty) from fnb_sales
		where status in ('Paid')
		and (category_id = '21'
		or category_id = '17')
		and occupancy_id = '$lobbyid'
		and update_date >= '$start'
		and update_date <= '$end' ";
	$beertot = 0;
	$beerlobbytot = R::getCell($_sql);


	//misc
	$_sql = "select sum(unit_cost*qty) from room_sales
		where status in ('Paid')
		and category_id <> '3'
		and occupancy_id = '$lobbyid'
		and update_date >= '$start'
		and update_date <= '$end' ";
	$misclobbytot = R::getCell($_sql);
	$misctot = 0;

	$lobbytotal = $fnblobbytot + $beerlobbytot + $misclobbytot;

	$ret .= "<td>$fnblobbytot</td>";
	$ret .= "<td>$beerlobbytot</td>";
	$ret .= "<td>$misclobbytot</td>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td><b>$lobbytotal</b></td>";
	$ret .= "</tr>";

	$ret .= "<tr>";
	$ret .= "<td colspan=13>
			<div style='border-width:3px;
			border-top-color:black;
			border-top-style:solid;
			border-right-style:hidden;
			border-bottom-style:hidden;
			border-left-style:hidden;
			text-align:left'>

			</div>
			</td>";
	$ret .= "</tr>";

	$site = "2";
	$ctr = 1;
	$res = R::getAll($sql);
	foreach ($res as $r)
	{
		$room_type_id = $r['room_type_id']; 
		$room_type_name = $r['room_type_name'];
		$site_id = $r['site_id'];

		$ret .= "<tr>";
		$ret .= "<td>".$room_type_name."</td>
		<td>".getNumRoomByTypeID($room_type_id,$start,$end)."</td>";


		//$_sql = "select b.rate_name, count(a.occupancy_id) from (rates b left join occupancy a  on a.rate_id=b.rate_id ) left join rooms c
		//		on a.room_id=c.room_id
		//		where a.actual_checkout >= '$start'
		//		and a.actual_checkout <= '$end'
		//		and c.room_type_id = '$room_type_id'
		//		and count(a.occupancy_id) = ''
		//		group by a.rate_id
		//		limit 0,3";

		$_sql = "select rate_id from rates limit 0,3";
		$_res = R::getAll($_sql);
		foreach ($_res as $_r)
		{
			$rate_id = $_r['rate_id'];

			$__sql = "select count(a.occupancy_id) from rates b , occupancy a, rooms c
			where a.rate_id=b.rate_id
			and c.room_id = a.room_id
			and a.actual_checkout = '0000-00-00 00:00:00'
			and b.rate_id = '$rate_id'
			and c.room_type_id = '$room_type_id'";
			$cnt = R::getCell($__sql);
			if($cnt=='0')$cnt='&nbsp;';
			$ret .= "<td>$cnt</td>";

		}

		//food
		$_sql = "select sum(b.unit_cost*b.qty) from occupancy a, fnb_sales b, rooms c
			where b.status in ('Paid')
			and a.occupancy_id = b.occupancy_id
			and a.room_id = c.room_id
			and a.actual_checkout = '0000-00-00 00:00:00'
			and a.actual_checkin < b.sales_date
			and b.sales_date between '$start' and '$end'
			and c.room_type_id = '$room_type_id'
			and (b.category_id <> '21' and b.category_id <> '17')";
		$fnbtot ="";
		$fnbtot = R::getCell($__sql);


		//beer
		$_sql = "select sum(b.unit_cost*b.qty) from occupancy a, fnb_sales b, rooms c
			where b.status in ('Paid')
			and a.occupancy_id = b.occupancy_id
			and a.room_id = c.room_id
			and a.actual_checkout = '0000-00-00 00:00:00'
			or a.actual_checkin < b.sales_date
			and b.sales_date between '$start' and '$end'
			and c.room_type_id = '$room_type_id'
			and (b.category_id = '21' or b.category_id = '17')";

		$beertot = R::getCell($__sql);

		//misc
		$_sql = "select sum(b.unit_cost*b.qty) from occupancy a, room_sales b, rooms c
			where b.status in ('Paid')
			and a.occupancy_id = b.occupancy_id
			and a.room_id = c.room_id
			and a.actual_checkout = '0000-00-00 00:00:00'
			or a.actual_checkin < b.sales_date
			and b.sales_date between '$start' and '$end'
			and c.room_type_id = '$room_type_id'
			and b.category_id <> '3'";
		$misctotal = R::getCell($__sql);

		

		$_sql = "select sum(unit_cost*qty) from occupancy a, room_sales b,  rooms c
			where b.status in ('Paid')
			and a.occupancy_id = b.occupancy_id
			and a.room_id = c.room_id
			and a.actual_checkout = '0000-00-00 00:00:00'
			or a.actual_checkin < b.sales_date
			and b.sales_date between '$start' and '$end'
			and c.room_type_id = '$room_type_id'
			and b.item_id = '17'";

		$disctotal = R::getCell($__sql);


		//adjustment
		$_sql = "select sum(unit_cost*qty) from occupancy a, room_sales b,  rooms c
			where b.status in ('Paid')
			and a.occupancy_id = b.occupancy_id
			and a.room_id = c.room_id
			and a.actual_checkout = '0000-00-00 00:00:00'
			and a.actual_checkin < b.sales_date
			and b.sales_date between '$start' and '$end'
			and c.room_type_id = '$room_type_id'
			and b.item_id = '18'";
		$adjtot = R::getCell($__sql);

		//deduction
		$_sql = "select unit_cost*qty from occupancy a, room_sales b,  rooms c
			where b.status in ('Paid')
			and a.occupancy_id = b.occupancy_id
			and a.room_id = c.room_id
			and a.actual_checkout = '0000-00-00 00:00:00'
			and a.actual_checkin < b.sales_date
			and b.sales_date between '$start' and '$end'
			and c.room_type_id = '$room_type_id'
			and b.item_id = '27'";

		$detot = R::getCell($__sql);
		$detot = abs($detot);

		$roomtot = getRoomTotalByTypeID($room_type_id,$start,$end) + $disctotal;

		$disctotal = 0;
		$ottot = getOTByRoomTypeID($room_type_id,$start,$end);

		if($roomtot>0)$froomtot=number_format($roomtot);else $froomtot='';
		if($ottot>0)$fottot=number_format($ottot);else $fottot='';
		if($fnbtot>0)$ffnbtot=number_format($fnbtot);else $ffnbtot='';
		if($beertot>0)$fbeertot=number_format($beertot);else $fbeertot='';
		if($misctotal>0)$fmisctotal=number_format($misctotal);else $fmisctotal='';
		if($adjtot>0)$fadjtot=number_format($adjtot);else $fadjtot='';
		if($detot>0)$fdetot=number_format($detot);else $fdetot='';

		$ret .= "<td>".$froomtot."</td>";
		$ret.= "<td>".$fottot."</td>";
		$ret .= "<td>".$ffnbtot."</td>";
		$ret .= "<td>".$fbeertot."</td>";
		$ret .= "<td>".$fmisctotal."</td>";
		$ret .= "<td>".$fadjtot."</td>";
		$ret .= "<td>".$fdetot."</td>";
		$sitetotal += ($roomtot + $ottot + $fnbtot + $beertot + $misctotal + $adjtot) - ($detot);
		$siteroomtotal += $roomtot;
		$siteottotal += $ottot;
		$sitefnbtotal += $fnbtot;
		$sitebeertotal += $beertot;
		$sitemisctotal += $misctotal;
		$siteadjtot += $adjtot;
		$sitedetot += $detot;
		$grandtotal = ($roomtot + $ottot + $fnbtot + $beertot + $misctotal + $adjtot) - ($detot);

		$ret.= "<td>".number_format($grandtotal)."</td>";
		$ret .= "</tr>";


		$i++;

		if($ctr == 6)
		{
			$ret .= "<tr>";
			$ret .= "<td colspan=13>
			<div style='border-width:3px;
			border-top-color:black;
			border-top-style:solid;
			border-right-style:hidden;
			border-bottom-style:hidden;
			border-left-style:hidden;
			text-align:left'>

			</div>
			</td>";
			$ret .= "</tr>";
			$ret .= "<tr>";
			$sql1 = "select site_name from sites where site_id = '$site'";
			$sitename = R::getCell($sql1);
			$ret .= "<td colspan='5'><b>$sitename TOTAL</b></td>";
			$ret .= "<td>".number_format($siteroomtotal)."</td>";
			$ret .= "<td>".number_format($siteottotal)."</td>";
			$ret .= "<td>".number_format($sitefnbtotal)."</td>";
			$ret .= "<td>".number_format($sitebeertotal)."</td>";
			$ret .= "<td>".number_format($sitemisctotal)."</td>";
			$ret .= "<td>".number_format($siteadjtot)."</td>";
			$ret .= "<td>".number_format($sitedetot)."</td>";
			$ret .= "<td><b>".number_format($sitetotal)."</b></td>";
			$ret .= "</tr>";

			$gtotal += $sitetotal;
			$sitetotal = 0;
			$siteroomtotal = 0;
			$siteottotal = 0;
			$sitefnbtotal = 0;
			$sitebeertotal = 0;
			$sitemisctotal = 0;
			$siteadjtot = 0;
			$sitedetot = 0;

			$site += 1;

		}

		$ctr = $ctr + 1;


	}
	$ret .= "<tr>";
		$ret .= "<td colspan=13>
			<div style='text-align:right'>
			<br><br><br>
			<b>GRAND TOTAL: ".number_format($gtotal+$lobbytotal)."<b>
			</div>
			</td>";
		$ret .= "</tr>";
	$ret .= "</table>";




	return $ret;
}

function getRoomTotalByTypeID($room_type_id,$startshift,$end)
{
	$sql = "select a.occupancy_id from  occupancy a,rooms b
	where a.room_id = b.room_id
	and a.actual_checkout = '0000-00-00 00:00:00'
	or a.actual_checkout > '$end'
	and b.room_type_id = '$room_type_id'";
	$res = R::getAll($sql);
	$total = 0;
	foreach ($res as $r)
	{
		$id = $r['occupancy_id'];
		$_sql = "select unit_cost*qty as total from room_sales where item_id = '15' and occupancy_id = '$id' and sales_date between '$startshift' and '$end' ";
		$_res = R::getRow($_sql);
		$unitqty = $_res['total'];
		$total += $unitqty;
	}

	return $total;

}

function getNumRoomByTypeID($room_type_id,$startshift,$end)
{
	$sql = "select a.occupancy_id from  occupancy a,rooms b
	where a.room_id = b.room_id
	and a.actual_checkout = '0000-00-00 00:00:00'
	or a.actual_checkout > '$end'
	and b.room_type_id = '$room_type_id'";
	$res = R::getAll($sql);
	$num = count($res);
	return $num;
}
function getAmountByRoomTypeID($roomtypeid,$startdt,$end)
{

	$sql = "select a.occupancy_id from  occupancy a,rooms b
	where a.room_id = b.room_id
	and a.actual_checkout = '0000-00-00 00:00:00'
	and b.room_type_id = '$roomtypeid'";

	$res = R::getAll($sql);

	foreach ($res as $r)
	{
		$occupancy_id =  $r['occupancy_id'];
		$id .= "'".$occupancy_id."',";
	}
	$id  = substr_replace($id ,"",-1);
	if($id!="")
	{
		$_sql = "select sum(unit_cost*qty) from room_sales
		where item_id ='15'
		and occupancy_id in ($id)
		and sales_date between '$startshift' and '$end'
		and status in ('Paid')";
		
		$subtotal = R::getCell($_sql);
		$total += $subtotal;

		//food and beer
		$_sql = "select sum(unit_cost*qty) from fnb_sales
			where status in ('Paid')
			and sales_date between '$startshift' and '$end'
			and occupancy_id in ($id)";
		$subtotal = R::getCell($_sql);
		$total += $subtotal;

		//adjustment
		$_sql = "select sum(unit_cost*qty) from room_sales
			where status in ('Paid')
			and item_id = '18'
			and sales_date between '$startshift' and '$end'
			and occupancy_id in ($id)";
		$subtotal = R::getCell($_sql);
		$total += $subtotal;

		//misc
		$_sql = "select sum(unit_cost*qty) from room_sales
			where status in ('Paid')
			and category_id <> 3
			and sales_date between '$startshift' and '$end'
			and occupancy_id in ($id)";
		$subtotal = R::getCell($_sql);
		$total += $subtotal;

		//deduction
		$_sql = "select sum(unit_cost*qty) from room_sales
			where status in ('Paid')
			and item_id = '27'
			and sales_date between '$startshift' and '$end'
			and occupancy_id in ($id)";
		$dtotal = R::getCell($_sql);

		$total = $total - abs($dtotal);
	}

	return $total;
}
function getOTByRoomTypeID($roomtypeid,$startdt,$end)
{

	$sql = "select a.occupancy_id from  occupancy a,rooms b
	where a.room_id = b.room_id
	and a.actual_checkout = '0000-00-00 00:00:00'
	and b.room_type_id = '$roomtypeid'";

	$res = R::getAll($sql)

	foreach ($res as $r)
	{
		$occupancy_id = $r['occupancy_id'];
		$id .= "'".$occupancy_id."',";
	}
	$id  = substr_replace($id ,"",-1);
	if($id!="")
	{
		$_sql = "select sum(unit_cost*qty) from room_sales
		where item_id ='16'
		and occupancy_id in ($id)
		and sales_date between '$startshift' and '$end'
		and status in ('Paid')";
		$total = R::getCell($_sql);
	}

	return $total;
}

function getLatestShifts($shiftid)
{
	$sql = "SELECT `shift-transaction_id`,datetime,user_id  FROM `shift-transactions` where shift = 'start' order by datetime desc ";
	$res = R::getAll($sql);
	$ret = "<select name='rblshifts' id='rblshifts' onchange='myform.submit();'>";
	$ret .= "<option value=''>&nbsp;</option>";
	foreach ($res as $r)
	{
		$shift_transaction_id = $r['shift-transaction_id'];
		$datetime = $r['datetime'];
		$userid = $r['user_id'];

		if($shiftid == $shift_transaction_id)
		{
			$select = "selected";
		}else
		{
			$select = " ";
		}
		$__sql = "SELECT user_id  FROM `shift-transactions` where shift = 'end'
		and `datetime` > '$datetime'
		order by datetime asc
		limit 0,1";
		$userid = R::getCell($__sql);
		$_sql = "select fullname from users where user_id = '$userid'";
		$username = R::getCell($_sql);
		$ret .= "<option value='$shift_transaction_id' $select>$datetime - $username - ".getshift($datetime)."</option>";
	}
	$ret .= "</select>";

	return $ret;
}


$shiftid = $_POST["rblshifts"];
$sql = "select datetime,user_id from `shift-transactions` where `shift-transaction_id` = '$shiftid'";
$res = R::getRow($sql);
$sdatetime = $res['datetime'];
$suser_id = $res['user_id'];
$startshift = $sdatetime;


$sql = "select datetime,user_id from `shift-transactions` where datetime  > '$startshift' order by datetime asc limit 0,1";
$res = R::getRow($sql);
$edatetime = $res['datetime'];
$euser_id = $res['user_id'];
$end = $edatetime;

if(!$end)
{
	$end = date("Y-m-d H:i:s");
}


function getshift($date) {
	if(!$date)$date=date("Y-m-d H:i:s");
	list($d, $t) = explode(" ", $date);
	list($h, $m, $s) = explode(":", $t);
	$sql = "select shift_id from shifts where $h between shift_start and shift_end";
	$shift = R::getCell($sql);

	if($h==14||$h==13)
	{
		return $shift = "3rd";
	}
	elseif($h==6||$h==5)
	{
		return $shift = "2nd";
	}
	return  "1st";
}
?>
<style>
		.printable {
			font-family: sans-serif;
			font-size: 14px;
			font-weight: 550;
			}
		.report{
			font-family: sans-serif;
			font-size: 14px ;
			text-align:left;
			font-weight: 550;

		}
		.report td{
			padding-bottom:12px;
		}
		.summary{
			font-family: sans-serif;
			font-size: 14px;
			text-align:left;
			font-weight: 550;
		}
</style>
<script src="../js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery.print.js"></script>
<script type="text/javascript">

		// When the document is ready, initialize the link so
		// that when it is clicked, the printable area of the
		// page will print.
		$(document).ready(function(){
				$("a").attr( "href", "javascript:void( 0 )" ).click(
						function(){
							// Print the DIV.
							$(".printable2").print();
							$(".printable").print();
							// Cancel click event.
							return( false );
						});


		});

</script>
<form name=myform method=post>
<div>
<? echo getLatestShifts($shiftid); ?>
</div>
<br>
<a href="#">Print Report</a>
<br>
<br>
<div class='printable'>
<? //echo getCheckoutReport($startshift,$end,$suser_id,$euser_id,$lobbyid); ?>
</div>
<br>
<br>
<br>
<div class='printable2'>
<? echo getCheckoutSummary($startshift,$end,$suser_id,$euser_id,$lobbyid); ?>
</div>
<br />
<a href="#">Print Report</a>
</form>
