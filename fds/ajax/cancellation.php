<?php
//transfer.php
session_start();
include_once("config/config.inc.php");
include_once("common.inc.php");
include_once("config/lobby.inc.php");
include_once("reporting.php");
include_once("class.room.php");

$p = $_POST;

if($p["act"]=="Proceed with cancellation"){
	//check
	if($p["new_remark"]==""){
		echo "<script>alert('Please select a reason');</script>";	
	}else{
		$room =($_GET["roomid"]) ? $_GET["roomid"] : $_POST["roomid"];
		$rm = new room($room);
		$obj = new Reporting($room);
		$rm->getovertimehours();		
		if($rm->overtime_hours >= 1) {
		$add_cost = $rm->ot_amount * $rm->overtime_hours;		
		}
		
		$sql  = " select occupancy_id from occupancy where room_id='$room' and actual_checkout='0000-00-00 00:00:00' limit 1";
        $occupancy_id = R::getCell($sql);

		$f=$rm->floor_id - 1;
		
		if($obj->totalbalance+$add_cost <= 0)
		{
			setCancellation($occupancy_id,$p["new_remark"],$room);
			echo "<script>alert('Successful cancellation');parent.document.location.href='../index.php?f=$f&room=$room'</script>";
			//echo "<script>alert('Please proceed to Checkout Panel');</script>";
		}
		else
		{
			echo "<script>alert('You still have Balances, please proceed to Checkout Panel');</script>";
		}
		
		exit;

	}
}

//set room status as make up
function setCancellation($occupancyid,$reason,$roomid){
	//update occupancy
	$now = date("Y-m-d H:i:s");
	$user = $_SESSION["hotel"]["userid"];
	$sql = " update occupancy set actual_checkout = '$now', room_id='$roomid', update_by='$user' where occupancy_id='$occupancyid'";
	R::exec($sql);
	
	//cancel in occupancy log
	$sql = " insert into occupancy_log (transaction_date, occupancy_id, update_by, remarks, transaction_type) 
			values ('$now', '$occupancyid', '$user', '$reason', 'Cancel')";
    R::exec($sql);

	//update rooms --> set  as make up
	$sql = " update rooms set status=5, last_update='$now', update_by='$user' where room_id='$roomid' ";
    R::exec($sql);



}



function getFormDDLRemarks(){
	$ret = "<select name='new_remark' id='new_remark' width='100%' placeholder='Select a reason for cancelling.'>";
	$ret .= "<option value=''></option>";
	$sql = "select remark_id,remark_text from remarks where remark_classification = '2'";
	$res = R::getAll($sql);
	foreach ($res as $r) {
        $remark_id = $r['remark_id'];
        $remark_text = $r['remark_text'];
		$ret .= "<option value='$remark_id'>$remark_text</option>";
	}
	$ret .= "</select>";
	return $ret;
}

?>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery.approver.css" />
<link rel="stylesheet" type="text/css" href="../css/selectize.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.approver.js"></script>
<script type="text/javascript" src="../js/select2/js/selectize.min.js"></script>
<style>
.formtable {
	width: 90%;
}
</style>
<form name="myform" id="myform" action="" method="post">
<fieldset id="changestatus">
<legend>Room Cancellation</legend>
<table class="formtable">
<!-- <tr>
<td>Select New Room: </td>
<td>
 get search filter functions from reservation to generate cascading select boxes
</td>
</tr> -->
<tr>
<td style="width: 20%">Reasons:</td>
<td>
<?php echo getFormDDLRemarks() ?>
</td>
</tr>
<tr>
<td colspan="2" align="right">
<input type="submit" name="act" id="act" value="Proceed with cancellation" class="approver" />
<!--
<input type="button" value="Proceed with Transfer" name="cmdApprove" id="cmdApprove" />
-->
<div id='diverror'></div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</fieldset>
</form>
<script lang="javascript">
$(document).ready(function(){

	$("#new_remark").selectize({
		sortField: 'text',
		maxItems: 1
	});

    $('#act').approver({
        url : 'oiclist_json.php',
        approvaltype : 'Cancellation',
        approvalinfo : $('#new_remark').val()
    });
});
</script>
