<?php
session_start();
include_once("config/config.inc.php");
include_once("monthly.class.php");

$lsql = "select settings_value from settings where id = '3'";
$lobbyid = R::getCell($lsql);


function getCoopReport($month,$year)
{
	$num = cal_days_in_month(CAL_GREGORIAN, $month, $year) ;

	$ret = "<table border=1 cellpadding=3 cellspacing=0>";
	$ret .= "<tr>";
	$ret .= "<th>&nbsp;</th>";
	$ret .= "<th>&nbsp;</th>";
	$ret .= "<th>1st shift</th>";
	$ret .= "<th>2nd shift</th>";
	$ret .= "<th>3rd shift</th>";
	$ret .= "<th>Total</th>";
	$ret .= "</tr>";
	for($i = 1;  $i <= $num; $i++)
	{
		$start = date('Y-m-d H:i:s', strtotime('-1800 seconds',strtotime($year."-".$month."-".$i." 00:00:00")));
		$end = date('Y-m-d H:i:s', strtotime('+1800 seconds',strtotime($year."-".$month."-".$i." 23:59:59")));

		
		$ret .= "<tr>";
		$ret .= "<td>$i</td>";
		$ret .= "<td>&nbsp;</td>";
		$ret .= "<td style='text-align:right'>".number_format($shift1->getCoopSales())."</td>";
		$ret .= "<td style='text-align:right'>".number_format($shift2->getCoopSales())."</td>";
		$ret .= "<td style='text-align:right'>".number_format($shift3->getCoopSales())."</td>";
		$ret .= "<td style='text-align:right'>".number_format($total)."</td>";
		$ret .= "</tr>";

		
	}
	$ret .= "</tr>";
	$ret .= "</table>";

	$ret .= "<br><br>";
	$ret .= "This are the cooperative sales";
	$ret .= "<br>";
	$sql = "SELECT sas_id,sas_description FROM `sales_and_services` where sas_cat_id = '2' order by sas_description";
	$res = R::getAll($sql);
	$ret .= "<table>";

	$start = date('Y-m-d H:i:s', strtotime($year."-".$month."-1 00:00:00"));
	$end = date('Y-m-d H:i:s', strtotime($year."-".$month."-".$num." 23:59:59"));

	foreach ($res as $r)
	{
		$sas_id = $r['sas_id'];
		$sas = $r['sas_description'];
		$sastot = 0;
		$_sql = "select sum(unit_cost*qty) from room_sales where item_id = '$sas_id' and status = 'Paid'";
		$sastot = R::getCell($_sql);
		$ret .= "<tr>";
		$ret .= "<td>$sas</td>";
		$ret .= "<td>&nbsp;</td>";
		$ret .= "<td>$sastot</td>";
		$ret .= "</tr>";
	}
	$ret .= "</table>";
	return $ret;
}

function getMonthDropdown($name="month", $selected=null)
	{
			$dd = '<select name="'.$name.'" id="'.$name.'">';

			$months = array(
					1 => 'January',
					2 => 'February',
					3 => 'March',
					4 => 'April',
					5 => 'May',
					6 => 'June',
					7 => 'July',
					8 => 'August',
					9 => 'September',
					10 => 'October',
					11 => 'November',
					12 => 'December');
			/*** the current month ***/
			$selected = is_null($selected) ? date('n', time()) : $selected;

			for ($i = 1; $i <= 12; $i++)
			{
					$dd .= '<option value="'.$i.'"';
					if ($i == $selected)
					{
							$dd .= ' selected';
					}
					/*** get the month ***/
					$dd .= '>'.$months[$i].'</option>';
			}
			$dd .= '</select>';
			return $dd;
	}

	function getYearDropdown($name="year", $selected=null)
	{
			$dd = '<select name="'.$name.'" id="'.$name.'">';

			$months = array(
					1 => '2009',
					2 => '2010',
					3 => '2011',
					4 => '2012',
					5 => '2013',
					6 => '2014',
					7 => '2015',
					8 => '2016');
		   
			$selected = is_null($selected) ? date('n', time()) : $selected;

			for ($i = 1; $i <= 8; $i++)
			{
					$dd .= '<option value="'.$months[$i].'"';
					if ($months[$i] == $selected)
					{
							$dd .= ' selected';
					}
					
					$dd .= '>'.$months[$i].'</option>';
			}
			$dd .= '</select>';
			return $dd;
	}

?>
<style>
		.printable {
			font-family: sans-serif;
			font-size: 14px;
			font-weight: 550;
			}
		.report{
			font-family: sans-serif;
			font-size: 14px ;
			text-align:left;
			font-weight: 550;
				
		}
		.report td{
			padding-bottom:12px;
		}
		.summary{
			font-family: sans-serif;
			font-size: 14px;
			text-align:left;
			font-weight: 550;
		}
</style>
<script src="../js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery.print.js"></script>
<script type="text/javascript">
 
		// When the document is ready, initialize the link so
		// that when it is clicked, the printable area of the
		// page will print.
		$(document).ready(function(){		
				$("a").attr( "href", "javascript:void( 0 )" ).click(
						function(){
							// Print the DIV.
							$(".printable2").print();
							$(".printable").print(); 							
							// Cancel click event.
							return( false );
						});
 
			
		});
 
</script>
<form name=myform method=post>
<div>
Month: <? echo getMonthDropdown("ddlmonth",$_POST["ddlmonth"]); ?>
<br>
<br>
Year: <? echo getYearDropdown("ddlyear",$_POST["ddlyear"]); ?>
</div>
<br>
<input type='submit' value='Search' name='btnSearch' />
<br>
<br>
<a href="#">Print Coop Report</a>
<br>
<br>
<div class='printable'>
<? if($_POST){ echo getCoopReport($_POST["ddlmonth"],$_POST["ddlyear"]);} ?>
</div><br />
<a href="#">Print Report</a>
</form>