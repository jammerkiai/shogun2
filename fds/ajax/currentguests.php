<?php /* currentguests.php */

session_start();
include_once("config/config.inc.php");

$sql = " select a.guest_id, a.firstname,  a.lastname, d.door_name, c.actual_checkin, c.expected_checkout
			 from guests a, guest_history b, occupancy c, rooms d
			 where a.guest_id = b.guest_id
			 and b.occupancy_id = c.occupancy_id
			 and c.room_id = d.room_id
			 and c.actual_checkout='0000-00-00 00:00:00'
			 order by a.firstname, a.lastname
			 ";

$guests = R::getAll($sql);

?>
<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" />
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<style>
body, tr, td, th {
	font-size: small;
}
</style>
<body>
<table class="table table-hover table-condensed">
<thead>
<tr class="active"><th colspan="6"><div class="btn btn-primary">Currently Checked-In Guests Found: 
<span class="badge"><?php echo count($guests) ?></span></div>
</th></tr>
<tr class="info">
<th>#</th>
<th>First Name</th>
<th>Last Name</th>
<th>Room No.</th>
<th>Check-In</th>
<th>Check-Out</th>
</tr>
</thead>
<tbody>
<?php foreach($guests as $guest): ?>
<tr>
<td><?php echo $guest['guest_id'] ?></td>
<td><?php echo $guest['firstname'] ?></td>
<td><?php echo $guest['lastname'] ?></td>
<td><?php echo $guest['door_name'] ?></td>
<td><?php echo $guest['actual_checkin'] ?></td>
<td><?php echo $guest['expected_checkout'] ?></td>
</tr>
<?php endforeach; ?>
</tbody>
</table>
</body>

