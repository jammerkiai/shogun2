<?php
session_start();
include_once("config/config.inc.php");
include_once("monthly.class.php");

$lsql = "select settings_value from settings where id = '3'";
$lres = mysql_query($lsql);
list($lobbyid)=mysql_fetch_row($lres);


function getSalesReport($month,$year)
{
	$num = cal_days_in_month(CAL_GREGORIAN, $month, $year) ;

	$sql = "select settings_value from settings where id = '1'";
	$res = mysql_query($sql);
	list($value) = mysql_fetch_row($res);

	$ret = "<div style='font-weight:bold'>".strtoupper($value)."<br>
	HOTEL SALES SUMMARY<br>
	FOR THE MONTH OF ".strtoupper(getMonthName($month))." ".$year."</div><br><br>";
	$ret .= "<table border=1 cellpadding=3 cellspacing=0>";
	$ret .= "<tr>";
	$ret .= "<th>Date</th>";
	$ret .= "<th>&nbsp;</th>";
	$ret .= "<th>100%</th>";	
	$ret .= "<th>50%</th>";
	$ret .= "<th>30%</th>";
	$ret .= "<th>20%</th>";
	$ret .= "<th>10%</th>";
	$ret .= "<th>Others</th>";
	$ret .= "<th>Amount</th>";	
	$ret .= "</tr>";
	for($i = 1;  $i <= $num; $i++)
	{
		
		$start = date('Y-m-d H:i:s', strtotime('-1800 seconds',strtotime($year."-".$month."-".$i." 00:00:00")));
		$end = date('Y-m-d H:i:s', strtotime('+1800 seconds',strtotime($year."-".$month."-".$i." 23:59:59")));

		$sql = "select  `datetime` from `shift-transactions` where `datetime` between '$start' and '$end' 
		and shift ='start' order by `datetime` asc limit 0,1";
		$res = mysql_query($sql);
		list($startdt)=mysql_fetch_row($res);
		
		$sql = "select  `datetime` from `shift-transactions` where `datetime` between '$start' and '$end' 
		and shift ='end' order by `datetime` desc limit 0,1";
		$res = mysql_query($sql);
		list($enddt)=mysql_fetch_row($res);
		
		$all = new monthly('',$startdt,$enddt);

		$perc100 = $all->getDiscountAmount("100");
		$perc50 = $all->getDiscountAmount("50");
		$perc30 = $all->getDiscountAmount("30");
		$perc20 = $all->getDiscountAmount("20");
		$perc10 = $all->getDiscountAmount("10");
		$percothers = $all->getDiscountAmount("others");
		$perctotal = $all->getDiscountAmount("");
		$ret .= "<tr>";
		$ret .= "<td>$i</td>";
		$ret .= "<td>&nbsp;</td>";
		$ret .= "<td>".number_format($perc100)."</td>";
		$ret .= "<td>".number_format($perc50)."</td>";
		$ret .= "<td>".number_format($perc30)."</td>";
		$ret .= "<td>".number_format($perc20)."</td>";
		$ret .= "<td>".number_format($perc10)."</td>";
		$ret .= "<td>".number_format($percothers)."</td>";
		$ret .= "<td>".number_format($perctotal)."</td>";
		$ret .= "</tr>";
		$_perc100 += $perc100;
		$_perc50 += $perc50;
		$_perc30 += $perc30;
		$_perc20 += $perc20;
		$_perc10 += $perc10;
		$_percothers += $percothers;	
		$_perctotal += $perctotal;	
		

		
	}
	$ret .= "<tr>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<th style='text-align:left'>".number_format($_perc100)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($_perc50)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($_perc30)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($_perc20)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($_perc10)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($_percothers)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($_perctotal)."</th>";
	$ret .= "</tr>";

	$ret .= "</table>";

	
	return $ret;
}

function getMonthDropdown($name="month", $selected=null)
	{
			$dd = '<select name="'.$name.'" id="'.$name.'">';

			$months = array(
					1 => 'January',
					2 => 'February',
					3 => 'March',
					4 => 'April',
					5 => 'May',
					6 => 'June',
					7 => 'July',
					8 => 'August',
					9 => 'September',
					10 => 'October',
					11 => 'November',
					12 => 'December');
			/*** the current month ***/
			$selected = is_null($selected) ? date('n', time()) : $selected;

			for ($i = 1; $i <= 12; $i++)
			{
					$dd .= '<option value="'.$i.'"';
					if ($i == $selected)
					{
							$dd .= ' selected';
					}
					/*** get the month ***/
					$dd .= '>'.$months[$i].'</option>';
			}
			$dd .= '</select>';
			return $dd;
	}

	function getYearDropdown($name="year", $selected=null)
	{
			$dd = '<select name="'.$name.'" id="'.$name.'">';

			$months = array(
					1 => '2009',
					2 => '2010',
					3 => '2011',
					4 => '2012',
					5 => '2013',
					6 => '2014',
					7 => '2015',
					8 => '2016');
		   
			$selected = is_null($selected) ? date('n', time()) : $selected;

			for ($i = 1; $i <= 8; $i++)
			{
					$dd .= '<option value="'.$months[$i].'"';
					if ($months[$i] == $selected)
					{
							$dd .= ' selected';
					}
					
					$dd .= '>'.$months[$i].'</option>';
			}
			$dd .= '</select>';
			return $dd;
	}
	function getMonthName($i)
	{
		$months = array(
					1 => 'January',
					2 => 'February',
					3 => 'March',
					4 => 'April',
					5 => 'May',
					6 => 'June',
					7 => 'July',
					8 => 'August',
					9 => 'September',
					10 => 'October',
					11 => 'November',
					12 => 'December');
		return $months[$i];
	}

?>
<style>
		.printable {
			font-family: sans-serif;
			font-size: 14px;
			font-weight: 550;
			}
		.report{
			font-family: sans-serif;
			font-size: 14px ;
			text-align:left;
			font-weight: 550;
				
		}
		.report td{
			padding-bottom:12px;
		}
		.summary{
			font-family: sans-serif;
			font-size: 14px;
			text-align:left;
			font-weight: 550;
		}
</style>
<script src="../js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery.print.js"></script>
<script type="text/javascript">
 
		// When the document is ready, initialize the link so
		// that when it is clicked, the printable area of the
		// page will print.
		$(document).ready(function(){		
				$("a").attr( "href", "javascript:void( 0 )" ).click(
						function(){
							// Print the DIV.
							$(".printable2").print();
							$(".printable").print(); 							
							// Cancel click event.
							return( false );
						});
 
			
		});
 
</script>
<form name=myform method=post>
<div>
Month: <? echo getMonthDropdown("ddlmonth",$_POST["ddlmonth"]); ?>
<br>
<br>
Year: <? echo getYearDropdown("ddlyear",$_POST["ddlyear"]); ?>
</div>
<br>
<input type='submit' value='Search' name='btnSearch' />
<br>
<br>
<a href="#">Print Report</a>
<br>
<br>
<div class='printable'>
<? if($_POST){ echo getSalesReport($_POST["ddlmonth"],$_POST["ddlyear"]);} ?>
</div><br />
<a href="#">Print Report</a>
</form>