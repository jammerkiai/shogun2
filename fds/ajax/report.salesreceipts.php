<?php
/**
* refactored report.salesreceipts.php
*/

session_start();
include_once("config/config.inc.php");
include_once("functions.report.php");
include_once("acctg/class.salesreceiptsreport.php");



if(isset($_POST["rblshifts"])) {
	$shiftid = $_POST["rblshifts"];
} else {
	$sql = "select `shift-transaction_id` from `shift-transactions` order by `shift-transaction_id` desc limit 0, 1";
	$shiftid = R::getCell($sql);
}


$sql = "select datetime,user_id from `shift-transactions` where `shift-transaction_id` = '$shiftid'";
$res = R::getRow($sql);
$sdatetime = $res['datetime'];
$suser_id = $res['user_id'];

$startshift = $sdatetime;
$sql = "select datetime,user_id from `shift-transactions` where datetime  > '$startshift' order by datetime asc limit 0,1";

$res = R::getRow($sql);
$sdatetime = $res['datetime'];
$suser_id = $res['user_id'];

$end = $edatetime;

if(!$end)
{
	$end = date("Y-m-d H:i:s");
}

$report = new salesreceiptsreport( array('start'=>$startshift, 'end'=>$end) );

?>


<html>
<head>
<title>Shogun 1 Sales Receipts Report</title>
<style>
	body, h1, h2, h3, h4 {
		margin:0;
		margin-width:0;
		margin-height:0;
		font-size:14px;
	}

	h1 {
		font-size:1em;
	}
	h2 {
		font-size:0.9em;
	}
		.printable, .printable2, .printable3, .printable4, .printable5 {
			font-family: sans-serif;
			font-size: 14px;
			font-weight: 550;
			border-bottom:1px dotted #cccccc;
			}
		.report th {
			padding:4px;
			text-align:center;
			background-color: #efefef;
		}
		.report{
			font-family: sans-serif;
			font-size: 14px ;
			text-align:left;
			font-weight: 550;
				
		}
		.report td{
			padding:4px;
			padding-bottom:8px;
			background-color:#f9f9f9;
			font-size:14px;
		}
		
		.summary{
			font-family: sans-serif;
			font-size: 14px;
			text-align:left;
			font-weight: 550;
		}

		.menubar {
			font-size:12px;
			font-family:arial, helvetica,sans-serif;
			font-weight:bold;
			list-style:none;
			background-color:#dddddd;
			padding:4px;
			margin:0;
		}
		.menubar li {
			display:inline;
			border:1px solid #eeeeee;
			background-color:#ececec;
			padding:2px;
			padding-left:8px;
			padding-right:8px;
		}

		.menubar li:hover {
			background-color:#ffffff;
		}

		.toolbar {
			background-color:#cccccc;
			padding:0px;
		}

		.aggregates th {
			border-top:1px solid #111111;
		}

		.recordcount {
			padding-top:24px;
			padding-bottom:12px;
		}
		
		td.numeric {
			text-align:right;
		}
</style>
<script src="../js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery.print.js"></script>
</head>
<body>
<script type="text/javascript">
 
		// When the document is ready, initialize the link so
		// that when it is clicked, the printable area of the
		// page will print.
		$(document).ready(function(){
			$(".debug").hide();
			$("#printall").click(function(){
				$(".printable2").print();
				$(".printable").print(); 
				return( false );
			});

			$("#printcheckoutdetails").click(function(){
				$(".printable").print(); 
				return( false );
			});

			$("#printcheckoutsummary").click(function(){
				$(".printable2").print();
				return( false );
			});
			
			$("#debug").click(function(){
				$(".debug").toggle();
				return false;
			});
		});
 
</script>
<form name=myform method=post>
<div class="toolbar">

<ul class="menubar">
Select Shift: <?php echo $report->getLatestShifts($shiftid); ?> &nbsp;&nbsp;&nbsp;&nbsp;
Print
<li><a href="#" id='printall'>All</a></li>
<li><a href="#" id='debug'>Debug</a></li>
</ul>
</div>
<div style='padding:10px;'>
<?php echo $report->getCashOnHand() ?>
<br>
<?php echo $report->getSafekeep() ?>
<br>
<?php echo $report->getSalesReceipts() ?>
<br>
<?php echo $report->getCumulativeTransactions() ?>
<hr>
<h3>Breakdown of Cash Payments Received</h3>
<?php echo $report->getClosedSales() ?>
<br>
<?php echo $report->getInTransitFromNewCheckin() ?>
<br>
<?php echo $report->getSalesForRechit() ?>
<br>
<?php echo $report->getPaymentOnCheckout() ?>
<br>
<hr />
<h3>SUMMARY OF PAYMENTS RECEIVED</h3>
<br>
<?php echo $report->summary() ?>
</div>
</body>
</html>