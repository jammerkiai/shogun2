<?php
session_start();
include_once("config/config.inc.php");
include_once("common.inc.php");
include_once("safekeep.function.php");

$gsafekeep = "10000";
$now = date("Y-m-d H:i:s");



if($_POST["new_occupancy"]!="")
{
	$sql = "update occupancy set wakeup ='1' where occupancy_id ='".$_POST["new_occupancy"]."'";
	R::exec($sql);
}

if($_POST["new_requestid"]!="")
{
	$sql = "update guest_requests set isdone ='Yes' where gr_id ='".$_POST["new_requestid"]."'";
	R::exec($sql);
}
//methods
function getDoornameByOccupancy($occupancy_id){
	$sql = "select b.door_name 
			FROM occupancy a,rooms b 
			where a.occupancy_id = '$occupancy_id' 
			and a.room_id=b.room_id";

	return  R::getCell($sql);
}

function getSpecialRoomIdList()
{
	$fid = getSpecialFloorId();
	return R::getCell("select group_concat(room_id) from rooms where floor_id=$fid");
	
}

function getRechitReminders() {
	$exceptList = getSpecialRoomIdList();
	$start = getShiftStartTime();
	$end = date('Y-m-d H:i:s', strtotime($start . ' +8 hours'));
	$cutoff = date('Y-m-d H:i:s', strtotime($start . ' -12 hours'));
	$h = date("H");
	if($h<=21||$h<=22)
	{
		$shiftnum=1;
	}
	elseif($h>=5||$h>=6)
	{
		$shiftnum=3;
	}else{
		$shiftnum=2;
	}

	$sql = "
		select  distinct b.door_name, a.occupancy_id,a.actual_checkin,a.actual_checkout,
				b.site_id,b.door_name,b.room_id,c.room_type_id, c.room_type_name,c.rank

				from occupancy a, rooms b, room_types c, rates d , room_sales e
				where
				(
				(e.sales_date >='$start' and e.sales_date <='$end' and a.actual_checkin <> e.sales_date  and e.item_id=15)
				)
				and a.actual_checkout='0000-00-00 00:00:00'
				and a.occupancy_id=e.occupancy_id
				and a.room_id = b.room_id
				and b.room_type_id=c.room_type_id
				and a.rate_id=d.rate_id
				and b.site_id=2
				and a.room_id not in ($exceptList)
	";
	/*
	$sql = "
				select b.door_name,a.occupancy_id,a.actual_checkin,a.actual_checkout,
				b.site_id,b.door_name,b.room_id,c.room_type_id, c.room_type_name,c.rank
				from occupancy a, rooms b, room_types c, rates d
				where
				(
				(a.actual_checkout = '0000-00-00 00:00:00' and a.actual_checkin < '$start' and a.shift_checkin=$shiftnum)
				or (a.actual_checkin >= '$cutoff' and (a.actual_checkout > '$end' or a.actual_checkout='0000-00-00 00:00:00') and d.duration=12
					and a.shift_checkin in (1,2,3) and ( (timestampdiff(HOUR,a.actual_checkin,'$end')/d.duration) >= 1 ) )
				)
				and a.room_id = b.room_id
				and b.room_type_id=c.room_type_id
				and a.rate_id=d.rate_id
				and b.site_id=2
				and a.room_id not in ($exceptList)
				order by b.door_name
				";


		*/
	$rechits = R::getAll($sql);

	$ret = "<fieldset class='rechit'><legend>Rechit Reminder</legend>
	<ul class='rechitlist'>";
	foreach($rechits as $rc) {
		$ret.="<li>{$rc['door_name']}</li>";
	}
	$ret .= "</ul></fieldset>";
	return $ret;
}

function getReservations() {
	$today = date('Y-m-d');
	$tom = date('Y-m-d', strtotime("$today + 1 days"));
	$sql = "
				select distinct b.door_name
				from reserve_rooms a, rooms b
				where a.room_id = b.room_id
				and a.checkin >='$today' and a.checkin <='$today'
				order by b.door_name
				";
	$res = R::getAll($sql);

	$ret = "<fieldset class='rechit'><legend>Reservations</legend>
	<ul class='rechitlist'>";
	foreach($res as $r) {
		$ret.="<li>{$r['door_name']}</li>";
	}
	$ret .= "</ul></fieldset>";
	return $ret;
}

function getExpectedCheckout() {
	$today = date('Y-m-d');
	$tom = date('Y-m-d', strtotime("$today + 2 days"));
	$sql = "
			select b.door_name,a.expected_checkout
			from occupancy a, rooms b
			where a.room_id = b.room_id
			and a.expected_checkout >='$today'
			and a.expected_checkout <='$tom'
			and a.actual_checkout = '0000-00-00 00:00:00'
			order by a.expected_checkout, b.door_name
			";
	$res = R::getAll($sql);

	$ret = "<fieldset class='rechit'>
	<legend>Next Expected Checkout</legend>
	<ul class='expectedcheckout'>";
	foreach ($res as $r) {
		$ret.="<li><span>{$r['door_name']}</span> {$r['expected_checkout']}</li>";
	}
	$ret .= "</ul></fieldset>";
	return $ret;
}

function getReminderAlerts(){
	$now = date("Y-m-d H:i:s");
	//$sql = "select * from guest_requests where  1800 >= (SELECT TIME_TO_SEC( TIMEDIFF( date_required , '$now' ) ) ) 	";
	$sql = "select a.gr_id,	
			a.date_required,
			a.occupancy_id, 
			group_concat(a.message) as msgs 
			from guest_requests a,
			occupancy b 
			where   b.occupancy_id = a.occupancy_id
			and a.isdone ='No'
			and b.actual_checkout = '0000-00-00 00:00:00' 
			group by occupancy_id
		";

	$res = R::getAll($sql);

	if(empty($res)){
		$retval = "No guest request." ;
	}else{
		foreach ($res as $r){

		$gr_id = $r['gr_id'];
		$date_required = $r['date_required'];
		$oid = $r['occupancy_id'];
		$msg = $r['msgs'];
				
		$doorname = getDoornameByOccupancy($oid);
		$each .="<li class='$class'>
		<div id='m_$oid' class='msgbox'>$msg</div>
		<input type='button' alt='#m_$oid' class='btnAlert' value='".$doorname." ".$date_required."' onclick=\"if(confirm('Set requests for Room $doorname done?')){new_requestid.value='$gr_id';myform.submit();}\" />
		</li>";
	}
	$retval = "<div>";
	$retval .= "Guest Requests:<br><ul id='avlist'>$each</ul>";
	$retval .= "<input type='hidden' value='' name='new_requestid' id='new_requestid'>";
	$retval .= "</div>";
	}
	return $retval;
}





function getSafekeepAlerts($gsafekeep)
{

	$now = date("Y-m-d H:i:s");

	$sql = "select current_amount from current_cash order by cc_date desc, cc_id desc limit 0,1";
	$current_amount = R::getCol($sql)[0];

	if($current_amount >= $gsafekeep)
	{
		$ret = '<span class="safekeep">SAFEKEEP ALERT</span>';
		$ret = '<div style="clear:both"></div><center><div class="safekeep">SAFEKEEP ALERT</div></center>';
	}

	return $ret;
}

function getCheckoutAlert()
{
	$now = date("Y-m-d H:i:s");

	$sql = "SELECT a.occupancy_id,b.door_name,a.expected_checkout  FROM occupancy a,rooms b WHERE
	a.actual_checkout = '0000-00-00 00:00:00'
	and a.room_id = b.room_id
	and a.wakeup = '0'
	and 2100 >= (SELECT TIME_TO_SEC( TIMEDIFF( expected_checkout , '$now' ) ) )
	order by expected_checkout ";

	$res = R::getAll($sql);
	if (!empty($res))
	{
		$alert = false;
		foreach ($res as $r)
		{
			$occupancy_id = $r['occupancy_id'];
			$door_name = $r['door_name'];
			$expected_checkout = $r['expected_checkout'];

			$_sql = "select isalerted from occupancy where isalerted ='No'
			and occupancy_id = '$occupancy_id'";
			$isalerted = R::getAll($_sql);

			if (!empty($isalerted))
			{
				$alert = true;


				$_gr .= "'".$occupancy_id."',";
			}
			//echo $expected_checkout."__";
			list($date,$time)=explode(" ",$expected_checkout);

			$_sql = "SELECT occupancy_id FROM occupancy WHERE occupancy_id = '$occupancy_id'
				and 0 >= (SELECT TIME_TO_SEC( TIMEDIFF( expected_checkout , '$now' ) ) )";

			$occnt = R::getAll($_sql);
				
			if(count($occnt) > 0)
			{
				$class = "reserved";
			}
			$tbody .= "<tr>";
			$tbody .= "<td align=\"left\">
			<a href=\"#\"
			onfocus=\"this.blur()\"
			onclick=\"if(confirm('Wakeup done for room $door_name?')){new_occupancy.value='$occupancy_id';myform.submit();}\"><b>".$door_name."</b>&nbsp;&nbsp;".date("m/d/Y g:i a " ,(strtotime($expected_checkout)-(30*60)))."</a>
			</td>";
			//$tbody .= "<td class='total'><label for='asetwake_$door_name'></label></td>";
			//$tbody .= "<td class='num'><label for='asetwake_$door_name'> ".  ."</label></td>";
			//$tbody .= "<td class='num'><a id='asetwake_$door_name' class='setwake'\"><img src=''></a></td>";
			//$tbody .= "</tr>";
			$each .="<li class='$class'><a class='setwake' href='#' onclick=\"if(confirm('Wakeup done for room $door_name?')){myform.submit()}\">".$door_name."<br>". date("Y-m-d g:i a " ,strtotime($expected_checkout)) ."-$wakeuptime</a></li>";

			$class = "";

		}
		$ret ="<div>Wakeup Calls:</div>";

		$ret .= "<input type='hidden' name='new_occupancy' id='new_occupancy' value='' />";
		$ret .= "<div class=\"menu\">";
		//$ret .= "<div>Rooms need to checkout within 30 mins</div><ul id='avlist'>$each</ul>";
		//$ret .= "<table class='frontstats'>";
		$ret.="<table class=\"menu\" align=\"center\" cellpadding=\"1\" cellspacing=\"1\">";
		//$ret .= "<tr>";
		//$ret .= "<th colspan='2' style='text-align:left'>Room</th>";
		//$ret .= "</tr>";
		$ret .= $tbody;
		$ret .= "</table>";
		$ret .= "</div>";

		if($alert)
		{
			$_gr =  substr_replace($_gr ,"",-1);
			$_sql = "update occupancy set isalerted ='Yes' where occupancy_id in (".$_gr.")";
			R::exec($_sql);

		}



	}
	else
	{
		$ret = "<div>No Wakeup Call as of the moment</div>";
	}



	return $ret;
}


?>

<body onload="JavaScript:timedRefresh(300000);">
<script type="text/JavaScript">
function timedRefresh(timeoutPeriod) {
	setTimeout("location.reload(true);",timeoutPeriod);
}
</script>

<form name='myform' id='myform' method='post'>
<div id="roomlisting">
<?php echo getCheckoutAlert() ?>
</div>



<div style='clear: both'></div>
<br>
<div id="divReminders">
<?php echo getReminderAlerts() ?>
</div>
<br>
<?php echo getSafekeepAlerts($gsafekeep) ?>

<div style='clear: both'></div>
<?php echo getRechitReminders() ?>
<?php echo getReservations() ?>
<?php echo getExpectedCheckout() ?>
</form>
</body>
<style>
body{font-family:lucida,arial,helvetica;font-size:.7em;}
#avlist { list-style:none;}
#avlist li {float:left;border:1px solid #95D5EF;padding:2px;margin:1px;}
#avlist li.reserved {float:left;border:1px solid #ff6600;}
.safekeep {color:#ff0000;border:1px solid #FF0000; background-color:#ffcccc;padding:4px;margin:4px;width:140px;text-align:center;font-weight:bold;}
.btnAlert {font-size:12px;border:1px solid #95D5EF; background-color:#ffcc99}
.btnAlert:hover {font-size:12px;border:1px solid #95D5EF; background-color:#ff9966}
.msgbox {font-size:12px}
.rechit legend {color:#cc3322;font-weight:bold;}
.rechitlist,.expectedcheckout {list-style:none;margin-left:-30px;margin-top:-2px;}
.expectedcheckout li {font-weight:bold;text-align:center;border:1px solid #ffcc99;padding:2px;margin:1px;}
.expectedcheckout li span{color:#0000ff;font-size:12px}
.rechitlist li{font-weight:bold;text-align:center;display:inline;float:left;width:30px;border:1px solid #ffcc99;padding:2px;margin:1px;}
table.menu a {
			width:125px;
			border:1px solid #333333;
			display: block;}

div.menu	a{
					color: #333333;
					background: #ffffff;
					text-decoration:none;
					font-size:11px;
					line-height:16px;
					padding: 2px 5px;
					font-family: Tahoma, verdana, sans-serif;}
div.menu (position:absolute;top:0; left:0;) /*fixes IE slowness?? */

div.menu	a:link {color: #333333; text-decoration: none; background: #cccc99;}
div.menu	a:active {color: #000000; text-decoration: none; background: #cccc99;}
div.menu	a:visited {color: #333333; text-decoration: none; background: #cccc99;}
div.menu	a:hover {color: #eeeeee; text-decoration: none; background: #333333;border:1px solid #000000;}


</style>
<script type="text/javascript" src="../js/jquery.js"></script>
<script lang="javascript">
$(document).ready(function(){
	$(".msgbox").hide();
	$(".btnAlert").mouseover(function(){
		$( $(this).attr("alt") ).show();
	}).mouseout(function(){
		$( $(this).attr("alt") ).hide();
	});
});
</script>
