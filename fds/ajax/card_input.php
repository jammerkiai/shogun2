<?php
	$cardTypes = R::getAll('select id, name from card_types');
?>
<fieldset id="carddetails">
<legend  style="font-size:1em">Card Details</legend>
<table class="formtable"  style="font-size:1em">
<tr>
<td>
Card Type</td>
<td>

<?php foreach($cardTypes as $cardType): ?>
<input type="radio" name="new_card_type" value="<?php echo $cardType['name'] ?>"  
	id="ct_<?php echo $cardType['id'] ?>" /> 
	<label for="ct_<?php echo $cardType['id'] ?>"><span width="80px"><?php echo $cardType['name'] ?></span>
</label><br/>
<?php endforeach;?>

</td></tr>
<tr>
<td>
Card Suffix</td>
<td>
<input type="text" name="new_card_suffix" value=""  id="new_card_suffix"  class="money full"/> 
</td>
</tr>
<tr>
<td>
Approval Code</td>
<td>
<input type="text" name="new_approval_code" value=""  id="new_approval_code"  class="money full"/> 
</td>
</tr>
<tr>
<td>
Batch #</td>
<td><input type="text" name="new_batch_number" value=""  id="new_batch_number"  class="money full"/> 
</td>
</tr>
<tr>
<td>
Terminal</td>
<td>
<select id="terminal" name="terminal">
	<option value="BDO">BDO</option>
	<option value="BPI">BPI</option>
</select>
</td>
</tr>
<tr>
<td align=right><input type='checkbox' name='newisdebit' value='1'  id='newisdebit' class='' /></td>
<td><label for='newisdebit'>Debit / ATM</label></td>
</tr>
</table>
</fieldset>