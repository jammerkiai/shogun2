<?php
session_start();
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');



function specialsList($date = null) {
    if (is_null($date)) {
        $date = $_GET['d'];
    }

    list($start, $end) = getStartEndUsingDate($date);
    $specials =[500, 1427, 1428, 24479, 24481, 37346, 56795];

    $specialList = implode(",", $specials);
    $sql = "
        (SELECT '$date' as postdate, 
          (a.unit_cost * a.qty) as amount, 
          if (a.occupancy_id=500, if (a.category_id in (17, 21, 39, 49, 50), '4007b', '4007a'), 
            if (a.occupancy_id=1427, if ( a.category_id in (17, 21, 39, 49, 50), '4008b', '4008a'),
              if (a.occupancy_id=1428, if ( a.category_id in (17, 21, 39, 49, 50), '4006b', '4006a'),
                if (a.occupancy_id=24479, if ( a.category_id in (17, 21, 39, 49, 50), '4005b', '4005a'), 
                  if (a.occupancy_id=24481, if ( a.category_id in (17, 21, 39, 49, 50), '4004b', '4004a'), 
                    '4002'
                  )
                )
              ) 
            )
          ) as cr_code,
          if (a.tendertype='Cash', '1000', '1022') as dr_code,
          a.regflag as book,
          a.tendertype,
          a.occupancy_id,
          a.item_id,
          a.fnbsales_id as trxnid,
          a.sales_date,
          a.update_date,
          'Current' as occupancy_status,
          e.fnb_name as item,
          'room_sales' as transactionSource,
          '1' as is_special
          FROM fnb_sales a, occupancy b, fnb e 
          WHERE a.update_date BETWEEN '$start' and '$end' 
          and a.occupancy_id=b.occupancy_id
          and a.status in ('Paid')
          and b.occupancy_id in ($specialList)
          and a.item_id=e.fnb_id
          order by a.occupancy_id)
        
        UNION
        
        (SELECT '$date' as postdate,
          (d.unit_cost * d.qty) as amount,
          c.code as cr_code,
          if (d.tendertype='Cash', '1000', '1022') as dr_code,
          d.regflag as book,
          d.tendertype,
          d.occupancy_id,
          d.item_id,
          d.roomsales_id as trxnid,
          d.sales_date,
          d.update_date,
          'Current' as occupancy_status,
          c.sas_description as item,
          'room_sales' as transactionSource,
          '1' as is_special
          FROM room_sales d, occupancy b, sales_and_services c
          WHERE d.update_date BETWEEN '$start' and '$end'
          and d.occupancy_id=b.occupancy_id
          and d.status in ('Paid')
          and b.occupancy_id in ($specialList)
          and d.item_id=c.sas_id
          order by c.code)
    ";

    saveToSalesDetails($date, $sql);
    return $sql;
}


function saveToSalesDetails($date, $sql) {
    $insertSql = "insert into sales_journal_details 
                  (postdate, amount, cr_code, dr_code, type, tendertype, occupancy_id, 
                   item_id, transactionId, sales_date, update_date, occupancy_status,
                   item, transactionSource, is_special
                  )
                  $sql
                  ";

    R::exec($insertSql);
}

function checkinRoomsalesList($date=null) {
    if (is_null($date)) {
        $date = $_GET['d'];
    }

    list($start, $end) = getStartEndUsingDate($date);
    $specials =[500, 1427, 1428, 24479, 24481, 37346, 56795];

    $specialList = implode(",", $specials);
    $sql = "
        SELECT '$date' as postdate, 
          (a.qty * a.unit_cost) as amount, 
          if (a.category_id=500, if (a.category_id in (17, 21, 39, 49, 50), '4007b', '4007a'), 
            if (a.occupancy_id=1427, if ( a.category_id in (17, 21, 39, 49, 50), '4008b', '4008a'),
              if (a.occupancy_id=1428, if ( a.category_id in (17, 21, 39, 49, 50), '4006b', '4006a'),
                if (a.occupancy_id=24479, if ( a.category_id in (17, 21, 39, 49, 50), '4005b', '4005a'), 
                  if (a.occupancy_id=24481, if ( a.category_id in (17, 21, 39, 49, 50), '4004b', '4004a'), 
                    '4002'
                  )
                )
              ) 
            )
          ) as cr_code,
          if (a.tendertype='Cash', '1018', '1022') as dr_code,
          a.regflag as book,
          a.tendertype,
          a.occupancy_id,
          a.item_id,
          a.roomsales_id as trxnid,
          a.sales_date,
          a.update_date,
          'Checked In' as occupancy_status,
          c.sas_description as item,
          'room_sales' as transactionSource,
          '0' as is_special
          FROM room_sales a, occupancy b, sales_and_services c 
          WHERE b.actual_checkin BETWEEN '$start' and '$end' 
          and a.sales_date=b.actual_checkin
          and a.occupancy_id=b.occupancy_id
          and b.occupancy_id not in ($specialList)
          and a.item_id=c.sas_id
          order by a.occupancy_id, c.code, a.tendertype
    ";

    saveToSalesDetails($date, $sql);
    return $sql;
}

function checkinRoomsalesRechitList($date=null) {
    if (is_null($date)) {
        $date = $_GET['d'];
    }

    list($start, $end) = getStartEndUsingDate($date);
    $specials =[500, 1427, 1428, 24479, 24481, 37346, 56795];

    $specialList = implode(",", $specials);
    $sql = "
        SELECT '$date' as postdate, 
          (a.qty * a.unit_cost) as amount, 
          if (a.category_id=500, if (a.category_id in (17, 21, 39, 49, 50), '4007b', '4007a'), 
            if (a.occupancy_id=1427, if ( a.category_id in (17, 21, 39, 49, 50), '4008b', '4008a'),
              if (a.occupancy_id=1428, if ( a.category_id in (17, 21, 39, 49, 50), '4006b', '4006a'),
                if (a.occupancy_id=24479, if ( a.category_id in (17, 21, 39, 49, 50), '4005b', '4005a'), 
                  if (a.occupancy_id=24481, if ( a.category_id in (17, 21, 39, 49, 50), '4004b', '4004a'), 
                    '4002'
                  )
                )
              ) 
            )
          ) as cr_code,
          if (a.tendertype='Cash', '1018', '1022') as dr_code,
          a.regflag as book,
          a.tendertype,
          a.occupancy_id,
          a.item_id,
          a.roomsales_id as trxnid,
          a.sales_date,
          a.update_date,
          'Rechit' as occupancy_status,
          c.sas_description as item,
          'room_sales' as transactionSource,
          '0' as is_special
          FROM room_sales a, occupancy b, sales_and_services c 
          WHERE a.rechit_date BETWEEN '$start' and '$end' 
          and a.occupancy_id=b.occupancy_id
          and b.occupancy_id not in ($specialList)
          and a.item_id=c.sas_id
          order by a.occupancy_id, c.code, a.tendertype
    ";

    saveToSalesDetails($date, $sql);
    return $sql;
}

function checkinRoomsalesCheckedOutList($date=null) {
    if (is_null($date)) {
        $date = $_GET['d'];
    }

    list($start, $end) = getStartEndUsingDate($date);
    $specials =[500, 1427, 1428, 24479, 24481, 37346, 56795];

    $specialList = implode(",", $specials);
    $sql = "
        SELECT '$date' as postdate, 
          (a.qty * a.unit_cost) as amount, 
          if (a.category_id=500, if (a.category_id in (17, 21, 39, 49, 50), '4007b', '4007a'), 
            if (a.occupancy_id=1427, if ( a.category_id in (17, 21, 39, 49, 50), '4008b', '4008a'),
              if (a.occupancy_id=1428, if ( a.category_id in (17, 21, 39, 49, 50), '4006b', '4006a'),
                if (a.occupancy_id=24479, if ( a.category_id in (17, 21, 39, 49, 50), '4005b', '4005a'), 
                  if (a.occupancy_id=24481, if ( a.category_id in (17, 21, 39, 49, 50), '4004b', '4004a'), 
                    '4002'
                  )
                )
              ) 
            )
          ) as cr_code,
          if (a.tendertype='Cash', '1018', '1022') as dr_code,
          a.regflag as book,
          a.tendertype,
          a.occupancy_id,
          a.item_id,
          a.roomsales_id as trxnid,
          a.sales_date,
          a.update_date,
          'Checked Out' as occupancy_status,
          c.sas_description as item,
          'room_sales' as transactionSource,
          '0' as is_special
          FROM room_sales a, occupancy b, sales_and_services c 
          WHERE b.update_date between '$start' and '$end'
          and b.actual_checkout between '$start' and '$end'
          and a.occupancy_id=b.occupancy_id
          and b.occupancy_id not in ($specialList)
          and a.item_id=c.sas_id
          order by a.occupancy_id, c.code, a.tendertype
    ";

    saveToSalesDetails($date, $sql);
    return $sql;
}

function foodSalesList($date=null) {
    if (is_null($date)) {
        $date = $_GET['d'];
    }
    list($start, $end) = getStartEndUsingDate($date);
    $specials =[500, 1427, 1428, 24479, 24481, 37346, 56795];

    $specialList = implode(",", $specials);
    $sql = "
        SELECT '$date' as postdate, 
          (a.qty * a.unit_cost) as amount, 
          if ( a.category_id in (17, 21, 39, 49, 50), '4004b', '4004a') as cr_code,
          if (a.tendertype='Cash', '1018', '1022') as dr_code,
          a.regflag as book,
          a.tendertype,
          a.occupancy_id,
          a.item_id,
          a.fnbsales_id as trxnid,
          a.sales_date,
          a.update_date,
          if (b.actual_checkout='0000-00-00 00:00:00', 'Current', 
            if (b.actual_checkout > '$end', 'Current', 'Checked Out')
          ) as occupancy_status,
          c.fnb_name as item,
          'fnb_sales' as transactionSource,
          '0' as is_special
          FROM fnb_sales a, occupancy b, fnb c
          WHERE a.update_date BETWEEN '$start' and '$end' 
          and b.actual_checkin between '$start' and '$end'
          and a.occupancy_id=b.occupancy_id
          and b.occupancy_id not in ($specialList)
          and a.item_id=c.fnb_id
          order by a.occupancy_id, a.tendertype
    ";
    saveToSalesDetails($date, $sql);
    return $sql;
}

function getSpecialsFromSalesDetails($date) {
    return "select * from sales_journal_details where is_special=1 and postdate='$date'";
}

function getRoomSalesFromSalesDetails($date) {
    return "select * from sales_journal_details 
            where is_special=0 and transactionSource='room_sales' 
            and occupancy_status='Checked In'
            and postdate='$date'";
}

function getRoomSalesRechitFromSalesDetails($date) {
    return "select * from sales_journal_details 
            where is_special=0 
            and transactionSource='room_sales' 
            and occupancy_status='Rechit'
            and postdate='$date'";
}

function getRoomSalesCheckedOutFromSalesDetails($date) {
    return "select * from sales_journal_details 
            where is_special=0 
            and transactionSource='room_sales' 
            and occupancy_status='Checked Out'
            and postdate='$date'";
}

function getFoodSalesFromSalesDetails($date) {
    return "select * from sales_journal_details where is_special=0 and transactionSource='fnb_sales' and postdate='$date'";
}

//function checkoutList($date=null) {
//    if (is_null($date)) {
//        $date = $_GET['d'];
//    }
//    list($start, $end) = getStartEndUsingDate($date);
//    $specials =[500, 1427, 1428, 24479, 24481, 37346, 56795];
//
//    $specialList = implode(",", $specials);
//    $sql = "
//        SELECT '$date' as postdate,
//          a.amount,
//          if (a.reference_id=0, '4001', '4002') as cr_code,
//          if (a.tendertype='Cash', '1000', '1022') as dr_code,
//          if (a.tendertype='Card', 1, b.regflag) as book,
//          a.tendertype,
//          if (a.reference_id=0, 'Room', 'Food') as type,
//          a.occupancy_id
//          FROM salesreceipts a, occupancy b
//          WHERE a.receipt_date BETWEEN '$start' and '$end'
//          and b.actual_checkout between '$start' and '$end'
//          and a.occupancy_id=b.occupancy_id
//          and b.occupancy_id not in ($specialList)
//          order by type, tendertype
//    ";
//    return $sql;
//}

function specialsTotal($date = null) {
    if (is_null($date)) {
        $date = $_GET['d'];
    }

    list($start, $end) = getStartEndUsingDate($date);
    $specials =[500, 1427, 1428, 24479, 24481, 37346, 56795];

    $specialList = implode(",", $specials);
    $sql = "
        (SELECT '$date' as postdate, 
          sum(a.unit_cost * a.qty) as amount, 
          if (a.occupancy_id=500, if (a.category_id in (17, 21, 39, 49, 50), '4007b', '4007a'), 
            if (a.occupancy_id=1427, if ( a.category_id in (17, 21, 39, 49, 50), '4008b', '4008a'),
              if (a.occupancy_id=1428, if ( a.category_id in (17, 21, 39, 49, 50), '4006b', '4006a'),
                if (a.occupancy_id=24479, if ( a.category_id in (17, 21, 39, 49, 50), '4005b', '4005a'), 
                  if (a.occupancy_id=24481, if ( a.category_id in (17, 21, 39, 49, 50), '4004b', '4004a'), 
                    '4002'
                  )
                )
              ) 
            )
          ) as cr_code,
          if (a.tendertype='Cash', '1000', '1022') as dr_code,
          a.regflag as book,
          '0' as is_posted
          FROM fnb_sales a, occupancy b 
          WHERE a.update_date BETWEEN '$start' and '$end' 
          and a.occupancy_id=b.occupancy_id
          and a.status in ('Paid')
          and b.occupancy_id in ($specialList)
          group by cr_code, dr_code, book)
        
        UNION
        
        (SELECT '$date' as postdate,
          sum(d.unit_cost * d.qty) as amount,
          c.code as cr_code,
          if (d.tendertype='Cash', '1000', '1022') as dr_code,
          d.regflag as book,
          '0' as is_posted
          FROM room_sales d, occupancy b, sales_and_services c
          WHERE d.update_date BETWEEN '$start' and '$end'
          and d.occupancy_id=b.occupancy_id
          and d.status in ('Paid')
          and b.occupancy_id in ($specialList)
          and d.item_id=c.sas_id
          group by cr_code, dr_code, book)
          
    ";
    return $sql;
}

function transitTotal($date=null) {
    if (is_null($date)) {
        $date = $_GET['d'];
    }
    list($start, $end) = getStartEndUsingDate($date);
    $specials =[500, 1427, 1428, 24479, 24481, 37346, 56795];

    $specialList = implode(",", $specials);
    $sql = "
        SELECT '$date' as postdate, 
          sum(a.amount) as amount, 
          if (a.reference_id=0, '4001', '4002') as cr_code,
          if (a.tendertype='Cash', '1018', '1022') as dr_code,
          if (a.tendertype='Card', 1, b.regflag) as book,
          '0' as is_posted
          FROM salesreceipts a, occupancy b 
          WHERE a.receipt_date BETWEEN '$start' and '$end'    
          and (b.actual_checkout = '0000-00-00 00:00:00' or b.actual_checkout > '$end')
          and a.occupancy_id=b.occupancy_id
          and b.occupancy_id not in ($specialList)
          group by dr_code, cr_code, book
    ";
    return $sql;
}

function checkinRoomsalesTotal($date=null) {
    if (is_null($date)) {
        $date = $_GET['d'];
    }
    list($start, $end) = getStartEndUsingDate($date);
    $specials =[500, 1427, 1428, 24479, 24481, 37346, 56795];

    $specialList = implode(",", $specials);
    $sql = "
        SELECT '$date' as postdate, 
          sum(a.unit_cost * a.qty) as amount, 
          if (a.reference_id=0, '4001', '4002') as cr_code,
          if (a.tendertype='Cash', '1018', '1022') as dr_code,
          a.regflag as book,
          a.sales_date,
          a.update_date
          FROM room_sales a, occupancy b, sales_and_services c  
          WHERE a.update_date BETWEEN '$start' and '$end' 
          and b.actual_checkin between '$start' and '$end'
          and a.occupancy_id=b.occupancy_id
          and a.item_id=c.sas_id
          and b.occupancy_id not in ($specialList)
          group by dr_code, cr_code, book
    ";

    return $sql;
}

function saveToJournal($now) {
    $fnlist = ['specialsTotal'/*, 'transitTotal', 'checkinTotal', 'checkoutTotal'*/];
    foreach ($fnlist as $fn) {
        $sql = $fn($now);

        $all = R::getAll($sql);
        foreach ($all as $row) {
            R::exec("insert into sales_journal (postdate, amount, dr_code, cr_code, book, is_posted)
                  values ('{$row['postdate']}', '{$row['amount']}', '{$row['dr_code']}', '{$row['cr_code']}', '{$row['book']}', '{$row['is_posted']}')", []);
        }
    }
}

function postSalesToGl($now) {
    $all = R::getAll("select * from sales_journal where postdate='$now'");

    foreach ($all as $row) {
        if ($row['is_posted'] == 0) {
            $pair = ['dr' => $row['dr_code'], 'cr' => $row['cr_code']];
            $type = ($row['book'] == 1) ? 'l' : 'm';
            postGL($pair, $row['amount'], $row['id'], 'sales', "$now sales", $type, $row['postdate']);
            R::exec("update sales_journal set is_posted=1 where postdate='$now' and id=" . $row['id']);
        }

    }

}

//$sql = $_GET['t']();
//$all = R::getAll($sql);
//echo json_encode(['total' => count($all), 'records' => $all]);