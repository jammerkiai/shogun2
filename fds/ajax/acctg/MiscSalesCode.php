<?php

require_once('SalesCodeInjector.php');

class MiscSalesCode implements SalesCodeInjector
{
	public function getOptions($ci) {
		$cr_code = trim($ci['code'])!='' ? $ci['code'] : 4001;
		$cr_desc = 'Misc Income';

		if ($ci['category_id'] == 7) {
			$cr_desc .= ' Tariff'; 
		} elseif ($ci['category_id'] == 4) {
			$cr_desc .= ' Drop Off'; 
		} 

		if ($ci['status'] == 'Paid' && $ci['tendertype']=='Cash') {

			$out = [
				'key' => 'Cash_Misc',
				'options' => [
					'source'   => 'room_sales',
					'cr'	   => ['code' => $cr_code, 'desc' => $cr_desc],
					'dr'	   => ['code' => '1000', 'desc' => 'Cash'],
				]
			];

		} else {

			$out = [
				'key' => 'AR_Misc',
				'options' => [
					'source'   => 'room_sales',
					'cr'	   => ['code' => $cr_code, 'desc' => $cr_desc],
					'dr'	   => ['code' => '1018', 'desc' => 'Accounts Receivable'],
				]
			];
		}

		return $out;
	}
}