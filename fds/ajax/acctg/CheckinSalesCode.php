<?php

require_once('SalesCodeInjector.php');

class CheckinSalesCode implements SalesCodeInjector
{
	public function getOptions($ci) {

		if ($ci['status'] == 'Paid' && $ci['tendertype']=='Cash') {

			$out = [
				'key' => 'Cash_Unearned',
				'options' => [
					'source'   => 'room_sales',
					'cr'	   => ['code' => '2015', 'desc' => 'Unearned Income - Sales'],
					'dr'	   => ['code' => '1000', 'desc' => 'Cash'],
				]
			];

		} else {

			$out = [
				'key' => 'AR_Unearned',
				'options' => [
					'source'   => 'room_sales',
					'cr'	   => ['code' => '2015', 'desc' => 'Unearned Income - Sales'],
					'dr'	   => ['code' => '1018', 'desc' => 'Accounts Receivable'],
				]
			];
		}

		return $out;
	}
}