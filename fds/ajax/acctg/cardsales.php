<?php
/**
* @FILE:   cardsales.php
*/

if (isset($_POST['cmd']) && $_POST['cmd'] === 'view monthly') header('location: monthlycardsales.php');

require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');

$now = isset($_POST['newdate']) ? $_POST['newdate'] : ( isset($_GET['d']) ? $_GET['d'] : date('Y-m-d'));
$batchnum = isset($_POST['batchnum']) ? $_POST['batchnum'] : '';

$site = isset($_POST['site']) ? $_POST['site'] : 1;

$db = 'shogunfds' . $site;

if (isset($_POST['cmd']) && $_POST['cmd'] === 'post to gl') {

    $cpds = implode(',', $_POST['cpdId']);
    $sql = "select sum(a.amount) 
            from $db.salesreceipts a, $db.card_payment_details b
            where a.salesreceipt_id=b.salesreceipt_id
            and b.cpd_ids in ($cpds)";
    $total = R::getCell($sql);

    postGl(['cr' => '1022', 'dr' => '1000'], $total, 'sales', 'credit card conversions', 'l', $now);

    $sql = "update $db.card_payment_details set is_posted=1, post_date='$now' where cpd_ids in ($cpds)";
    R::exec($sql);
    header("Refresh:0; url=cardsales.php?d=" . $now);
}


function postdata($date, $site, $data) {
    $sql = "select count(*) from monthlycardsales where cardsale_date='$date' and site_id='$site'";
    $posted = R::getCell($sql);
    $amount = $data['amount'];
    $commission = $data['commission'];
    $tax = $data['tax'];
    $net = $data['net'];
    if ($posted === '0') {
        //insert
        $sql = "insert into monthlycardsales (cardsale_date, site_id, amount, commission, tax, net)
                values ('$date', '$site', '$amount', '$commission', '$tax', '$net')
             "; 
    } else {
        //update
        $sql = "update monthlycardsales set
            amount = '$amount',
            commission = '$commission',
            tax = '$tax',
            net = '$net'
            where cardsale_date = '$date'
            and site_id = '$site'";
    }
    R::exec($sql);
}

function getdata($now, $site=1, $batchnum ='') {
    $db = 'shogunfds'. $site;
    if ($batchnum != '') {
        $sqBn = " and a.batch_number = '$batchnum'";
    }

    $sql = "select b.receipt_date, b.salesreceipt_id, a.card_type, 
		a.card_suffix, a.multi_entry_approver,
		a.approval_code, a.batch_number, 
        if (a.is_debit = 1, 'Debit/ATM', 'Credit') as 'system',
        b.amount, 
        if (a.is_debit=1, b.amount * 0.02, b.amount * 0.035) as 'commission', 
        if (a.is_debit=1, 0, b.amount * 0.005) as 'tax', 
        if (a.is_debit=1, (b.amount * (1 - 0.02)), (b.amount * (1 - 0.035 - .005))) as 'net',
        a.is_posted,
        a.cpd_ids,
        a.post_date
        from $db.card_payment_details a, $db.salesreceipts b 
        where a.salesreceipt_id=b.salesreceipt_id
        and date(b.receipt_date) = '$now'
        $sqBn
        order by a.is_debit, b.receipt_date
    ";
    $res = R::getAll($sql);
    $numrows = count($res);
    $ret = '';
    $reservetotals = $totals = array(
        /*
        'receipt_date' => '',
        'sales' => '',
        'type' => '',
        'approval' => '',
        'batch' => '',
        'system' => '',
        */
        'amount' => 0,
        'pctcomm' => '',
        'commission' => 0,
        'pcttax' => '',
        'tax' => 0,
        'net' => 0,
    );
    $currentSystem = 'Credit';
    $system = array();
    foreach ($res as $obj) {
        $obj = json_decode(json_encode($obj));
        $ret .= '<tr>';
        $ret .= '<td>' . $obj->receipt_date . '</td>';
        $ret .= '<td>' . $obj->salesreceipt_id . '</td>';
        $ret .= '<td>' . $obj->card_type . '</td>';
        $ret .= '<td>' . $obj->card_suffix . '</td>';
        $ret .= '<td>' . $obj->approval_code . '</td>';
        $ret .= '<td>' . $obj->batch_number . '</td>';
        $ret .= '<td>' . $obj->multi_entry_approver . '</td>';
        $ret .= '<td>' . $obj->system . '</td>';
        $ret .= '<td>' . number_format($obj->amount, 2) . '</td>';
        if($obj->is_debit == 1) {
            $ret .= '<td>0.02</td>';
        } else {
            $ret .= '<td>0.035</td>';
        }
       
        $ret .= '<td>' . round($obj->commission, 2) . '</td>';
        if($obj->is_debit == 1) {
            $ret .= '<td>0</td>';
        } else {
            $ret .= '<td>0.005</td>';
        }
        $ret .= '<td>' . round($obj->tax, 2) . '</td>';
        $ret .= '<td>' . round($obj->net, 2) . '</td>';
        if ($obj->is_posted) {
            $ret .= '<td>'. $obj->post_date .'</td>';
        } else {
            $ret .= '<td><input type="checkbox" value="'. $obj->cpd_ids .'" data-batchnum="'. $obj->batch_number .'" name="cpdId[]" class="bn bn-'. $obj->batch_number .'"></td>';
        }

        $ret .= '</tr>';
                
        $currentSystem = $obj->system;
        $system[$obj->system]['amount'] += $obj->amount;
        $system[$obj->system]['pctcomm'] = '-';
        $system[$obj->system]['commission'] += $obj->commission;
        $system[$obj->system]['pcttax'] = '-';
        $system[$obj->system]['tax'] += $obj->tax;
        $system[$obj->system]['net'] += $obj->net;
        $totals['amount'] += $obj->amount;
        $totals['pctcomm'] = '';
        $totals['commission'] += $obj->commission;
        $totals['pcttax'] = '';
        $totals['tax'] += $obj->tax;
        $totals['net'] += $obj->net;
    }
    
    foreach ($system as $systemName => $systemTotals) {
        $ret .= "<tr><th class='subtotals subtotaltitle' colspan='8'>$systemName Total</th>";
        foreach ($systemTotals as $total) {
            $ret .= '<th class="subtotals">' . (($total !== '') ? number_format($total, 2) : '') . '</th>';        
        }
        $ret .= '</tr>';

    }
    
    $ret .= '<tr><th class="totals subtotaltitle" colspan="8">Card Sales Total</th>';

    foreach ($totals as $total) {
        $ret .= '<th class="totals">' . (($total !== '') ? number_format($total, 2) : '') . '</th>';        
    }
    $ret .= '</tr>';
    
    if ($site != 1) {
        $ret .= "<tr><th colspan='10'>Reservations</th></tr>";
        $ret .= getheaders();

        $sql2 = "select date_created, original_amount_paid, card_type, approval_code, batch_number,
			card_suffix, multi_entry_approver,
            if (is_debit = 1, 'Debit/ATM', 'Credit') as 'system',
            original_amount_paid, 
            if (is_debit=1, original_amount_paid * 0.02, original_amount_paid * 0.035) as 'commission', 
            if (is_debit=1, 0, original_amount_paid * 0.005) as 'tax', 
            if (is_debit=1, (original_amount_paid * (1 - 0.02)), (original_amount_paid * (1 - 0.035 - .005))) as 'net'
            from reservations
            where date(date_created) = '$now' 
            and original_amount_paid != 0
            and payment_type='Card'
        ";

        $res2 = R::getAll($sql2);
        $numrows = count($res2);
        
        foreach ($res2 as $obj) {
            $obj = json_decode(json_encode($obj));
            $ret .= '<tr>';
            $ret .= '<td>' . $obj->date_created . '</td>';
            $ret .= '<td>' . $obj->reserve_code . '</td>';
            $ret .= '<td>' . $obj->card_type . '</td>';
            $ret .= '<td>' . $obj->card_suffix . '</td>';
            $ret .= '<td>' . $obj->multi_entry_approver . '</td>';
            $ret .= '<td>' . $obj->approval_code . '</td>';
            $ret .= '<td>' . $obj->batch_number . '</td>';
            $ret .= '<td>' . $obj->system . '</td>';
            $ret .= '<td>' . number_format($obj->original_amount_paid, 2) . '</td>';
            if($obj->is_debit == 1) {
                $ret .= '<td>0.02</td>';
            } else {
                $ret .= '<td>0.035</td>';
            }
            $ret .= '<td>' . round($obj->commission, 2) . '</td>';
            if($obj->is_debit == 1) {
                $ret .= '<td>0</td>';
            } else {
                $ret .= '<td>0.005</td>';
            }
            $ret .= '<td>' . round($obj->tax, 2) . '</td>';
            $ret .= '<td>' . number_format($obj->net, 2) . '</td>';
            $ret .= '<td><input type="checkbox" name="reserveId[]"></td>';
            $ret .= '</tr>';
            $reservetotals['amount'] += $obj->original_amount_paid;
            $reservetotals['commission'] += $obj->commission;
            $reservetotals['tax'] += $obj->tax;
            $reservetotals['net'] += $obj->net;
            $totals['amount'] += $obj->original_amount_paid;
            $totals['commission'] += $obj->commission;
            $totals['tax'] += $obj->tax;
            $totals['net'] += $obj->net;
        }
        $ret .= '<tr><th class="totals subtotaltitle" colspan="8">Card Reservations Total</th>';
        foreach ($reservetotals as $reservetotal) {
            $ret .= '<th class="totals">' . (($reservetotal !== '') ? number_format($reservetotal, 2) : '') . '</th>';        
        }
        $ret .= '</tr>';
    }
    
    //$ret .= '<tr><th colspan="10">Grand Totals</th></tr>';
    $ret .= '<tr><th class="totals subtotaltitle" colspan="8">Card Sales Grand Total</th>';
    foreach ($totals as $total) {
        $ret .= '<th class="totals">' . (($total !== '') ? number_format($total, 2) : '') . '</th>';        
    }
    $ret .= '</tr>';
    

    postdata($now, $site, $totals);
    return $ret;
}


function getHeaders() {
    $hdrs = array('Date', 'Transaction ID', 'Card Type', 'Card Suffix', 
		'Approval Code', 'Batch No.',  'Multi-entry Approver', 
        'System', 'Amount', '%Com', 'Commission', '%Tax', 'Tax', 'Net Sales',
        '<input type="checkbox" id="bnparent" />');
    $ret = '<tr>';
    foreach ($hdrs as $hdr) {
        $ret .= "<th>$hdr</th>";
    }
    $ret .= '</tr>';
    return $ret;
}

?>
<html>
<head>
    <title>Daily Card Sales Summary</title>
    <link rel="stylesheet" type="text/css" href="./reports.css">
    <link rel="stylesheet" type="text/css" href="w2ui-1.4.3.min.css" />
    <script src="../../js/jquery.js"></script>
    <script type="text/javascript" src="w2ui-1.4.3.min.js"></script>
<style>
table {
	border-collapse:collapse;
}

table th,td {
	padding:4px;
	text-align:center;
}

th.grand, td.grand {
	background-color:#eeffcc;
}

tr.weekend {
	background-color:#ffeecc;
}
#newdate {
    width: 120px;
}

.menubar {
    background-color: grey;
    color: white;
    font-weight: bold;
}

.totals {
    color: green;    
}

select {
    font-weight: bold;
    font-size: 12px;
}

.subtotals {
    color: blue;
}

.subtotaltitle {
    text-align: right;
    padding-right: 10px;
}


</style>
</head>
<body>
<form method='post'>
<h5>Daily Card Sales Summary</h5>
<div class="menubar">


<select name="site">
<?php

for($x = 1; $x <= 2; $x++) {
    echo "<option value='$x'";
    if ($site == $x) echo " selected ";
    echo ">Shogun $x</option>";
}

?>
</select>
Select Date: <input type="text" id="newdate" name="newdate" value="<?php echo $now ?>" />
Batch Number:  <input type="text" id="batchnum" name="batchnum" value="<?php echo $batchnum ?>"/>
<input type='submit' name='cmd' value='go' />
<input type='submit' name='cmd' value='view monthly'/>
    <input type='submit' name='cmd' value='post to gl'/>
</div>
<div id="workpanel">
<table border='1'>
<thead>
<?php echo getHeaders() ?>
</thead>
<tbody>
<?php echo getdata($now, $site, $batchnum) ?>
</tbody>
</table>
</div>
</form>
<script>
$(document).ready(function(){
	//$("#newdate").datepicker({dateFormat:'yy-mm-dd'});
    $('#newdate').w2field('date',  { format: 'yyyy-mm-dd' });

    $('#bnparent').on('click', function(e){
       $('.bn').attr('checked', $(this).is(':checked'));
    });
});
</script>
</body>
</html>
