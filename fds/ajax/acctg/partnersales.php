<?php
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('../date.functions.php');

$title = ' Partner Sales ';
$partner = isset( $_GET['partner'] ) ? $_GET['partner'] : '';
$month = isset($_GET['mo'])? $_GET['mo'] : date('m');
$year = isset($_GET['yr'])? $_GET['yr'] : date('Y');

$sql = 'select "Select a Partner"';

if ($partner && $month && $year) {
	$sql= "
	select concat( g.firstname,' ', g.lastname) as guest, a.*, b.*, d.door_name, sum(e.qty * e.unit_cost) as amount
	from partner_transactions a, occupancy b, reservation_transactions c, rooms d, room_sales e, reservations f, guests g
	where b.occupancy_id=c.occupancy_id
	and c.reservation_code=a.reserve_code
	and c.reservation_code=f.reserve_code
    and f.guest_id=g.guest_id
	and b.room_id=d.room_id
	and b.occupancy_id=e.occupancy_id
	and a.partner_name='$partner'
	and e.category_id=3
	and e.remarks not like 'Extension%'
	and year(b.actual_checkout)='$year'
	and month(b.actual_checkout)='$month'
	group by e.occupancy_id
	order by b.actual_checkout desc
		";	
} 


$arrReport = array(
		'title'    => $title,
		'subtitle' => 'Partner: '  . $partner,
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();

function partners($selected) {
	$sql = "select partner_name from partners";
	$all = R::getAll($sql);

	$ret = "<select name='partner' id='partner'>";
	foreach ($all as $p) {
		$ret .= '<option value="' . $p['partner_name'] . '" ';
		$ret .= ($p['partner_name'] == $selected) ? ' selected >' : '>';
		$ret .= $p['partner_name'] . '</option>';
	}
	$ret .= "</select>";
	return $ret;
}

?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="./reports.css">
<link rel="stylesheet" type="text/css" href="../../css/start/jquery-ui.css">
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>
<style>
body {font-family:helvetica;margin:0px;}
table {font-size:12px;font-family:helvetica;border:1px transparent #000;margin: 0 4px;}
th {width:auto; border:1px solid #cccccc;padding: 2px 6px}
.totals {color:#00F;font-weight:bold;}
td {border-bottom:1px solid #cccccc; text-align:center; padding: 2px; }
.amt {text-align:right}
h3 {font-size:12px;}
.menubar {background-color:#eeeeee;font-size:11px;padding:4px;border-bottom:1px solid #cccccc;}
.content {padding:10px; font-size:11px}
.message {display:inline; color:#ff0000;}
.targets {color: orange;}
.actuals {color: green;}
.menutitle {font-weight: bold;}
.origfc {cursor: pointer; background-color: #eee;}
</style>
</head>
<body>
<form>
<div style="background-color: #eee; border-bottom: 1px solid #aaa;">
Select Date:
<?php echo getMonthDropdown($month); ?>
<input type="text" name="yr" value="<?php echo $year ?>" id="yr" size="4" maxlength="4">
Select Partner: <?php echo partners($partner) ?>
<input type="submit" name="submit" value="go" />
</div>
<?php $report->show(); ?> 
</form>
<script>
$(document).ready(function(){
	
});
</script>
</body>
</html>

