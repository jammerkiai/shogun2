<?php

interface SalesCodeInjector
{
	public function getOptions($ci);
}