<?php
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');

/******************************/
// edit room count here
$WPROOMS = 69;
$HSROOMS = 116;
$TOTALROOMS = $WPROOMS + $HSROOMS;
/******************************/

$title = 'Monthly Sales Summary';
$thismonth = isset($_POST['thismonth']) ? $_POST['thismonth'] : date('m');
$thisyear = isset($_POST['thisyear']) ? $_POST['thisyear'] : date('Y');

if($_POST["submit"]=="export to excel") {
	$table="<h3 style='font-size:12px;font-family:arial,helvetica'>$title - $thismonth/$thisyear</h3>";
	$table.="<table border='0' cellspacing='2' cellpadding='2' style='font-size:11px;font-family:arial,helvetica'>";
	$table.=getHeaders();
	$table.=getData($thisyear,$thismonth);
	$table.="</table>";
	$excel_file_name="mss_$thisyear_$thismonth.xls";
	header("Content-type: application/octet-stream");//A MIME attachment with the content type "application/octet-stream" is a binary file.
	header("Content-Disposition: attachment; filename=$excel_file_name");//with this extension of file name you tell what kind of file it is.
	header("Pragma: no-cache");//Prevent Caching
	header("Expires: 0");//Expires and 0 mean that the browser will not cache the page on your hard drive
	echo $table;
	exit;
}


//functions
function getHeaders() {
	$retval="<tr>

<th rowspan=2 colspan=2>Day</th>

<th colspan=3>Guests</th>
<th colspan=3>Room</th>
<th colspan=10>Food</th>
<th colspan=10>Beer</th>

<th rowspan=2 class='grand'>Kitchen<br />Sales</th>

<th colspan=11>Misc</th>

<th colspan=12 class='grand'>Grand Totals</th>

<th rowspan=2 class='grand'>DAILY</th>
<th rowspan=2 class='grand'>AVGCUM</th>
</tr>


<tr>

<!-- guests -->
<th>WP</th>
<th>S2</th>
<th>Total</th>

<!-- room -->
<th>WP</th>
<th>S2</th>
<th>Total</th>

<!-- food -->
<th>BQ</th>
<th>LB1</th>
<th>IN1</th>
<th>LB2</th>
<th>IN2</th>
<th>GA</th>
<th>RE</th>
<th>WP</th>
<th>S2</th>
<th>Total</th>

<!-- beer -->
<th>BQ</th>
<th>LB1</th>
<th>IN1</th>
<th>LB2</th>
<th>IN2</th>
<th>GA</th>
<th>RE</th>
<th>WP</th>
<th>S2</th>
<th>Total</th>

<!-- misc -->
<th>BQ</th>
<th>LB1</th>
<th>IN1</th>
<th>LB2</th>
<th>IN2</th>
<th>GA</th>
<th>RE</th>
<th>NS</th>
<th>TF</th>
<th>WP</th>
<th>S2</th>
<th>Total</th>


<!-- grand -->
<th>BQ</th>
<th>LB1</th>
<th>IN1</th>
<th>LB2</th>
<th>IN2</th>
<th>GA</th>
<th>RE</th>
<th>NS</th>
<th>TF</th>
<th>WP</th>
<th>S2</th>
<th>GT</th>

</tr>";
return $retval;
}

function getData($thisyear,$thismonth) {
	$cumulative = '';
	$avg = '';
	$totals = [];
	for($x=1; $x < 32; $x++) {
		if(strlen($x)==1) $x = '0'.$x;
		$date = $thisyear.'-'.$thismonth.'-'.$x;
		$day = date('D',strtotime($date));
		$checkmonth = date('m',strtotime($date));
		if($checkmonth != $thismonth) break;

		$dataRow = [
			$x, 	//day 
			$day,	//day name
			$wp = getSales($date, 'guests', 1, 1),  //walkup guests
			$hs = getSales($date, 'guests', 2, 2),  //hotel guests
			$guests = $wp + $hs, //total guests

			$rooms_wp = getSales($date,'roomsales + overtime', 1, 1),
			$rooms_hs = getSales($date, 'roomsales + overtime', 2, 2),
			$rooms_total = $rooms_wp + $rooms_hs,

			$food_banquet = getSpecialSales($date, 'food + deductions + discounts + adjust', 'Banqu', 2, 2),
			$food_lb1 = getSpecialSales($date, 'food + deductions + discounts + adjust', 'Lobby', 1, 1),
			$food_in1 = getSpecialSales($date, 'food + deductions + discounts + adjust', 'In-house', 1, 1),
			$food_lobby = getSpecialSales($date, 'food + deductions + discounts + adjust', 'Lobby', 2, 2),
			$food_inhouse = getSpecialSales($date, 'food + deductions + discounts + adjust', 'InHou', 2, 2),
			$food_garden = getSpecialSales($date, 'food + deductions + discounts + adjust', 'Garde', 2, 2),
			$food_resto = getSpecialSales($date, 'food + deductions + discounts + adjust', 'Resta', 2, 2),
			$food_wp = getSales($date, 'food', 1, 1),
			$food_hs = getSales($date, 'food', 2, 2) - $food_banquet - $food_lobby - $food_inhouse - $food_garden - $food_resto,

			$food_total = $food_wp + $food_hs + $food_banquet + $food_lobby + $food_inhouse + $food_garden + $food_resto + $food_lb1 + $food_in1,

			$beer_banquet = getSpecialSales($date, 'beer', 'Banqu', 2, 2),
			$beer_lb1 = getSpecialSales($date, 'beer', 'Lobby', 1, 1),
			$beer_in1 = getSpecialSales($date, 'beer', 'In-house', 1, 1),
			$beer_lobby = getSpecialSales($date, 'beer', 'Lobby', 2, 2),
			$beer_inhouse = getSpecialSales($date, 'beer', 'InHou', 2, 2),
			$beer_garden = getSpecialSales($date, 'beer', 'Garde', 2, 2),
			$beer_resto = getSpecialSales($date, 'beer', 'Resta', 2, 2),
			$beer_wp = getSales($date, 'beer', 1, 1) ,
			$beer_hs = getSales($date, 'beer', 2, 2) - $beer_banquet - $beer_lobby - $beer_inhouse - $beer_garden - $beer_resto,
			$beer_total = $beer_wp + $beer_hs + $beer_banquet + $beer_lobby + $beer_inhouse + $beer_garden + $beer_resto + $beer_lb1 + $beer_in1,

			$kitchen = $food_total + $beer_total, // - banquet - other food


			$misc_banquet = getSpecialSales($date, 'misc', 'Banqu', 2, 2),
			$misc_lb1 = getSpecialSales($date, 'misc', 'Lobby', 1, 1),
			$misc_in1 = getSpecialSales($date, 'misc', 'In-house', 1, 1),
			$misc_lobby = getSpecialSales($date, 'misc', 'Lobby', 2, 2),
			$misc_inhouse = getSpecialSales($date, 'misc', 'InHou', 2, 2),
			$misc_garden = getSpecialSales($date, 'misc', 'Garde', 2, 2),
			$misc_resto = getSpecialSales($date, 'misc', 'Resta', 2, 2),
			$misc_noshow = getSpecialSales($date, 'misc', 'NoShow', 2, 2),
			$misc_tariff = getSpecialSales($date, 'misc', 'Tariff', 2, 2),
			$misc_wp = getSales($date, 'misc', 1, 1) ,
			$misc_hs = getSales($date, 'misc', 2, 2) - $misc_lobby - $misc_inhouse - $misc_garden - $misc_resto - $misc_noshow - $misc_tariff - $misc_banquet,
			$misc_total = $misc_wp + $misc_hs + $misc_lobby + $misc_inhouse + $misc_garden + $misc_resto + $misc_noshow + $misc_tariff + $misc_lb1 + $misc_in1 + $misc_banquet,

			$grand_banquet = $food_banquet + $beer_banquet + $misc_banquet,
			$grand_lb1 = $food_lb1 + $beer_lb1 + $misc_lb1,
			$grand_in1 = $food_in1 + $beer_in1 + $misc_in1,
			$grand_lobby = $food_lobby + $beer_lobby + $misc_lobby,
			$grand_inhouse = $food_inhouse + $beer_inhouse + $misc_inhouse,
			$grand_garden = $food_garden + $beer_garden + $misc_garden,
			$grand_resto = $food_resto + $beer_resto + $misc_resto,
			$grand_noshow = $misc_noshow,
			$grand_tariff = $misc_tariff,
			$grand_wp = $rooms_wp + $food_wp + $beer_wp + $misc_wp,
			$grand_hs = $rooms_hs + $food_hs + $beer_hs + $misc_hs ,
			$grand_total = $rooms_total + $food_total + $beer_total + $misc_total,

			$cumulative += $grand_total,
			$avg = ceil($cumulative/$x),

		];
		
		$rows .= getDataTableRow($dataRow);

		for ($j=2; $j <= count($dataRow) - 3; $j++) {
			$totals[$j] += $dataRow[$j];
		}

	}
	
	$rows .= getFooter($totals);
	return $rows;
}


function getDataTableRow($data) {
	$class = in_array($data[1], ['Sun', 'Sat']) ? 'weekend' : '';
	foreach ($data as $index => $d) {
		if ($index > 4 && $d != 0) {
			$d = number_format($d, 2);
		}
		$html .= "<td>$d</td>";
	}
	return "<tr class='$class'>" . $html . '</tr>';
}


function saveTotalsForYearlySummary($totals, $year, $month) {
	if (strlen($month) === 1) $month = '0'.$month;
	$np = R::find("yearlysalessummary", "postmonth=:month and postyear=:year",
		[':month' => $month , ':year' => $year]);

	if (empty($np)) {
		$np = R::dispense('yearlysalessummary');
	}

	$np->postdate = $year . $month;
	$np->postmonth = date('F', strtotime("$year-$month-01"));
	$np->postyear = $year;
	foreach ($totals as $key => $value) {
		$np->{$key} = $value;
	}

	R::store($np);
}

function getFooter($totals) {
	$ftr = '';
	foreach($totals as $index => $t) {
		if ($index > 4 && $t != 0) {
			$t = number_format($t, 2);
		}
		$ftr .= '<td class="grand">' . $t . '</td>';
	}

	return "<tr><th colspan='2' class='grand'>Totals</th>" . $ftr . '<th colspan="2"></th></tr>';
}

function getMonth($selected) {
	$ret = "<select name='thismonth' id='thismonth'>";
	for($x=1; $x <=12; $x++) {
		$month = date('F', strtotime('2010-' . $x . '-01'));
		$ret.="<option value='$x' ";
		if($selected==$x) $ret.=" selected ";
		$ret.=">$month</option>";
	}
	$ret.="</select>";
	return $ret;
}

function getYear($selected) {
	$ret = "<select name='thisyear' id='thisyear'>";
	for($x=2010; $x <=2019; $x++) {
		$ret.="<option value='$x' ";
		if($selected==$x) $ret.=" selected ";
		$ret.=">$x</option>";
	}
	$ret.="</select>";
	return $ret;
}

function getSales($date,$category,$type=null,$site=null) {
	$sql  = " select sum($category) from salessummary 
			  where salesdate='$date'  ";
	if(!is_null($type) && !is_null($site)) $sql.=" and type='$type' and site='$site' ";
	return R::getCell($sql);
}

function getSpecialSales($date,$category,$location,$type=null,$site=null) {
	$sql  = " select sum($category) from specialsalessummary 
			  where salesdate='$date' and location='$location' ";
	if(!is_null($type) && !is_null($site)) $sql.=" and type='$type' and site='$site' ";
	return R::getCell($sql);
}

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./reports.css">
<link rel="stylesheet" type="text/css" href="../../css/start/jquery-ui.css">
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>
<style>
table {
	border-collapse:collapse;
}
table th,td {
	padding:4px;
}

th {
	background-color: #ccc;
}

table td {
	text-align:right;
}

th.grand, td.grand, tr.total {
	background-color:#eeffcc;
}

tr.weekend {
	background-color:#ffeecc;
}
</style>
</head>
<body>
<form method='post'>
<div>
Select Month: <?php echo getmonth($thismonth) . getyear($thisyear) ?>
<input type="submit" name="submit" value="go" />
<input type="submit" name="submit" value="export to excel" />
</div>
<div id="workpanel">
<div>Total Rooms: <?php echo $TOTALROOMS ?></div>
<table border='1'>
<?php echo getHeaders() ?>
<?php

echo getData($thisyear,$thismonth);
?>
</table>
</div>
</form>
<script>
$(document).ready(function(){
	$("#newdate").datepicker({dateFormat:'yy-mm-dd'});
});
</script>
</body>
</html>

