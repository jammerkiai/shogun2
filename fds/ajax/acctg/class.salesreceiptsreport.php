<?php

class salesreceiptsreport
{
	public function salesreceiptsreport($params)
	{
		foreach($params as $key => $value) {
			$this->{$key}=$value;
		}
	}
	public function getshift($date) {
		if(!$date)$date=date("Y-m-d H:i:s");
		list($d, $t) = explode(" ", $date);
		list($h, $m, $s) = explode(":", $t);
		$sql = "select shift_id from shifts where $h between shift_start and shift_end";
		$shift = R::getCell($sql);

		if($h==14||$h==13)
		{
			$this->shiftnum=3;
			return $shift = "3rd";
		}
		elseif($h==6||$h==5)
		{
			$this->shiftnum=2;
			return $shift = "2nd";
		}
		$this->shiftnum=1;
	return  "1st";
	}

	public function getLatestShifts($shiftid)
	{
		$sql = "SELECT `shift-transaction_id`,datetime,user_id
				FROM `shift-transactions`
				where shift = 'start'
				order by datetime desc
				limit 0, 50";
		$res = R::getAll($sql);
		$ret = "<select name='rblshifts' id='rblshifts' onchange='myform.submit();'>";
		$ret .= "<option value=''>&nbsp;</option>";
		foreach ($res as $r)
		{
			$shift_transaction_id = $r['shift-transaction_id'];
			$datetime = $r['datetime'];
			$userid = $r['user_id'];

			if($shiftid == $shift_transaction_id)
			{
				$select = "selected";
			}else
			{
				$select = " ";
			}
			$__sql = "SELECT user_id  FROM `shift-transactions` where shift = 'end'
			and `datetime` > '$datetime'
			order by datetime asc
			limit 0,1";
			$userid = R::getCell($__sql);
			$_sql = "select fullname from users where user_id = '$userid'";
			$username = R::getCell($_sql);
			$ret .= "<option value='$shift_transaction_id' $select>$datetime - $username - ".$this->getshift($datetime)."</option>";
		}
		$ret .= "</select>";
		return $ret;
	}

	public function getCashOnHand()
	{
		$sql = "select amount,datetime from cash_on_hand where datetime >='$this->start' and datetime <= '$this->end' ";
		$row = R::getRow($sql);
		$this->cashOnHand=$row['amount'];
		return '<h3>Declared Cash On Hand ['. $row['datetime'] . ']  -  Php ' . number_format($this->cashOnHand,2) .'</h3>';
	}

	public function getSafeKeep()
	{

		$sql = "select a.safekeep_date, a.amount, b.fullname as 'cashier', c.fullname as 'oic'
				from safekeep a, users b, users c
				where a.cashier_id=b.user_id
				and a.oic_id=c.user_id
				and a.safekeep_date >='$this->start' and a.safekeep_date <='$this->end'
				order by a.safekeep_date
			";
		$res = R::getAll($sql);
		if(!empty($res)) {
			$ret = '<h3>Safekeeping Records</h3>';
			$ret .= '<table class=report>';
			$ret.='<tr><th>Date</th><th>Amount</th><th>Cashier</th><th>OIC</th></tr>';
			foreach ($res as $r) {

				$date = $r['safekeep_date'];
				$amount = $r['amount'];
				$cashier= $r['cashier'];
				$oic = $r['fullname'];

				$ret.='<tr>';
				$ret.="<td>$date</td>";
				$ret.="<td>$amount</td>";
				$ret.="<td>$cashier</td>";
				$ret.="<td>$oic</td>";
				$ret.='</tr>';
			}
			$ret .= '</table>';
		}
		return $ret;
	}

	public function getSalesreceipts()
	{

		$sql = "select a.salesreceipt_id, a.receipt_date, d.door_name,
				if(a.tendertype<>'Card', a.amount, '') as 'Cash', if(a.tendertype='Card', a.amount, '') as 'Card' ,
				a.tendertype, b.fullname as 'cashier',a.occupancy_id
				from salesreceipts a, users b, occupancy c, rooms d
				where a.update_by=b.user_id
				and a.occupancy_id=c.occupancy_id
				and c.room_id=d.room_id
				and a.receipt_date >='$this->start' and a.receipt_date <='$this->end'
				order by a.receipt_date
			";
		$res = R::getAll($sql);
		if(!empty($res)) {
			$ret = '<h3>Sales Receipts Records</h3>';
			$ret .= '<table class=report>';
			$ret.='<tr><th>Date</th><th>Room No.</th><th>Cash</th><th>Card</th><th>TenderType</th><th>Cashier</th></tr>';
			foreach ($res as $r) {

				$sid = $r['salesreceipt_id'];
				$date = $r['receipt_date'];
				$door = $r['door_name'];
				$cash = $r['Cash'];
				$card = $r['Card'];
				$tender = $r['tendertype'];
				$cashier = $r['cashier'];
				$occupancy = $r['occupancy_id'];

				$ret.='<tr>';
				$ret.="<td>$date</td>";
				$ret.="<td class='numeric'>$door</td>";
				$ret.="<td class='numeric'>$cash</td>";
				$ret.="<td class='numeric'>$card</td>";
				$ret.="<td>$tender</td>";
				$ret.="<td>$cashier</td>";
				$ret.="<td class='debug'>$sid</td>";
				$ret.='<td class="debug"><a href="occupancydetails.php?occ='.$occupancy.'" target="_blank">'.$occupancy.'</a></td>';
				$ret.='</tr>';
				$this->total['cash']+=$cash;
				$this->total['card']+=$card;
			}
			$ret .= "<tr><th>&nbsp;</th><th>&nbsp;</th>
			<th class='numeric'>{$this->total['cash']}</th>
			<th class='numeric'>{$this->total['card']}</th>
			<th>&nbsp;</th><th>&nbsp;</th></tr>";
			$ret .= '</table>';
		}
		return $ret;
	}

	public function getPrintableSalesreceipts()
	{

		$sql = "select a.salesreceipt_id, a.receipt_date, d.door_name,
				if(a.tendertype not in ('Card', 'Reservation Fee'), a.amount, '0') as 'Cash', if(a.tendertype='Card', a.amount, '0') as 'Card' ,
				a.tendertype, b.fullname as 'cashier',a.occupancy_id
				from salesreceipts a, users b, occupancy c, rooms d
				where a.update_by=b.user_id
				and a.occupancy_id=c.occupancy_id
				and c.room_id=d.room_id
				and a.receipt_date >='$this->start' and a.receipt_date <='$this->end'
				order by a.receipt_date
			";
		$res = R::getAll($sql);
		if(!empty($res)) {
			$ret = '=== SALES BREAKDOWN ===';
			$ret .= "\n\n";
			$ret.= str_pad("Transaction Date", 20);
			$ret.= str_pad("Room No.", 8, ' ', STR_PAD_BOTH);
			$ret.= str_pad("Cash", 10, ' ', STR_PAD_LEFT);
			$ret.= str_pad("Card", 10, ' ', STR_PAD_LEFT);
			$ret.= "\n";
			foreach ($res as $r) {

				$sid = $r['salesreceipt_id'];
				$date = $r['receipt_date'];
				$door = $r['door_name'];
				$cash = $r['Cash'];
				$card = $r['Card'];
				$tender = $r['tendertype'];
				$cashier = $r['cashier'];
				$occupancy = $r['occupancy_id'];

				$ret.="\n";
				$ret.= str_pad($date, 20);
				$ret.= str_pad($door, 8, ' ', STR_PAD_BOTH);
				$ret.= str_pad($cash, 10, ' ', STR_PAD_LEFT);
				$ret.= str_pad($card, 10, ' ', STR_PAD_LEFT);
				$this->total['cash']+=$cash;
				$this->total['card']+=$card;
			}
			$ret .= "\n";
			$ret .= str_pad('Total:', 30);
			$ret .= str_pad($this->total['cash'], 8, ' ', STR_PAD_LEFT);
			$ret .= str_pad($this->total['card'], 10, ' ', STR_PAD_LEFT);

		}
		return $ret;
	}

	public function getCumulativeTransactions()
	{

		$sql = "select a.cc_id, a.cc_date, a.transaction_amount,  a.current_amount, a.transaction_type,  b.fullname as 'cashier'
				from current_cash a, users b
				where a.update_by=b.user_id
				and a.cc_date >='$this->start' and a.cc_date <='$this->end'
				order by a.cc_date
			";
		$res = R::getAll($sql);
		if(!empty($res)) {
			$ret = '<h3>Transactions</h3>';
			$ret .= '<table class=report>';
			$ret.='<tr><th>Date</th><th>Transaction Amount</th><th>Running Amount</th><th>Transaction Type</th><th>Cashier</th></tr>';
			foreach ($res as $r) {

				$sid = $r['cc_id'];
				$date = $r['cc_date'];
				$tamt = $r['transaction_amount'];
				$camt = $r['current_amount'];
				$tender = $r['transaction_type'];
				$cashier = $r['cashier'];

				$ret.='<tr>';
				$ret.="<td>$date</td>";
				$ret.="<td class='numeric'>$tamt</td>";
				$ret.="<td class='numeric'>$camt</td>";
				$ret.="<td>$tender</td>";
				$ret.="<td>$cashier</td>";
				$ret.="<td>$sid</td>";
				$ret.='</tr>';
				if($tender=='In')
				$this->total['tamt']+=$tamt;

			}
			$ret .= "<tr><th>&nbsp;</th>
			<th class='numeric'>{$this->total['tamt']}</th>
			<th>&nbsp;</th>
			<th class='numeric'>&nbsp;</th>
			<th>&nbsp;</th><th>&nbsp;</th></tr>";
			$ret .= '</table>';
		}
		return $ret;
	}
	public function getInTransitFromNewCheckin()
	{
		$sql = "select distinct b.door_name as 'Room No.',
			a.actual_checkin as 'Checkin',
			a.actual_checkout as 'Checkout',
			if(b.site_id=1,'Walkup','Hotel') as 'Site',
			c.room_type_name as 'Room Type',
			d.rate_name as 'Rate',
			b.room_id ,
			c.room_type_id,
			d.rate_id,
			a.occupancy_id,
			sum(e.unit_cost*e.qty) as 'ROS'
		from occupancy a, rooms b, room_types c, rates d , room_sales e
		where

			a.actual_checkin >= '$this->start'
			and
			a.actual_checkin <= '$this->end'
			and
			a.actual_checkout = '0000-00-00 00:00:00'

		and a.occupancy_id=e.occupancy_id
		and a.room_id = b.room_id
		and b.room_type_id=c.room_type_id
		and a.rate_id=d.rate_id
		and e.status='Paid'
		group by e.occupancy_id
		";

		$res = R::getAll($sql);
		if(!empty($res)) {
			$fieldnames = array_keys($res[0]);
			$numfields = count($fieldnames);
			$retval="
			<strong>InTransit Sales</strong>: New checkin w/in shift
			<br/>$numfields records found.<br/><table class='report'>";
			$retval.="<tr>";
			for($i=0; $i < $numfields; $i++) {
				$field=$fieldnames[$i];
				$retval.="<th>$field</th>";
			}
			$retval.="<th>Food</th>";
			$retval.="<th>Beer</th>";
			$retval.="<th>Total</th>";
			$retval.="</tr>";
			$grandtotal=$rostotal=$foodtotal=$beertotal=0;
			foreach ($res as $row) {
				$retval.="<tr>";
				foreach($row as $rowvalue) {
					$retval.="<td>$rowvalue</td>";
				}
				$food=$this->getFoodSales($row['occupancy_id'],'food');
				$beer=$this->getFoodSales($row['occupancy_id'],'beer');
				$total = $food + $beer + $row['ROS'];
				$rostotal += $row['ROS'];
				$foodtotal+=$food;
				$beertotal+=$beer;
				$grandtotal +=$total;
				$retval.="<td>$food</td>";
				$retval.="<td>$beer</td>";
				$retval.="<td>$total</td>";
				$retval.="</tr>";
			}
			$retval.="<tr><td colspan='9'>&nbsp;</td><td>Grand Total:</td>
			<td>$rostotal</td>
			<td>$foodtotal</td>
			<td>$beertotal</td>
			<td>$grandtotal</td></tr>";
			$retval.="</table>";
		}
		$this->grandtotals['InTransit Sales: New Checkin within Shift']=$grandtotal;
		return $retval;

	}

	public function getSalesForRechit()
	{
		$sql = "select distinct b.door_name as 'Room No.',a.actual_checkin as 'Checkin',
				a.actual_checkout as 'Checkout',
				if(b.site_id=1,'Walkup','Hotel') as 'Site', c.room_type_name as 'Room Type',
				d.rate_name as 'Rate',
				b.room_id , c.room_type_id
				,d.rate_id,a.occupancy_id, sum(e.unit_cost*e.qty) as 'ROS'
			from occupancy a, rooms b, room_types c, rates d , room_sales e
			where
			(
				e.update_date >= '$this->start' and e.update_date <= '$this->end'
				and
				a.actual_checkin < '$this->start'
				and (a.actual_checkout >= '$this->end' or a.actual_checkout='0000-00-00 00:00:00')
			)
			and a.occupancy_id=e.occupancy_id
			and a.room_id = b.room_id
			and b.room_type_id=c.room_type_id
			and a.rate_id=d.rate_id
			and e.status='Paid'
			group by e.occupancy_id
			";
		$res = R::getAll($sql);

		if(!empty($res)) {
			$fieldnames = array_keys($res[0]);
			$numfields = count($fieldnames);
			$retval="
			<strong>In Transit</strong>: Cash Accepted for Rechit Later
			<br/>$numfields records found.<br/><table class='report'>";
			$retval.="<tr>";
			for($i=0; $i < $numfields; $i++) {
				$field=$fieldnames[$i];
				$retval.="<th>$field</th>";
			}
			$retval.="<th>Food</th>";
			$retval.="<th>Beer</th>";
			$retval.="<th>Total</th>";
			$retval.="</tr>";
			$grandtotal=$foodtotal=$beertotal=$rostotal=0;
			foreach ($res as $row) {
				$retval.="<tr>";
				foreach($row as $rowvalue) {
					$retval.="<td>$rowvalue</td>";
				}
				$food=$this->getFoodSales($row['occupancy_id'],'food');
				$beer=$this->getFoodSales($row['occupancy_id'],'beer');
				$total = $food + $beer + $row['ROS'];
				$retval.="<td>$food</td>";
				$retval.="<td>$beer</td>";
				$retval.="<td>$total</td>";
				$rostotal += $row['ROS'];
				$foodtotal+=$food;
				$beertotal+=$beer;
				$grandtotal+=$total;
				$retval.="</tr>";
			}
			$retval.="<tr><td colspan='9'>&nbsp;</td><td>Grand Total:</td>
			<td>$rostotal</td>
			<td>$foodtotal</td>
			<td>$beertotal</td>
			<td>$grandtotal</td></tr>";
			$retval.="</table>";
		}
		$this->grandtotals['InTransit Sales: Payment Received for Rechit Later']=$grandtotal;
		return $retval;
	}

	public function getPaymentOnCheckout()
	{
		$sql = "select distinct b.door_name as 'Room No.',a.actual_checkin as 'Checkin',
				a.actual_checkout as 'Checkout',
				if(b.site_id=1,'Walkup','Hotel') as 'Site', c.room_type_name as 'Room Type',
				d.rate_name as 'Rate',
				b.room_id , c.room_type_id
				,d.rate_id,a.occupancy_id
			from occupancy a, rooms b, room_types c, rates d , room_sales e, fnb_sales f
			where
			(
				(f.update_date >= '$this->start' and f.update_date <= '$this->end' or
				e.update_date >= '$this->start' and e.update_date <= '$this->end')
				and
				a.actual_checkin <='$this->start'
				and
				a.actual_checkout >= '$this->start' and a.actual_checkout <= '$this->end'
			)
			and a.occupancy_id=e.occupancy_id
			and a.room_id = b.room_id
			and b.room_type_id=c.room_type_id
			and a.rate_id=d.rate_id
			and a.occupancy_id=f.occupancy_id
			and f.status='Paid'
			and e.status='Paid'
			";
		$res = R::getAll($sql);

		if(!empty($res)) {
			$fieldnames = array_keys($res[0]);
			$numfields = count($fieldnames);
			$retval="
			<strong>Checkout Sales</strong>: Payment Received Upon Checkout
			<br/>$numfields records found.<br/><table class='report'>";
			$retval.="<tr>";
			for($i=0; $i < $numfields; $i++) {
				$field = $fieldnames[$i];
				$retval.="<th>$field</th>";
			}
			$retval.="<th>ROS</th>";
			$retval.="<th>Food</th>";
			$retval.="<th>Beer</th>";
			$retval.="<th>Total</th>";
			$retval.="</tr>";
			$grandtotal=$rostotal=$foodtotal=$beertotal=0;
			foreach ($res as $row) {
				$retval.="<tr>";
				foreach($row as $rowvalue) {
					$retval.="<td>$rowvalue</td>";
				}
				$ros =$this->getRoomSales($row['occupancy_id']);
				$food=$this->getFoodSales($row['occupancy_id'],'food');
				$beer=$this->getFoodSales($row['occupancy_id'],'beer');
				$total = $food + $beer + $ros;
				$retval.="<td>$ros</td>";
				$retval.="<td>$food</td>";
				$retval.="<td>$beer</td>";
				$retval.="<td>$total</td>";
				$rostotal += $ros;
				$foodtotal+=$food;
				$beertotal+=$beer;
				$grandtotal+=$total;
				$retval.="</tr>";
			}
			$retval.="<tr><td colspan='9'>&nbsp;</td><td>Grand Total:</td>
			<td>$rostotal</td>
			<td>$foodtotal</td>
			<td>$beertotal</td>
			<td>$grandtotal</td></tr>";
			$retval.="</table>";
		}
		$this->grandtotals['Checkout Sales: Payment Received Upon Checkout']=$grandtotal;
		return $retval;
	}

	public function getClosedSales()
	{
		$sql = "select distinct b.door_name as 'Room No.',a.actual_checkin as 'Checkin',
				a.actual_checkout as 'Checkout',
				if(b.site_id=1,'Walkup','Hotel') as 'Site', c.room_type_name as 'Room Type',
				d.rate_name as 'Rate',
				b.room_id , c.room_type_id
				,d.rate_id,a.occupancy_id, sum(e.unit_cost*e.qty) as 'ROS'
			from occupancy a, rooms b, room_types c, rates d , room_sales e
			where
			(
				a.actual_checkout >= '$this->start' and a.actual_checkout <= '$this->end'
				and
				a.actual_checkin >= '$this->start' and a.actual_checkin <= '$this->end'
			)
			and a.occupancy_id=e.occupancy_id
			and a.room_id = b.room_id
			and b.room_type_id=c.room_type_id
			and a.rate_id=d.rate_id
			and e.status='Paid'
			group by e.occupancy_id
			";
		$res = R::getAll($sql);

		if(!empty($res)) {
			$fieldnames = array_keys($res[0]);
			$numfields = count($fieldnames);
			$retval="
			<strong>Actual Sales</strong>: Checkin/Checkout w/in shift
			<br/>$numfields records found.<br/><table class='report'>";
			$retval.="<tr>";
			for($i=0; $i < $numfields; $i++) {
				$field=$fieldnames[$i];
				$retval.="<th>$field</th>";
			}
			$retval.="<th>Food</th>";
			$retval.="<th>Beer</th>";
			$retval.="<th>Total</th>";
			$retval.="</tr>";
			$grandtotal=$rostotal=$foodtotal=$beertotal=0;
			foreach ($res as $row) {
				$retval.="<tr>";
				foreach($row as $rowvalue) {
					$retval.="<td>$rowvalue</td>";
				}
				$food=$this->getFoodSales($row['occupancy_id'],'food');
				$beer=$this->getFoodSales($row['occupancy_id'],'beer');
				$total = $food + $beer + $row['ROS'];
				$retval.="<td>$food</td>";
				$retval.="<td>$beer</td>";
				$retval.="<td>$total</td>";
				$rostotal += $row['ROS'];
				$foodtotal +=$food;
				$beertotal +=$beer;
				$grandtotal+=$total;
				$retval.="</tr>";
			}
			$retval.="<tr><td colspan='9'>&nbsp;</td><td>Grand Total:</td>
			<td>$rostotal</td>
			<td>$foodtotal</td>
			<td>$beertotal</td>
			<td>$grandtotal</td></tr>";
			$retval.="</table>";
		}
		$this->grandtotals['Actual Sales: Checkin/Checkout Within Shift']=$grandtotal;
		return $retval;
	}

	public function getRoomSales($occ)
	{

		$sql = "select sum(unit_cost*qty) from
				room_sales
				where
				occupancy_id='$occ'
			";
		return R::getCell($sql);
	}

	public function getFoodSales($occ,$type='food')
	{
		$not = ($type=='food') ? 'not' : '';
		$sql = "select sum(unit_cost*qty) from
				fnb_sales
				where
				category_id $not in (17,21)
				and
				occupancy_id='$occ'
			";
		return R::getCell($sql);
	}

	public function summary()
	{
		$sql = "select sum(unit_cost*qty) from room_sales
				where update_date >='$this->start' and update_date <='$this->end'
				and status='Paid'
			";
		$roomsales = R::getCell($sql);


		$sql = "select sum(unit_cost*qty) from fnb_sales
				where update_date >='$this->start' and update_date <='$this->end'
				and status='Paid'  and category_id not in (17,21)
			";
		$foodsales = R::getCell($sql);

		$sql = "select sum(unit_cost*qty) from fnb_sales
				where update_date >='$this->start' and update_date <='$this->end'
				and status='Paid' and category_id in (17,21)
			";
		$beersales = R::getCell($sql);

		$retval="<table class='report'>";
		$total=0;

		foreach($this->grandtotals as $key=>$value) {
			$retval.="<tr><td>$key</td><th>$value</th></tr>";
			$total += $value;
		}

		$retval.="<tr><th>Total Sales This Shift</th><th>$total</th></tr>";
		$retval.="</table>";
		$retval.="<hr /><h3>All Sales (Cash + Card)</h3>";
		$retval.="<table class='report'>";
		$retval.="<tr><td>Room Sales</td><th>$roomsales</th></tr>";
		$retval.="<tr><td>FnB Sales</td><th>$foodsales</th></tr>";
		$retval.="<tr><td>Beer Sales</td><th>$beersales</th></tr>";
		$totalsales = $roomsales + $foodsales + $beersales;
		$retval.="<tr><th>Total Sales (Cash + Card)</th><th>$totalsales</th></tr>";
		$retval.="</table>";
		return $retval;
	}

}
