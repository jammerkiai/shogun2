<?php
/** slview_modal.php **/
?>
<div id="updateCode" style="display: none; width: 600px; height: 200px;
    overflow: auto;position:absolute; border: 1px solid gray; top: 20%; left: 20%; background-color: white;">
    <div rel="title">
        Update GL Codes
    </div>
    <div rel="body">
        <div style="padding: 10; font-size: 11px; line-height: 150%;">

            <div class="w2ui-page page-0">
                <div class="w2ui-field">
                    <label>DEBIT:</label>
                    <div>
                        <select id="newdr" name="newdr">
                            <?php foreach ($codes as $c): ?>
                                <option value="<?php echo $c['code']; ?>">
                                    <?php echo $c['code'] . ' - ' . trim($c['name']); ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="w2ui-field">
                    <label>CREDIT:</label>
                    <div>
                        <select id="newcr" name="newcr">
                            <?php foreach ($codes as $c): ?>
                                <option value="<?php echo $c['code']; ?>">
                                    <?php echo $c['code'] . ' - ' . trim($c['name']); ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="w2ui-buttons">
                <button class="btn cancel" type="button" value="Cancel" name="cancel">Cancel</button>
                <button class="btn save" type="button" value="Save" name="save">Save</button>
                <input type="hidden" id="updateId" name="updateId" />
            </div>


        </div>
    </div>
</div>