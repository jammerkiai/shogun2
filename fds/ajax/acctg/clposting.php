<?php
/**
* @file: clposting.php
* views/posts cl transactions
*/

session_start();
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');
require_once('salesforposting.php');

$now = isset($_POST['newdate']) ? $_POST['newdate'] : ( isset($_GET['d']) ? $_GET['d'] : date('Y-m-d'));

if (isset($_POST['cmd']) && $_POST['cmd'] === 'refresh summary') {
    R::exec("delete from sales_journal where postdate='$now'");
    R::exec("delete from sales_journal_details where postdate='$now'");
}

if (isset($_POST['cmd']) && $_POST['cmd'] === 'refresh details') {
    R::exec("delete from sales_journal where postdate='$now'");
    R::exec("delete from sales_journal_details where postdate='$now'");
}

if (isset($_POST['cmd']) && $_POST['cmd'] === 'post to gl') {
    postSalesToGl($now);
    header('location: clposting.php?d=' . $now);
}


$sql = "select * from sales_journal where postdate='$now'";
$all = R::getAll($sql);

if (empty($all)) {
    saveToJournal($now);
}


$sql = "select * from sales_journal where postdate='$now'";
$all = R::getAll($sql);

$sql = "select count(*) from sales_journal_details where postdate='$now'";
$exists = R::getCell($sql);

if (!$exists) {
    $mesg = "Created new sales detail records";
    $fnlist = ['specialsList', 'checkinRoomsalesList', 'checkinRoomsalesRechitList','checkinRoomsalesCheckedOutList','foodSalesList'];
    foreach ($fnlist as $fn) {
        $fn($now);
    }
}

$fnlist = [
    ['label' => 'Lobby / Garden / Restaurant / Banquet / In-house', 'name' => 'getSpecialsFromSalesDetails'],
    ['label' => 'Room Sales - Checked In', 'name' => 'getRoomSalesFromSalesDetails'],
    ['label' => 'Room Sales - Rechit', 'name' => 'getRoomSalesRechitFromSalesDetails'],
    ['label' => 'Room Sales - Checked Out', 'name' => 'getRoomSalesCheckedOutFromSalesDetails'],
    ['label' => 'Food Sales', 'name' => 'getFoodSalesFromSalesDetails']
];

$codes = R::getAll("select code, name from chart_of_accounts");

?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="./reports.css">
    <link rel="stylesheet" type="text/css" href="w2ui-1.4.3.min.css" />
    <script src="../../js/jquery.js"></script>
    <script type="text/javascript" src="w2ui-1.4.3.min.js"></script>
</head>
<body>
<form method='post'>
    <h5>Sales For Posting</h5>
    <div class="menubar">
        <select name="site">
            <?php

            for($x = 1; $x <= 2; $x++) {
                echo "<option value='$x'";
                if ($site == $x) echo " selected ";
                echo ">Shogun $x</option>";
            }

            ?>
        </select>
        Select Date:
        <input type="us-date" id="newdate" name="newdate" value="<?php echo $now ?>" />
        <input type='submit' name='cmd' value='go' />
        <input type='submit' name='cmd' value='refresh summary' onclick="return confirm('The summary for this date will be deleted! Proceed?')"/>
        <input type='submit' name='cmd' value='refresh details' onclick="return confirm('All special updates previously saved for this date will be deleted! Proceed?')"/>
        <input type='submit' name='cmd' value='post to gl'/>
        <?php echo $mesg ?>
    </div>
    <a name="updateglcode"></a>
    <div>
        <h5>Summary</h5>
        <table class="report">
            <thead>
                <th>Receipt Date</th>

                <th>Debit</th>
                <th>Credit</th>
                <th>Type</th>
                <th>Posted</th>
                <th>Amount</th>
            </thead>
            <tbody>
                <?php foreach ($all as $row): ?>
                    <tr>
                        <td><?php echo $row['postdate'] ?></td>

                        <td><?php echo $row['dr_code'] ?></td>
                        <td><?php echo $row['cr_code'] ?></td>
                        <td><?php echo ($row['book']) ? 'L' : 'M' ?></td>
                        <td><?php echo ($row['is_posted']) ? 'Y' : 'N' ?></td>
                        <td><?php echo $row['amount'] ?></td>
                    </tr>
                    <?php $total += $row['amount']; ?>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="5">Total: </th>
                    <th><?php echo number_format($total, 2) ?></th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div>
        <h3>Details</h3>
        <?php foreach ($fnlist as $fn): ?>
            <h3><?php echo $fn['label']; ?></h3>
            <table class="report">
                <thead>
                <th>Receipt Date</th>
                <th>Sales Date</th>
                <th>Update Date</th>
                <th>Occupancy</th>
                <th>Debit</th>
                <th>Credit</th>
                <th>Type</th>
                <th>Tendertype</th>
                <th>Status</th>
                <th>Transaction ID</th>
                <th>Item</th>
                <th>Amount</th>
                <th>Action</th>
                </thead>
                <tbody>
                <?php $list = R::getAll( $fn['name']($now) ); $total = []; $grand=0; ?>
                <?php foreach ($list as $row): ?>
                    <tr>
                        <td><?php echo $row['postdate'] ?></td>
                        <td><?php echo $row['sales_date'] ?></td>
                        <td><?php echo $row['update_date'] ?></td>
                        <td><a href="../occupancydetails.php?occ=<?php echo $row['occupancy_id'] ?>" target="_blank">
                                <?php echo $row['occupancy_id'] ?></a></td>
                        <td><?php echo $row['dr_code'] ?></td>
                        <td><?php echo $row['cr_code'] ?></td>
                        <td><?php echo ($row['book']) ? 'L' : 'M' ?></td>
                        <td><?php echo $row['tendertype'] ?></td>
                        <td><?php echo $row['occupancy_status'] ?></td>
                        <td><?php echo $row['trxnid'] ?></td>
                        <td><?php echo $row['item'] ?></td>
                        <td><?php echo $row['amount'] ?></td>
                        <td>
                            <a class="updater" href="#updateglcode"
                               data-id="<?php echo $row['id'] ?>"
                               data-dr="<?php echo $row['dr_code'] ?>"
                               data-cr="<?php echo $row['cr_code'] ?>">Update</a>
                        </td>
                    </tr>
                <?php $total[$row['cr_code']] += $row['amount']; $grand += $row['amount']; ?>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <?php foreach ($total as $key => $value): ?>
                    <tr>
                        <th colspan="11">Total <?php echo $key ?>:</th>
                        <th class="subtotals"><?php echo number_format($value, 2) ?></th>
                    </tr>
                    <?php endforeach; ?>
                    <tr>
                        <th class="grand" colspan="11">Grand Total <?php echo $key ?>:</th>
                        <th class="grand subtotals"><?php echo number_format($grand, 2) ?></th>
                    </tr>
                </tfoot>
            </table>
        <?php endforeach; ?>
    </div>
</form>

<div id="updateCode" style="display: none; width: 600px; height: 200px;
    overflow: auto;position:absolute; border: 1px solid gray; top: 20%; left: 20%; background-color: white;">
    <div rel="title">
        Update GL Codes
    </div>
    <div rel="body">
        <div style="padding: 10; font-size: 11px; line-height: 150%;">

            <div class="w2ui-page page-0">
                <div class="w2ui-field">
                    <label>DEBIT:</label>
                    <div>
                        <select id="newdr" name="newdr">
                            <?php foreach ($codes as $c): ?>
                                <option value="<?php echo $c['code']; ?>">
                                    <?php echo $c['code'] . ' - ' . trim($c['name']); ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="w2ui-field">
                    <label>CREDIT:</label>
                    <div>
                        <select id="newcr" name="newcr">
                            <?php foreach ($codes as $c): ?>
                                <option value="<?php echo $c['code']; ?>">
                                    <?php echo $c['code'] . ' - ' . trim($c['name']); ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="w2ui-buttons">
                <button class="btn cancel" type="button" value="Cancel" name="cancel">Cancel</button>
                <button class="btn save" type="button" value="Save" name="save">Save</button>
                <input type="hidden" id="updateId" name="updateId" />
            </div>


        </div>
    </div>
</div>

<script>

    $(function () {
        $('input[type=us-date]').w2field('date',  { format: 'yyyy-mm-dd' });

        $('.updater').on('click', function(e) {
            //e.preventDefault();
            $('#updateCode').show();

            $('#updateId').val($(this).data('id'));

            $('#newdr').val($(this).data('dr')).trigger('change');
            $('#newcr').val($(this).data('cr')).trigger('change');
        });

        $('.btn.cancel').on('click', function(e){
            e.preventDefault();
            $('#updateCode').hide();
        });

        $('.btn.save').on('click', function(e){
            e.preventDefault();
            console.log($('#newdr').val());
            console.log($('#newcr').val());
            console.log($('#updateId').val());

            $.post('sjdfns.php',
                {
                    dr_code: $('#newdr').val(),
                    cr_code: $('#newcr').val(),
                    sjdId: $('#updateId').val()
                },
                function(res){
                    console.log(res);
                    document.location.href="clposting.php?d=<?php echo $now ?>";
                }
            );

        });
    });

</script>
</body>
</html>


