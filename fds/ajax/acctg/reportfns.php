<?php

function getHotel() 
{
	return "Shogun 2";
}


function makeShiftSelect($selected=0){
	$retval="<select name='shiftno' id='shiftno'>";
	$retval.="<option value='0'>All</option>";
	for($x=1; $x <= 3; $x++) {
		 $retval.="<option value='$x' ";
		 $retval.=($x==$selected) ? " selected " : "";
		 $retval.=">$x</option>";
	}
	$retval.="</select>";
	return $retval;
}

function makeSiteSelect($selected=0){
	$arrSite = array('All','Walkup','Hotel');
	$retval="<select name='siteid'>";
	for($x=0; $x < count($arrSite); $x++) {
		 $retval.="<option value='$x' ";
		 $retval.=($x==$selected) ? " selected " : "";
		 $retval.=">$arrSite[$x]</option>";
	}
	$retval.="</select>";
	return $retval;
}

function getSpecialFloorId()
{
	$sql  ="select settings_value from settings where settings_name='SPECIALFLOORID'";
	return R::getCell($sql);
}

function getSpecialRoomlist($floor)
{
	$sql  ="select group_concat(room_id) from rooms where floor_id='$floor'";
	return R::getCell($sql);
}

function isSpecialFloor($occupancy)
{
	$exceptList  = explode(',',getSpecialRoomList(getSpecialFloorId()));
	$sql  = "select room_id from occupancy where occupancy_id='$occupancy'";
	$row = R::getCell($sql);
	return in_array($exceptList,$row) ;
}

function RoomSalesDropDown()
{
	$sql = " select a.sas_id, a.sas_cat_id, b.sas_cat_name, a.sas_description
			from sales_and_services a, sas_category b 
			where  a.sas_cat_id=b.sas_cat_id
			order by b.sas_cat_name, a.sas_description
			";
	$res = R::getAll($sql);
	$ret="<select name='roomsales' id='roomsales'>";
	$ret.="<option value=''></option>";
	foreach ($res as $r) {
		$sas = $r['sas_id'];
		$cat = $r['sas_cat_id'];
		$name = $r['sas_cat_name'];
		$description = $r['sas_description'];
		$value = $sas.'_'.$cat;
		$ret.="<option value='$value'>$name - $description</option>";
	}
	$ret.="</select>";
	return $ret;
}

function FoodSalesDropDown()
{
	$sql = " select a.fnb_id, a.food_category_id, b.food_category_name, a.fnb_name
			from fnb a, food_categories b 
			where  a.food_category_id=b.food_category_id
			order by b.food_category_name, a.fnb_name
			";

	$res = R::getAll($sql);
	$ret="<select name='foodsales' id='foodsales'>";
	$ret.="<option value=''></option>";
	foreach ($res as $r) {
		$sas = $r['fnb_id'];
		$cat = $r['food_category_id'];
		$name = $r['food_category_name'];
		$description = $r['fnb_name'];
		$strvalue = "$name - $description";
		$idvalue = $sas.'_'.$cat;
		$strvalue = (strlen($strvalue) > 50) ? substr($strvalue,0,50) : "$strvalue ...";
		$ret.="<option value='$idvalue'>$strvalue</option>";
	}
	$ret.="</select>";
	return $ret;
}

function getStartEndUsingDate($date) {
	$start = date('Y-m-d H:i:s', strtotime($date) - 60 * 60 * 2);
	$end = date('Y-m-d H:i:s', strtotime($date) + 60 * 60 * 22);

	return [$start, $end];
}