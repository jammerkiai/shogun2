<?php

require_once('SalesCodeInjector.php');

class RechitSalesCode implements SalesCodeInjector
{
	public function getOptions($ci) {
		$out = [
			'key' => 'Unearned_Sales',
			'options' => [
				'source'   => 'room_sales',
				'cr'	   => ['code' => '4001', 'desc' => 'Room Sales'],
				'dr'	   => ['code' => '2015', 'desc' => 'Unearned Income - Sales']
			]
		];

		return $out;
	}
}