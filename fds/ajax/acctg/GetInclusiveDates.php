<?php
/**
* GetInclusiveDates
*/

trait GetInclusiveDates
{
	/**
	* @Type: datetime
	* @Description: beginning of first shift for that date
	*/
	var $startDate;

	/**
	* @Type: datetime 
	* @Description: End of third shift for the date given
	*/ 
	var $endDate;

	public function getStartDate($date) {
		$sql = "SELECT datetime
				FROM `shift-transactions`
				where shift = 'start'
                and datetime < '$date'
				order by datetime desc
				limit 0, 1";
		$this->startDate = R::getCell($sql);
		return $this->startDate;
	}

	public function getEndDate($date) {
		$sql = "SELECT datetime
				FROM `shift-transactions`
				where shift = 'end'
                and date_format(datetime, '%Y-%m-%d') = '$date'
				order by datetime desc
				limit 0, 1";

		$this->endDate = R::getCell($sql);
		return $this->endDate;
	}
}