<?php 
/** for shogun 1 **/

require_once('../config/config.inc.php');



$year = ($_GET['year']) ? $_GET['year'] : date('Y');
$month= ($_GET['month']) ? $_GET['month'] : date('m');
$site= ($_GET['site']) ? $_GET['site'] : 'S2';
$occupancy_id= ($_GET['occupancy_id']) ? $_GET['occupancy_id'] : 0;

$venues = R::getAll('select distinct b.occupancy_id, a.door_name 
from rooms a, room_sales b, occupancy c  
where b.occupancy_id=c.occupancy_id
and a.room_id=c.room_id
and a.floor_id=7');


$start = <<<start
	SELECT datetime FROM `shift-transactions` WHERE shift='start'
	and date_format(datetime, '%Y')='$year'
	and date_format(datetime, '%m')<'$month'
	order by datetime desc limit 0, 1
start;

$end = <<<end
	SELECT datetime FROM `shift-transactions` WHERE shift='end'
	and date_format(datetime, '%Y')='$year'
	and date_format(datetime, '%m')='$month'
	order by datetime desc limit 0, 1
end;



$filter = '';

if ($occupancy_id) {
	$filter = "  occupancy_id=$occupancy_id and ";
}

if ($site=='S2') {
	$final = <<<final

select 
			if(
	     		date_format(d.actual_checkout, '%m')<>'$month',
	     		'$year-$month-01',
	     		date_format(d.actual_checkout, '%Y-%m-%d')
	     	) as date_field,
	     	d.actual_checkout as 'trxndate',
	     	b.sas_description as item, 
	     	a.occupancy_id,
	     	c.door_name,
	     	a.unit_cost*a.qty as amount, 
	     	'checkout' as src, 'S2' as org
	      	from room_sales a, sales_and_services b, rooms c, occupancy d
	      	where $filter 
	      	a.category_id =3 and
			a.item_id=b.sas_id and
			a.occupancy_id=d.occupancy_id and
			d.room_id=c.room_id and
			a.status='Paid' and
	      	d.actual_checkout between 
	      	($start)
	      	and 
	      	($end)
			
union

select 
			if(
	     		date_format(a.rechit_date, '%m')<>'$month',
	     		'$year-$month-01',
	     		date_format(a.rechit_date, '%Y-%m-%d')
	     	) as date_field,
	     	a.rechit_date as 'trxndate',
	     	b.sas_description as item, 
	     	a.occupancy_id,
	     	c.door_name,
	     	a.unit_cost*a.qty as amount, 
	     	'room' as src, 'S2' as org
	      	from room_sales a, sales_and_services b, rooms c, occupancy d
	      	where $filter 
	      	a.category_id =3 and
			a.item_id=b.sas_id and
			a.occupancy_id=d.occupancy_id and
			d.room_id=c.room_id and
			a.status='Paid' and
	      	a.rechit_date between 
	      	($start)
	      	and 
	      	($end)
			
union

select 
			if(
	     		date_format(a.update_date, '%m')<>'$month',
	     		'$year-$month-01',
	     		date_format(a.update_date, '%Y-%m-%d')
	     	) as date_field,
	     	a.update_date as 'trxndate',
	     	b.sas_description as item, 
	     	a.occupancy_id,
	     	c.door_name,
	     	a.unit_cost*a.qty as amount, 
	     	'misc' as src, 'S2' as org
	      	from room_sales a, sales_and_services b, rooms c, occupancy d
	      	where $filter 
	      	a.category_id <> 3 and
			a.item_id=b.sas_id and
			a.occupancy_id=d.occupancy_id and
			d.room_id=c.room_id and
			a.status='Paid' and
	      	a.update_date between 
	      	($start)
	      	and 
	      	($end)
			
union
		
select  
			if(
	     		date_format(c.update_date, '%m')<>'$month',
	     		'$year-$month-01',
	     		date_format(c.update_date, '%Y-%m-%d')
	     	) as date_field,
			c.update_date as 'trxndate',
	     	d.fnb_name as item, 
	     	c.occupancy_id,
	     	e.door_name,
			c.unit_cost * c.qty as amount, 
			'fnb' as src, 'S2' as org
	      	from fnb_sales c, fnb d, rooms e, occupancy f
	      	where $filter 
	      	c.item_id=d.fnb_id and
	      	c.occupancy_id=f.occupancy_id and
	      	f.room_id=e.room_id and
			c.status='Paid' and
	      	c.update_date between
	      	($start)
	      	and 
	      	($end)
	
order by date_field      	


final;

} else {
	$final = <<<final


select 
	     	if(
	     		date_format(update_date, '%m')<>'$month',
	     		'$year-$month-01',
	     		date_format(update_date, '%Y-%m-%d')
	     	) as date_field,
	     	a.update_date as 'trxndate',
	     	b.sas_description as item, 
	     	a.occupancy_id,
	     	c.door_name,
	     	a.unit_cost*a.qty as amount, 
	     	'room' as src, 'S1' as org
	      	from shogunfds1.room_sales a, shogunfds1.sales_and_services b, shogunfds1.rooms c, shogunfds1.occupancy d
	      	where $filter 
			a.item_id=b.sas_id and
			a.occupancy_id=d.occupancy_id and
			d.room_id=c.room_id and
			a.status='Paid' and
	      	a.update_date between 
	      	($start)
	      	and 
	      	($end)
			
union

select  
			 if(
	     		date_format(update_date, '%m')<>'$month',
	     		'$year-$month-01',
	     		date_format(update_date, '%Y-%m-%d')
	     	) as date_field,
	     	c.update_date as 'trxndate',
	     	d.fnb_name as item, 
	     	c.occupancy_id,
	     	e.door_name,
			c.unit_cost * c.qty as amount, 
			'fnb' as src, 'S1' as org
	      	from shogunfds1.fnb_sales c, shogunfds1.fnb d, shogunfds1.rooms e, shogunfds1.occupancy f
	      	where $filter 
	      	c.item_id=d.fnb_id and
			c.occupancy_id=f.occupancy_id and
	      	f.room_id=e.room_id and
			c.status='Paid' and
	      	c.update_date between
	      	($start)
	      	and 
	      	($end)
order by trxndate	      	


final;
}



$days = R::getAll($final, [ ':st'=>$st, ':en'=>$en ]);

$totals = [];
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
	<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Monthly Sales Detail Summary</a>
			</div>
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<form class="navbar-form navbar-right" role="search">
					<div class="form-group">
						<select name="site" id="site" class="form-control">
							<?php foreach ( ['S1', 'S2'] as $si): ?>
								<option value="<?php echo $si ?>"
								<?php if ($si==$site) echo 'selected' ?>
								>
									<?php echo $si ?>
								</option>
							<?php endforeach ?>
						</select>
					</div>
					<div class="form-group">
						<select name="year" id="year" class="form-control">
							<?php for ($x=$year-3; $x <= $year; $x++): ?>
								<option value="<?php echo $x ?>"
								<?php if ($x==$year) echo 'selected' ?>
								>
									<?php echo $x ?>
								</option>
							<?php endfor ?>
						</select>
					</div>
					<div class="form-group">
						<select name="month" id="month" class="form-control">
							<?php for ($y=1; $y <= 12; $y++): ?>
								
								<option value="<?php echo str_pad( $y, 2, 0, STR_PAD_LEFT ) ?>"
								<?php if ($y==$month) echo 'selected' ?>
								>
									<?php echo date( 'F', strtotime("$year-$y-01")) ?>
								</option>

							<?php endfor ?>
						</select>
					</div>
					<div class="form-group">
						<select name="occupancy_id" id="occupancy_id" class="form-control">
							<option value="0">All</option>
							<?php foreach ($venues as $v): ?>
								<option value="<?php echo $v['occupancy_id'] ?>"
								<?php if ($v['occupancy_id']==$occupancy_id) echo 'selected' ?>
								>
									<?php echo $v['door_name'] ?>
								</option>
							<?php endforeach ?>
						</select>
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
				
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>
  	
    <div class="container">
	    <div class="panel panel-default">
	    	<div class="panel-body row">
		    	<div class="table-responsive">
		    		<table class="table table-hover">
						<thead>
							<tr>
								<th>Site</th>
								<th>Source</th>
								<th>Date</th>
								<th>Transaction Date</th>
								<th>Occupancy</th>
								<th>Room</th>
								<th>Item</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($days as $day): ?>
							<tr class="<?php echo $day['org'] . ' ' . $day['src']?>">
								<td><?php echo $day['org']?></td>
								<td><?php echo $day['src']?></td>
								<td><?php echo $day['date_field']?></td>
								<td><?php echo $day['trxndate']?></td>
								<td><?php echo $day['occupancy_id']?></td>
								<td><?php echo $day['door_name']?></td>
								<td><?php echo $day['item']?></td>
								<td><?php echo $day['amount']?></td>
							</tr>
							<?php $total += $day['amount'] ?>
						<?php endforeach ?>
							
						</tbody>
						<tfoot>
							<tr>
								<th colspan="5">Total</th>
								<th><?php echo $total ?></th>
							</tr>
						</tfoot>
					</table>
				</div>

	    	</div>
	    </div>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

  </body>
</html>




