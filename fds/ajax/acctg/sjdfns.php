<?php
require_once('../config/config.inc.php');

if ($_POST['sjdId']) {
    $sql = "update sales_journal_details set dr_code=:dr, cr_code=:cr where id=:id";
    R::exec($sql, [
        ':dr' => $_POST['dr_code'],
        ':cr' => $_POST['cr_code'],
        ':id' => $_POST['sjdId'] ]
    );
    echo json_encode($_POST);
    echo json_encode(['success' => true]);
}