<?php
class report extends baseobject
{
	public function __construct($params=array())
	{
		$this->aggregates=array();
		parent::init($params);
	}
	
	public function buildHeader() 
	{
		$header  = '<h1>' . $this->title . '</h1>';
		$header .= '<h2>' . $this->subtitle . '</h2>';
		$this->header=$header;
	}
	
	public function buildReport()
	{
		$this->buildHeader();
		$this->buildBody();
		$this->output = $this->header . $this->body . $this->footer;
	}
	
	public function buildBody()
	{
		$grandTotals = array();
		$res = R::getAll($this->sql);
		$fieldnames = array_keys($res[0]);
		$numfields = count($fieldnames);
		$numrows = count($res);
		$body = "<div class='recordcount'>Found: $numrows records</div>" ;
		$body .= '<table border="0" class="report">';
		$body .= '<tr>';
		for($x=0; $x < $numfields; $x++) {
			$body .= '<th>&nbsp;' . $fieldnames[$x] . '</th>';
		}
		$body .= '</tr>';
		foreach ($res as $row) {
			$body .= '<tr>';
			for($x=0; $x < $numfields; $x++) {
				$fieldname = $fieldnames[$x];
				$body .= '<td>&nbsp;' . $row[$fieldname] ;
				if( in_array($fieldname, $this->aggregates) ) {
					$this->aggregate[$fieldname]+=(float) $row[$fieldname];
					$grandTotals[$x] += (float) $row[$fieldname];
				}
				$body.= '</td>';
			}
			$body.= '</tr>';
		}
		
		$body .= '<tr class="aggregates">';
		for($x=0; $x < $numfields; $x++) {
			$body .= '<th>&nbsp;' ;
			if( isset($grandTotals[$x]) ) $body.=number_format($grandTotals[$x],2);
			$body .= '</th>';
		}
		$body.='</tr>';
		$body .= '</table>';
		$this->body=$body;
	}
	
	public function show()
	{
		echo $this->output;
	}
	
	public function html()
	{
		return $this->output;
	}
}