<?php
/** DataNormalizer.php **/

class DataNormalizer
{

	public function normalizeData($ciList, SalesCodeInjector $sci) {
		$out = [];
		foreach ($ciList as $ci) {
			$occ = $ci['occupancy_id'];
			$out[$occ]['actual_checkin'] = $ci['actual_checkin']; 
			$out[$occ]['actual_checkout'] = $ci['actual_checkout'];
			$out[$occ]['expected_checkout'] = $ci['expected_checkout'];
			$out[$occ]['door_name'] = $ci['door_name'];
			$out[$occ]['regflag'] = $ci['regflag'];

			$out[$occ]['sales'][] = [
				"transaction_id" => $ci['transaction_id'],
				"sales_date" => $ci['sales_date'],
				"category_id" => $ci['category_id'],
				"item_id" => $ci['item_id'],
				"unit_cost" => $ci['unit_cost'],
				"qty" => $ci['qty'],
				"status" => $ci['status'],
				"remarks" => $ci['remarks'],
				"room_id" => $ci['room_id'],
				"regdate" => $ci['regdate'],
				"regshift" => $ci['regshift'],
				"tendertype" => $ci['tendertype'],
				"rechit_date" => $ci['rechit_date'],
				"paid_date" => $ci['update_date'],
				"code" => $ci['code'],
				"occupancy_id" => $ci['occupancy_id'],
			];
			
			$out[$occ]['total_amount'] += $ci['unit_cost'] * $ci['qty'];

			$salesCode = $sci->getOptions($ci);
			$out[$occ][$salesCode['key']][] = $this->addLedgerEntry($ci, $salesCode['options']);

		} 

		return $out;
	}

	public function addLedgerEntry($ci, $options) {
		return [
			'refid'    => $ci['roomsales_id'],
			'postdate' => $ci['update_date'],
			'item'     => $ci['item'],
			'amount'   => $ci['unit_cost'] * $ci['qty']
		] + $options;

	}	
}