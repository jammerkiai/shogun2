<?php 
/** slview_listing.php **/
?>
<div>
	<table class="occupancy <?php echo $data['classname'] ?> hidden">
		<caption><?php echo $data['caption'] ?></caption>
		<thead>
			<tr>
			<th>Room</th>
			<th>Actual Check In</th>
			<th>Expected Check Out</th>
			<th>Actual Check Out</th>
			<th>Reg</th>
			<th>Total Amount</th>
			</tr>
		</thead>
		<tbody>
		<?php $grandTotal = 0; foreach($data['list'] as $occ_id => $occ): ?>
			<tr class="occline">
				<td><a href="../occupancydetails.php?occ=<?php echo $occ_id ?>"><?php echo $occ['door_name'] ?></a></td>
				<td><?php echo $occ['actual_checkin']?></td>
				<td><?php echo $occ['expected_checkout']?></td>
				<td><?php echo $occ['actual_checkout']?></td>
				<td><?php echo $occ['regflag']?></td>
				<td class="numeric"><?php echo number_format($occ['total_amount'],2) ?></td>
			</tr>
			<tr class="roomsalesdetails">
				<td>&nbsp;</td>
				<td colspan="5">
					<table class="roomsales">
					<thead>
						<tr><th colspan="13">Room Sales</th></tr>
						<tr>
						<th>Id</th>
						<th>Sales</th>
						<th>CatId</th>
						<th>ItemId</th>
						<th>Cost</th>
						<th>Qty</th>
						<th>Status</th>
						<th>Remarks</th>
						<th>RegDate</th>
						<th>RegShift</th>
						<th>TenderType</th>
						<th>Rechit</th>
						<th>Paid</th>
						</tr>
					</thead>
					<?php foreach ($occ['sales'] as $rs): ?>
						<tr>				
							<td><?php echo $rs['transaction_id'] ?></td>
							<td><?php echo $rs['sales_date'] ?></td>
							<td><?php echo $rs['category_id'] ?></td>
							<td><?php echo $rs['item_id'] ?></td>
							<td><?php echo $rs['unit_cost'] ?></td>
							<td><?php echo $rs['qty'] ?></td>
							<td><?php echo $rs['status'] ?></td>
							<td><?php echo $rs['remarks'] ?></td>
							<td><?php echo $rs['regdate'] ?></td>
							<td><?php echo $rs['regshift'] ?></td>
							<td><?php echo $rs['tendertype'] ?></td>
							<td><?php echo $rs['rechit_date'] ?></td>
							<td><?php echo $rs['paid_date'] ?></td>
						</tr>
					<?php endforeach; ?>
					</table>
				</td>
			</tr>
			<?php foreach ($data['code_pairs'] as $key): ?>

				<?php if (isset($occ[$key])): ?>
					<tr class="slpostdetails">
						<td>&nbsp;</td>
						<td colspan="5">
							<table class="slposts">
							<thead>
								<tr><th colspan="13"><?php echo $key ?></th></tr>
							</thead>
							<tbody>
							<?php foreach ($occ[$key] as $sl): ?>
								<tr>
									<td style="width:20%"><?php echo date('Y-m-d', strtotime($sl['postdate'])) ?> - <?php echo $sl['item'] ?></td>
									<td colspan=4></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td  style="width:20%"><?php echo implode(' ', $sl['dr']) ?></td>
									<td>&nbsp;</td>
									<td style="text-align:right"><?php echo number_format($sl['amount'],2) ?></td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td  style="width:20%"><?php echo implode(' ', $sl['cr']) ?></td>
									<td>&nbsp;</td>
									<td style="text-align:right"><?php echo number_format($sl['amount'],2) ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
							</table>
						</td>
					</tr>
				<?php endif; ?>
			<?php endforeach; ?>
			<?php $grandTotal += $occ['total_amount']; $dayTotal += $occ['total_amount'];?>
		<?php endforeach; ?>
		<tr class="grandtotal">
			<td colspan="5">Room Sales Total: </td>
			<td class="numeric"><?php echo number_format($grandTotal, 2) ?></td>
		</tr>
		</tbody>
	</table>

</div>