<?php
/**
 * @file: clposting.php
 * views/posts cl transactions
 */

session_start();
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');
require_once('salesforposting.php');

$date = isset($_POST['newdate']) ? $_POST['newdate'] : ( isset($_GET['d']) ? $_GET['d'] : date('Y-m-d'));
list($start, $end) = getStartEndUsingDate($date);

$sql = "
        (SELECT 
          (a.unit_cost * a.qty) as amount, 
          e.fnb_name as item,
          a.remarks,  
          a.regflag as book,
          a.tendertype,
          a.occupancy_id,
          a.item_id,
          a.fnbsales_id as trxnid,
          a.sales_date,
          a.update_date,
          a.status
          FROM fnb_sales a, occupancy b, fnb e
          WHERE a.update_date BETWEEN '$start' and '$end' 
          and a.occupancy_id=b.occupancy_id
          and a.item_id = e.fnb_id
          and b.occupancy_id = 1427
          order by a.occupancy_id)
        
        UNION
        
        (SELECT 
          (d.unit_cost * d.qty) as amount,
          c.sas_description as item,
          d.remarks,
          d.regflag as book,
          d.tendertype,
          d.occupancy_id,
          d.item_id,
          d.roomsales_id as trxnid,
          d.sales_date,
          d.update_date,
          d.status
          FROM room_sales d, occupancy b, sales_and_services c
          WHERE d.update_date BETWEEN '$start' and '$end'
          and d.occupancy_id=b.occupancy_id
          and b.occupancy_id = 1427
          and d.item_id=c.sas_id
          order by c.code)
    ";

$all = R::getAll($sql);

?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="./reports.css">
    <link rel="stylesheet" type="text/css" href="w2ui-1.4.3.min.css" />
    <script src="../../js/jquery.js"></script>
    <script type="text/javascript" src="w2ui-1.4.3.min.js"></script>
</head>
<body>
<form method='post'>
    <h5>Sales For Posting</h5>
    <div class="menubar">
        Select Date:
        <input type="us-date" id="newdate" name="newdate" value="<?php echo $date ?>" />
        <input type='submit' name='cmd' value='go' />
    </div>

    <div>
        <h5>Details</h5>
        <table class="report">
            <thead>
            <th>Sales Date</th>
            <th>Update Date</th>
            <th>Occupancy</th>
            <th>Transaction ID</th>
            <th>Item</th>
            <th>Remarks</th>
            <th>Status</th>
            <th>Amount</th>
            </thead>
            <tbody>
            <?php foreach ($all as $row): ?>
                <tr>
                    <td><?php echo $row['sales_date'] ?></td>
                    <td><?php echo $row['update_date'] ?></td>

                    <td><?php echo $row['occupancy_id'] ?></td>
                    <td><?php echo $row['trxnid'] ?></td>
                    <td><?php echo $row['item'] ?></td>
                    <td><?php echo $row['remarks'] ?></td>
                    <td><?php echo $row['status'] ?></td>
                    <td><?php echo $row['amount'] ?></td>
                </tr>
                <?php $total += $row['amount']; ?>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
                <th class="grand" colspan="7">Total: </th>
                <th class="grand"><?php echo number_format($total, 2) ?></th>
            </tr>
            </tfoot>
        </table>
    </div>

</form>
<script>

    $(function () {
        $('input[type=us-date]').w2field('date',  { format: 'yyyy-mm-dd' });
    });
</script>
</body>
</html>


