<?php
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');

/******************************/
// edit room count here
$WPROOMS = 69;
$HSROOMS = 116;
$TOTALROOMS = $WPROOMS + $HSROOMS;
/******************************/

$title = 'Monthly Sales Summary';
$thismonth = isset($_POST['thismonth']) ? $_POST['thismonth'] : date('m');
$thisyear = isset($_POST['thisyear']) ? $_POST['thisyear'] : date('Y');

if($_POST["submit"]=="export to excel") {
	$table="<h3 style='font-size:12px;font-family:arial,helvetica'>$title - $thismonth/$thisyear</h3>";
	$table.="<table border='0' cellspacing='2' cellpadding='2' style='font-size:11px;font-family:arial,helvetica'>";
	$table.=getHeaders();
	$table.=getData($thisyear,$thismonth);
	$table.="</table>";
	$excel_file_name="mss_$thisyear_$thismonth.xls";
	header("Content-type: application/octet-stream");//A MIME attachment with the content type "application/octet-stream" is a binary file.
	header("Content-Disposition: attachment; filename=$excel_file_name");//with this extension of file name you tell what kind of file it is.
	header("Pragma: no-cache");//Prevent Caching
	header("Expires: 0");//Expires and 0 mean that the browser will not cache the page on your hard drive
	echo $table;
	exit;
}


//functions
function getHeaders() {
	$retval="<tr>
<th rowspan=2 colspan=2>Day</th>
<th colspan=9>Guests</th>
<th colspan=3>Room</th>
<!--th rowspan=2 class='grand'>Room<br />Sales</th-->
<th colspan=3>Food</th>
<th colspan=3>Beer</th>
<th rowspan=2 class='grand'>Kitchen<br />Sales</th>

<th colspan=3>Misc</th>

<th colspan=8 class='grand'>Grand Totals</th>

<th rowspan=2 class='grand'>DAILY</th>
<th rowspan=2 class='grand'>AVGCUM</th>
</tr>
<tr>
<th>WP</th>
<!--th>S1</th-->
<th>S2</th>
<th>Total</th>
<th>Daily TOR</th>
<th>T CUM TOR</th>
<th>TOR WU</th>
<th>TOR HS1 & HS2</th>
<th>CUM TOR WU</th>
<th>CUM TOR HS1 & HS2</th>
<th>WP</th>
<!--th>S1</th-->
<th>S2</th>
<th>Total</th>

<th>WP</th>
<!--th>S1</th-->
<th>S2</th>
<th>Total</th>

<th>WP</th>
<!--th>S1</th-->
<th>S2</th>
<th>Total</th>

<th>WP</th>
<!--th>S1</th-->
<th>S2</th>
<th>Total</th>

<th>WP</th>
<th>S2</th>
<th>Lobby - S1</th>

<th>Lobby - S2</th>
<th>Banquet</th>
<th>Garden</th>
<th>Resto</th>
<th>No Show /<br>Cancel</th>
<th>GT</th>


</tr>";
return $retval;
}

function getData($thisyear,$thismonth) {
	global $WPROOMS, $HSROOMS;
	$TOTALROOMS = $WPROOMS + $HSROOMS;
	$cumulative = '';
	$avg = '';
	for($x=1; $x < 32; $x++) {
		if(strlen($x)==1) $x = '0'.$x;
		$date = $thisyear.'-'.$thismonth.'-'.$x;
		$day = date('D',strtotime($date));
		$checkmonth = date('m',strtotime($date));
		if($checkmonth != $thismonth) break;
		$class='';
		if($day=='Sun' or $day=='Sat') $class='class="weekend"';
		$rows .= "<tr $class>";
		$rows .= "<td>$x</td>";
		$rows .= "<td>" . $day ."</td>";
		
		//guests
		$wp = getSales($date,'guests',1,1);
		$totals['wp'] += $wp;
		$hs1 = getSales($date,'guests',2,1);
		$totals['hs1'] += $hs1;
		$hs2 = getSales($date,'guests',2,2);
		$totals['hs2'] += $hs2;
		$totalguests = getSales($date,'guests');
		$totals['totalguests'] += $totalguests;
		
		$rows .= "<td>" . number_format($wp,0) . "</td>";
		//$rows .= "<td>" . $hs1 . "</td>";
		$hs = $hs2 + $hs1;
		$rows .= "<td>" . number_format($hs,0) . "</td>";
		$rows .= "<td>" . $totalguests . "</td>";
		
		//Daily TOR = $totalguests/178;
		if($totalguests!='') {
			$daily_tor = $totalguests/$TOTALROOMS;
			$daily_tor = ($daily_tor) ? number_format($daily_tor, 2) : '';
		} else {
			$daily_tor = '';
		}
		
		//T CUM TOR = SUM(N^Total)/163/N where N=number of days
		if($totalguests!='') {
			$t_cum_tor = $totals['totalguests']/$TOTALROOMS/$x;
			//$t_cum_tor = $totalguests/178/$x;
			$t_cum_tor = ($t_cum_tor) ? number_format($t_cum_tor, 2) : '';
		} else {
			$t_cum_tor = '';
		}
		
		//TOR WU = WU/69
		$tor_wu = $wp/$WPROOMS;
		$tor_wu = ($tor_wu) ? number_format($tor_wu,2) : '';
		
		
		//TOR HS1 & HS2 = SUM(HS1 + HS2)/109
		$tor_hs1_hs2 = ($hs1+$hs2)/$HSROOMS;
		$tor_hs1_hs2 = ($tor_hs1_hs2) ? number_format($tor_hs1_hs2, 2) : '';
		
		//CUM TOR WU = SUM(N^WU)/69/N where N=number of days
		if($tor_wu!='') {
			$cum_tor_wu = $totals['wp']/$WPROOMS/$x;
			$cum_tor_wu = ($cum_tor_wu) ? number_format($cum_tor_wu, 2) : '';
		} else {
			$cum_tor_wu = '';
		}
		
		//CUM TOR HS1 & HS2 = SUM(N^HS1 + N^HS2)/109/N
		if($tor_hs1_hs2=='') {
			$cum_tor_hs1_hs2 = '';
		} else {
			$cum_tor_hs1_hs2 = ($totals['hs1'] + $totals['hs2'])/$HSROOMS/$x;
			$cum_tor_hs1_hs2 = ($cum_tor_hs1_hs2) ? number_format($cum_tor_hs1_hs2, 2) : '';
		}
		
		$rows .= "<td>" . $daily_tor . "</td>";
		$rows .= "<td>" . $t_cum_tor. "</td>";
		$rows .= "<td>" . $tor_wu . "</td>";
		$rows .= "<td>" . $tor_hs1_hs2 . "</td>";
		$rows .= "<td>" . $cum_tor_wu . "</td>";
		$rows .= "<td>" . $cum_tor_hs1_hs2 . "</td>";
		//rooms + ot
		
		$rooms_wp = getSales($date,'roomsales + overtime',1,1);
		$totals['rooms_wp'] += $rooms_wp;
		$rooms_hs1 = getSales($date,'roomsales + overtime',2,1);
		$totals['rooms_hs1'] += $rooms_hs1;
		$rooms_hs2 = getSales($date,'roomsales + overtime',2,2);
		$totals['rooms_hs2'] += $rooms_hs2 + $rooms_hs1;
		$rooms_total = getSales($date,'roomsales + overtime');
		$totals['rooms_total'] += $rooms_total;
		
		$rows .= "<td>" . number_format($rooms_wp, 2) . "</td>";
		//$rows .= "<td>" .$rooms_hs1. "</td>";
		$rooms_hs = $rooms_hs1 + $rooms_hs2;
		$rows .= "<td>" . number_format($rooms_hs,2) . "</td>";
		$rows .= "<td>" .number_format($rooms_total,2) . "</td>";
		
		list($startDate, $endDate) = getExactRangeForDate($thisyear,$thismonth,$x);
		
		$inhouse = getInHousePerDay($startDate, $endDate);
		$banquet = getBanquetPerDay($startDate, $endDate);
		$garden = getGardenPerDay($startDate, $endDate);
		$resto = getRestoPerDay($startDate, $endDate);
		$noshow = getNoShowPerDay($startDate, $endDate);
		$lobby1food = getLobbyFood($startDate, $endDate, 1);
		$lobby2food = getLobbyFood($startDate, $endDate, 2);

		//food
		$food_wp = getSales($date,'food',1,1);
		$totals['food_wp'] += $food_wp - $lobby1food;
		$food_hs1 = getSales($date,'food',2,1);
		$totals['food_hs1'] += $food_hs1;
		$food_hs2 = getSales($date,'food',2,2) - $banquet - $garden - $resto;
		$totals['food_hs2'] += $food_hs2 + $food_hs1 - $lobby2food;
		$food_total = getSales($date,'food') - $banquet - $garden - $resto - $lobby1food - $lobby2food;
		$totals['food_total'] += $food_total;
		
		$rows .= "<td>" . number_format($food_wp,2 ). "</td>";
		//$rows .= "<td>" .$food_hs1. "</td>";
		$food_hs = $food_hs1 + $food_hs2 - $lobby1food - $lobby2food;
		$rows .= "<td>" . number_format($food_hs,2)  . "</td>";
		$rows .= "<td>" . number_format($food_total,2) . "</td>";
		
		
		//beer
		$beer_wp = getSales($date,'beer',1,1);
		$totals['beer_wp'] += $beer_wp;
		$beer_hs1 = getSales($date,'beer',2,1);
		$totals['beer_hs1'] += $beer_hs1;
		$beer_hs2 = getSales($date,'beer',2,2);
		$totals['beer_hs2'] += $beer_hs2 + $beer_hs1;
		$beer_total = getSales($date,'beer');
		$totals['beer_total'] += $beer_total;
		
		$rows .= "<td>" . number_format($beer_wp, 2) . "</td>";
		//$rows .= "<td>" .$beer_hs1. "</td>";
		$beer_hs = $beer_hs1 + $beer_hs2;
		$rows .= "<td>" . number_format($beer_hs, 2)  . "</td>";
		$rows .= "<td>" . number_format($beer_total, 2)  . "</td>";
		
		$kitchen = getTotalSales($date,' food + beer ')  - $banquet;
		$totals['kitchen']+=$kitchen;
		$rows .= "<th class='grand'>" . number_format($kitchen, 2) . "</td>";
		
		
		$misc_wp = getSales($date,'misc',1);
		// $misc_wp = getS1MiscPerDay($startDate, $endDate);

		$totals['misc_wp'] += $misc_wp;
		// $misc_hs1 = getSales($date,'misc',2,1);
		// $totals['misc_hs1'] += $misc_hs1;	
		$misc_hs = getSales($date,'misc',2);
		
		// $misc_hs = $misc_hs2 + $misc_hs1 - $noshow;
		$totals['misc_hs'] += $misc_hs;
		$misc_total = getSales($date,'misc');
		
		//deduct $noshow from misc, then add to global totals
		$misc_total = $misc_total - $noshow;
		$totals['misc_total'] += $misc_total;
		
		$rows .= "<td>" . number_format($misc_wp, 2)  . "</td>";
		$rows .= "<td>" . number_format($misc_hs, 2)  . "</td>";
		$rows .= "<td>" . number_format($misc_total, 2)  . "</td>";
		
		//rooms
		$grand_wp = getTotalSales($date,'roomsales + overtime + food + beer + misc',1,1) - $lobby1food ;
		//$grand_wp -= $banquet;
		$totals['grand_wp'] += $grand_wp;
		$grand_hs1 = getTotalSales($date,'roomsales + overtime + food + beer + misc',2,1);

		$totals['lobby'] += $grand_hs1;
		$grand_hs2 = getTotalSales($date,'roomsales + overtime + food + beer + misc',2,2)  - $lobby2food;
		
		$s2total = $grand_hs2 - $inhouse - $banquet - $garden - $resto - $noshow ;
		$totals['grand_hs2'] += $s2total;
		
		$rows .= "<td>" . number_format($grand_wp, 2)  . "</td>";
		$rows .= "<td>" . number_format($s2total, 2)  . "</td>";

		

		// $rows .= "<td>" . number_format($grand_hs1, 2)  . "</td>";
		
		// $totals['inhouse']+=$inhouse;
		// $rows .= "<td>" . number_format($inhouse, 2)  . "</td>";

		$totals['lobby1food'] += $lobby1food;
		$rows .= "<td>" . number_format($lobby1food, 2)  . "</td>";
		
		$totals['inhouse']+=$inhouse;
		$totals['lobby2food'] += $lobby2food;
		$rows .= "<td>" . number_format($lobby2food, 2)  . "</td>";

		
		$totals['banquet']+=$banquet;
		$rows .= "<td>" . number_format($banquet, 2) . "</td>";
		$total = getTotalSales($date,'roomsales + overtime + food + beer + misc') - $lobby1food - $lobby2food;
		//$total -= $banquet;
		$totals['garden']+=$garden;
		$rows .= "<td>" . number_format($garden, 2)  . "</td>";
		
		//$total -= $garden;
		$totals['resto']+=$resto;
		$rows .= "<td>" . number_format($resto, 2) . "</td>";
		
		//$total -= $garden;
		$totals['noshow']+=$noshow;
		$rows .= "<td>" . number_format($noshow, 2) . "</td>";
		
		//$total -= $resto;
		$totals['grand_total']+=$total;
		$rows .= "<th class='grand'>" .number_format($total, 2) . "</th>";
		
		
		if($total) {
			$prev = $avg; 
			$cumulative += $total;
			$avg = ceil($cumulative/$x );
			$rows .= "<th class='grand'>" .number_format($avg,2). "</td>";
			$rows .= "<th class='grand'>" .number_format($cumulative,2). "</td>";
			
		}else{
			$rows .= "<td></td>";
			$rows .= "<td></td>";
		}
		
		$rows .= '</tr>';
	}
	saveTotalsForYearlySummary($totals, $thisyear,$thismonth);
	$rows .= getFooter($totals);
	return $rows;
}

function saveTotalsForYearlySummary($totals, $year, $month) {
	if (strlen($month) === 1) $month = '0'.$month;
	$np = R::find("yearlysalessummary", "postmonth=:month and postyear=:year",
		[':month' => $month , ':year' => $year]);

	if (empty($np)) {
		$np = R::dispense('yearlysalessummary');
	}

	$np->postdate = $year . $month;
	$np->postmonth = date('F', strtotime("$year-$month-01"));
	$np->postyear = $year;
	foreach ($totals as $key => $value) {
		$np->{$key} = $value;
	}

	R::store($np);
}

function getFooter($totals) {
	$ftr = "<tr><th colspan='2'>Totals</th>";
	$ftr.= "<th>". $totals['wp']."</th>";
	//$ftr.= "<th>". $totals['hs1']."</th>";
	$ftr.= "<th>". $totals['hs2']."</th>";
	$ftr.= "<th>". $totals['totalguests']."</th>";
	for($x=0; $x < 6; $x++) {
		$ftr .= "<th>&nbsp;</th>";
	}
	$ftr.= "<th>". number_format($totals['rooms_wp'], 2) ."</th>";
	//$ftr.= "<th>". $totals['rooms_hs1']."</th>";
	$ftr.= "<th>". number_format($totals['rooms_hs2'], 2) ."</th>";
	$ftr.= "<th>". number_format($totals['rooms_total'], 2) ."</th>";
	
	$ftr.= "<th>". number_format($totals['food_wp'], 2) ."</th>";
	//$ftr.= "<th>". $totals['food_hs1']."</th>";
	$ftr.= "<th>". number_format($totals['food_hs2'], 2) ."</th>";
	$ftr.= "<th>". number_format($totals['food_total'], 2) ."</th>";
	
	$ftr.= "<th>". number_format($totals['beer_wp'], 2) ."</th>";
	//$ftr.= "<th>". $totals['beer_hs1']."</th>";
	$ftr.= "<th>". number_format($totals['beer_hs2'], 2) ."</th>";
	$ftr.= "<th>". number_format($totals['beer_total'], 2) ."</th>";
	
	$ftr.= "<th>". number_format($totals['kitchen'], 2) ."</th>";
	$ftr.= "<th>". number_format($totals['misc_wp'], 2) ."</th>";
	//$ftr.= "<th>". $totals['misc_hs1']."</th>";
	$ftr.= "<th>". number_format($totals['misc_hs'], 2) ."</th>";
	$ftr.= "<th>". number_format($totals['misc_total'], 2) ."</th>";
	
	$ftr.= "<th>". number_format($totals['grand_wp'], 2) ."</th>";

	$ftr.= "<th>". number_format($totals['grand_hs2'], 2) ."</th>";
	$ftr.= "<th>". number_format($totals['lobby1food'], 2) ."</th>";
		
	$ftr.= "<th>". number_format($totals['lobby2food'], 2) ."</th>";
	$ftr.= "<th>". number_format($totals['banquet'], 2) ."</th>";
	$ftr.= "<th>". number_format($totals['garden'], 2) ."</th>";
	$ftr.= "<th>". number_format($totals['resto'], 2) ."</th>";
	$ftr.= "<th>". number_format($totals['noshow'], 2) ."</th>";
	$ftr.= "<th>". number_format($totals['grand_total'], 2) ."</th>";
	$ftr.= "<th></th>";
	$ftr.="</tr>";
	return $ftr;
}

function getMonth($selected) {
	$ret = "<select name='thismonth' id='thismonth'>";
	for($x=1; $x <=12; $x++) {
		$month = date('F', strtotime('2010-' . $x . '-01'));
		$ret.="<option value='$x' ";
		if($selected==$x) $ret.=" selected ";
		$ret.=">$month</option>";
	}
	$ret.="</select>";
	return $ret;
}

function getYear($selected) {
	$ret = "<select name='thisyear' id='thisyear'>";
	for($x=2010; $x <=2019; $x++) {
		$ret.="<option value='$x' ";
		if($selected==$x) $ret.=" selected ";
		$ret.=">$x</option>";
	}
	$ret.="</select>";
	return $ret;
}

function getSales($date,$category,$type=null,$site=null) {
	$sql  = " select sum($category) from salessummary 
			  where salesdate='$date'  ";
	if( !is_null($site) ) $sql.=" and site='$site' ";

	if( !is_null($type) ) $sql.=" and type='$type' ";

	return R::getCell($sql);
}

function getBanquetPerDay($startDate, $endDate) {
	$sql = "select sum(unit_cost*qty) as totalcost from fnb_sales 
	where item_id=115 and update_date between '$startDate' and '$endDate'";
	return R::getCell($sql);
}
function getGardenPerDay($startDate, $endDate) {
	$sql = "select sum(totalcost) 
			from ( 
				select sum(unit_cost * qty) as totalcost 
				from room_sales where status='Paid' and occupancy_id=24479 
				and update_date between '$startDate' and '$endDate' 
				union 
				select sum( unit_cost * qty) as totalcost 
				from fnb_sales where status='Paid' and occupancy_id=24479 
				and update_date between '$startDate' and '$endDate' ) combinedTable";

	return R::getCell($sql);
}
function getRestoPerDay($startDate, $endDate) {
	$sql = "select sum(unit_cost*qty) as totalcost from fnb_sales 
	where  status='Paid' and occupancy_id=24481 and update_date between '$startDate' and '$endDate'";
	return R::getCell($sql);
}
function getInHousePerDay($startDate, $endDate) {
	$sql = "select sum(unit_cost*qty) as totalcost from fnb_sales 
	where  status='Paid' and occupancy_id=1427 and update_date between '$startDate' and '$endDate'";
	return R::getCell($sql);
}

function getLobbyFood($startDate, $endDate, $site) {
	if ($site == 1) {
		$sql = "select sum(unit_cost*qty) as totalcost from shogunfds1.fnb_sales 
				where  status='Paid' and occupancy_id in (3622, 22844) and update_date between '$startDate' and '$endDate'";
	} elseif ($site==2) {	
		$sql = "select sum(unit_cost*qty) as totalcost from fnb_sales 
				where  status='Paid' and occupancy_id in (500, 1427) and update_date between '$startDate' and '$endDate'";
	}
	
	return R::getCell($sql);
}


function getNoShowPerDay($startDate, $endDate) {
	//$sql = "select sum(unit_cost*qty) as totalcost from room_sales 
	//where status='Paid' and occupancy_id='37346' and update_date between '$startDate' and '$endDate'";
	$sql = "select sum(unit_cost*qty) as totalcost from room_sales 
	where status='Paid' and category_id='5' and sales_date between '$startDate' and '$endDate'";

	return R::getCell($sql);
}

function getS1MiscPerDay($startDate, $endDate) {
	//$sql = "select sum(unit_cost*qty) as totalcost from room_sales 
	//where status='Paid' and occupancy_id='37346' and update_date between '$startDate' and '$endDate'";
	$sql = "select sum(unit_cost*qty) as totalcost from shogunfds1.room_sales 
	where status='Paid' and category_id not in (3, 5) and sales_date between '$startDate' and '$endDate'";

	return R::getCell($sql);
}

function getExactRangeForDate($yr, $mo, $dy) {
    $yrmo = $yr . '-' . $mo;
    $dayminus = $dy - 1;
    $dayplus = $dy + 1;
    $today = $yrmo . '-' . $dy;
    $yesterday = date('Y-m-d', strtotime($yrmo . '-' . $dayminus));
    $tomorrow = date('Y-m-d', strtotime($yrmo . '-' . $dayplus));
    $sql = "SELECT datetime
            FROM `shift-transactions`
            WHERE 
            (
            ( date( datetime ) = '$yesterday' and hour(datetime)=21 ) 
            or
            ( date( datetime ) = '$yesterday' and hour(datetime)=22 ) 
            )
            and shift = 'end'";

    $start = $yesterday . ' 22:00:00';
	$stemp =R::getCell($sql);
	if ($stemp) {
		$start = $stemp;
	}
    
    $sql = "SELECT datetime
            FROM `shift-transactions`
            WHERE 
            (
            ( date( datetime ) = '$today' and hour(datetime)=21 ) 
            or
            ( date( datetime ) = '$today' and hour(datetime)=22 ) 
            )
            and shift = 'end'";
    
    $end = $today . ' 21:59:59';
	$tend = R::getCell($sql);
	if ($tend) {
		$end = $tend;
	}

//echo "$yr/$mo/$dy : $start ++ $end<br>";
    return array($start, $end);
}

function getTotalSales($date,$fields,$type=null,$site=null) {
	$sql  = " select sum($fields) from salessummary 
			  where salesdate='$date'  ";
	if(!is_null($type) && !is_null($site)) $sql.=" and type='$type' and site='$site' ";
	return R::getCell($sql);
}
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./reports.css">
<link rel="stylesheet" type="text/css" href="../../css/start/jquery-ui.css">
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>
<style>
table {
	border-collapse:collapse;
}
table th,td {
	padding:4px;
	text-align:center;
}

th.grand, td.grand {
	background-color:#eeffcc;
}

tr.weekend {
	background-color:#ffeecc;
}
</style>
</head>
<body>
<form method='post'>
<div>
Select Month: <?php echo getmonth($thismonth) . getyear($thisyear) ?>
<input type="submit" name="submit" value="go" />
<input type="submit" name="submit" value="export to excel" />
</div>
<div id="workpanel">
<div>Total Rooms: <?php echo $TOTALROOMS ?></div>
<table border='1'>
<?php echo getHeaders() ?>
<?php

echo getData($thisyear,$thismonth);
?>
</table>
</div>
</form>
<script>
$(document).ready(function(){
	$("#newdate").datepicker({dateFormat:'yy-mm-dd'});
});
</script>
</body>
</html>

