<?php
class shift extends baseobject
{
	public function __construct($params)
	{
		parent::init($params);
		if(!isset($params['shiftno'])) {
			$h = date("H");
			if($h>=14&&$h<=21)
			{
				$this->shiftno=3;
			}
			elseif($h>=6&&$h<=13)
			{
				$this->shiftno=2;
			}else{
				$this->shiftno=1;
			}
		}
	}

	public function getShiftDuration()
	{
		$sql = "select shift_start, shift_end from shifts where shift_id=".$this->shiftno;
		$res = R::getRow($sql);
		$this->hrStart = $res['shift_start'];
		$this->hrEnd = $res['shift_end'];
		$this->shiftStart = $this->date . ' ' . $this->hrStart . ':01:00';
		$this->shiftEnd = date('Y-m-d H:i:s', strtotime($this->date . ' ' . $this->hrEnd . ':01:00'));
	}

	public function getShiftDuration_standard()
	{
		$sql = "select shift_start, shift_end from shifts where shift_id=".$this->shiftno;
		$res = R::getRow($sql);
		$this->hrStart = $res['shift_start'];
		$this->hrEnd = $res['shift_end'];
		list($year,$month,$day) = explode('-',$this->date);
		$startmax = date('Y-m-d H:i:s', strtotime($this->date . ' ' . $this->hrStart . ':05:00'));
		$sql = "select `datetime` from `shift-transactions`
				where shift='end'
				and year(`datetime`)='$year'
				and month(`datetime`)='$month'
				and day(`datetime`)='$day'
				and `datetime` < '$startmax'
				order by `datetime` desc
				limit 1";

		$this->shiftStart = R::getCell($sql);

		if(is_null($this->shiftStart)) {
			$this->shiftStart=date('Y-m-d H:i:s', strtotime($this->date . ' ' . $this->hrStart . ':00:01'));
		}

		$endmax =  date('Y-m-d H:i:s', strtotime($this->date . ' ' . $this->hrEnd . ':05:00'));
		$sql = "select `datetime` from `shift-transactions`
				where shift='end'
				and year(`datetime`)='$year'
				and month(`datetime`)='$month'
				and day(`datetime`)='$day'
				and `datetime` < '$endmax'
				order by `datetime` desc
				limit 1";
		$this->shiftEnd = R::getCell($sql);
		if(is_null($this->shiftEnd) || $this->shiftEnd == $this->shiftStart) {
			$this->shiftEnd=date('Y-m-d H:i:s', strtotime($this->date . ' ' . $this->hrEnd . ':00:00'));
		}

	}
}
