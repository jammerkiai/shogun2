<?php
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');

/******************************/
// edit room count here
$WPROOMS = 69;
$HSROOMS = 116;
$TOTALROOMS = $WPROOMS + $HSROOMS;
/******************************/

$title = 'Room Sales Summary by Type';
$thismonth = isset($_POST['thismonth']) ? $_POST['thismonth'] : date('m');
$thisyear = isset($_POST['thisyear']) ? $_POST['thisyear'] : date('Y');

if($_POST["submit"]=="export to excel") {
	$table="<h3 style='font-size:12px;font-family:arial,helvetica'>$title - $thismonth/$thisyear</h3>";
	$table.="<table border='0' cellspacing='2' cellpadding='2' style='font-size:11px;font-family:arial,helvetica'>";
	$table.=getHeaders();
	$table.=getData($thisyear,$thismonth);
	$table.="</table>";
	$excel_file_name="mss_$thisyear_$thismonth.xls";
	header("Content-type: application/octet-stream");//A MIME attachment with the content type "application/octet-stream" is a binary file.
	header("Content-Disposition: attachment; filename=$excel_file_name");//with this extension of file name you tell what kind of file it is.
	header("Pragma: no-cache");//Prevent Caching
	header("Expires: 0");//Expires and 0 mean that the browser will not cache the page on your hard drive
	echo $table;
	exit;
}

function getHeaders() {
	$retval = "<tr>";
	$retval.="<th>Date</th>";
	$retval.="<th>Room Type</th>";
	$retval.="<th>Sold/Occupied</th>";
	$retval.="<th>Available</th>";
	$retval.="<th>Occupancy Rate</th>";
	$retval.="<th>Room Sales</th>";
	$retval.="<th>Average Room Sales</th>";
	$retval .= "</tr>";
	return $retval;
}

function getData($thisyear,$thismonth) {
	$sql = "select a.salesdate,
				a.roomtypename,
				sum(1) as rooms_occupied,
				(select count(c.door_name) from room_types b, rooms c
where b.room_type_name=a.roomtypename and b.room_type_id=c.room_type_id and c.room_id not in (146,147,148)) as room_count,

format((sum(1)/(select count(c.door_name) from room_types b, rooms c 
where b.room_type_name=a.roomtypename and b.room_type_id=c.room_type_id and c.room_id not in (146,147,148)) 
), 2) as occupancy_rate,

sum(a.roomsales) as total_room_sales_per_type, format((sum(a.roomsales)/sum(1)), 2) as avg_roomrate_per_type
from salesdetailsummary a
where year(salesdate)='$thisyear' and month(salesdate)='$thismonth'
and a.occupancy not in (500, 1427, 1428)
group by a.salesdate, a.roomtypename
order by a.salesdate desc, a.roomtypename";
	$res = R::getAll($sql);
	$thisDay = '';
	$dailyRoomsOccupied = 0;
	$dailyRoomsAvailable = 0;
	$dailyTotalSales = 0;
	$roomtypes = array();
	foreach ($res as $row) {
		//		0 - salesdate,
		//		1 - roomtypename,
		//		2 - rooms_occupied,
		//		3 - room_count
		//		4 - occupancy_rate
		//		5 -avg_roomrate_per_type

		if($row['room_count'] > 0) {
			$dailyRoomsOccupied += $row['rooms_occupied'];
			$dailyRoomsAvailable += $row['room_count'];
			$dailyTotalSales += $row['avg_roomrate_per_type'];
			$monthlyRoomsOccupied[$row['roomtypename']] += $row['rooms_occupied'];
			$monthlyRoomsAvailable[$row['roomtypename']] += $row['room_count'];
			$monthlyTotalSales[$row['roomtypename']] += $row['avg_roomrate_per_type'];
			if (!in_array($row['roomtypename'], $roomtypes)) {
				$roomtypes[]=$row['roomtypename'];
			}
			if ($thisDay != $row['salesdate']) {
				//do daily summary data
				if ($thisDay != '') {
					$occRate = number_format(($dailyRoomsOccupied/$dailyRoomsAvailable), 2);
					$avgDailyRate = number_format(($dailyTotalSales/$dailyRoomsOccupied), 2);
					$rows.='<tr class=totals><th colspan=2>All Rooms (Daily)</th>';
					$rows.="<th>$dailyRoomsOccupied</th>";
					$rows.="<th>$dailyRoomsAvailable</th>";
					$rows.="<th>$occRate</th>";
					$rows.="<th>$dailyTotalSales</th>";
					$rows.="<th>$avgDailyRate</th>";
					$rows.='</tr>';
					$rows.='<tr><th colspan=7></th></tr>';
					$dailyRoomsOccupied = 0;
					$dailyRoomsAvailable = 0;
					$dailyTotalSales = 0;
				}
				$thisDay = $row[0];
			}
			$rows .= "<tr>";
			foreach($row as $fieldvalue) {
				$rows .= "<td>$fieldvalue</td>";
			}
			$rows .= "</tr>";
		}
	}
	
	$occRate = number_format(($dailyRoomsOccupied/$dailyRoomsAvailable), 2);
	$avgDailyRate = number_format(($dailyTotalSales/$dailyRoomsOccupied), 2);
	$rows.='<tr class=totals><th colspan=2>All Rooms (Daily)</th>';
	$rows.="<th>$dailyRoomsOccupied</th>";
	$rows.="<th>$dailyRoomsAvailable</th>";
	$rows.="<th>$occRate</th>";
	$rows.="<th>$dailyTotalSales</th>";
	$rows.="<th>$avgDailyRate</th>";
	$rows.='</tr>';
	$rows.='<tr><th colspan=7></th></tr>';
	
	//get the monthly
	$rows.='<tr><th colspan=7>Monthly Summary</th></tr>';
	$rows.="<tr>";
	$rows.="<th colspan=2>Room Type</th>";
	$rows.="<th>Sold/Occupied</th>";
	$rows.="<th>Available</th>";
	$rows.="<th>Occupancy Rate</th>";
	$rows.="<th>Room Sales</th>";
	$rows.="<th>Average Room Sales</th>";
	$rows .= "</tr>";
	$totalOcc=$totalAvail=$totalSales=0;
	sort($roomtypes);
	foreach($roomtypes as $roomtype) {
		if ($monthlyRoomsAvailable[$roomtype] > 0) {
			$totalOcc += $monthlyRoomsOccupied[$roomtype];
			$totalAvail += $monthlyRoomsAvailable[$roomtype];
			$totalSales += $monthlyTotalSales[$roomtype];
			$monthlyOccupancyRate = number_format( $monthlyRoomsOccupied[$roomtype]/$monthlyRoomsAvailable[$roomtype], 2 );
			$monthtlyAvgRate = number_format( $monthlyTotalSales[$roomtype]/$monthlyRoomsOccupied[$roomtype], 2);
			$rows.="<tr><td colspan=2>$roomtype</td>";
			$rows.="<td>$monthlyRoomsOccupied[$roomtype]</td>";
			$rows.="<td>$monthlyRoomsAvailable[$roomtype]</td>";
			$rows.="<td>$monthlyOccupancyRate</td>";
			$rows.="<td>$monthlyTotalSales[$roomtype]</td>";
			$rows.="<td>$monthtlyAvgRate</td>";
			$rows.='</tr>';
		}
	}
	$totalOccRate = number_format($totalOcc/$totalAvail, 2);
	$totalAvgRate = number_format($totalSales/$totalOcc);
	
	$rows.='<tr class=totals><th colspan=2>All Rooms (Monthly)</th>';
	$rows.="<th>$totalOcc</th>";
	$rows.="<th>$totalAvail</th>";
	$rows.="<th>$totalOccRate</th>";
	$rows.="<th>$totalSales</th>";
	$rows.="<th>$totalAvgRate</th>";
	$rows.='</tr>';
	$rows.='<tr><th colspan=7></th></tr>';
	return $rows;
}

function getMonth($selected) {
	$ret = "<select name='thismonth' id='thismonth'>";
	for($x=1; $x <=12; $x++) {
		$month = date('F', strtotime('2010-' . $x . '-01'));
		$ret.="<option value='$x' ";
		if($selected==$x) $ret.=" selected ";
		$ret.=">$month</option>";
	}
	$ret.="</select>";
	return $ret;
}

function getYear($selected) {
	$ret = "<select name='thisyear' id='thisyear'>";
	for($x=2010; $x <=2015; $x++) {
		$ret.="<option value='$x' ";
		if($selected==$x) $ret.=" selected ";
		$ret.=">$x</option>";
	}
	$ret.="</select>";
	return $ret;
}

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./reports.css">
<link rel="stylesheet" type="text/css" href="../../css/start/jquery-ui.css">
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>
<style>
table {
	border-collapse:collapse;
}
table th,td {
	padding:4px;
	text-align:center;
}

th.grand, td.grand {
	background-color:#eeffcc;
}

tr.weekend {
	background-color:#ffeecc;
}
tr.totals {
	color: #0000FF;
}
</style>
</head>
<body>
<form method='post'>
<div>
Select Month: <?php echo getmonth($thismonth) . getyear($thisyear) ?>
<input type="submit" name="submit" value="go" />
<input type="submit" name="submit" value="export to excel" />
</div>
<div id="workpanel">

<table border='1'>
<?php
echo getHeaders();
echo getData($thisyear,$thismonth);
?>
</table>
</div>
</form>
<script>
$(document).ready(function(){
	$("#newdate").datepicker({dateFormat:'yy-mm-dd'});
});
</script>
</body>
</html>

