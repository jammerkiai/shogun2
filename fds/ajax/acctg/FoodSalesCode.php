<?php

require_once('SalesCodeInjector.php');

class FoodSalesCode implements SalesCodeInjector
{
	var $beverageCategories = [17, 21, 39, 49, 50];
	var $specialRoomIds = [
		500 => 4007, 
		1427 => 4008, 
		1428 => 4006, 
		24479 => 4005, 
		24481 => 4004
	];

	public function getOptions($ci) {
		
		$code = 4002;
		$cr_desc = 'Kitchen Sales - ';
		if (array_key_exists($ci['occupancy_id'], $this->specialRoomIds)) {
			$code = $this->specialRoomIds[$ci['occupancy_id']];

		}

		if (in_array($ci['category_id'], $this->beverageCategories)) {
			$code .= 'b';
			$cr_desc .= 'Beer';	
		} else {
			$code .= 'a';
			$cr_desc .= 'Food';	
		}


		if ($ci['status'] == 'Paid' && $ci['tendertype']=='Cash') {

			$out = [
				'key' => 'Cash_Kitchen',
				'options' => [
					'source'   	=> 'fnb_sales',
					'cr'	=> ['code' => $code, 'desc' => $cr_desc],
					'dr' 	=> ['code' => '1000', 'desc' => 'Cash'],
				]
			];

		} else {

			$out = [
				'key' => 'AR_Kitchen',
				'options' => [
					'source'   => 'fnb_sales',
					'cr'	=> ['code' => $code, 'desc' => $cr_desc],
					'dr' 	=> ['code' => '1018', 'desc' => 'Accounts Receivable'],
				]
			];
		}

		return $out;
	}
}