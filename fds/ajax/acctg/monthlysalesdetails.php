<?php 
require_once('../config/config.inc.php');

$year = ($_GET['year']) ? $_GET['year'] : date('Y');
$month= ($_GET['month']) ? $_GET['month'] : date('m');
$occupancy_id= ($_GET['occupancy_id']) ? $_GET['occupancy_id'] : 0;

$venues = R::getAll('select distinct b.occupancy_id, a.door_name 
from rooms a, room_sales b, occupancy c  
where b.occupancy_id=c.occupancy_id
and a.room_id=c.room_id
and a.floor_id=7');


$start = <<<start
	SELECT datetime FROM `shift-transactions` WHERE shift='start'
	and date_format(datetime, '%Y')='$year'
	and date_format(datetime, '%m')<'$month'
	order by datetime desc limit 0, 1
start;

$end = <<<end
	SELECT datetime FROM `shift-transactions` WHERE shift='end'
	and date_format(datetime, '%Y')='$year'
	and date_format(datetime, '%m')='$month'
	order by datetime desc limit 0, 1
end;

$filter = '';

if ($occupancy_id) {
	$filter = "  occupancy_id=$occupancy_id and ";
}

$final = <<<final
	select AAA.date_field, 
	sum(if(BBB.src='room', if(BBB.category_id=3,BBB.amount,0), 0)) as room,
	sum(if(BBB.src='room', if(BBB.category_id<>3,BBB.amount,0), 0)) as misc,
    sum(if(BBB.src='fnb', if(BBB.category_id in (17,21),BBB.amount,0), 0)) as beer,
    sum(if(BBB.src='fnb', if(BBB.category_id not in (17,21),BBB.amount,0), 0)) as food
	from (SELECT date_field
	      FROM
	      (
	          SELECT MAKEDATE($year,1) +
	          INTERVAL ($month-1) MONTH +
	          INTERVAL daynum DAY date_field
	          FROM
	          (
	              SELECT t*10+u daynum FROM
	              (SELECT 0 t UNION SELECT 1 UNION SELECT 2 UNION SELECT 3) A,
	              (SELECT 0 u UNION SELECT 1 UNION SELECT 2 UNION SELECT 3
	               UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7
	               UNION SELECT 8 UNION SELECT 9) B ORDER BY daynum
	          ) AA
	      ) AA WHERE MONTH(date_field) = $month
	     ) AAA left join 
	     (
	     	select if(
	     		date_format(update_date, '%m')<>'$month',
	     		'$year-$month-01',
	     		date_format(update_date, '%Y-%m-%d')
	     	) as date_field, 
	     	(unit_cost*qty) as amount, 
	     	category_id, 'room' as src
	      	from room_sales 
	      	where $filter 
			status='Paid' and
	      	update_date between 
	      	($start)
	      	and 
	      	($end)

	      	union
		
			select if(
	     		date_format(update_date, '%m')<>'$month',
	     		'$year-$month-01',
	     		date_format(update_date, '%Y-%m-%d')
	     	) as date_field, 
			(unit_cost*qty) as amount, 
			category_id, 'fnb' as src
	      	from fnb_sales 
	      	where $filter 
			status='Paid' and
	      	update_date between
	      	($start)
	      	and 
	      	($end)

	     ) BBB
	using (date_field)
	group by AAA.date_field
final;

$days = R::getAll($final, [ ':st'=>$st, ':en'=>$en ]);
$totals = [];
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
	<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Monthly Sales Detail Summary</a>
			</div>
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<form class="navbar-form navbar-right" role="search">
					<div class="form-group">
						<select name="year" id="year" class="form-control">
							<?php for ($x=$year-3; $x <= $year; $x++): ?>
								<option value="<?php echo $x ?>"
								<?php if ($x==$year) echo 'selected' ?>
								>
									<?php echo $x ?>
								</option>
							<?php endfor ?>
						</select>
					</div>
					<div class="form-group">
						<select name="month" id="month" class="form-control">
							<?php for ($y=1; $y <= 12; $y++): ?>
								
								<option value="<?php echo str_pad( $y, 2, 0, STR_PAD_LEFT ) ?>"
								<?php if ($y==$month) echo 'selected' ?>
								>
									<?php echo date( 'F', strtotime("$year-$y-01")) ?>
								</option>

							<?php endfor ?>
						</select>
					</div>
					<div class="form-group">
						<select name="occupancy_id" id="occupancy_id" class="form-control">
							<option value="0">All</option>
							<?php foreach ($venues as $v): ?>
								<option value="<?php echo $v['occupancy_id'] ?>"
								<?php if ($v['occupancy_id']==$occupancy_id) echo 'selected' ?>
								>
									<?php echo $v['door_name'] ?>
								</option>
							<?php endforeach ?>
						</select>
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
				
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>
  	
    <div class="container">
	    <div class="panel panel-default">
	    	<div class="panel-body row">
		    	<div class="table-responsive">
		    		<table class="table table-hover">
						<thead>
							<tr>
								<th>Date</th>
								<th>Room</th>
								<th>Misc</th>
								<th>Beer</th>
								<th>Food</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($days as $day): ?>
							<?php $daytotal = 0; ?>
							<tr>
								<?php foreach ($day as $k => $v): ?>
									<?php 
										if (in_array($k, ['room', 'misc', 'food', 'beer'])) {
											$totals[$k] += $v;
											$daytotal += $v;
										}  
									?>
									<td><?php echo $v ?></td>
								<?php endforeach ?>
								<td><?php echo $daytotal ?></td>
							</tr>
						<?php endforeach ?>
							
						</tbody>
						<tfoot>
							<tr>
								<th>Totals</th>
								<?php foreach ($totals as $k => $v): ?>
									<th><?php echo $v ?></th>
									<?php $fulltotal += $v ?>
								<?php endforeach ?>
								<th><?php echo $fulltotal ?></th>
							</tr>
						</tfoot>
					</table>
				</div>

	    	</div>
	    </div>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  </body>
</html>




