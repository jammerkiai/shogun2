<?php
/** builds sql statements **/

trait BuildSqlStatement
{
	public function sqlRechit() {
		return "select a.occupancy_id, a.actual_checkin, a.expected_checkout, a.actual_checkout, a.regflag, 
				c.door_name, b.*, d.sas_description as item, b.roomsales_id as transaction_id, d.code
				from occupancy a, room_sales b, rooms c, sales_and_services d
				where a.room_id = c.room_id
				and b.item_id=d.sas_id
				and a.occupancy_id = b.occupancy_id
				and b.category_id = 3
				and b.rechit_date between :start and :end";
	}

	public function sqlCheckin() {
		return "select a.occupancy_id, a.actual_checkin, a.expected_checkout, a.actual_checkout, a.regflag, 
				c.door_name, b.*, d.sas_description as item, b.roomsales_id as transaction_id, d.code
				from occupancy a, room_sales b, rooms c, sales_and_services d
				where a.room_id = c.room_id
				and b.item_id=d.sas_id
				and a.occupancy_id = b.occupancy_id
				and b.category_id = 3
				and a.actual_checkin between :start and :end";
	}

	public function sqlExtension() {
		return "select a.occupancy_id, a.actual_checkin, a.expected_checkout, a.actual_checkout, a.regflag, 
				c.door_name, b.*, d.sas_description as item, b.roomsales_id as transaction_id, d.code
				from occupancy a, room_sales b, rooms c, sales_and_services d
				where a.room_id = c.room_id
				and b.item_id=d.sas_id
				and a.occupancy_id = b.occupancy_id
				and b.category_id = 3
				and b.update_date between :start and :end
				and a.actual_checkin not between :start and :end
				";
	}

	public function sqlFoodSales() {
		return "select a.occupancy_id, a.actual_checkin, a.expected_checkout, a.actual_checkout, a.regflag, 
				c.door_name, b.*, d.fnb_name as item, b.fnbsales_id as transaction_id, d.code
				from occupancy a, fnb_sales b, rooms c, fnb d
				where a.room_id = c.room_id
				and b.item_id=d.fnb_id
				and a.occupancy_id = b.occupancy_id
				and b.update_date between :start and :end";
	}

	public function sqlMiscSales() {
		return "select a.occupancy_id, a.actual_checkin, a.expected_checkout, a.actual_checkout, a.regflag, 
				c.door_name, b.*, d.sas_description as item, b.roomsales_id as transaction_id, d.code
				from occupancy a, room_sales b, rooms c, sales_and_services d
				where a.room_id = c.room_id
				and b.item_id=d.sas_id
				and a.occupancy_id = b.occupancy_id
				and b.category_id != 3
				and b.update_date between :start and :end";
	}
}