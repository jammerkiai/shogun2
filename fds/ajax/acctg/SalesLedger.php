<?php

/**
* salesledger.php
*/

require_once('GetInclusiveDates.php');
require_once('BuildSqlStatement.php');
require_once('DataNormalizer.php');
require_once('FoodSalesCode.php');
require_once('RechitSalesCode.php');
require_once('CheckinSalesCode.php');
require_once('MiscSalesCode.php');

class SalesLedger
{
	use GetInclusiveDates, BuildSqlStatement;

	var $checkInList;

	public function __construct($date, DataNormalizer $dn) {
		$this->startDate = $this->getStartDate($date);
		$this->endDate = $this->getEndDate($date);
		$this->dn = $dn;
		$this->setDateRange([':start' => $this->startDate, ':end' => $this->endDate]);
		return $this;
	}

	public function setDateRange(array $range) {
		$this->dateRange = $range;
	}

	public function getCheckInList() {
		$this->checkInList = $this->dn->normalizeData(
			R::getAll($this->sqlCheckin(), $this->dateRange),
			new CheckinSalesCode()
		);

		return $this->checkInList;
	}

	public function getExtensionList() {
		$this->extensionList = $this->dn->normalizeData(
			R::getAll($this->sqlExtension(), $this->dateRange),
			new CheckinSalesCode()
		);

		return $this->extensionList;
	}

	public function getFoodSalesList() {
		$this->foodSalesList = $this->dn->normalizeData(
			R::getAll($this->sqlFoodSales(), $this->dateRange),
			new FoodSalesCode()
		);
		return $this->foodSalesList;
	}

	public function getRechitList() {
		$this->rechitList = $this->dn->normalizeData(
			R::getAll($this->sqlRechit(), $this->dateRange),
			new RechitSalesCode()
		);
		return $this->rechitList;
	}

	public function getMiscSalesList() {
		$this->miscSalesList = $this->dn->normalizeData(
			R::getAll($this->sqlMiscSales(), $this->dateRange),
			new MiscSalesCode()
		);
		return $this->miscSalesList;
	}

	// public function postToLedger() {
	// 	foreach ($this->rechitList as $occ => $se) {
	// 		if ( isset($se['Unearned_Sales']) ) {
	// 			foreach ($se['Unearned_Sales'] as $us) {
	// 				$drSql = "insert into "
	// 			}
	// 		}
	// 	}
	// }

}




