<?php

/**
* slview.php
*
* Sales Ledger posting details viewer
*
*/
session_start();
require_once('../config/config.inc.php');
require_once('SalesLedger.php');
require_once('DataNormalizer.php');

$now = isset($_POST['newdate']) ? $_POST['newdate'] : ( isset($_GET['d']) ? $_GET['d'] : date('Y-m-d'));

$sl = new SalesLedger($now, new DataNormalizer());

$checkInList = $sl->getCheckInList();
$extensionList = $sl->getExtensionList();
$rechitList = $sl->getRechitList();
$miscList = $sl->getMiscSalesList();
$foodList = $sl->getFoodSalesList();

$groupings = [
    [
        'classname' => 'checkinsales',
        'caption' => 'Check In Sales', 
        'list' => $checkInList, 
        'code_pairs' => ['Cash_Unearned', 'AR_Unearned']
    ],
    [
        'classname' => 'extensionsales',
        'caption' => 'Extension Sales', 
        'list' => $extensionList, 
        'code_pairs' => ['Cash_Unearned', 'AR_Unearned']
    ],
    [
        'classname' => 'rechitsales',
        'caption' => 'Rechit Sales', 
        'list' => $rechitList, 
        'code_pairs' => ['Unearned_Sales']
    ],
    [
        'classname' => 'foodsales',
        'caption' => 'Food Sales', 
        'list' => $foodList, 
        'code_pairs' => ['Cash_Kitchen', 'AR_Kitchen']
    ],
    [
        'classname' => 'miscsales',
        'caption' => 'Misc Sales', 
        'list' => $miscList, 
        'code_pairs' => ['Cash_Misc', 'AR_Misc']
    ]
];

if(isset($_POST['cmd']) && $_POST['cmd'] === 'POST TO GL') {
    // $sl->postToLedger();
}

?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="./reports.css">
    <link rel="stylesheet" type="text/css" href="w2ui-1.4.3.min.css" />
    <script src="../../js/jquery.js"></script>
    <script type="text/javascript" src="w2ui-1.4.3.min.js"></script>
    <style>
        .occupancy, .roomsales, .slposts {
            font-size: 12px;
            border: 1px solid gray;
            width: 100%;
        }

        .occupancy th {
            text-align: left;
            background-color: #ffeecc;
        }

        .occupancy td {
            text-align: left;
            background-color: #fff;
            font-size: 12px;
            padding: 6px 4px;
        }

        
        .roomsales th {
            text-align: left;
            background-color: #aaaaaa;
        }

        .roomsales td {
            text-align: left;
            background-color: #ececec;
        }

        .occline td {
            background-color: #ffcccc;
        }

        .slposts td {
            background-color: #ddffdd;
        }

        .toggler {
            border: 1px solid #ccc;
            border-radius: 2px;
            background-color: #fff;
            padding: 4px 8px;
            text-decoration: none;
        }

        .pressed {
            background-color: #ccc;
        }

        .hidden {
            display: none;
        }

        .counter {
            background-color: orange;
            font-size: 10px;
            padding: 3px 6px;
            border-radius: 12px;
            color: #fff;
        }

        caption {
            font-size: 14px;
            font-weight: bold;
        }

        td.numeric {
            text-align: right;
        }

        tr.grandtotal td {
            background-color: #663333;
            font-weight: bold;
            text-align: right;
            color: #fff;
        }

        input.btnsubmit {
            background-color: #4545FF;
            color: #ffffff;
        }

        input.btnsubmit:hover {
            background-color: #5555cc;
        }
    </style>    
</head>
<body>
<form method='post'>
    <h5>Sales For Posting</h5>
    <div class="menubar">
        <select name="site">
            <?php

            for($x = 1; $x <= 2; $x++) {
                echo "<option value='$x'";
                if ($site == $x) echo " selected ";
                echo ">Shogun $x</option>";
            }

            ?>
        </select>
        Select Date:
        <input type="us-date" id="newdate" name="newdate" value="<?php echo $now ?>" />
        <input type='submit' name='cmd' value='go' />
        <button type="button" class="toggler" data-target=".roomsalesdetails" title="Click to toggle Room Sales Details">RS</button>
        <button type="button" class="toggler" data-target=".slpostdetails" title="Click to toggle Sales Ledger Posting Data">SL</button>
<!--         <input type='submit' name='cmd' value='refresh summary' onclick="return confirm('The summary for this date will be deleted! Proceed?')"/>
        <input type='submit' name='cmd' value='refresh details' onclick="return confirm('All special updates previously saved for this date will be deleted! Proceed?')"/>-->
        

        <button type="button" class="toggler" data-target=".checkinsales">
        Checkin Sales 
        <span class="counter">
            <?php echo count($checkInList) ?>
        </span>
        </button>

         <button type="button" class="toggler" data-target=".extensionsales">
        Extension Sales 
        <span class="counter">
            <?php echo count($extensionList) ?>
        </span>
        </button>

        <button type="button" class="toggler" data-target=".rechitsales">Rechit Sales
        <span class="counter">
            <?php echo count($rechitList) ?>
        </span>
    </button>

        
            <button type="button" class="toggler" data-target=".miscsales">
            Misc Sales 
            <span class="counter">
                <?php echo count($miscList) ?>
            </span>
            </button>

            <button type="button" class="toggler" data-target=".foodsales">
            Food Sales 
            <span class="counter">
                <?php echo count($foodList) ?>
            </span>
            </button>
        
            <input type='submit' class="btnsubmit" name='cmd' value='POST TO GL'/> 
        <?php echo $mesg ?>
    </div>
    <a name="updateglcode"></a>
    

    <?php 
        $dayTotal = 0;
        foreach ($groupings as $data) {
            include('slview_listing.php');
        }
    ?>
 

    <table class="table occupancy">
        <tr class="grandtotal">
        <td>Total Sales for <?php echo $now ?></td>
        <td><?php echo number_format($dayTotal, 2) ?></td>
        </tr>
    </table>
</form>

<?php include('slview_modal.php'); ?>

<script>

    $(function () {
        $('input[type=us-date]').w2field('date',  { format: 'yyyy-mm-dd' });

        $('.updater').on('click', function(e) {
            //e.preventDefault();
            $('#updateCode').show();

            $('#updateId').val($(this).data('id'));

            $('#newdr').val($(this).data('dr')).trigger('change');
            $('#newcr').val($(this).data('cr')).trigger('change');
        });

        $('.btn.cancel').on('click', function(e){
            e.preventDefault();
            $('#updateCode').hide();
        });

        $('.toggler').on('click', function(e){
            e.preventDefault();
            $($(this).data('target')).toggle();
            $(this).toggleClass('pressed');
        });

        $('.btn.save').on('click', function(e){
            e.preventDefault();
            console.log($('#newdr').val());
            console.log($('#newcr').val());
            console.log($('#updateId').val());

            // $.post('sjdfns.php',
            //     {
            //         dr_code: $('#newdr').val(),
            //         cr_code: $('#newcr').val(),
            //         sjdId: $('#updateId').val()
            //     },
            //     function(res){
            //         console.log(res);
            //         document.location.href="clposting.php?d=<?php echo $now ?>";
            //     }
            // );

        });
    });

</script>
</body>
</html>