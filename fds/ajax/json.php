<?php
/**
* json.php
*/
include_once("./config/config.inc.php");

$act = trim($_POST["act"]);
$floorsort = '';

if($act=="floor") { 
$fid = $_POST["fid"];

$sql2 = "
	select a.room_id,  a.door_name, a.room_type_id, b.room_type_name, a.status, a.ui_top, a.ui_left, a.ui_width, a.ui_height, a.site_id
	from rooms a, room_types b
	where a.room_type_id=b.room_type_id
	and a.floor_id=$fid
";

}elseif($act=="building"){
	$sql2 = "	select * from floors order by floor_label ";
	$floorsort = 'floor_label';

}elseif($act=="roomrates"){
	$rtid = $_POST["roomtype"];

	$sql2 = "SELECT room_type_rates.rtr_id, room_type_rates.amount, rates.rate_name, rates.rate_id, room_type_rates.room_type_id, room_type_rates.ot_amount,rates.duration
FROM room_type_rates
LEFT JOIN rates ON room_type_rates.rate_id = rates.rate_id
WHERE room_type_rates.room_type_id =$rtid and room_type_rates.active=1";

}elseif($act=="roomdiscounts"){
	$rtid = $_POST["roomtype"];
	$rateid = isset($_POST["rate"]) ? $_POST["rate"]  : 0;
	$sql2 = "SELECT discounts.discount_id, discounts.discount_label, discounts.discount_percent,room_type_discounts.rate_id
		FROM room_type_discounts
		LEFT JOIN discounts ON room_type_discounts.discount_id = discounts.discount_id
		WHERE room_type_discounts.room_type_id =$rtid ";

	if($rateid) {
		$sql .= " and room_type_discounts.rate_id=$rateid ";
	}

}elseif($act=="availablerooms"){
	$rtype = $_POST["roomtype"];
	$roomid = $_POST["roomid"];
	$sql2 = "select room_id, door_name from rooms 
			where room_type_id=$rtype and room_id <> $roomid and status=1";
}

$res2 = R::getAll($sql2);


$retval="{}";

	$numfields = count($res2[0]);
	$fieldNames = array_keys($res2[0]);
	$idProperty = $fieldNames[0];
	// $ctr2=0;
	// for ($i=0; $i < $numfields; $i++) {
	// 	if($ctr2) {
	// 		$fields .= ",";
	// 	}else{
	// 		$idProperty = mysql_field_name($res2, $i);
	// 	}
	// 	$fields.= '"' . mysql_field_name($res2, $i) . '"';
	// 	$ctr2++;
	// }

	// $ctr=0;
	// $retval="";
	// while($row = mysql_fetch_array($res2)) {
	// 	$retval.=($ctr) ? "," : "";
	// 	$retval.=json_encode($row);
	// 	$ctr++;
	// }

	if ($floorsort!='') $idProperty=$floorsort;

	echo json_encode([
		'metaData' => [
			'root' => $act,
			'idProperty' => $idProperty,
			'fields' => $fieldNames,
			'sortInfo' => [
				'field' => $idProperty,
				'direction' =>'ASC'
			] 
		],
		$act => $res2
	]);


// 	$meta = ' metaData : {
// 		root : "'.$act.'",
// 		idProperty : "'.$idProperty.'",
// 		fields : [ ' .$fields. '],
// 		sortInfo : { "field" : "'.$idProperty.'", "direction": "ASC" }
// 	}';

// 	$retval = '{
// 		'.$meta.',
// 		"'.$act.'" : ' . json_encode($res2) . ' 
// 		}';

// echo $retval;
?>