<?php
/*
makeup.php
*/
include_once("config/config.inc.php");
include_once("common.inc.php");
include_once("config/lobby.inc.php");

session_start();
$user = $_SESSION["hotel"]["userid"];

if (isset($_POST['act']) && $_POST['act'] === 'Save') {
    $now = date('Y-m-d H:i:s');
    $stockitems = array();
    foreach ($_POST as $key => $val) {
        if (stristr($key, 'item_') !== false && $val != 0) {
            list(, $stockitemid) = explode('_', $key);
            $stockitems[$stockitemid] = $val;
        }  
        ${$key} = $val;
    }
       
    foreach ($stockitems as $stockid => $qty) {
        $sql2 = "insert into linen_consumption 
                    (date_logged, occupancy_id, stock_id, qty, updated_by, for_date, for_shift, remarks)
                    values 
                    ('$now', '$occupancy', '$stockid', '$qty', '$user', 
                    '$new_date', '$shift', 'Make Up - $remarks');
                ";
        R::exec($sql2);
    }
    
    header('location: makeup.php?roomid=' . $roomid);
} elseif (isset($_GET['act']) && $_GET['act'] === 'del') {
    $lci = $_GET['lci'];
    $sql = "delete from linen_consumption where linen_consumption_id=$lci";
    R::exec($sql);
    header("location: makeup.php?roomid=" . $_GET['roomid']);
}


function getLinen() {
    $sql = "select stockid, description from stockmaster order by display_order";
    $linen = R::getAssoc($sql);
	return $linen;
}

function getItemsFromRoomtype($roomtype) {
    $sql = "select stockid, stock_item_qty
            from roomtype_linenkit a, kit_items b
            where a.kit_id=b.kit_id
            and a.room_type_id= '$roomtype'
            ";

    $qty = R::getAssoc($sql);
    return $qty;
}

function getTopTransactions($room=null, $occupancy=null, $count = 10) {
    $sql = "select a.linen_consumption_id, a.date_logged, a.for_date, a.for_shift, 
            b.description, a.qty, c.fullname, a.remarks
            from linen_consumption a, stockmaster b, users c             
            where 
            a.stock_id=b.stockid and
            a.updated_by=c.user_id and
            a.occupancy_id='$occupancy'
            order by a.date_logged desc
            limit 0, $count;
        ";
    $res = R::getAll($sql);
    
    if (!empty($res)) {
        $retval .= "<table class='datatable'>";    
        $retval .= "<tr>";
        $retval .= "<th>MakeUp Date</th>";
        $retval .= "<th>Shift</th>";
        $retval .= "<th>Linen</th>";
        $retval .= "<th>Qty</th>";
        $retval .= "<th>Remarks</th>";
        $retval .= "<th>Updated By</th>";
        $retval .= "<th>Action</th>";
        $retval .= "</tr>";        
        foreach ($res as $r) {
            $lci = $r['linen_consumption_id'];
            $logdate = $r['date_logged'];
            $fordate = $r['for_date'];
            $forshift = $r['for_shift'];
            $item = $r['description'];
            $qty = $r['qty'];
            $user = $r['fullname'];
            $remarks = $r['remarks'];
            $retval .= "<tr>";
            $retval .= "<td>$fordate</td>";
            $retval .= "<td>$forshift</td>";
            $retval .= "<td>$item</td>";
            $retval .= "<td>$qty</td>";
            $retval .= "<td>$remarks</td>";
            $retval .= "<td>$user</td>";
            $retval .= "<td><a href='makeup.php?roomid=$room&lci=$lci&act=del'>Del</a></td>";
            $retval .= "</tr>";
        }
        $retval .= "</table>";
    } 
    return $retval;
}

if($_GET["roomid"]) { //run on page load to get occupancy id
	$room = $_GET["roomid"];
	$sql = " select a.occupancy_id, a.actual_checkin, b.door_name,
            c.rate_name, a.shift_checkin, d.fullname, b.room_type_id
			from occupancy a, rooms b, rates c, users d
			where a.room_id=b.room_id and a.rate_id=c.rate_id and a.update_by=d.user_id and a.room_id=$room and actual_checkout='0000-00-00 00:00:00' ";
	$r = R::getRow($sql);
    $occupancy = $r['occupancy_id'];
    $checkindate  = $r['actual_checkin'];
    $doorname = $r['door_name'];
    $ratename  = $r['rate_name'];
    $shift = $r['shift_checkin'];
    $fullname = $r['fullname'];
    $roomtype = $r['room_type_id'];

}

$kitItems = getItemsFromRoomtype($roomtype);
$setdate = isset($_POST['new_date']) ? $_POST['setdate'] : date('Y-m-d');

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script lang="javascript">
$(document).ready(function(){
        $( "#radio" ).buttonset();
		$("#new_date").datepicker({dateFormat:'yy-mm-dd'});
		
		$( ".itemsliders" ).slider({
			range: "max",
			min: 0,
			max: 10,
			create: function( event, ui) {
			    var target = '#' + $(this).attr('data-bind'),
			        initvalue = $(target).val();
			    $(this).slider('option', 'value', initvalue);
			},
			slide: function( event, ui ) {
				$( "#" + $(this).attr('data-bind') ).val( ui.value );
			}
		});
		
        $('input:submit').button();
 });
</script>
<style>

    div.item label {
        width: 50px;
        display: inline-block;
    }
    
    label,
    div#radio > label,
    input {
        width: auto;
        line-height: 20px;
        font-size: 14px;
        text-align: center;
        display: inline-block;
        padding: 6px;
        margin: 4px 0 4px 0;
    }
    
    
    input#new_date {
        width: 120px;
        font-size: 18px;
    }
    
    input[type=text] {
        width: 60px;
        font-size: 18px;
    }
    
    input[type=text].remarks {
        width: 350px;
        text-align: left;
    }
    
    input[type=submit] {
        font-size: 18px;
        padding: 6px;
        width: 80px;
    }
    
    div.itemdesc,
    div.slidercontainer {
        text-align: right;
        display: inline-block;
        width: 150px;
    }

    div.slidercontainer {
        width: 200px;
        padding-left: 10px;
    }
    
    div.itemrow {
        display: block;
        width: 400px;
    }
    
    .datatable {
        font-size: 11px;
        border: 1px solid green;
        width: 300px;
    }
    
    .datatable th {
        border-bottom: 1px solid #f0f;
    }
    
</style>
</head>
<body>
<form name="myform" id="myform" action="" method="post">
<fieldset id="changestatus">
<legend>Make Up</legend>
<table class="formtable">
<tr>
<td>
<label>Make-up Room Date:</label>
<input type="text" name="new_date" id="new_date"  class="datefield" value="<?php echo $setdate ?>" />
</td>
<td>
<div id="radio">
	<input type="radio" id="radio1" name="shift" value='1' /><label for="radio1">Shift 1</label>
	<input type="radio" id="radio2" name="shift" value='2' checked="checked" /><label for="radio2">Shift 2</label>
	<input type="radio" id="radio3" name="shift" value='3' /><label for="radio3">Shift 3</label>
</div>
</td>
</tr>
<tr>
<td colspan=2>
<label>Remarks:</label>
<input type="text" class="remarks" name="remarks" value="" />
</td>
</tr>
</tr>
<tr>
<td colspan=2>
<label>Count Per Linen Item: </label>
<div class="items">
<?php
$linen = getLinen();
foreach($linen as $id => $desc ) {
    $value = isset($kitItems[$id]) ? $kitItems[$id] : 0;
	echo "
	    <div class='itemrow'>
	        <div class='itemdesc'>
	            <label for='item_$id'>$desc</label>
		        <input type='text' name='item_$id' id='item_$id' value='$value' />
            </div>
            <div class='slidercontainer'>
                <div id='slider-$desc' class='itemsliders' data-bind='item_$id'></div>
            </div>
        </div>
             ";
}
?>
</div>
* Default values are the standard linen items for this room type
</td>
<td valign="top">
<?php echo getTopTransactions($room, $occupancy, 10) ?>
<input type="submit" name="act" id="act" value="Save" onclick="return confirm('Proceed to Make Up?');"/>
<input type="hidden" name="occupancy" value="<?php echo $occupancy ?>"/>
<input type="hidden" name="roomid" value="<?php echo $room ?>" />
</td>
</tr>
</table>
</fieldset>
</form>

 </body>
 </html>
