<?php
session_start();
include_once("config/config.inc.php");
include_once("monthly.class.php");

$lsql = "select settings_value from settings where id = '3'";
$lobbyid = R::getCell($lsql);

function getSalesReport($month,$year)
{
	$num = cal_days_in_month(CAL_GREGORIAN, $month, $year) ;

	$sql = "select settings_value from settings where id = '1'";
	$value = R::getCell($lsql);

	$ret = "<div style='font-weight:bold'>".strtoupper($value)."<br>
	WALK-UP SALES SUMMARY<br>
	FOR THE MONTH OF ".strtoupper(getMonthName($month))." ".$year."</div><br><br>";
	$ret .= "<table border=1 cellpadding=3 cellspacing=0>";
	$ret .= "<tr>";
	$ret .= "<th>Date</th>";
	$ret .= "<th>&nbsp;</th>";
	$ret .= "<th>Room</th>";
	$ret .= "<th>NOG</th>";
	$ret .= "<th>Food</th>";
	$ret .= "<th>FOG</th>";
	$ret .= "<th>Beer</th>";
	$ret .= "<th>BOG</th>";
	$ret .= "<th>Beverage</th>";
	$ret .= "<th>BBOG</th>";
	$ret .= "<th>Misc</th>";
	$ret .= "<th>Total Sales</th>";
	$ret .= "</tr>";
	for($i = 1;  $i <= $num; $i++)
	{
		
		$start = date('Y-m-d H:i:s', strtotime('-1800 seconds',strtotime($year."-".$month."-".$i." 00:00:00")));
		$end = date('Y-m-d H:i:s', strtotime('+1800 seconds',strtotime($year."-".$month."-".$i." 23:59:59")));

		$sql = "select  `datetime` from `shift-transactions` where `datetime` between '$start' and '$end' 
		and shift ='start' order by `datetime` asc limit 0,1";
		$startdt = R::getCell($lsql);
		
		$sql = "select  `datetime` from `shift-transactions` where `datetime` between '$start' and '$end' 
		and shift ='end' order by `datetime` desc limit 0,1";
		$enddt = R::getCell($lsql);
		
		$all = new monthly(1,$startdt,$enddt);

		$_getfonlyog = $all->getFonlyOG();
		$_getBOG=$all->getBOG();
		$_getBBOG=$all->getBBOG();
		
		$ret .= "<tr>";
		$ret .= "<td>$i</td>";
		$ret .= "<td>&nbsp;</td>";
		$ret .= "<td>".number_format($all->roomsales)."</td>";
		$ret .= "<td>".number_format($all->numguest)."</td>";
		$ret .= "<td>".number_format($all->foodonlysales)."</td>";
		$ret .= "<td>".number_format($_getfonlyog)."</td>";
		$ret .= "<td>".number_format($all->beersales)."</td>";
		$ret .= "<td>".number_format($_getBOG)."</td>";
		$ret .= "<td>".number_format($all->bevsales)."</td>";
		$ret .= "<td>".number_format($_getBBOG)."</td>";
		$ret .= "<td>".number_format($all->miscsales)."</td>";
		$ret .= "<td>".number_format($all->totalsales)."</td>";
		$ret .= "</tr>";
		
		$roomsales += $all->roomsales;
		$numguest+=$all->numguest;
		$foodonlysales+=$all->foodonlysales;
		$getFonlyOG+=$_getfonlyog;
		$beersales+=$all->beersales;
		$getBOG+=$_getBOG;
		$bevsales+=$all->bevsales;
		$getBBOG+=$_getBBOG	;
		$miscsales+=$all->miscsales;
		$totalsales+=$all->totalsales;
	}
	$ret .= "<tr>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<th style='text-align:left'>".number_format($roomsales)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($numguest)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($foodonlysales)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($getFonlyOG)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($beersales)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($getBOG)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($bevsales)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($getBBOG)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($miscsales)."</th>";
	$ret .= "<th style='text-align:left'>".number_format($totalsales)."</th>";
	$ret .= "</tr>";

	$ret .= "</table>";

	
	return $ret;
}

function getMonthDropdown($name="month", $selected=null)
	{
			$dd = '<select name="'.$name.'" id="'.$name.'">';

			$months = array(
					1 => 'January',
					2 => 'February',
					3 => 'March',
					4 => 'April',
					5 => 'May',
					6 => 'June',
					7 => 'July',
					8 => 'August',
					9 => 'September',
					10 => 'October',
					11 => 'November',
					12 => 'December');
			/*** the current month ***/
			$selected = is_null($selected) ? date('n', time()) : $selected;

			for ($i = 1; $i <= 12; $i++)
			{
					$dd .= '<option value="'.$i.'"';
					if ($i == $selected)
					{
							$dd .= ' selected';
					}
					/*** get the month ***/
					$dd .= '>'.$months[$i].'</option>';
			}
			$dd .= '</select>';
			return $dd;
	}

	function getYearDropdown($name="year", $selected=null)
	{
			$dd = '<select name="'.$name.'" id="'.$name.'">';

			$months = array(
					1 => '2009',
					2 => '2010',
					3 => '2011',
					4 => '2012',
					5 => '2013',
					6 => '2014',
					7 => '2015',
					8 => '2016');
		   
			$selected = is_null($selected) ? date('n', time()) : $selected;

			for ($i = 1; $i <= 8; $i++)
			{
					$dd .= '<option value="'.$months[$i].'"';
					if ($months[$i] == $selected)
					{
							$dd .= ' selected';
					}
					
					$dd .= '>'.$months[$i].'</option>';
			}
			$dd .= '</select>';
			return $dd;
	}
	function getMonthName($i)
	{
		$months = array(
					1 => 'January',
					2 => 'February',
					3 => 'March',
					4 => 'April',
					5 => 'May',
					6 => 'June',
					7 => 'July',
					8 => 'August',
					9 => 'September',
					10 => 'October',
					11 => 'November',
					12 => 'December');
		return $months[$i];
	}

?>
<style>
		.printable {
			font-family: sans-serif;
			font-size: 14px;
			font-weight: 550;
			}
		.report{
			font-family: sans-serif;
			font-size: 14px ;
			text-align:left;
			font-weight: 550;
				
		}
		.report td{
			padding-bottom:12px;
		}
		.summary{
			font-family: sans-serif;
			font-size: 14px;
			text-align:left;
			font-weight: 550;
		}
</style>
<script src="../js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery.print.js"></script>
<script type="text/javascript">
 
		// When the document is ready, initialize the link so
		// that when it is clicked, the printable area of the
		// page will print.
		$(document).ready(function(){		
				$("a").attr( "href", "javascript:void( 0 )" ).click(
						function(){
							// Print the DIV.
							$(".printable2").print();
							$(".printable").print(); 							
							// Cancel click event.
							return( false );
						});
 
			
		});
 
</script>
<form name=myform method=post>
<div>
Month: <? echo getMonthDropdown("ddlmonth",$_POST["ddlmonth"]); ?>
<br>
<br>
Year: <? echo getYearDropdown("ddlyear",$_POST["ddlyear"]); ?>
</div>
<br>
<input type='submit' value='Search' name='btnSearch' />
<br>
<br>
<a href="#">Print Report</a>
<br>
<br>
<div class='printable'>
<? if($_POST){ echo getSalesReport($_POST["ddlmonth"],$_POST["ddlyear"]);} ?>
</div><br />
<a href="#">Print Report</a>
</form>