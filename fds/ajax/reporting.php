<?php


class Reporting
{

	public function __construct($room) 
	{
		$sql = " select a.occupancy_id, a.actual_checkin, b.door_name, c.rate_name, a.shift_checkin, 
		d.fullname,a.expected_checkout from occupancy a, rooms b, rates c, users d
		where a.room_id=b.room_id and a.rate_id=c.rate_id and a.update_by=d.user_id and a.room_id=$room and
		actual_checkout='0000-00-00 00:00:00' order by a.actual_checkin desc";

		$res = R::getRow($sql);

		$occupied = 0;
		if(!empty($res)) {
			$occupied = 1;
		}

		$this->occupied = $occupied;
		$this->occupancy = $res['occupancy_id'];
		$this->checkindate = $res['actual_checkin'];
		$this->doorname = $res['door_name'];
		$this->ratename = $res['rate_name'];
		$this->shift = $res['shift_checkin'];
		$this->fullname = $res['fullname'];
		$this->etd = $res['expected_checkout'];
		$indate = new \DateTime($this->etd);
		$newcheckout = $indate->sub( new DateInterval("PT30M") );
		$this->wakeup = $indate->format("Y-m-d H:i:s");
		$this->totalbalance = $this->getTotalBalance();
		$this->totalbalance_display = $this->getTotalBalanceDisplay();
		$this->total = 0; 
    
	    $this->publishrate = $this->getPublishRate();
		$sql = "select settings_value from settings where id = '1'";
		$set = R::getCol($sql)[0];

		$this->hotel = $set;
	}
	
	public function getTotalBalance()
	{
		$now = date("Y-m-d H:i:s");
		$occupancy = $this->occupancy; 
		//retrieve room sales
		$sql =  " select sum(a.unit_cost * a.qty) as  cost
				from room_sales a, sales_and_services b		
				where a.item_id=b.sas_id and a.category_id=3
				and a.charge_to=$occupancy
				and a.sales_date <= '$now' ";

		$rtotal = R::getCell($sql);

		//retrieve misc sales
		$mtotal = 0;
		$sql = " select sum(a.unit_cost*a.qty) as cost 
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (1, 2,4) and a.charge_to=$occupancy   and a.status not in ('Draft','Cancelled') ";
		$mtotal = R::getCell($sql);

		
		//retrieve fnb sales
		$ftotal = 0;
		$sql = "select sum(a.unit_cost * a.qty) as totalcost
				from fnb_sales a, fnb b, food_categories c		
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.charge_to=$occupancy and a.status not in ('Draft','Cancelled') ";
		$ftotal = R::getCell($sql);
		
		//retrieve payments
		$ptotal = 0;
		$sql  = "select sum(amount) from salesreceipts where occupancy_id='$occupancy' ";
		$ptotal = R::getCell($sql);

		$grandtotal = number_format($rtotal + $ftotal + $mtotal,2);
		$rtotal_f =number_format($rtotal,2);
		$mtotal_f =number_format($mtotal,2);
		$ftotal_f =number_format($ftotal,2);
		$payments_f = number_format($ptotal,2);
		$totalbalance = ($rtotal + $ftotal + $mtotal) - $ptotal;

		$this->rtotal = $rtotal;
		$this->ftotal = $ftotal;
		$this->mtotal = $mtotal;
		$this->total  = $rtotal + $ftotal + $mtotal;
		$this->totaldisplay =  number_format($this->total,2);
		return $totalbalance;
	}
	
	public function getTotalBalanceDisplay()
	{
		$change = 0;
		if($this->totalbalance < 0) $change = abs($this->totalbalance);
		$totalbalance_display = number_format($this->totalbalance,2);
		return $totalbalance_display;
	}
	
	
	public function getFnbSales()
	{
		$ftotal = 0;
		$sql = " select a.fnbsales_id, a.order_code, a.category_id,  a.item_id, c.food_category_name, b.fnb_code,b.fnb_name,
		a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost, a.status,a.sales_date
		from fnb_sales a, fnb b, food_categories c		
		where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.charge_to=$this->occupancy and a.status in ('Printed','Paid') ";

		$res = R::getAll($sql);
		$fsales ="<table width='300' cellpadding=1 cellspacing=1>";
		$oldoc="";
		foreach ($res as $r) {
			$sid = $r['fnbsales_id'];
			$oc = $r['order_code'];
			$catid = $r['category_id'];
			$itemid  = $r['item_id'];
			$catname  = $r['food_category_name'];
			$code  = $r['fnb_code'];
			$itemdesc  = $r['fnb_name'];
			$unitcost = $r['unit_cost'];
			$qty = $r['qty'];
			$totalcost  = $r['totalcost'];
			$status = $r['status'];
			$date = $r['sales_date'];

			if($oldoc!=$oc) {
				$date = date('m-d H:i',strtotime($date));
				$fsales .= "<tr><td colspan=4 style='border-bottom:1px dotted #cccccc;'><span style='font-style:italic'>$date: Order Slip#: $oc</span></td></tr>";
			}
			$fsales .= "<tr><td>&nbsp;</td>";
			$fsales.="<td>$itemdesc</td><td>$totalcost</td>";
			$fsales.="<td>$status</td>";
			$fsales .= "</tr>";
			$ftotal += $totalcost;
			$oldoc=$oc;
		}
		$fsales .= "<tr><td>&nbsp;</td><td>Total:</td><td style='text-align:right;border-top:1px solid #000000;border-bottom:2px solid #000000;'>".number_format($ftotal,2)."</td></tr>";
		$fsales .="</table>";
		
		return $fsales;
	}
	
	public function getMiscSales()
	{
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.order_code, a.category_id, a.item_id, b.sas_description, a.unit_cost*a.qty as cost,a.status,a.sales_date
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (1, 2, 4) and a.charge_to=$this->occupancy and a.status  in ('Printed','Paid')  ";

		$res = R::getAll($sql);

		$msales ="<table width='300' cellpadding=1 cellspacing=1>";
		$oldoc = "";
		foreach ($res as $r) {
			$rsid = $r['roomsales_id'];
			$oc = $r['order_code'];
			$catid = $r['category_id'];
			$itemid = $r['item_id'];
			$itemdesc = $r['sas_description'];
			$cost = $r['cost'];
			$stat = $r['status'];
			$date = $r['sales_date'];

			if($oldoc!=$oc) {
				$date = date('m-d H:i',strtotime($date));
				$msales .= "<tr><td colspan=4 style='border-bottom:1px dotted #cccccc;'><span style='font-style:italic'>$date: Order Slip#: $oc</span></td></tr>";
			}
			$msales .= "<tr><td>&nbsp;</td>";
			$msales.="<td>$itemdesc</td><td style='text-align:right'>".number_format($cost,2)."</td>";
			$msales.="<td>$stat</td>";
			$msales .= "</tr>";
			$mtotal += $cost;
			$oldoc=$oc;
		}
		$msales .= "<tr><td>&nbsp;</td><td>Total:</td><td style='text-align:right;border-top:1px solid #000000;border-bottom:2px solid #000000;'>".number_format($mtotal,2)."</td></tr>";
		$msales .="</table>";
		
		return $msales;
	}
	
	
	public function getRoomCharges()
	{
		//retrieve room sales
		$now = date("Y-m-d H:i:s");
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost * a.qty as cost,a.update_date
				from room_sales a, sales_and_services b	
				where a.item_id=b.sas_id and a.category_id=3 and a.charge_to=$this->occupancy and a.order_code=0 and a.sales_date <= '$now' ";
		$res = R::getAll($sql);
		$rtotal = 0;

		foreach ($res as $r) {
			$rsid = $r['roomsales_id'];
			$catid = $r['category_id'];
			$itemid = $r['item_id'];
			$itemdesc = $r['sas_description'];
			$cost = $r['cost'];
			$update_date = $r['update_date'];

			$date = date('m-d H:i',strtotime($update_date));

			$rsales .= "<tr>";
			$rsales.="<td>&nbsp;</td><td width='150'>$date $itemdesc</td><td style='text-align:right'>".number_format($cost,2)."</td><td>&nbsp;</td>";
			$rsales .= "</tr>";
			$rtotal += $cost;
		}
		
		$rtotal_f =number_format($rtotal,2);
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'>
		<tr><th colspan=4>Room Charges</th></tr>
		<tr><th colspan=4>Room Rate: {$this->publishrate}</th></tr>
		$rsales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$rtotal_f</td></tr></table>";
		
		return $retval;
		
		
	}
	
	public function getPayments()
	{
		$ptotal = 0;
		$sql  = "select tendertype, amount,receipt_date from salesreceipts where occupancy_id='$this->occupancy' ";
		$res = R::getAll($sql);

		$payments = "";
		foreach ($res as $r) {
			$type = $r['tendertype'];
			$amount = $r['amount'];
			$date = $r['receipt_date'];
			$date = date('m-d H:i',strtotime($date));
			$payments .= "<tr>";
			$payments.="<td>$date&nbsp;</td><td width='150'>$type</td><td style='text-align:right'>".number_format($amount,2)."</td><td>&nbsp;</td>";
			$payments .= "</tr>";
			$ptotal += $amount;
		}
		
		$payments_f = number_format($ptotal,2);
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'><tr>
		<th colspan=4>Deposit/Payments</th></tr>
		$payments
		<tr><td>&nbsp;</td><td colspan=2 style='font-weight:bold;font-size:.7em;'>Total of all payments:</td><td  style='text-align:right;font-weight:bold;font-size:.7em;'>$payments_f</td></tr>
		</table>";
		
		return $retval;
	}
	public function getRoomSales()
	{
		//retrieve room sales
		$now = date("Y-m-d H:i:s");
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, (a.unit_cost * a.qty) as cost, a.sales_date
				from room_sales a, sales_and_services b	
				where a.item_id=b.sas_id and a.category_id=3 and a.charge_to=$this->occupancy  and a.order_code=0 and a.sales_date <= '$now' ";

		$res = R::getAll($sql);
		$rtotal = 0;
		$rsales = '';

		foreach ($res as $r) {
			$date = date('m-d H:i',strtotime($r['sales_date']));
			$rsales .= "<tr>";
			$rsales.="<td>&nbsp;</td><td width='150'>$date: {$r['sas_description']}</td><td style='text-align:right'>".number_format($r['cost'],2)."</td><td>&nbsp;</td>";
			$rsales .= "</tr>";
			$rtotal += $r['cost'];
		}
		
		
		$ptotal = 0;
		$sql  = "select tendertype, amount,receipt_date from salesreceipts where occupancy_id='$this->occupancy'  and reference_id=0";

		$res2 = R::getAll($sql);

		$payments = "";
		
		foreach ($res2 as $r2) {
			$date = date('m-d H:i',strtotime($r2['receipt_date']));
			$amount = $r2['amount'];
			$type = $r2['tendertype'];
			$payments .= "<tr>";
			$payments.="<td>&nbsp;</td><td width='150'>$date: $type</td><td style='text-align:right'>".number_format($amount,2)."</td><td>&nbsp;</td>";
			$payments .= "</tr>";
			$ptotal += $amount;
		}
		

		$rtotal_f =number_format($rtotal,2);
		$payments_f = number_format($ptotal,2);
		$totalbalance = number_format($rtotal + $ftotal + $mtotal - $ptotal,2);
		
		$retval = "
			<table border=0 cellpadding=2 cellspacing=2 style='margin-left:10px;margin-top:10px'>
			<tr><th colspan=4>Room Charges</th></tr>
			<tr><th colspan=4>Room Rate: {$this->publishrate}</th></tr>
			$rsales
			<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$rtotal_f</td></tr>
			<tr><th colspan=4>Deposit/Payments</th></tr>
			$payments
			<tr><td>&nbsp;</td><td colspan=2>Sub Total:</td><td style='text-align:right'>$payments_f</td></tr>
			</table>
			";
			
	
		return $retval;  
	}
	
	public function getCheckoutFnbSales()
	{
		//retrieve fnb sales
		$ftotal = 0;
		$sql = " select a.fnbsales_id, a.category_id,  a.item_id,
 				c.food_category_name, b.fnb_code,b.fnb_name,
 				a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost, a.update_date
				from fnb_sales a, fnb b, food_categories c		
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.charge_to=$this->occupancy and a.status  in ('Paid','Printed') ";

		$res = R::getAll($sql);

		foreach ($res as $r) {
			$sid = $r['fnbsales_id'];
			$catid = $r['category_id'];
			$itemid = $r['item_id'];
			$catname  = $r['food_category_name'];
			$code = $r['fnb_code'];
			$itemdesc  = $r['fnb_name'];
			$unitcost = $r['unit_cost'];
			$qty = $r['qty'];
			$ftcost = $r['totalcost'];
			$date  = $r['update_date'];
			$date = date('m-d H:i',strtotime($date));
			$fsales .= "<tr>";
			$fsales.="<td>&nbsp;</td><td width='150'>$date $itemdesc</td><td style='text-align:right'>".number_format($ftcost,2)."</td><td>&nbsp;</td>";
			$fsales .= "</tr>";
			$ftotal += $ftcost;
		}
		$ftotal_f =number_format($ftotal,2);
		
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'><tr>
		<th colspan=4>Food and Beverage Sales</th></tr>
		$fsales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$ftotal_f</td></tr></table>";
	
		
		return $retval;	
	}
	
	public function getCheckoutFnbBalance()
	{
		//retrieve fnb sales
		$ftotal = 0;
		$sql = " select a.fnbsales_id, a.category_id,  a.item_id, c.food_category_name,
 				b.fnb_code,b.fnb_name, a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost
				from fnb_sales a, fnb b, food_categories c		
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.charge_to=$this->occupancy and a.status  in ('Printed') ";
		$res = R::getAll($sql);

		foreach ($res as $r) {
			$sid = $r['fnbsales_id'];
			$catid = $r['category_id'];
			$itemid = $r['item_id'];
			$catname = $r['food_category_name'];
			$code = $r['fnb_code'];
			$itemdesc = $r['fnb_name'];
			$unitcost = $r['unit_cost'];
			$qty = $r['qty'];
			$ftcost = $r['totalcost'];
			$fsales .= "<tr>";
			$fsales.="<td width='10px'><input type='checkbox' name='fnb_cb[]' checked=true value='$sid' id='fnb_cb$sid'/>
						</td><td width='65%'><label for='fnb_cb$sid'>$itemdesc</label></td><td style='text-align:right' width='30%'>$ftcost</td>";
			$fsales .= "</tr>";
			$ftotal += $ftcost;
		}
		$ftotal_f =number_format($ftotal,2);
		
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'><tr>
		<th colspan=4>Food and Beverage Sales</th></tr>
		$fsales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$ftotal_f</td></tr></table>";
	
		$retval = "<table style='font-size:11px;width:100%;' border=0>$fsales</table>";
		
		if($ftotal > 0) {
			return $retval;
			//return "<div>Food & Beverage Sales: <span style='float:right'>$ftotal_f</span></div>";	

		}
	}

	public function getCheckoutMosBalance()
	{
		//retrieve fnb sales
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description,
 				a.unit_cost * a.qty as cost
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (1, 2,4) and a.charge_to=$this->occupancy and a.status  in ('Printed') ";
		$res = R::getAll($sql);

		foreach ($res as $r) {
			$rsid = $r['roomsales_id'];
			$catid = $r['category_id'];
			$itemid = $r['item_id'];
			$itemdesc= $r['sas_description'];
			$cost= $r['cost'];
			$msales.= "<tr>";
			//$msales.="<td>&nbsp;</td><td width='150'>$itemdesc</td><td style='text-align:right'>".number_format($cost,2)."</td><td>&nbsp;</td>";
			$msales.="<td width='10px'><input type='checkbox' name='misc_cb[]' checked=true value='$rsid' id='misc_cb$rsid'/>
						</td><td width='65%'><label for='misc_cb$rsid'>$itemdesc</label></td><td style='text-align:right' width='30%'>$cost</td>";
			
			$msales.= "</tr>";
			$mtotal += $cost;
			
		}
		$mtotal_f =number_format($mtotal,2);
		
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'><tr>
		<th colspan=4>Miscellaneous Sales</th></tr>$msales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$mtotal_f</td></tr></table>";
		$retval = "<table style='font-size:11px;width:100%;' border=0>$msales</table>";
		
		if($mtotal > 0) {
			return $retval;
			//return "<div>Miscellaneous Sales: <span style='float:right'>$mtotal_f</span></div>";
		}
	}

	public function checkRegistryEntry($shift)
	{
		$now = date('Y-m-d');
		$sql = "select count(*) from fcprintlog where regdate='$now' and regshift='$shift' ";
		
		$prints = R::getCell($sql);

		if ($prints == 0) {
			return '<strong>* Please complete registry entries</strong>';
		}
	}

	public function getCheckoutRoomBalance()
	{
		//retrieve fnb sales
		$now = date("Y-m-d H:i:s");
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.category_id, a.item_id,
 				b.sas_description, a.unit_cost,a.sales_date
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (3) and a.charge_to=$this->occupancy and a.status  in ('Draft')  ";
		$res = R::getAll($sql);

		foreach ($res as $r)  {
			$rsid = $r['roomsales_id'];
			$catid = $r['category_id'];
			$itemid = $r['item_id'];
			$itemdesc = $r['sas_description'];
			$cost = $r['unit_cost'];
			$date = $r['sales_date'];
			$date = date('m-d H:i',strtotime($date));
			$msales.= "<tr>";
			//$msales.="<td>&nbsp;</td><td width='150'>$itemdesc</td><td style='text-align:right'>".number_format($cost,2)."</td><td>&nbsp;</td>";
			$msales.="<td width='10px'><input type='checkbox' name='room_cb[]' value='$rsid' checked=true id='room_cb$rsid'/>
						</td><td width='65%'><label for='room_cb$rsid'>$itemdesc - Adv. ($date)</label></td><td style='text-align:right' width='30%'>$cost</td>";
			
			$msales.= "</tr>";
			$mtotal += $cost;
			
		}
		$mtotal_f =number_format($mtotal,2);
	
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'><tr>
		<th colspan=4>Room Sales</th></tr>
		<tr><th colspan=4>Room Rate: {$this->publishrate}</th></tr>
		$msales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$mtotal_f</td></tr></table>";
		$retval = "<table style='font-size:11px;width:100%;' border=0>$msales</table>";
		
		if ($mtotal != 0) return $retval; 
			//return "<div>Room Sales: <span style='float:right'>$mtotal_f</span></div>";	

		
		
	}

	public function getCheckoutMosSales()
	{
		//retrieve fnb sales
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.category_id, a.item_id,
 				b.sas_description, a.unit_cost * a.qty as cost, a.update_date
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (1, 2,4) and a.charge_to=$this->occupancy and a.status  in ('Paid','Printed') ";
		$res = R::getAll($sql);

		foreach ($res as $r) {
			$rsid = $r['roomsales_id'];
			$catid = $r['category_id'];
			$itemid = $r['item_id'];
			$itemdesc = $r['sas_description'];
			$cost = $r['cost'];
			$date = $r['update_date'];

			$date = date('m-d H:i',strtotime($date));
			$msales.= "<tr>";
			$msales.="<td>&nbsp;</td><td width='150'>$date $itemdesc</td><td style='text-align:right'>".number_format($cost,2)."</td><td>&nbsp;</td>";
			$msales.= "</tr>";
			$mtotal += $cost;
			
		}
		$mtotal_f =number_format($mtotal,2);
		
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'><tr>
		<th colspan=4>Miscellaneous Sales</th></tr>$msales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$mtotal_f</td></tr></table>";
		return $retval;	
		
	}
	
	public function displayTotalBalance()
	{
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'>
		<tr><td colspan=3 style='font-weight:bold;font-size:.7em;color:#ff0000;'>Total Balance:</td><td style='text-align:right;font-weight:bold;font-size:.7em;color:#ff0000;'>$this->totalbalance_display</td></tr>
		</table>";
		
		return $retval; 
	}
	
	public function displayTotal()
	{
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'>
		<tr><td colspan=3 style='font-weight:bold;font-size:.7em;color:black;'>Total:</td><td style='text-align:right;font-weight:bold;font-size:.7em;color:black;'>$this->totaldisplay</td></tr>
		</table>";
		
		return $retval; 
	}
	public function getCheckoutSoa()
	{
		
	}

    public function getPublishRate()
	{
	    $sql = "select a.publishrate, a.amount 
	        from room_type_rates a, occupancy b, rooms c
	        where c.room_type_id = a.room_type_id
	        and b.room_id = c.room_id
	        and a.rate_id = b.rate_id
	        and b.occupancy_id = '{$this->occupancy}'
	        ";
	    $rates = R::getRow($sql);
	    return ($rates['publishrate'] > 0) ? $rates['publishrate'] : $rates['amount'];
	}

	/**
		taken from remarks table, $class is remarks_classification field
		$class: 1 = discounts, 2 = transfers , 3= adjustment, 4=checkout
	**/
	public function getdropdownremarks($class)
	{
		$sql = " select remark_text 
				from remarks 
				where remark_classification='$class'
				order by remark_text ";
		$res = R::getAll($sql);
		$retval="";
		if(!empty($res)) {
			$retval="<select name='new_remark' id='new_remark_$class' class='adjustelement'>";
			$retval.="<option></option>";
			foreach ($res as $r) {
				$rem = $r['remark_text'];
				$retval.="<option value='$rem'>$rem</option>";
			}
			$retval.="</select>";
		}
		return $retval;
	}

	public function printreportfile($file) {
		shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
	}

	public function printablesoa() 
	{
		$this->getTotalBalance();
		$retval=$this->printableheader("Statement of Account");	
		$retval.="\nRoom Sales: \t\t\t " . number_format($this->rtotal,2);
		$retval.="\nFood Sales: \t\t\t " .number_format($this->ftotal,2);
		$retval.="\nMiscellaneous Sales: \t\t\t " .number_format($this->mtotal,2);
		$paytotal = number_format($this->total,2);
		$retval.="
Please pay this Amount:\t\t $paytotal

	Thank you and come again.
	This is not a receipt.

Please ask your original receipt.

";
		return $retval;
	}

	public function printablefinalsoa() 
	{
		$this->getTotalBalance();
		$retval=$this->printableheader("Statement of Account");	
		$retval.="\nRoom Sales: \t\t\t " . number_format($this->rtotal,2);
		$retval.="\nFood Sales: \t\t\t " .number_format($this->ftotal,2);
		$retval.="\nMiscellaneous Sales: \t\t\t " .number_format($this->mtotal,2);
		$paytotal = number_format($this->total,2);
		$retval.="
Total:\t\t\t\t $paytotal

	Thank you and come again.
	This is not a receipt.

Please ask your original receipt.

";
		return $retval;
	}

	public function printableCheckoutsoa() 
	{
		$this->getTotalBalance();
		$retval=$this->printablecheckoutheader("Statement of Account");	
		$retval.="\nRoom Sales: \t\t\t " . number_format($this->rtotal,2);
		$retval.="\nFood Sales: \t\t\t " .number_format($this->ftotal,2);
		$retval.="\nMiscellaneous Sales: \t\t\t " .number_format($this->mtotal,2);
		$paytotal = number_format($this->total,2);
		$retval.="
Total:\t\t\t\t $paytotal

	Thank you and come again.
	This is not a receipt.

Please ask your original receipt.

";
		return $retval;
	}

	public function printableros($hdr=1) {
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.category_id, a.item_id,
 				b.sas_description, a.unit_cost * a.qty as cost
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (3) and a.charge_to=$this->occupancy and a.status  in ('Paid','Draft') ";
		$res = R::getAll($sql);
		$now = date("Y-m-d H:i:s");
		if($hdr==1) $msales = $this->printableheader("Room Sales");
		
		foreach ($res as $r) {
			$rsid = $r['roomsales_id'];
			$catid = $r['category_id'];
			$itemid = $r['item_id'];
			$itemdesc = $r['sas_description'];
			$cost = $r['cost'];
			$msales.="$itemdesc:\t\t\t ".number_format($cost,2)."\n";
			$mtotal += $cost;
		}
		$mtotal_f =number_format($mtotal,2);
		$msales .= "Total:\t\t\t\t $mtotal_f \n";
		$retval = $msales;
		return $retval;	
	}

	public function printablefos($hdr=1,$ordercode="") 
	{
		//print miscellaneous sales
		if($hdr==1) $msales = $this->printableheader("Food Order Stub");
		$mtotal = 0;
		$ftotal = 0;

		$sql = " select a.fnbsales_id, a.category_id,  a.item_id, c.food_category_name,
 				b.fnb_code,b.fnb_name, a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost
				from fnb_sales a, fnb b, food_categories c		
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.charge_to=$this->occupancy and a.status  in ('Paid','Printed') ";
		if(trim($ordercode)!="") 
		{
			$sql .= " and a.order_code='$ordercode' ";

			$msales .="Order Slip# : $ordercode \n";
		}
		$res = R::getAll($sql);
		
		$now = date("Y-m-d H:i:s");	
		foreach ($res as $r) {
			$sid = $r['fnbsales_id'];
			$catid = $r['category_id'];
			$itemid = $r['item_id'];
			$catname = $r['food_category_name'];
			$code = $r['fnb_code'];
			$itemdesc = $r['fnb_name'];
			$unitcost = $r['unit_cost'];
			$qty = $r['qty'];
			$ftcost = $r['totalcost'];
			$msales.="$itemdesc:\t $qty  $unitcost  ".number_format($ftcost,2)."\n";
			$mtotal += $ftcost;
		}
		$mtotal_f =number_format($mtotal,2);
		$msales .= "Total:\t\t\t\t $mtotal_f \n\n";
		$retval = $msales;

		return $retval;	
	}
	
	public function printablefosreplenish($hdr=1,$ordercode="") 
	{
		//print miscellaneous sales
		if($hdr==1) $msales = $this->printableheader("Food Order Stub - Mini Ref Replenish");
		$mtotal = 0;
		$ftotal = 0;

		$sql = " select a.fnbsales_id, a.category_id,  a.item_id, c.food_category_name,
 				b.fnb_code,b.fnb_name, a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost
				from fnb_sales a, fnb b, food_categories c		
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.charge_to=$this->occupancy and a.status  in ('Paid','Printed') ";
		if(trim($ordercode)!="") 
		{
			$sql .= " and a.order_code='$ordercode' ";

			$msales .="Order Slip# : $ordercode \n";
		}
		$res = R::getAll($sql);
		
		$now = date("Y-m-d H:i:s");

		foreach ($res as $r) {
			$sid = $r['fnbsales_id'];
			$catid = $r['category_id'];
			$itemid = $r['item_id'];
			$catname = $r['food_category_name'];
			$code = $r['fnb_code'];
			$itemdesc = $r['fnb_name'];
			$unitcost = $r['unit_cost'];
			$qty = $r['qty'];
			$ftcost = $r['totalcost'];
			$msales.="$itemdesc:\t $qty  $unitcost  ".number_format($ftcost,2)."\n";
			$mtotal += $ftcost;
		}
		$mtotal_f =number_format($mtotal,2);
		$msales .= "Total:\t\t\t\t $mtotal_f \n\n";
		$retval = $msales;
		return $retval;	
	}

	public function printablefoscancel($items="") 
	{
		//print Food sales cancellation
		if($items!='') {
			$msales = $this->printableheader("Food Order - Cancellation");
			$mtotal = 0;
			$sql = " select a.fnbsales_id, a.category_id,  a.item_id, c.food_category_name, b.fnb_code,b.fnb_name, a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost, a.order_code
				from fnb_sales a, fnb b, food_categories c		
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.charge_to=$this->occupancy and a.fnbsales_id in ($items) and a.status  in ('Cancelled') ";
		
			$res = R::getAll($sql);
			$now = date("Y-m-d H:i:s");
			$order_code = '';
			foreach ($res as $r) {
				$sid = $r['fnbsales_id'];
				$catid = $r['category_id'];
				$itemid = $r['item_id'];
				$catname = $r['food_category_name'];
				$code = $r['fnb_code'];
				$itemdesc = $r['fnb_name'];
				$unitcost = $r['unit_cost'];
				$qty = $r['qty'];
				$ftcost = $r['totalcost'];
				$ordercode = $r['order_code'];
				$itemlist.="$itemdesc:\t $qty  $unitcost  ".number_format($ftcost,2)."\n";
				$mtotal += $ftcost;
				$order_code=$ordercode;
			}
			
			$msales .="Order Slip#: $order_code \n";
			$msales .= $itemlist;
			
			$mtotal_f =number_format($mtotal,2);
			$msales .= "Total:\t\t\t\t $mtotal_f \n\n";
			$retval = $msales;
			return $retval;	
		}
	}
	
	public function printablemoscancel($items="") 
	{
		//print miscellaneous sales
		if($items!='') {
			$msales = $this->printableheader("Miscellaneous Order - Cancellation");
			$mtotal = 0;
			$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, (a.unit_cost * a.qty) as total, a.order_code
				from room_sales a, sales_and_services b		
				where a.item_id=b.sas_id and a.category_id in (1, 2,4, 7) and a.charge_to=$this->occupancy and a.roomsales_id in ($items) and a.status  in ('Cancelled') ";

			$res = R::getAll($sql);
			$now = date("Y-m-d H:i:s");
			$order_code='';
			foreach ($res as $r) {
				$rsid = $r['roomsales_id'];
				$catid = $r['category_id'];
				$itemid = $r['item_id'];
				$itemdesc = $r['sas_description'];
				$cost = $r['total'];
				$ordercode = $r['order_code'];
				$itemlist.="$itemdesc:\t\t\t ".number_format($cost,2)."\n";
				$mtotal += $cost;
				$order_code=$ordercode;
			}
			
			$msales .="Order Slip#: $order_code \n";
			$msales .= $itemlist;
			
			$mtotal_f =number_format($mtotal,2);
			$msales .= "Total:\t\t\t\t $mtotal_f \n\n";
			$retval = $msales;
			return $retval;	
		}
	}
	public function printablemos($hdr=1,$ordercode="") 
	{
		//print miscellaneous sales
		if($hdr==1) $msales = $this->printableheader("Miscellaneous Order Stub");
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, (a.unit_cost * a.qty) as total
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (1, 2,4, 7) and a.charge_to=$this->occupancy and a.status  in ('Paid','Printed') ";
		if(trim($ordercode)!="") 
		{
			$sql .= " and a.order_code='$ordercode' ";
			$msales .="Order Slip#: $ordercode \n";
		}
		$res = R::getAll($sql);
		$now = date("Y-m-d H:i:s");
		
		foreach ($res as $r) {
			$rsid = $r['roomsales_id'];
			$catid = $r['category_id'];
			$itemid = $r['item_id'];
			$itemdesc = $r['sas_description'];
			$cost = $r['total'];
			$msales.="$itemdesc:\t\t\t ".number_format($cost,2)."\n";
			$mtotal += $cost;
		}
		$mtotal_f =number_format($mtotal,2);
		$msales .= "Total:\t\t\t\t $mtotal_f \n\n";
		$retval = $msales;
		return $retval;	
	}
	
	public function printableheader($title) {
		$now=date("Y-m-d H:i:s");
		$hdr  = "
	$title 

$this->hotel
Room No. {$this->doorname}
Date: $now
CheckIn: {$this->checkindate}
CheckOut: {$this->etd}
Shift: {$this->shift}

";
		return $hdr;
	}

	public function printablecheckoutheader($title) {
		$now=date("Y-m-d H:i:s");
		$hdr  = "
	$title 

$this->hotel
Room No. {$this->doorname}
CheckIn: {$this->checkindate}
CheckOut: {$now}
Shift: {$this->shift}

";
		return $hdr;
	}

}

?>
