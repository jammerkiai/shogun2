<?php
session_start();
if($_SESSION['hotel']['oic_ok']==false) {
	header("location: oic_login.php");
}
include_once("./config/config.inc.php");
include_once("./acctg/class.baseobject.php");
include_once("./acctg/class.report.php");

if(isset($_POST) && $_POST["submit"]=="go") {
	$doorname = $_POST["doorname"];
	$sql =" select b.actual_checkin as 'Actual CheckIn', 
			b.expected_checkout as 'Expected CheckOut', 
			b.actual_checkout as 'Actual Checkout', 
			b.shift_checkin as 'Shift Checked In',
			c.rate_name as 'Rate Upon CheckIn', 
			concat('<a href=fdadj.php?occ=', b.occupancy_id ,'>',b.occupancy_id,'</a>') as 'View Details'
			from rooms a, occupancy b, rates c 
			where 
			a.room_id=b.room_id
			and b.rate_id=c.rate_id
			and a.door_name='$doorname' 
			order by b.occupancy_id desc
			" ;

	$opts = array(
			'sql'=>$sql,
			'title'=>'Results for Room: ' . $doorname
		);
	$report = new report($opts);
	$report->buildReport();
}
?>
<html>
<head>
<title>Shogun 1 Transaction Finder</title>
<style>
	body, h1, h2, h3, h4 {
		margin:0;
		margin-width:0;
		margin-height:0;
		font-size:14px;
	}

	h1 {
		font-size:1em;
	}
	h2 {
		font-size:0.9em;
	}
		.printable, .printable2, .printable3, .printable4, .printable5 {
			font-family: sans-serif;
			font-size: 14px;
			font-weight: 550;
			border-bottom:1px dotted #cccccc;
			}
		.report th {
			padding:4px;
			text-align:center;
			background-color: #efefef;
		}
		.report{
			font-family: sans-serif;
			font-size: 13px ;
			text-align:left;
			font-weight: 550;
				
		}
		.report td{
			padding-bottom:10px;
			background-color:#f9f9f9;
			font-size:13px;
		}
		
		.summary{
			font-family: sans-serif;
			font-size: 13px;
			text-align:left;
			font-weight: 550;
		}

		.menubar {
			font-size:12px;
			font-family:arial, helvetica,sans-serif;
			font-weight:bold;
			list-style:none;
			background-color:#dddddd;
			padding:4px;
			margin:0;
		}
		.menubar li {
			display:inline;
			border:1px solid #eeeeee;
			background-color:#ececec;
			padding:2px;
			padding-left:8px;
			padding-right:8px;
		}

		.menubar li:hover {
			background-color:#ffffff;
		}

		.toolbar {
			background-color:#cccccc;
			padding:0px;
		}

		.aggregates th {
			border-top:1px solid #111111;
		}

		.recordcount {
			padding-top:24px;
			padding-bottom:12px;
		}
</style>
<script src="../js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery.print.js"></script>
</head>
<body>
<script type="text/javascript">
 
		// When the document is ready, initialize the link so
		// that when it is clicked, the printable area of the
		// page will print.
		$(document).ready(function(){
			
		});
 
</script>
<form name=myform method=post>
<div class="toolbar">

Enter Room No. <input type="text" name="doorname" id="doorname" value="<?php echo $doorname ?>" /> 
<input type="submit" name="submit" value="go" />
</div>
<?php if(is_object($report)) $report->show(); ?>
<br>
</body>
</html>