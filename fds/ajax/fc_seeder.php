<?php
include_once('config/config.inc.php');
require_once('class.forecast.php');
require_once('date.functions.php');
session_start();

if(isset($_POST) && $_POST['fr'] != '' && $_POST['to'] != '')  {

    $orig = $_POST['orig'];
    $adj = $_POST['adj'];
    $act = $_POST['act'];
    $start = new \DateTime($_POST['fr']); 
    $end = new \DateTime($_POST['to']); 

    
    while($start <= $end) {
        $thisdate = $start->format('Y-m-d');

        for ( $x=0; $x < 3; $x++ ) {
            $original = $orig[$x];
            $adjusted = $adj[$x];
            $actual = $act[$x];
            $shift =  $x + 1;

            $sql = "insert into forecast (forecast_date, shiftnum, forecast_value, actual_value, adjusted_value) 
                values ('$thisdate', '$shift', '$original', '$actual', '$adjusted')";

            R::exec($sql);
 
        }
        

        $start->add(new \DateInterval('P1D'));               
    }
    $result = "Seeded values for range " . $_POST['fr'] . ' to ' . $_POST['to'];
}

?>

<html>
<head>
<title></title>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<style>
body {font-family:helvetica;margin:0px;}
table {font-size:12px;font-family:helvetica;border:1px transparent #000;margin: 0 4px;}
th {width:auto; border:1px solid #cccccc;padding: 2px 6px}
.totals {color:#00F;font-weight:bold;}
td {border-bottom:1px solid #cccccc; text-align:center; padding: 2px; }
.amt {text-align:right}
h3 {font-size:12px;}
.menubar {background-color:#eeeeee;font-size:11px;padding:4px;border-bottom:1px solid #cccccc;}
.content {padding:10px; font-size:11px}
.message {display:inline; color:#ff0000;}
.targets {color: orange;}
.actuals {color: green;}
.menutitle {font-weight: bold;}
.origfc {cursor: pointer; background-color: #eee;}
</style>
</head>
<body>
<form method="post" action="fc_seeder.php">
<div class="menubar">
<span class="menutitle">Shogun 2 Forecast Seeder for </span>
<input type="date" name="fr" value="" id="fr" required>
<input type="date" name="to" value="" id="to" required>
<input type="submit" name="submit" value="Seed Now" />
<div class="message"><?php echo $result ?></div>
</div>
<table>
<thead>
<tr>
<th>Shift #</th>
<th>Original</th>
<th>Adjusted</th>
<th>Actual</th>
</tr>
</thead>
<tbody>
<tr>
<td>
    1
</td>
<td>
    <input type="number" name="orig[]" value="0" />
</td>
<td>
    <input type="number" name="adj[]" value="0" />
</td>
<td>
    <input type="number" name="act[]" value="0" />
</td>
</tr>
<tr>
<td>2</td>
<td>
    <input type="number" name="orig[]" value="0" />
</td>
<td>
    <input type="number" name="adj[]" value="0" />
</td>
<td>
    <input type="number" name="act[]" value="0" />
</td>
</tr>
<tr>
<td>3</td>
<td>
    <input type="number" name="orig[]" value="0" />
</td>
<td>
    <input type="number" name="adj[]" value="0" />
</td>
<td>
    <input type="number" name="act[]" value="0" />
</td>
</tr>
</tbody>
</table>
</div>
</form>
<script>

</script>
</body>
</html>
