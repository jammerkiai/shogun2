<?php
//daily.php

require_once('../../config/config.inc.php');

?>
<link rel="stylesheet" type="text/css" href="../../../css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css" href="../../../css/bootstrap.min.css" />
<script src="../../../js/jquery.js"></script>
<script src="../../../js/bootstrap.min.js"></script>
<style>
body, tr, td, th {
	font-size: small;
}
</style>
<body>
<table class="table table-hover table-condensed">
<thead>
<tr class="active"><th colspan="6"><div class="btn btn-primary">Currently Checked-In Guests Found: 
<span class="badge"><?php echo count($guests) ?></span></div>
</th></tr>
<tr class="info">
<th>#</th>
<th>First Name</th>
<th>Last Name</th>
<th>Room No.</th>
<th>Check-In</th>
<th>Check-Out</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</body>

