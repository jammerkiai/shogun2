<?php
/**
* reserve.functions.php
*/

function getreservedbyguest($gid=0,$fn='',$mn='',$ln=''){
	$now = date('Y-m-d');
	$sql = " select a.reserve_code, a.reserve_date, a.reserve_fee, a.pax, a.status, b.firstname,  b.middlename, b.lastname
			from reservations a, guests b where a.status='Active' and a.reserve_date >= '$now' and a.guest_id=b.guest_id ";
	if($gid) $sql.=" and b.guest_id='$gid' ";	
	if($fn) $sql.=" and b.firstname = '$fn' ";
	if($mn) $sql.=" and b.middlename = '$mn' ";
	if($ln) $sql.=" and b.lastname = '$ln' ";
	
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res)) {
		$ret = "<table id='guestreservetable'>";
		$ret.="<tr><th>RC#</th><th>Date</th><th>Guest Name</th><th>Fee</th><th>Pax</th><th>Status</th></tr>";
		while(list($code,$date,$fee,$pax,$stat,$f,$m,$l)=mysql_fetch_row($res)) {
			$ret.="<tr>";
			$ret.="<td><a href='reserve.php?code=$code'>$code</a></td>";
			$ret.="<td>$date</td>";
			$ret.="<td>$f $m $l</td>";
			$ret.="<td>$fee</td>";
			$ret.="<td>$pax</td>";
			$ret.="<td>$stat</td>";
			$ret.="</tr>";
		}
		$ret .= "</table>";
	}else{
		$ret = "<span >no reservations found</span>";
	}
	return $ret ;
}

function getall() {
	$sql = "select rooms.room_id, rooms.door_name, room_types.room_type_name, themes.theme_name
		from rooms left join room_types on rooms.room_type_id=room_types.room_type_id
		left join themes on rooms.theme_id=themes.theme_id
		where rooms.site_id=2";
	$res = mysql_query($sql) or die(mysql_error());
	while(list($rid,$door,$type,$theme)=mysql_fetch_row($res)) {
		$retval.="<tr>";
		$retval.="<td>$rid</td>";
		$retval.="<td>$door</td>";
		$retval.="<td>$type</td>";
		$retval.="<td>$theme</td>";
		$retval.="</tr>";
	}
	return $retval;
}



function getroomtypes($selected='') {
	$sql = "select distinct room_type_id, room_type_name from room_types order by room_type_name ";
	$sql = "select distinct room_types.room_type_id, room_types.room_type_name
		from room_types  left join rooms on room_types.room_type_id=rooms.room_type_id
		where rooms.site_id=2";
	
	$res = mysql_query($sql) or die(mysql_error());
	while(list($id, $name)=mysql_fetch_row($res)) {
		$retval.="<option value='$id'";
		$retval.= ($id==$selected) ? " selected " : "";
		$retval.=">$name</option>";
	}
	return $retval;
}


function getthemes($selected='') {
	$sql = "select theme_id, theme_name from themes order by theme_name ";
	$res = mysql_query($sql) or die(mysql_error());
	while(list($id, $name)=mysql_fetch_row($res)) {
		$retval.="<option value='$id'";
		$retval.= ($id==$selected) ? " selected " : "";
		$retval.=">$name</option>";
	}
	return $retval;
}

function getrooms($roomtype, $theme=0) {	
	$sql = "select distinct rooms.room_id, rooms.door_name, themes.theme_name
		from rooms left join themes on rooms.theme_id=themes.theme_id
		where   rooms.site_id=2";
	if($roomtype) $sql.=" and rooms.room_type_id='$roomtype'";	
	elseif($theme) $sql.=" and rooms.theme_id='$theme'";	
	
	$res = mysql_query($sql) or die(mysql_error());
	while(list($id, $name, $theme)=mysql_fetch_row($res)) {
		$retval.="<option value='$id'";
		$retval.= ($id==$selected) ? " selected " : "";
		$retval.=">$name </option>";
	}
	return $retval;
}

function getnextreservecode($pre=1) {
	return $pre . "-" . mktime();
}


function getreservedroomsbycode($code,$status) {
	$sql = 
		" 
		select rooms.room_id, rooms.door_name, room_types.room_type_name, 
		themes.theme_name,reserve_rooms.reserve_code, reserve_rooms.checkin, 
		reserve_rooms.checkout,reserve_rooms.rr_id, reserve_rooms.deposit, reserve_rooms.status
		from rooms 
		left join reserve_rooms on rooms.room_id=reserve_rooms.room_id 
		left join room_types on rooms.room_type_id=room_types.room_type_id
		left join themes on rooms.theme_id=themes.theme_id
		where rooms.site_id=2 and reserve_rooms.reserve_code='$code'
		";
		$res=mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res)) {	
		$ret = "
		<input type='button' class='cmdbtn' id='delroom' value='Delete' />
		<input type='hidden' id='hiddencode' value='$code' /><input type='hidden' id='hiddenroom' />
		<table id='guestreservetable'>";
		$ret.="<tr><th>&nbsp;</th><th style='width:100px'>Days Reserved</th><th>Room Reserved</th><th>Deposit</th><th>Action</th></tr>";
		
		while(list($room,$door,$rtype,$theme,$code,$in,$out,$rrid,$deposit,$rstatus)=mysql_fetch_row($res)) {
			$ret .= "<tr>";
			$ret .= "<td><input type='checkbox' name='editroom[]' id='rm_$room' value='$rrid'></td>
				<td><label for='rm_$room'>
				<input type='hidden' name='edit_checkin_$rrid' id='edit_ci_$room' value='$in' class='roomdate' />Fr: $in<br>To: $out</td>
				<td>$door</td>
				<td>$deposit<input type='hidden' name='edit_deposit_$rrid' id='edit_deposit_$rrid' value='$deposit' /></td><td>";
			if($rstatus=='Claimed' || $rstatus=='Cancelled') {
				$ret .= "$rstatus";
			}else{
				$ret .=	getAvailableRoomsDropdown($room);
				if ($status != 'Cancelled') {
					$ret .= "<input type='button' id='checkinhere_$rrid' class='checkinhere' value='Check In' />";
					$ret .= "<input type='button' id='checkinconvert_$rrid' class='checkinconvert' value='Check In+Convert' />";
				}
			}
			
			$ret .= "</td></tr>";
		}
		$ret .= "</table>";
	}else{
		if ($status == 'Cancelled') {
			$ret = "<span style='font-size:.7em;color:#ff0000'>This reservation has been cancelled.</span>";
		} else {
			$ret = "<span style='font-size:.7em;color:#ff0000'>Select Room &nbsp;&nbsp;	</span>";
			$ret .=	getAvailableRoomsDropdown($room);
			$ret .= "<input type='button' id='checkinhere' value='Check In' />
			<input type='hidden' id='hiddencode' value='$code' /><input type='hidden' id='hiddenroom' />";
		}
	}
	return $ret;
}


function getreservedroomsbydate($date) {
		$sql = 
		" 
		select rooms.room_id, rooms.door_name, room_types.room_type_name, 
		themes.theme_name,reserve_rooms.reserve_code, 
		reserve_rooms.checkin, reserve_rooms.checkout,rooms.status
		from rooms 
		left join room_types on rooms.room_type_id=room_types.room_type_id
		left join themes on rooms.theme_id=themes.theme_id
		left join reserve_rooms on rooms.room_id=reserve_rooms.room_id
		where rooms.site_id=2 ";
		$sql.=" and rooms.room_id in (select room_id from reserve_rooms  where checkin='$date' )";
	
	
	$res=mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res)) {
	$ret = "<table id='guestreservetable'>";
	$ret.="<tr><th>&nbsp;</th><th>Room</th><th>Type</th><th>Theme</th><th>Checkin</th><th>Checkout</th><th>Reservation Code</th></tr>";
	while(list($room,$door,$rtype,$theme,$code,$in,$out)=mysql_fetch_row($res)) {
		$ret .= "<tr>";
		$ret .= "<td><input type='checkbox' name='addroom[]' id='rm_$room' value='$room'></td>
			<td><label for='rm_$room'> $door</td>
			<td> $rtype</td>
			<td> $theme</td><td>$in</td><td>$out</td><td><a href='reserve.php?code=$code'>$code</a></td>";
		$ret .= "</tr>";
	}
	$ret .= "</table>";
	}else{
		$ret = "<span style='font-size:.7em;color:#ff0000'>no reservations found</span>";
	}
	return $ret;
}

function is_reserved($room,$date) {
	$sql ="select rr_id from reserve_rooms where checkin<='$date' and checkout>='$date' and room_id='$room' and status='Pending'";
	$res = mysql_query($sql) or die(mysql_error());
	if( mysql_num_rows($res)) return true;
	else return false;
}

function is_cancelled($room,$date) {
	$sql ="select rr_id from reserve_rooms where checkin<='$date' and checkout>='$date' and room_id='$room' and status='Cancelled'";
	$res = mysql_query($sql) or die(mysql_error());
	if( mysql_num_rows($res)) {
		return true;
	} else {
		$sql = "select a.status from reservations a, reserve_rooms b 
			where a.reserve_code=b.reserve_code and b.checkin<='$date' and b.checkout>='$date' 
			and b.room_id='$room' and a.status='Cancelled'";
		$res = mysql_query($sql) or die(mysql_error());
		if (mysql_num_rows($res)) {
			$row = mysql_fetch_row($res);
			if ($row[0] == 'Cancelled') {
				return true;
			}
		}
	}
	return false;
}

function is_occupied($room, $date) {
	$now = date('Y-m-d');
	$sql ="select status from rooms where room_id='$room' and date_format(last_update,'%Y-%m-%d')<='$date' and '$now' >= '$date' ";
	$res = mysql_query($sql) or die(mysql_error());
	list($stat)=mysql_fetch_row($res);
	if ($stat==2) return true;
	return false;
}

/**
* $filter = actual roomtype or theme_id passed
* $type= none='', roomtype=1, theme=2
*/
function getavailable($date, $filter='', $type='', $from='', $to='') {
	$sql = 
	" 
	select rooms.room_id, rooms.door_name, room_types.room_type_name,rooms.status
	from rooms 
	left join room_types on rooms.room_type_id=room_types.room_type_id
	where rooms.site_id=2 and rooms.status='1' ";
	if($type==1) {
		if($filter) $sql.=" and room_types.room_type_id='$filter' "; 
	}elseif($type==2) {
		if($filter) $sql.=" and themes.theme_id='$filter' ";
	}
	if ($from != '' and $to != '') {
		$sql.=" and rooms.room_id not in (select room_id from reserve_rooms  where checkin >= '$from' and checkin <= '$to')";	
	} else {
		$tomorrow = new DateTime($date);
		$tomorrow->add(new DateInterval('P1D'));
		$tom = $tomorrow->format('Y-m-d');
		$sql.=" and rooms.room_id not in (select room_id from reserve_rooms  where checkin >= '$date' and checkin <= '$tom')";
	}

	$res=mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res)) {
	$arr = getdaterange($date,7);
	
	$enddate = $arr[count($arr)-1];
	$ret = "<table id='guestreservetable'>";
	$ret.="<tr><th>&nbsp;</th><th>Room</th><th>Type</th><th>CheckIn</th><th>CheckOut</th><th>Deposit</th></tr>";
	while(list($room,$door,$rtype,$theme,$status)=mysql_fetch_row($res)) {
		$ret .= "<tr>";
		$ret .= "<td><input type='checkbox' name='addroom[]' id='rm_$room' value='$room'></td>
			<td><label for='rm_$room'> $door</td><td>$rtype</td>
			<td><input type='text' name='add_checkin_$room' id='add_checkin_$room' class='roomdate' value='$from'></td>
			<td><input type='text' name='add_checkout_$room' id='add_checkout_$room' class='roomdate'  value='$to'></td>
			<td><input type='text' name='add_deposit_$room' id='add_deposit_$room' style='font-size:12px;width:80px;' class=' roomdeposit keypadfield'></td>";
			//<td><input type='text' name='add_checkout_$room' id='add_co_$room' value='$tomorrow' class='roomdate' /></td>
		$ret .= "</tr>";

	}
	$ret .= "</table>";
	}else{
		$ret="<span style='font-size:.7em;color:#ff0000'>no available rooms</span>";
	}
	return $ret;
}

function getAvailableRoomsDropdown($selected='') {
	$sql = "select a.room_id, a.door_name, b.room_type_name from rooms a, room_types b 
		where a.room_type_id=b.room_type_id and a.status=1";
	$res = mysql_query($sql);
	$ret = "<select name='checkinRoom' id='checkinRoom'>";
	$ret.="<option value='0'></option>";
	while(list($rid,$rname,$rtype)=mysql_fetch_row($res)) {
		$ret .= "<option value='$rid' ";
		$ret .= ($selected==$rid) ? " selected ": "";
		$ret .= ">$rname - $rtype</option>";
	}
	$ret .="</select>";
	return $ret;
}

function getPartners($selected) {
	$sql = "select partner_id, partner_name 
			from partners
			where active = 1";
	$res = mysql_query($sql) or die(mysql_error());
	$retval = "<select name='Partner' id='Partner'>";
	$retval.= "<option value=''></option>";
	while (list($id, $name)=mysql_fetch_row($res)) {
		$retval.= "<option value='$name' ";
		$retval.= ($selected === $name) ? ' selected ' : '';
		$retval.= ">$name</option>";
	}
	$retval.= "</select>";
	return $retval;
}

function getOneValue($sql) {
	return R::getCell($sql);	
}

function getOneRow($sql) {
	return R::getRow($sql);	
}

function getRoomOptions($rtid, $selected='') {
	$sql = "select room_id, door_name from rooms where room_type_id='$rtid'";
	$res = R::getAll($sql);

	$opt = "<option value='0'>TBD</option>";
	foreach ($res as $r) {
		$id = $r['room_id'];
		$name = $r['door_name'];
		$opt.= "<option value='$id'";
		$opt .= ( $selected == $id ) ? ' selected="selected" ' : '';
		$opt.=">$name</option>";
	}
	return $opt;
}

function getRoomOptionsForCheckin($rtid, $selected='') {
	//get unoccupied rooms with this room type
	$sql = "select room_id, door_name 
			from rooms 
			where room_type_id='$rtid'
			and status=1 ";
	$res = mysql_query($sql) or die($sql);
	$num = mysql_num_rows($res);
	if( $num == 0 ) {
		//grabbing all unoccupied rooms regardless of roomtype
		$sql = "select room_id, door_name from rooms where status=1";
		$res = mysql_query($sql) or die($sql);
	}
	
	$opt = "<option value='0'>TBD</option>";
	while (list($id, $name)=mysql_fetch_row($res)) {
		$opt.= "<option value='$id'";
		$opt .= ( $selected == $id ) ? ' selected="selected" ' : '';
		$opt.=">$name</option>";
	}
	return $opt;
}

// show availability for specified room from today to nth days
function getRoomAvailabilityForeCast($room, $daycount = 1, $guest = '') {
	$today = new Datetime();
	$retval = '';
	
	for ($x = 0; $x <= $daycount; $x++) {
		$date = $today->format('m/d');
		$class = '';
		$reserveDetails = getRoomAvailableDetailsForDate($room, $today->format('Y-m-d'));
		if ($reserveDetails !== null) {
			$class = 'reserved';
			$data = "data-guest='{$reserveDetails->guest}' data-code='{$reserveDetails->reserve_code}' 
    		data-partner='{$reserveDetails->partner}' data-fee='{$reserveDetails->reserve_fee}'";
    		if ($reserveDetails->guest === $guest) {
    			$class .= ' owner';
    		}
		}
		$class .= ( is_occupiedforduration( $room, $today->format('Y-m-d')) === true ) ? ' occupied' : '';
		 
		$retval .= "<span class='availability $class' $data>" . $date . '</span>';
		$today->add(new DateInterval('P1D'));
	}
	return $retval;
}

function is_occupiedforduration($room, $date) {
	$sql2 = "select count(*) from occupancy
    	where room_id='$room'
    	and actual_checkout='0000-00-00 00:00:00' 
    	and date_format(actual_checkin,'%Y-%m-%d') <='$date' 
    	and expected_checkout >='$date' ";	
    $cnt = R::getCol($sql2)[0];
    return ($cnt > 0);
}

function getRoomAvailableDetailsForDate($room, $date) {
	$sql = "select a.reserve_code, concat(b.firstname, ' ', b.lastname) as guest, c.partner, c.reserve_fee 
			from reserve_rooms a, guests b, reservations c
			where a.reserve_code=c.reserve_code and b.guest_id=c.guest_id and 
			a.room_id='$room' and a.checkin <= '$date' and a.checkout >= '$date' and a.status='Pending' ";

	return R::getRow($sql);
}

/**
	$args = array(
		'code' => '',
		'occupancy' => '',
		'remarks' => '',
		'deposit' => '',
		'claimed' =>
	)
*/
function logReserveTransaction($args = array()) {
	$date = date('Y-m-d H:i:s');
	$code = isset($args['code']) ? $args['code'] : '';
	$occupancy = isset($args['occupancy']) ? $args['occupancy'] : '';
	$remarks = isset($args['remarks']) ? $args['remarks'] : '';
	$deposit = isset($args['deposit']) ? $args['deposit'] : 0;
	$claimed = isset($args['claimed']) ? $args['claimed'] : 0;
	$user = $_SESSION["hotel"]["userid"];
	
	$sql  = "insert into reservation_transactions 
		(transaction_date, occupancy_id, reservation_code, amount_deposit, amount_claimed, update_by, remarks) 
		values ('$date', '$occupancy', '$code', '$deposit', '$claimed', '$user', '$remarks')";
		
	mysql_query($sql) or die($sql);
}
?>