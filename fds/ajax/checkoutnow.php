<?php
//checkoutnow.php
session_start();
include_once("config/config.inc.php");

if($_POST["act"]=="checkoutnow") {
	$room = $_POST["room"];
	$sql = " select a.occupancy_id, a.actual_checkin, b.door_name, c.rate_name, a.shift_checkin, d.fullname
			from occupancy a, rooms b, rates c, users d
			where a.room_id=b.room_id and a.rate_id=c.rate_id and a.update_by=d.user_id and a.room_id=$room and actual_checkout='0000-00-00 00:00:00' ";
	$res = R::getRow($sql);

	if(!empty($res)) {
		$occupancy = $res['occupancy_id'];
		$checkindate = $res['actual_checkin'];
		$doorname = $res['door_name'];
		$ratename = $res['rate_name'];
		$shift = $res['shift_checkin'];
		$fullname = $res['fullname'];
		//update occupancy
		$now = date("Y-m-d H:i:s");
		$sql = "update occupancy set actual_checkout='$now', update_by='$user' 
				where occupancy_id='$occupancy'";
		R::exec($sql);
		
		//add occupancy log
		$sql = " insert into occupancy_log(transaction_date, occupancy_id, update_by, remarks ) value ('$now', '$occupancy', '$user', 'CheckOut' ) ";
		R::exec($sql);
		
		//update room status
		$sql = " update rooms set status=3, last_update='$now',update_by='$user' where room_id='$room' ";
		R::exec($sql);
		
		//insert payment
		//payments
		if($cash) {
			$sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount) 
				values ('$now', '$newoccupancy', 'Cash', '$cash')";
			R::exec($sql);
		}

		if($card) {
			$sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount) 
				values ('$now', '$newoccupancy', 'Card', '$card')";
			R::exec($sql);
			//insert card details
			$newsalesid = R::getInsertID();
			$sql = " insert into card_payment_details (salesreceipt_id, cardtype, transaction_number, batch_number) 
					values ('$newsalesid','$cardtype','$trxnid','$batch')";
			R::exec($sql);
		}
	}
}
?>