<?php
/*
fcmanager.php
*/
include_once('forecast.function.php');

$shift = new shift(array());
$thisshift = $shift->shiftno;
$now = date('Y-m-d');
$result = "";

if ( $_POST['submit']=='GO' && $_POST['fcdate']!='' && $_POST['fcshift']!='' ) {
	$thisshift = $_POST['fcshift'];
	$now = $_POST['fcdate'];
} elseif ( $_POST['submit']=='Run Batch Collector' && $_POST['fcdate']!='' && $_POST['fcshift']!='') {
	$result = "Batch Collector Completed.";
	$thisshift = $_POST['fcshift'];
	$now = $_POST['fcdate'];
	runfc($now, $thisshift);
}

?>
<style>
body {font-family:helvetica;margin:0px;}
table {font-size:12px;font-family:helvetica;}
th {width:auto; border-bottom:1px solid #cccccc;}
td {border-bottom:1px solid #cccccc; width:150px; text-align:center }
.amt {text-align:right}
h3 {font-size:12px;}
.menubar {background-color:#eeeeee;font-size:11px;padding:4px;border-bottom:1px solid #cccccc;}
.content {padding:10px; font-size:11px}
.message {display:inline; color:#ff0000;}
</style>
<form method="post" action=""> 
<div class="menubar">
<div style="display:inline">
Today: <input type="text" value="<?php echo $now ?>" name="fcdate" id="fcdate" />
</div>
<div style="display:inline">
Shift: <input type="text" value="<?php echo $thisshift ?>" name="fcshift" id="fcshift" />
</div>
<input type="submit" name="submit" value="GO" />
<input type="submit" name="submit" value="Run Batch Collector" />
<div class="message"><?php echo $result ?></div>
</div>
<h3>Collected</h3>
<div class="content">
<?php echo getForecastManager($now, $thisshift); ?>
</div>
<hr>
<h3>Uncollected</h3>
<div class="content">
<?php echo getTempForecastManager($now, $thisshift); ?>
</div>
</form>