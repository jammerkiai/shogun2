<?php
include_once('config/config.inc.php');
require_once('class.forecast.php');
require_once('class.salesposting.php');
session_start();

/**
get all sales for occupancy items in forecast_list where checkout tag is true
and group them by the receipt flag
*/

$today = isset($_POST['fcdate']) ? $_POST['fcdate'] : ( isset($_GET['d']) ? $_GET['d'] : date('Y-m-d') );
$shift = isset($_POST['fcshift']) ? $_POST['fcshift'] : ( isset($_GET['s']) ? $_GET['s'] : forecast::getshift() ) ;

$sp = new salesposting($today, $shift);

$sp->getCardSalesSummary();

if (isset($_POST['act'])) {
    if ($_POST['act'] === 'register') {
        
    } elseif ($_POST['act'] === 'post') {
        
    } elseif ($_POST['act'] === 'print') {
        
    }
}

?>
<html>
<head>
<title>Sales Posting</title>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<style>
body {font-family:helvetica;margin:0px;}
table {font-size:12px;font-family:helvetica;}
th {width:auto; border-bottom:1px solid #cccccc;}
.totals {color:#00F;font-weight:bold;}
td {border-bottom:1px solid #cccccc; text-align:center }
.amt {text-align:right}
h3 {font-size:12px;}
.menubar {background-color:#eeeeee;font-size:11px;padding:4px;border-bottom:1px solid #cccccc;}
.content {padding:10px; font-size:11px}
.message {display:inline; color:#ff0000;}
.targets {color: orange;}
.actuals {color: green;}
</style>
</head>
<body>
<form method="post">
<div class="menubar">
<div style="display:inline">
Today: <input type="text" value="<?php echo $today ?>" name="fcdate" id="fcdate" />
</div>
<div style="display:inline">
Shift: <input type="text" value="<?php echo $shift ?>" name="fcshift" id="fcshift" size="4" maxlength="1"/>
</div>

<input type="submit" name="submit" value="GO" />
<div class="message"><?php echo $result ?></div>
</div>

<div class="content">
<h3>Registered</h3>
<table>
<thead>
<?php
$headers = array('ID', 'Room No.', 'Checkout', 'Room Sales', 'Food Sales' , 
                'Misc Sales','Tax', 'Total Sales', 'Receipt');
foreach ($headers as $label) {
    echo "<th>$label</th>";
}
?>
</thead>
<?php 

?>
</table>

</div>
</form>
<script>

$(document).ready(function(){
    $('#fcdate').datepicker({dateFormat:'yy-mm-dd'});
    $('#postaction').click(function(e) {
        return confirm('Are you sure you want to post this total?');
    });
    
    
    if ($('#printflag').val() == 1) {
        $('#mf').attr('src', 'reports/fcsummary.html');
        window.open('reports/fcsummary.html', 'fcreport', 'toolbar=yes,menubar=yes,width=200px,height=100px');
    }
});
</script>
<iframe src="" id="mf" title="forecastsummary" style="border: 0; width: 0; height: 0;" />
</body>
</html>

