<?php
session_start();
/*
* fooddiscount.php
*/
include_once("config/config.inc.php");
include_once("common.inc.php");


$user = $_SESSION["hotel"]["userid"];

if(isset($_POST) && $_POST["submit"]=="Save") {

	$code = $_POST["order_code"];
	$occupancy = $_POST["occupancy"];
	$discount =  $_POST["newdiscount"];
	$remark = $_POST["newremark"];
	$room = $_POST["roomid"];
	$now = date('Y-m-d H:i:s');
	//discount categid = 22
	//special discount itemid=112
	$type = $_POST['newtype'];
	
	if ($type == 29) {
	    //misc discount
	    $categ = 1;
	    $discount = $discount * -1;
	    $sql  = "insert into room_sales (sales_date, order_code,
				occupancy_id, charge_to, category_id, item_id,  unit_cost,
				qty,remarks,status,update_date,update_by,
				regflag, regdate, regshift)
			    values ('$now','$code','$occupancy','$occupancy',$categ,$type,
			    '$discount',1,'$remarks','Printed','$now','$user',
			    0, '$now', '1');
				    ";
		R::exec($sql);
	} else {
	    //fnb discount
	    $categ = 22;
	    if($type==113) {
		    $categ = 21;	
	    }elseif($type==112){
		    $discount = $discount * -1;
	    }
	    if($discount != 0 or $discount !='') {
		    $sql  = "insert into fnb_sales (sales_date, order_code,
					occupancy_id, charge_to, category_id, item_id,
					unit_cost, qty,remarks,status,update_date,update_by, regflag, regdate, regshift)
			    	values ('$now','$code','$occupancy','$occupancy',
			    		$categ,$type,'$discount',1,
			    		'$remarks','Printed','$now','$user', 0, '$now', '1')
				    ";


			R::exec($sql);
	    }
	}
	header('location:order.php?roomid='.$room);
}

$code = $_GET["code"];
$occupancy = $_GET["occupancy"];
$roomid = $_GET["roomid"];
$total = $_GET["total"] ;

echo "<h3>Special Discounts:</h3>
<pre>
Order Code#: $code
Total Price: $total
</pre>";

?>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type='text/javascript' src='../js/jquery.plugin.min.js'></script>
<script type='text/javascript' src='../js/jquery.keypad.min.js'></script>
<link rel="stylesheet" type="text/css" href="../js/jquery.keypad.css" />
<form method="post" action="fooddiscount.php">
<input type="hidden" name="order_code" id="order_code" value="<?php echo $code ?>" />
<input type="hidden" id="occupancy" name="occupancy" value="<?php echo $occupancy ?>" />
<input type="hidden" id="roomid" name="roomid" value="<?php echo $roomid ?>" />
<pre>
Type:            <select name="newtype">
				<option value="112">Special Discount</option>
				<option value="113">Beer Adjustment</option>
				<option value="114">Food Adjustment</option>
				<option value="29">Misc Adjustment</option>				
				 </select>
Discount Amount: <input type="text" name="newdiscount" id="newdiscount" value="" />
Remarks:         <input type="text" name="newremark" id="newremark" value="" />
</pre>
<input type="submit" name="submit" id="cmdbtn" value="Save" />
<input type="button" name="newcancel" id="newcancel" value="Cancel" />
<table cellpadding=5 cellspacing=5 class="formtable">
<tr>
<td valign="top">Supervisor/OIC:</td>
<td>
Username:<br>
<?php
echo getSupervisorDropdown('new_user');
?>
<input type="button" name="cmdFingerScan" id="cmdFingerScan" value="Finger Scan" /><br>
Password:<br>
<input type="password" name="new_adj_pwd" id="new_adj_pwd"  />
<input type="button" value="Proceed with Transfer" name="cmdApprove" id="cmdApprove" />
<div id='diverror'></div>
</td>
</tr>
</table>
</fieldset>
</form>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type='text/javascript' src='../js/jquery.plugin.min.js'></script>
<script type='text/javascript' src='../js/jquery.keypad.min.js'></script>
<link rel="stylesheet" type="text/css" href="../js/jquery.keypad.css" />
<script lang="javascript">
$(document).ready(function(){	
	var keypadOptions = {
			separator: '|', 
			prompt: '', 
    		layout: [
    			'7|8|9|' + $.keypad.CLOSE, 
        		'4|5|6|' + $.keypad.CLEAR, 
        		'1|2|3|' + $.keypad.BACK, 
        		'.|0|00'
        	]
        };
	$("#newdiscount").keypad(keypadOptions);
	
	$("#newcancel").click(function(){
		document.location.href='order.php?roomid='+ $("#roomid").val();
	});
	$("#cmdFingerScan").click(function(){
			$.post("oicfscan.php",{act:'scan', user: $("#new_user").val()});
			myinterval = setInterval(checkFScan, 3000);
		});

	$("#cmdApprove").hide().click(function(){
		//alert('approve');
	});
});

function checkFScan(){
	$.post("oicfscan.php",{act:'monitor', user: $("#new_user").val()},
	function(resp){
		if(resp.success==true) {
			$("#new_adj_pwd").val(resp.pass);
			//$("#cmdApprove").attr("enabled",true);
			$("#cmdbtn").show().trigger("click");
			clearInterval(myinterval);
		}
	},'json');
}
</script>
