<?php
session_start();
include_once("config/config.inc.php");
include_once("reporting.php");
include_once("printerfns.php");

$room = $_GET["roomid"];

$obj = new Reporting($room);
if($_GET["print"]==1) {
	$retval=chr(hexdec('1B')).chr(hexdec('4D')).chr(1);
	$retval.=$obj->printablefos()."\n\n\n\n\n";
	$retval.=chr(hexdec('1D')).chr(hexdec('56')).chr(49);	
	$file = "fos{$room}.txt";
	$fp = fopen("reports/" .$file, "w");
	fwrite( $fp,$retval);
	fclose($fp);
//	shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
	$prnt = new printerfns();
	$prnt->sendToPrinter('front', "reports/" . $file);
}

?>
<style>
h1 { text-align:center;font-size:.7em;}
table {font-family:lucida,arial,helvetica}
table td {font-size:.6em} 
table th {font-size:.7em}
.x-footer{font-size:.6em;}
</style>
<h1>Food Order Summary</h1>
<table >
<tr>
<td>Room</td><td><?php echo $obj->doorname ?></td>
<td>Check-in</td><td><?php echo date("Y-m-j h:i A",strtotime($obj->checkindate))?></td>
</tr>
<tr>
<td>Rate</td><td><?php echo $obj->ratename ?></td>
<td>Exp Checkout</td><td><?php echo date("Y-m-j h:i A",strtotime($obj->etd)) ?></td>
</tr>
<tr>
<td>Shift</td><td><?php echo $obj->shift ?></td>
<td>&nbsp;</td><td>&nbsp;</td>
</tr>
<tr>
<th colspan=2>Food Orders:</th><th colspan=2>&nbsp;</th>
</tr>
<tr>
<th>&nbsp;</th>
<td colspan=3>
<?php echo $obj->getFnbSales() ?>
</td>
</tr>
</table>
<br />
<br />
<br />

<div class='x-footer'>
Last Update: <?php echo $obj->checkindate ?><br />
Updated by: <?php echo $obj->fullname ?><br />
</div>
<h6  style="page-break-before:always;"></h6>
