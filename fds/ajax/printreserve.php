<?php
header("access-control-allow-origin: *");
include_once("config/config.inc.php");
include_once("printerfns.php");

if ($_POST['rescode']) {
	$code = $_POST['rescode'];

	$sql = "select a.reserve_code, a.date_created, a.pax, a.payment_type,
			a.reserve_fee,
			concat(b.firstname, ' ', b.lastname) as fullname
			from reservations a, guests b
			where a.guest_id=b.guest_id and
			a.reserve_code=:code";
	$res = R::getRow($sql, [':code' => $code]);
	$rsql = "select a.checkin, a.checkout, b.door_name 
			from reserve_rooms a, rooms b
			where a.room_id=b.room_id
			and a.reserve_code=:code";
	$rooms = R::getAll($rsql, [':code' => $code]);


	$retval=chr(hexdec('1B')).chr(hexdec('4D')).chr(1);
	$retval.=getReservationDetails($res, $rooms)."\n\n\n\n\n";
	$retval.=chr(hexdec('1D')).chr(hexdec('56')).chr(49);	

	$file = "reserve.txt";
	$fp = fopen("reports/" .$file, "w");
	fwrite( $fp,$retval);
	fclose($fp);
	$prnt = new printerfns();
	$prnt->sendToPrinter('front', "reports/" . $file);
}


function getReservationDetails($d, $rooms) {
	$retval = "
Shogun 2: Reservation Slip


Reservation Code: {$d['reserve_code']}
Date: {$d['date_created']}
Guest: {$d['fullname']}
Number of Pax: {$d['pax']}
Reservation Fee/Deposit: {$d['reserve_fee']}
Payment Type: {$d['payment_type']}

";

	foreach ($rooms as $r) {
		$retval .= "
Room: {$r['door_name']}
Checkin: {$r['checkin']}
Checkout: {$r['checkout']}
		";
	}

	return $retval;
}
