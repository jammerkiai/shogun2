<?php
/** common **/


function hasShiftStart()
{

	$sql = "select shift from `shift-transactions` order by datetime desc limit 0,1";
	$sh = R::getCol($sql);

	return ($sh[0] == 'start') ? 1 : 0;
}

function getSpecialFloorId() 
{
	$lsql = "select settings_value from settings where settings_name = 'SPECIALFLOORID'";
	return R::getCol($lsql)[0];	
}


function getShiftStartTime()
{
	$sql = "select datetime from `shift-transactions` where shift = 'start' order by datetime desc limit 0,1";
	return R::getCell($sql);
}


function getStartTime()
{
	$date = date("Y-m-d ");
	$h= date("H");
	if($h < 6 || $h >= 22) {
		$return = "$date 22:00:01";
	}elseif($h < 14) {
		$return = "$date 6:00:01";
	}elseif( $h < 22 ) {
		$return = "$date 14:00:01";
	}
	return $return;

	$sql = "select datetime from `shift-transactions` where shift = 'start' order by datetime desc";

	return R::getCol($sql)[0];
}

function getSupervisorDropdown($name) {
	$sql = "select username, fullname from users where group_id=4";
	$res = R::getAll($sql);
	$ret = "<select name='$name' id='$name'>";
	$ret .= "<option ></option>";
	foreach ($res as $r)
	{
		$usr = $r['username'];
		$full = $r['fullname'];
		$ret .=  "<option value='$usr'>$full</option>";
	}
	$ret .=  "</select>";
	return $ret;
}

function getshift($date) {
	if(!$date)$date=date("Y-m-d H:i:s");
	list($d, $t) = explode(" ", $date);
	list($h, $m, $s) = explode(":", $t);

	if($h>=14&&$h<=21)
	{
		return $shift = "3rd";
	}
	elseif($h>=6&&$h<=13)
	{
		return $shift = "2nd";
	}
	return  "1st";
}


function getCurrentShiftNum($startshift) {
    list($date, $time) = explode(' ', $startshift);
    list($hr, $mi, $se) = explode(':', $time);

    if ($hr >= '14' && $hr < '22') {
        return 3;
    } elseif ($hr >= '06' && $hr < '14') {
        return 2;
    } else {
        return 1;
    }
}
