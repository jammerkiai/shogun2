<?php
class room 
{
	public function room($roomid) 
	{
		$this->roomid=$roomid;
		$this->getroomdetails();
	}
	
	public function getroomdetails() 
	{
		$sql2 = "
			select a.door_name, a.room_type_id, b.room_type_name, a.status, a.ui_top, a.ui_left, a.ui_width, a.ui_height, a.site_id,a.floor_id
			from rooms a, room_types b
			where a.room_type_id=b.room_type_id
			and a.room_id='". $this->roomid."'";
		$row = R::getRow($sql2);

		foreach ($row as $key => $value) {
			$this->{$key} = $value;
		}
	}
	
	public function getroomrates() 
	{
		$sql2 = "SELECT room_type_rates.rtr_id, room_type_rates.amount, rates.rate_name, rates.rate_id, 
		    room_type_rates.room_type_id, room_type_rates.ot_amount,rates.duration, room_type_rates.publishrate
			FROM room_type_rates
			LEFT JOIN rates ON room_type_rates.rate_id = rates.rate_id
			WHERE   room_type_rates.active=1   and room_type_rates.room_type_id =" . $this->room_type_id;
		$rows  = R::getAll($sql2);

		$this->rates=array();
		foreach ($rows as $row) {
			array_push($this->rates, $row);
		}
	}
	
	public function getroomdiscounts() 
	{
		$sql2 = "SELECT discounts.discount_id, discounts.discount_label, discounts.discount_percent,room_type_discounts.rate_id
		FROM room_type_discounts
		LEFT JOIN discounts ON room_type_discounts.discount_id = discounts.discount_id
		WHERE room_type_discounts.room_type_id =" . $this->room_type_id;

		$rows  = R::getAll($sql2);
		$this->discounts=array();
		foreach ($rows as $row) {
			array_push($this->discounts, $row);
		}
	}
	
	public function updatestatus($args) {
		$roomid = ($args["roomid"]) ? $args["roomid"] : 0;
		$status = ($args["status_id"]) ? $args["status_id"] : 0;
		$reason = ($args["newremarks"]) ? $args["newremarks"] : '';
		$user = $_SESSION["hotel"]["userid"];
		
		$now = date("Y-m-d H:i:s");
		$sql = "insert into room_log values (null, '$roomid','$status','$now','$user','$reason') ";
		R::exec($sql);

		$sql = "update rooms set status='$status', last_update='$now', update_by='$user' where room_id='$roomid' ";
		R::exec($sql);

	}	

	public function getoccupancy() {
		$sql = " select a.occupancy_id, a.actual_checkin, a.expected_checkout,  c.rate_name, a.shift_checkin, d.fullname as guestname, a.rate_id
				from occupancy a, rooms b, rates c, users d
				where a.room_id=b.room_id and a.rate_id=c.rate_id and a.update_by=d.user_id 
				and a.room_id={$this->roomid} and actual_checkout='0000-00-00 00:00:00'
				order by a.actual_checkin desc";
		$row = R::getRow($sql);

		foreach ($row as $key => $value) {
			$this->{$key} = $value;
		}
	}
	
	public function getRateGroup() {
		return preg_replace('/[0-9*]/', '', $this->rate_name);
	}

	public function getotamount() {
		$sql = " select ot_amount from room_type_rates where room_type_id='$this->room_type_id' and rate_id='$this->rate_id' ";
		$this->ot_amount = R::getCol($sql)[0];
		return $this->ot_amount;
	}

	public function getExt12Amount(){
		$phrase = $this->getRateGroup();
		$sql = "select rate_id from rates where rate_name like '%{$phrase}%' and duration=12";
		$rate_id= R::getRow($sql)['rate_id'];

		$sql = "select amount from room_type_rates where rate_id = '$rate_id' and active = 1 and room_type_id = ".$this->room_type_id;
		$row = R::getCol($sql)[0];

		$this->ot_12_amount=$row;
		return $this->ot_12_amount;
	}

	public function getExtDayAmount(){
		$phrase = $this->getRateGroup();
		$sql = "select rate_id from rates where rate_name like '%{$phrase}%' and duration=24 order by rate_id desc";
		$rate_id = R::getCol($sql)[0];

		$sql = "select amount from room_type_rates where rate_id = '$rate_id' and active = 1 and room_type_id = ".$this->room_type_id;
		$this->ot_day_amount = R::getCol($sql)[0];
		return $this->ot_day_amount;
	}


	public function getovertimehours() {
		//prepare data 
		$this->getoccupancy();
		$this->getotamount();
		//compare expected_checkout date from date now
		$now = new DateTime();
		$eco = new DateTime($this->expected_checkout);

		if($now < $eco) {
			$this->overtime_hours=0;
		}else{
		
			$diff = strtotime(date("Y-m-d H:i:s")) - strtotime($this->expected_checkout) + 1;
			

			$oDTdiff = $now->diff($eco);
			$this->overtime_hours = $oDTdiff->h;
			$ottime = $oDTdiff->h+1;			
			//echo $ottime."x".abs($diff)."x".(1800*($ottime));
			
			if(abs($diff) >= 1800+(3600*$this->overtime_hours))
			{
				$this->overtime_hours = $this->overtime_hours + 1;	
			}
			
		}
	}

	public function getStaySummary()
	{
		$this->getoccupancy();
		$sql  ="select roomsales_id, sales_date, unit_cost*qty as total_cost, status, item_id, update_date
				from room_sales 
				where category_id=3 
				and occupancy_id=$this->occupancy_id";

		$res = R::getAll($sql);
		$occ = $this->occupancy_id;
		$roomid=$this->roomid;

		$this->lastRoomRate = 0;
		$this->lastDiscount = 0;
		$this->lastCommission = 0;
		$this->lastBPG = 0;
		$this->lastVAT = 0;
		$this->lastWtax = 0;

		$ret='<table class="staytable">
		<tr><th>Date</th><th>Status</th><th>Cost of Stay</th></tr>';
		$firstDate = '';
		foreach ($res as $r) {
			if ($firstDate == '') {
				$firstDate = $r['update_date'];
			}
			
			$this->setLastValue($r['total_cost'], $r['item_id']);

			$rid = $r['roomsales_id'];
			$salesdate = $r['sales_date'];
			$cost = $r['total_cost'];
			$status = $r['status'];
			$update = $r['update_date'];

			if($status=='Draft' && $firstDate != $update) {
				$ret.="<tr><td>$salesdate</td><td style='text-align:center'>$status</td><td style='text-align:right'>$cost</td>
				<td><a href='?roomid=$roomid&act=del&rid=$rid&occ=$occ&update=$update'>Delete</a></td>
				</tr>";
			}else{
				$ret.="<tr><td>$salesdate</td><td style='text-align:center'>$status</td><td style='text-align:right'>$cost</td></tr>";
			}
		}	
		$ret.='</table>';
		echo $ret;
	}

	public function setLastValue($amt, $item) {
		$itemList = [
			15 => 'lastRoomRate',
			17 => 'lastDiscount',
			18 => 'lastAdjustment',
			124 => 'lastWtax',
			123 => 'lastBPG',
			122 => 'lastVAT',
			121 => 'lastCommission',
		];
		
		if (isset($itemList[$item])) {
			$this->{$itemList[$item]} = $amt;
		}
		
	}
}
?>

