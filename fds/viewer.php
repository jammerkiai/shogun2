<html>
<head>
<title>Test</title>
<link rel="stylesheet" type="text/css" href="../ext3/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="./css/app.css" />
</head>
<body>
<script type="text/javascript" src="../ext3/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../ext3/ext-all.js"></script>
<script type="text/javascript" src="./js/ux-all.js"></script>
<script type="text/javascript" src="./js/bldgplan.js"></script>
<script type="text/javascript" src="./js/floorplan.js"></script>
<script type="text/javascript" src="./js/xdatetime.js"></script>
<script type="text/javascript" src="./js/ordergrid.js"></script>
<script type="text/javascript" src="./js/alphakeypad.js"></script>
<script type="text/javascript" src="./js/keypad.js"></script>
<script type="text/javascript" src="./js/simpleci.js"></script>
<script type="text/javascript" src="./js/forms.js"></script>
<script type="text/javascript" src="./js/guestinfo.js"></script>
<script type="text/javascript" src="./js/checkout.js"></script>
<script type="text/javascript" >
Ext.BLANK_IMAGE_URL = '../ext3/resources/images/default/s.gif';

Ext.ns('Application');

Ext.onReady(function(){
    Ext.QuickTips.init();
	
	
	var win = new Ext.Window({
		 title:'test'
		 ,id:'thiswin'
		,autoScroll:true
		,minimizable:true
		,maximizable:true
		,layout:'fit'
		,items:[{xtype:'checkoutform', roomid:6, roomtype:1,region:'center'}]
		//,items:[{region:'center',xtype:'simplecheckin', id:'checkinform', roomtype:2, roomid:11}]
		//,items:myform
		//,items:[{xtype:'xdatetime',fieldLabel:'date', roomtype:5}]

		//,items:[{xtype:'buildingplan'}]
		//,items:{xtype:'floorplan', floorid: 2}
		,buttons:[
			{
				text:'Submit',
				handler:function() {
					Ext.getCmp('checkinform').getForm().submit({
						url:'ajax/checkin.php'
					});
				}
			}
			,
			{text:'Cancel', handler:function(b) {
				Ext.getCmp('thiswin').close();
			}}
		]
	});

	win.show().maximize();
	
});
</script>
<div id='target' style="display:block;position:relative;top:100;left:100;"></div>
</body>
</html>