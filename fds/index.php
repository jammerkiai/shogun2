<?php 
/**
* index.php
* use this  for setting up session
* and other variables
*/
session_start();
include_once("./ajax/config/config.inc.php");
include_once("./ajax/class.room.php");
include_once("./ajax/common.inc.php");


if($_GET["room"]) {
	$rm= new room($_GET["room"]);
	$summ.="<span style='color:#ff0000;font-weight:bolder;font-size:1.2em;'>Updated: Room $rm->door_name</span>" ;
}

$_SESSION["hotel"]["shiftstart"] = hasShiftStart();

if(!$_SESSION["hotel"]["shiftstart"])
{
	echo "<script>document.location.href='ajax/shiftstart.php'</script>";
	exit;	
}


?>
<html>
<head>
<title>Shogun Hotel Frontdesk System</title>
</head>
<body>

<link rel="stylesheet" type="text/css" href="./css/app.css">
<div id="loading-mask" style=""></div>
<div id="loading">
    <div class="loading-indicator">
	<img src="./img/shogunlogo.jpg" style="margin-right:8px;vertical-align:middle;align:center" />
	<br />
	<img src="./img/loading-balls.gif" style="margin-right:8px;vertical-align:middle;align:center" /><span id="loading-msg">Loading styles and images...</span></div>
</div>

<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Loading styles...';</script>
 <link rel="stylesheet" type="text/css" href="../ext3/resources/css/ext-all.css" />
 <script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Loading Core API...';</script>
<script type="text/javascript" src="../ext3/adapter/ext/ext-base.js"></script>
<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Loading Components...';</script>
<script type="text/javascript" src="../ext3/ext-all.js"></script>

<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Loading User-Interface...';</script>
<?php
//if not logged in, load login.js
if( $_SESSION["hotel"]["logged"]==false) {
?>
<script type="text/javascript" src="./js/alphakeypasspad.js"></script>
<script type="text/javascript" src="./js/alphakeypad.js"></script>

<script type="text/javascript" src="./js/login.js"></script>
<?php 
}else{
?>
<script type="text/javascript">
	var userid = '<?php echo $_SESSION['hotel']['userid'] ?>';
</script>
<script type="text/javascript" src="./js/app.js"></script>
<script type="text/javascript" src="./js/bldgplan.js"></script>
<script type="text/javascript" src="./js/floorplan.js"></script>
<script type="text/javascript" src="./js/keypad.js"></script>
<script type="text/javascript" src="./js/xdatetime.js"></script>
<script type="text/javascript" src="./js/alphakeypad.js"></script>
<script type="text/javascript" src="./js/ordergrid.js"></script>
<script type="text/javascript" src="./js/guestinfo.js"></script>
<script type="text/javascript" src="./js/checkout.js"></script>

<script type="text/javascript" src="./js/statusbar.js"></script>
<script type="text/javascript" src="./js/forms.js"></script>
<script type="text/javascript" src="./js/guestinfo.js"></script>
<script type="text/javascript" src="./js/querystring.js"></script>


<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Ready...';</script>
<div id='whoami'><?php echo $_SESSION['hotel']['groupname'] . " : " . $_SESSION['hotel']['fullname'] ?></div>
<?php }  ?>
<div id='lastRoom'><?php echo $summ?></div>
</body>
</html>