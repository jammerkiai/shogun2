-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2016 at 01:59 PM
-- Server version: 5.7.12
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shogunold`
--

-- --------------------------------------------------------

--
-- Table structure for table `sales_journal_details`
--

CREATE TABLE `sales_journal_details` (
  `id` bigint(20) NOT NULL,
  `postdate` date NOT NULL,
  `sales_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `occupancy_id` bigint(20) NOT NULL,
  `dr_code` varchar(10) NOT NULL,
  `cr_code` varchar(10) NOT NULL,
  `type` varchar(2) NOT NULL,
  `tendertype` varchar(20) NOT NULL,
  `occupancy_status` varchar(40) NOT NULL,
  `transactionId` bigint(20) NOT NULL,
  `transactionSource` varchar(20) NOT NULL,
  `item` varchar(200) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `book` int(11) NOT NULL,
  `is_posted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `item_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_journal_details`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `sales_journal_details`
--
ALTER TABLE `sales_journal_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sales_journal_details`
--
ALTER TABLE `sales_journal_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
