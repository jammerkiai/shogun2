-- MySQL dump 10.13  Distrib 5.6.22, for osx10.10 (x86_64)
--
-- Host: localhost    Database: shogunfds2
-- ------------------------------------------------------
-- Server version	5.6.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


alter table occupancy add discount_rate decimal(12,2) not null default 0;
alter table temp_occupancy add discount_rate decimal(12,2) not null default 0;
alter table card_payment_details add is_posted boolean not null default false;
alter table card_payment_details add post_date date;

alter table partner_transactions add discount decimal(10, 2) not null default 0;
alter table partner_transactions add created_at timestamp not null default CURRENT_TIMESTAMP;
alter table partner_transactions add updated_at timestamp not null;
alter table partner_transactions add promo_code varchar(30) not null;

alter table reservations add bank_code varchar(20) not null;
alter table reservations add mode varchar(10) not null;

alter table reserve_rooms add created_at timestamp not null default CURRENT_TIMESTAMP;
alter table reserve_rooms add updated_at timestamp not null;

ALTER TABLE `room_sales` ADD `rechit_date` DATETIME NOT NULL AFTER `tendertype`;
ALTER TABLE `room_sales` ADD `paid_date` DATETIME NOT NULL AFTER `rechit_date`;
update room_sales set rechit_date=date_add(sales_date, interval 1 day);
update room_sales set paid_date=update_date;


--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_name` varchar(40) NOT NULL,
  `remarks` text NOT NULL,
  `active` int(11) NOT NULL,
  `commission` decimal(11,2) NOT NULL DEFAULT '0.00',
  `bpg` decimal(11,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`partner_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` VALUES (1,'AGODA','',1,17.00,0.00),(2,'Direct With Hotels','',1,10.00,30.00),(3,'Booking.com','',1,15.00,0.00),(4,'Asia Room','',1,0.00,0.00),(5,'Other','',1,0.00,0.00);
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-29 14:18:23
