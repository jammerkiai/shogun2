-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2016 at 03:27 PM
-- Server version: 5.7.12
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shogunold`
--

-- --------------------------------------------------------

--
-- Table structure for table `sales_and_services`
--

DROP TABLE IF EXISTS `sales_and_services`;
CREATE TABLE `sales_and_services` (
  `sas_id` int(11) NOT NULL,
  `sas_cat_id` int(11) NOT NULL,
  `sas_description` varchar(200) NOT NULL,
  `sas_amount` float NOT NULL,
  `code` varchar(10) NOT NULL,
  `parent` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_and_services`
--

INSERT INTO `sales_and_services` (`sas_id`, `sas_cat_id`, `sas_description`, `sas_amount`, `code`, `parent`) VALUES
(1, 2, 'Cigarettes', 65, '', ''),
(2, 2, 'Lighters', 25, '', ''),
(3, 2, 'Shampoo', 8, '', ''),
(4, 2, 'Conditioner', 8, '', ''),
(5, 2, 'Sanitary Napkin', 10, '', ''),
(6, 2, 'Panty Liner', 5, '', ''),
(7, 2, 'Lotion', 15, '', ''),
(8, 1, 'Bed Sheet - Extra', 15, '', '4011'),
(9, 1, 'Bath Towel - Extra', 15, '', '4011'),
(10, 1, 'Pillow with Case - Extra', 15, '', '4011'),
(11, 1, 'Complete Beddings - Extra', 400, '', '4011'),
(12, 1, 'Comforter', 60, '', '4011'),
(13, 1, 'Condom', 20, '', '4011'),
(14, 1, 'PCK', 20, '4013', '4011'),
(15, 3, 'Room Charge', 0, '4001', '4001'),
(16, 3, 'Extension', 0, '4001', '4001'),
(17, 3, 'Discount', 0, '4001', '4001'),
(18, 3, 'Adjustment', 0, '4001', '4001'),
(19, 4, 'Free Pick Up', 0, '4023', '4011'),
(20, 4, 'Free Drop Off ', 0, '4023', '4011'),
(21, 4, 'Drop Off / Pick Up (250)', 250, '4023', '4011'),
(22, 4, 'Drop Off / Pick Up (300)', 300, '4023', '4011'),
(23, 1, 'PAX', 100, '', '4011'),
(24, 4, 'Drop Off / Pick Up (200)', 200, '4023', '4011'),
(26, 1, 'Bucket of ICE', 10, '4014', '4011'),
(28, 1, 'Corkage Fee', 15, '4015', '4011'),
(27, 3, 'Deduction', 0, '4001', '4001'),
(29, 1, 'Adjustment', 0, '4019', '4011'),
(30, 4, 'Drop Off / Pick Up (170)', 170, '4023', '4011'),
(31, 4, 'Drop Off / Pick Up (350)', 350, '4023', '4011'),
(32, 4, 'Drop Off / Pick Up (400)', 400, '4023', '4011'),
(33, 4, 'Drop Off / Pick Up (450)', 450, '4023', '4011'),
(34, 4, 'Drop Off / Pick Up (500)', 500, '4023', '4011'),
(35, 4, 'Drop Off / Pick Up (150)', 150, '4023', '4011'),
(36, 1, 'Slippers (old)', 35, '', '4011'),
(37, 1, 'zMake-up of Room from WITHIN the Shift RECHIT (Free)', 0, '', '4011'),
(38, 1, 'zMake-up of Room from OTHER Shift RECHIT (Free)', 0, '', '4011'),
(39, 1, 'Bath Towel Charge', 300, '', '4011'),
(40, 1, 'Bed Sheet Charge', 530, '', '4011'),
(41, 1, 'Makeup Room - Linen Set (Free)', 0, '', '4011'),
(42, 1, 'Makeup Room - Bath Towel (Free)', 0, '', '4011'),
(43, 1, 'Makeup Room - Bed Sheet (Free)', 0, '', '4011'),
(44, 1, 'Makeup Room - Bed Sheet Single (Free)', 0, '', '4011'),
(45, 1, 'Makeup Room - Comforter (Free)', 0, '', '4011'),
(46, 1, 'Makeup Room - Pillow Case (Free)', 0, '', '4011'),
(47, 5, 'No Show', 0, '4001', '4001'),
(48, 5, 'Cancellation ', 0, '4001', '4001'),
(49, 5, 'No Show/Cancellation - Adjustment', 0, '4001', '4001'),
(50, 1, 'No Show / Cancellation', 0, '4001', '4001'),
(51, 1, 'Pillow Case Charge', 75, '4001', '4001'),
(52, 1, 'Pool Towel Rental', 40, '4001', '4001'),
(53, 1, 'Pool Towel Charge', 550, '4001', '4001'),
(54, 7, 'Tariff Com (20)', 20, '4024', '4011'),
(55, 7, 'Tariff Com (25)', 25, '4024', '4011'),
(56, 7, 'Tariff Com (30)', 30, '4024', '4011'),
(57, 7, 'Tariff Com (35)', 35, '4024', '4011'),
(58, 7, 'Tariff Com (40)', 40, '4024', '4011'),
(59, 7, 'Tariff Com (45)', 45, '4024', '4011'),
(60, 7, 'Tariff Com (50)', 50, '4024', '4011'),
(61, 7, 'Tariff Com (55)', 55, '4024', '4011'),
(62, 7, 'Tariff Com (60)', 60, '4024', '4011'),
(63, 7, 'Tariff Com (65)', 65, '4024', '4011'),
(64, 7, 'Tariff Com (70)', 70, '4024', '4011'),
(65, 7, 'Tariff Com (75)', 75, '4024', '4011'),
(66, 7, 'Tariff Com (80)', 80, '4024', '4011'),
(67, 7, 'Tariff Com (85)', 85, '4024', '4011'),
(68, 7, 'Tariff Com (90)', 90, '4024', '4011'),
(69, 7, 'Tariff Com (95)', 95, '4024', '4011'),
(70, 7, 'Tariff Com (100)', 100, '4024', '4011'),
(71, 7, 'Tariff Com (125)', 125, '4024', '4011'),
(72, 7, 'Tariff Com (150)', 150, '4024', '4011'),
(73, 7, 'Tariff Com (175)', 175, '4024', '4011'),
(74, 7, 'Tariff Com (200)', 200, '4024', '4011'),
(75, 7, 'Tariff Com (225)', 225, '4024', '4011'),
(76, 7, 'Tariff Com (250)', 250, '4024', '4011'),
(77, 7, 'Tariff Com (275)', 275, '4024', '4011'),
(78, 7, 'Tariff Com (300)', 300, '4024', '4011'),
(79, 7, 'Tariff Com (325)', 325, '4024', '4011'),
(80, 7, 'Tariff Com (350)', 350, '4024', '4011'),
(81, 7, 'Tariff Com (375)', 375, '4024', '4011'),
(82, 7, 'Tariff Com (400)', 400, '4024', '4011'),
(83, 7, 'Tariff Com (425)', 425, '4024', '4011'),
(84, 7, 'Tariff Com (450)', 450, '4024', '4011'),
(85, 7, 'Tariff Com (145)', 145, '4024', '4011'),
(86, 7, 'Tariff Com (530)', 530, '4024', '4011'),
(87, 7, 'Tariff Com (210)', 210, '4024', '4011'),
(88, 7, 'Tariff Com (255)', 255, '4024', '4011'),
(89, 7, 'Tariff Com (620)', 620, '4024', '4011'),
(90, 7, 'Tariff Com (480)', 480, '4024', '4011'),
(91, 7, 'Tariff Com (190)', 190, '4024', '4011'),
(92, 7, 'Tariff Com (415)', 415, '4024', '4011'),
(93, 7, 'Tariff Com (320)', 320, '4024', '4011'),
(94, 7, 'Tariff Com (453)', 453, '4024', '4011'),
(95, 1, '1 Hour Billiard', 150, '4021', '4011'),
(96, 1, '2 Hour Billiard', 250, '4021', '4011'),
(97, 1, '1 Hour Dart', 100, '4022', '4011'),
(98, 1, '2 Hour Dart', 200, '4022', '4011'),
(99, 1, 'Laundry Charges', 0, '4017', '4011'),
(100, 1, 'Laundry Commission', 0, '4017', '4011'),
(101, 1, 'Plate', 50, '4026', '4011'),
(102, 1, 'Spoon', 15, '4026', '4011'),
(103, 1, 'Fork', 15, '4026', '4011'),
(104, 1, 'Glass', 30, '4026', '4011'),
(105, 1, 'Saucer / Underliner', 25, '4026', '4011'),
(106, 1, 'Coffee Mug', 30, '4026', '4011'),
(107, 1, 'Soup Bowl Small', 30, '4026', '4011'),
(108, 1, 'Soup Bowl M/L', 50, '4026', '4011'),
(109, 1, 'Soydish', 15, '4026', '4011'),
(110, 1, 'EXTRA PAX(500)', 500, '4001', '4001'),
(111, 1, 'EXTRA PAX POOL(200)', 200, '4001', '4001'),
(112, 1, 'Extra Bed(500)', 500, '4001', '4001'),
(114, 1, 'Slippers (new)', 50, '4020', '4011'),
(115, 1, 'Massage Commission', 0, '4018', '4011'),
(116, 1, 'Extra Bed (600)', 600, '4001', '4001'),
(117, 1, 'Cigarettes', 100, '4030', '4011'),
(118, 1, 'Lighter', 30, '4031', '4011'),
(119, 1, 'EXTRA PAX(600)', 600, '4001', '4001'),
(120, 1, 'Videoke Songs (3 songs)	', 20, '4001', '4001'),
(121, 3, 'Partner Commission', 0, '4001', '4001'),
(122, 3, 'VAT', 0, '4001', '4001'),
(123, 3, 'BPG', 0, '4001', '4001'),
(124, 3, 'WTAX', 0, '4001', '4001');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sales_and_services`
--
ALTER TABLE `sales_and_services`
  ADD PRIMARY KEY (`sas_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sales_and_services`
--
ALTER TABLE `sales_and_services`
  MODIFY `sas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
